<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/login', 'HomeController@login')->name('login');
Route::get('/register', 'HomeController@register')->name('register');
Route::get('/thank-you', 'HomeController@thankYou')->name('thank-you');
Route::get('/reg-thank-you', 'HomeController@regThankYou')->name('reg-thank-you');
Route::get('logout', 'HomeController@userLogout')->name('logout');

//ajax call
Route::post('candidate-login', 'HomeController@candidateLoginByEmail');
Route::post('employer-login', 'HomeController@employerLoginByEmail');

//logged user route
Route::get('profile', 'UserController@profile')->name('profile');
Route::post('update-profile', 'UserController@updateProfile')->name('update-profile');

//Walkins controller
Route::get('walkins', 'WalkinsController@index')->name('walkins');
Route::get('walkins-detail/{walkin_id}', 'WalkinsController@walkinsDetail');
//Route::get('apply-walkin/{walkin_id}', 'WalkinsController@applyWalkin');
Route::post('apply-walkin', 'WalkinsController@applyWalkin');

//Seach controller
Route::get('consultant', 'SearchController@searchConsultant')->name('consultant');
Route::get('top-employers', 'SearchController@topEmployers')->name('top-employers');
Route::get('search-jobs', 'SearchController@searchJob')->name('search-jobs');

// Home Controller
Route::get('/', 'HomeController@index')->name('home');
Route::get('register', 'HomeController@candidateRegistartion')->name('candidate-register');
Route::post('save-candidate', 'HomeController@saveCandidate')->name('save-candidate');
Route::get('register-employer', 'HomeController@employerRegistartion')->name('employer-register');
Route::post('save-employer', 'HomeController@saveEmployer')->name('save-employer');
Route::get('search-employer', 'HomeController@searchEmployerView')->name('search-employer');

Route::get('registration', 'HomeController@consultantRegistration')->name('consultant-register');
Route::post('save-consultant', 'HomeController@saveConsultant')->name('save-consultant');

Route::any('jobs-by/{type}', 'HomeController@jobsBy')->name('jobs-by');
Route::get('job-plans', 'HomeController@jobPostingPlans')->name('job-plans');
Route::get('job-details', 'HomeController@jobDetails')->name('job-details');
Route::get('top-companies', 'HomeController@topCompanies')->name('top-companies');
Route::get('companies/{id}', 'HomeController@companyPage')->name('companies');
Route::get('consultant-detail/{id}', 'HomeController@consultantDetail')->name('consultant-detail');
Route::post('save-enquiry', 'HomeController@saveEnquiry')->name('save-enquiry');
Route::post('save-service-enquiry','HomeController@saveServiceEnquiry')->name('save-service-enquiry');
Route::get('our-plans', 'HomeController@ourPlans')->name('our-plans');

Route::get('contact-us', 'HomeController@contactUs')->name('contact-us');
Route::post('send-contact-message', 'HomeController@contactUsMessage')->name('send-contact-message');
Route::get('about-us', 'HomeController@aboutUs')->name('about-us');
Route::get('site-map', 'HomeController@siteMap')->name('site-map');



// user routes
Route::get('manage-users', 'UserController@index')->name('manage-users');
Route::get('edit-user/{id}',['uses'=>'UserController@editUser'])->name('edit-user');
Route::post('update-user', 'UserController@updateUser')->name('update-user');
Route::post('add-user', 'UserController@addUser')->name('add-user');
Route::get('send-verify-mail', 'UserController@sendVerificationMail')->name('send-verify-mail');

Route::get('email-verify', 'HomeController@emailVerify')->name('email-verify');

Route::get('/forgot-password','HomeController@forgot_password')->name('forgot-password');
Route::post('/forgot-password','HomeController@forgot_password_step1')->name('forgot-password1');
Route::get('/otp-verify-view','HomeController@otp_verify_view')->name('otp-verify-view');
Route::post('/otp-verify','HomeController@otp_verify')->name('otp-verify');
Route::post('/update-forgot-password','HomeController@updateForgotPassword')->name('update-forgot-password');


//paypal routes
Route::post('purchase-plan', 'PaypalController@purchasePlan')->name('purchase-plan');
Route::get('/subscribe/paypal/return', 'PaypalController@paypalReturn')->name('paypal.return');
Route::get('/subscribe/paypal/cancel', 'PaypalController@paypalCancel')->name('paypal.cancel');
Route::get('/subscribe/paypal/info', 'PaypalController@paypalProcessMsg')->name('paypal.info');

Route::get('/payment/{id}', 'HomeController@paymentGateway')->name('payment-gateway');


Route::get('job-posting', 'HomeController@postFreeJob')->name('free-job-post');
Route::post('save-job', 'HomeController@saveFreeJob')->name('save-free-job');

Route::group(['middleware' => ['isEmployer']], function()
{
    //Employer controller
    Route::get('employer-dashboard', 'EmployerController@dashboard')->name('employer-dashboard');
    
    Route::get('employer-profile', 'EmployerController@profileView')->name('emp-profile-view');
    Route::get('employer-profile-update', 'EmployerController@profileUpdateForm')->name('emp-profile-update');
    Route::post('empployer-save-profile', 'EmployerController@updateProfile')->name('emp-save-profile');
    
    Route::get('slider-images', 'EmployerController@sliderImages')->name('slider-images');
    Route::post('add-slider-image', 'EmployerController@addSliderImage')->name('add-slider-image');
    Route::get('delete-slider-image/{id}', 'EmployerController@deleteSliderImage')->name('delete-slider-image');
    Route::get('job-post', 'EmployerController@jobPost')->name('job-post');
    Route::get('posted-jobs', 'EmployerController@postededJobList')->name('posted-jobs');
    Route::get('purchased-plans', 'EmployerController@purchasedPlans')->name('purchased-plans');
    Route::get('edit-job/{id}', 'EmployerController@editJob')->name('job-edit');
    Route::post('update-job', 'EmployerController@updateJob')->name('job-update');
    Route::post('save-job-post', 'EmployerController@saveJobPost')->name('save-job-post');
    Route::get('delete-job/{job_id}', 'EmployerController@deleteJob')->name('delete-job')->where('job_id', '[0-9]+');
    Route::get('applied-candidates/{jobid}', 'EmployerController@appliedCandidates')->name('applied-candidates');
    Route::get('delete-applied-candidate/{job_id}/{user_id}', 'EmployerController@deleteAppliedCandidate')->name('delete-applied-candidate')->where('job_id', '[0-9]+')->where('user_id', '[0-9]+');
    Route::get('view-candidate-profile/{job_id}/{user_id}', 'EmployerController@viewCandidateProfile')->name('view-candidate-profile')->where('job_id', '[0-9]+')->where('user_id', '[0-9]+');

    Route::post('cv-moveto', 'EmployerController@cvMoveTo')->name('cv-moveto');

    //csv files
    Route::get('csv-applied-candidates/{job_id}', 'EmployerController@downloadCsvAppliedCandidates')->name('csv-applied-candidates');
});
Route::group(['middleware' => ['isCandidate']], function()
{
    Route::get('candidate-dashboard', 'JobseekerController@dasboard')->name('candidate-dashboard');
    Route::get('applied-jobs', 'JobseekerController@appliedJobs')->name('applied-jobs');
    Route::get('last-activity',function(){
        return view('front_panel.jobseeker.last_activity');
    })->name('last-activity');
    Route::get('recommended-jobs', 'JobseekerController@recommendedJobs')->name('recommended-jobs');
    Route::get('candidate-profile', 'JobseekerController@profile')->name('candidate-profile');
    Route::post('candidate-profile-update', 'JobseekerController@updateCandidateProfile')->name('candidate-profile-update');
    Route::get('apply-job/{job_id}', 'JobseekerController@applyJob');
});

Route::get('terms-employer', function() {
    return view('front_panel.terms_employer');
})->name('terms-employer');
Route::get('terms-seeker', function() {
    return view('front_panel.terms_seeker');
})->name('terms-seeker');
Route::get('unauthorized', function() {
    return view('unauthorized');
})->name('unauthorized');
Route::get('email-temp','HomeController@template_test');

//permission route
Route::get('manage-permission',['uses'=>'PermissionController@permissionList','middleware' => ['permission:display-permission|add-permission']])->name('manage-permission');
Route::post('storePermission',['uses'=>'PermissionController@storePermission','middleware' => ['permission:display-permission|add-permission']])->name('store-permission');

//roles route
Route::get('manage-roles',['uses'=>'RoleController@roleList','middleware' => ['permission:display-role|add-role|role-edit|role-delete']])->name('manage-roles');
Route::post('storeRole',['uses'=>'RoleController@storeRole','middleware' => ['permission:display-role|add-role']])->name('store-role');
Route::get('deleteRole/{id}',['uses'=>'RoleController@deleteRole','middleware' => ['permission:role-delete']])->name('delete-role');
Route::get('editRole/{id}',['uses'=>'RoleController@editRole','middleware' => ['permission:role-edit']])->name('edit-role');
Route::post('updateRole/{id}',['uses'=>'RoleController@updateRole','middleware' => ['permission:role-edit']])->name('update-role');

//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});
