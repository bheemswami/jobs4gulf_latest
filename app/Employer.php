<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
	protected $guarded = [];
	protected $with=['user'];
    function user(){
	 	return $this->hasOne('App\User','id','user_id');
	}
	function job_post(){
		return $this->hasMany('App\PostJob','user_id','user_id')->with(['country','functional_area'])->whereStatus(1)->where('expiry_date','>=',date('Y-m-d'));
	}
	function slider_images(){
		return $this->hasMany('App\SliderImage','employer_id','id')->whereStatus(1)->orderBy('sort_order','ASC');
	}
	function country(){
	 	return $this->hasOne('App\Country','id','comp_country');
	}
	function state(){
	 	return $this->hasOne('App\State','id','comp_state');
	}
	function city(){
	 	return $this->hasOne('App\City','id','comp_city');
	}
	function industry(){
	 	return $this->hasOne('App\Industry','id','comp_industry');
	}
	function designations(){
	 	return $this->hasOne('App\Designation','id','cp_designation');
	}
}
