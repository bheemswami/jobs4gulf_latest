<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Specialization extends Model
{
	protected $guarded=['id'];
	function course(){
	 	return $this->hasOne('App\Course','id','course_id');
	}
	
}
