<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $guarded=['id'];
     function features(){
        return $this->hasMany('App\ServiceFeature','service_id','id');
    }
}
