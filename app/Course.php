<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Course extends Model
{
	protected $guarded=['id'];
	function degree(){
	 	return $this->hasOne('App\Degree','id','degree_id');
	}
	function specialization(){
	 	return $this->hasMany('App\Specialization','course_id','id');
	}
}
