<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class FunctionalArea extends Model
{
	protected $guarded=['id'];

	function job_post(){
		return $this->hasMany('App\PostJob','functional_area_id','id')->whereStatus(1)->where('expiry_date','>=',date('Y-m-d'));
	}
	
}
