<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class City extends Model
{
	protected $guarded=['id'];
	function country(){
		return $this->hasOne('App\Country','id','country_id');
	}
	function state(){
		return $this->hasOne('App\State','id','state_id')->with('country');
	}
	function job_post(){
		return $this->hasMany('App\PostJobsCity','city_id','id')->whereStatus(1);
	}
	function active_jobs(){
		return $this->hasMany('App\PostJobsCity','city_id','id')->whereStatus(1)->with('jobs');
	}
	function consultant(){
		return $this->hasMany('App\Employer','comp_city','id')->where('comp_type',1)->where('is_deleted',0)->where('status',1);
	}
	function employer(){
		return $this->hasMany('App\Employer','comp_city','id')->where('comp_type',2)->where('status',1);
	}
	function walkins(){
		return $this->hasMany('App\WalkinInterviewDetail','city_id','id');
	}
}
