<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,Session,DB;
use Carbon\Carbon;
use App\JobPlan;
use App\TransactionHistory,App\PurchasePlan,App\TmpPaypalDetail;
use App\User;
// Used to process plans
use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\ShippingAddress;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaypalController extends Controller
{
    private $core;
    
    private $apiContext;
    private $mode;
    private $client_id;
    private $secret;
    private $plan_id;
    
    // Create a new instance with our paypal credentials
    public function __construct()
    {   
         $this->core=app(\App\Http\Controllers\CoreController::class);

            $this->client_id = config('paypal.client_id');
            $this->secret = config('paypal.secret');
            $this->plan_id = config('paypal.plan_id');
        
        // Set the Paypal API Context/Credentials
        $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        $this->apiContext->setConfig(config('paypal.settings'));


    }
    public function paypalProcessMsg()
    {
        return view('front_panel/paypal_process_msg');
    }

    public function generatePaymentUrl(Request $request){
        $tmpPaypalData['order_id']=$request->user_id.'-'.genRandomNum(5).date('yd');

        $date = Carbon::now();
        $jobPlan=JobPlan::whereId($request->plan_id)->first();
        
        $plan_name='Plan Name : '.$jobPlan->name;
        $plan_price=$jobPlan->price;

        // paypal payment process
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1->setName($plan_name)->setCurrency('USD')->setQuantity(1)->setPrice($plan_price);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal($plan_price);

        $transaction = new Transaction();
        $transaction->setAmount($amount)->setItemList($item_list)->setDescription('Your transaction description')->setInvoiceNumber($tmpPaypalData['order_id']);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('paypal.return'))->setCancelUrl(route('paypal.return'));

        $payment = new Payment();
        $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)->setTransactions(array($transaction));
        try {
            $payment->create($this->apiContext);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                return response()->json(['url'=>'','status'=>0,'msg'=>'Connection timeout']);
            } else {
                return response()->json(['url'=>'','status'=>0,'msg'=>'Some error occur, sorry for inconvenient']);
               
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
       
        if(isset($redirect_url)) {
            $tmpPaypalData['user_id']=$request->user_id;
            $tmpPaypalData['paypal_url']=$redirect_url;
            $tmpPaypalData['payment_id']=$payment->getId();
            $tmpPaypalData['plan_id']=$jobPlan->id;
            $id = TmpPaypalDetail::insertGetId($tmpPaypalData);
            return response()->json(['url'=>$redirect_url,'status'=>1,'msg'=>'','payemt_id'=>$id]);
        }
        return response()->json(['url'=>'','status'=>0,'msg'=>'Some error occur, sorry for inconvenient']);
        
    }
 
    
    public function paypalReturn(Request $request){
        $date = Carbon::now();
        $token = $request->token;
        $payer_id = $request->PayerID;
        $payment_id = $request->paymentId;

        //get paypal temporary date from database
        $paypalTmpData = TmpPaypalDetail::where('payment_id',$payment_id)->first();

        if (empty($payer_id) || empty($token)) {
            return redirect()->route('paypal.info')->with(['error'=>'Payment failed. Try again after some time.']);
        }
        $payment = Payment::get($payment_id, $this->apiContext);
        if ($payment->getState() == 'failed') { 
             return redirect()->route('paypal.info')->with(['error'=>'Payment failed']);
        } else {                
                $job_plan=JobPlan::with('features')->whereId($paypalTmpData->plan_id)->first();
                $plan_features = getPlanFeatureData($job_plan);
            DB::beginTransaction();
                $purchaseId = PurchasePlan::insertGetId([
                        'user_id'=>$paypalTmpData->user_id,
                        'plan_id'=>$paypalTmpData->plan_id,
                        'total_basic_jobs'=>$job_plan->basic_jobs,
                        'remaining_basic_jobs'=>$job_plan->basic_jobs,
                        'total_premium_jobs'=>$job_plan->premium_jobs,
                        'remaining_premium_jobs'=>$job_plan->premium_jobs,
                        'basic_job_live'=>$plan_features['basic_job_live'],
                        'premium_job_live'=>$plan_features['premium_job_live'],
                        'plan_validity_days'=>$plan_features['plan_validity_days'],
                        'straight_to_inbox'=>$plan_features['straight_to_inbox'],
                        'job_alert_emails'=>$plan_features['job_alert_emails'],
                        'plan_validity_days'=>$plan_features['plan_validity_days'],
                        'job_email_upto'=>$plan_features['job_email_upto'],
                        'unlimited_space'=>$plan_features['unlimited_space'],
                        'use_email_ids'=>$plan_features['use_email_ids'],
                        'job_refreshed'=>$plan_features['job_refreshed'],
                        'greater_visibility'=>$plan_features['greater_visibility'],
                        'job_promote'=>$plan_features['job_promote'],
                        'share_jobs'=>$plan_features['share_jobs'],
                        'bulk_downloads'=>$plan_features['bulk_downloads'],
                        'manage_response'=>$plan_features['manage_response'],
                        'redirect_to_website'=>$plan_features['redirect_to_website'],
                        'plan_start_date'=>$date->format('Y-m-d'),
                        'plan_end_date'=>$date->addDays($plan_features['plan_validity_days'])->format('Y-m-d'),
                        'plan_feature_ids'=>$plan_features['plan_feature_ids'],
                    ]);
               
                $trasId = TransactionHistory::insertGetId([
                    'user_id'=>$paypalTmpData->user_id,
                    'plan_id'=>$paypalTmpData->plan_id,
                    'order_id'=>$paypalTmpData->order_id,
                    'payment_id'=>$payment_id,
                    'intent'=>'',
                    'payment_state'=>'',
                    'cart'=>'',
                    'currency'=>'USD',
                    'plan_price'=>$job_plan->price,
                    'quantity'=>($job_plan->basic_jobs+$job_plan->premium_jobs),
                    'total_amount'=>$job_plan->price,
                    'payer_payment_method'=>$payment->getPayer()->getPaymentMethod(),
                    'payer_status'=>$payment->getPayer()->getStatus(),
                    'transaction_for'=>'Job Plan Purchase By Generated Url',
                    'payment_details'=>$payment
                ]);

            if($trasId>0 && $purchaseId){
                DB::commit();
            } else {
                DB::rollBack();
            }
            return redirect()->route('paypal.info')->with(['success'=>'Payment success']);
        }       
       
    }

     public function paypalCancel(Request $request)
    {
        
       dd($request->all());
    }

    public function sendUnpaidInvoice(Request $request){

        $paypalTmpData = TmpPaypalDetail::where('id',$request->payment_id)->first();
        $job_plan=JobPlan::with('features')->whereId($paypalTmpData->plan_id)->first();
        $plan_features = getPlanFeatureData($job_plan);

        $user = User::with('employer_info')->whereId($paypalTmpData->user_id)->first();
        $mailData['params']=['email'=>'bheemswami808@gmail.com','user'=>$user,'paypal_data'=>$paypalTmpData,'job_plan'=>$job_plan,'subject'=>'Plan Purchase Invoice','template'=>'unpaid_invoice'];
         $this->core->SendEmail($mailData);
        return response()->json([$user]);
         dd([$request->all(),$user]);
    }


    public function getPaymentStatus()
    {
        $token = "EC-42A31834MB597645A";
        $PayerID = "GY5ZRAGW2Z58U";
        $payment_id = "PAY-0SG6053780833831ALLUR57Q";
        /** clear the session payment ID **/
        //Session::forget('paypal_payment_id');
        // if (empty($PayerID) || empty(Input::get('token'))) {
        //     //\Session::put('error','Payment failed');
        //     //return redirect()->route('paypal.info')->with([''=>'']);
        // }
        $payment = Payment::get($payment_id, $this->apiContext);

      
        dd($payment->getState());
        if ($result->getState() == 'approved') { 
            
            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/

            return redirect()->route('paypal.info')->with(['success'=>'Payment success']);
        }
       
        return redirect()->route('paypal.info')->with(['error'=>'Payment failed']);
    }

    

}