<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash,Auth;
use App\Role;
use App\User,App\RoleUser;
use App\Language;
class UserController extends Controller
{
    private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('auth');
         $this->core=app(\App\Http\Controllers\CoreController::class);
    }


    public function index()
    {
        $this->data['users']=User::where('status','<>',2)->whereIn('id',$this->core->getUserIdsByRole('user'))->get();
        return view('manage_users', $this->data);
    }
   
    public function profile()
    {
        $this->data['user']=User::whereId(Auth::user()->id)->first();
        $this->data['lang_list']=$this->core->getLangList();
        return view('profile',$this->data);
    }
    public function updateProfile(Request $request)
    {

        if($this->core->checkUserExist($request->email,$request->id)){
            return redirect()->back()->with(['error' => config('constants.FLASH_EMAIL_ADDRESS_EXIST').' ('.$request->email.')']);
        }
        $userData = User::find($request->id);
        if($request->new_password!=''){
            $request->merge(['password'=>Hash::make($request->new_password)]);
        }
        if($request->hasFile('img')){
            $filename=$this->core->fileUpload($request->file('img'),'uploads/user_img');
            $this->core->unlinkImg($userData->image,'uploads/user_img');
            $request->merge(['image'=>$filename]);
        }

        $user = $userData->update($request->except(['_token','img','new_password','confirm_password']));
        if($user){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }

    public function addUser(Request $request)
    {
        if($this->core->checkUserExist($request->email,$request->id)){
            return redirect()->back()->with(['error' => config('constants.FLASH_EMAIL_ADDRESS_EXIST').' ('.$request->email.')']);
        }
        if($request->hasFile('img')){
            $filename=$this->core->fileUpload($request->file('img'),'uploads/user_img');
            $request->merge(['image'=>$filename]);
        }
        $request->merge(['password'=>Hash::make($request->new_password),'added_by'=>Auth::user()->id]);
        $user = User::create($request->except(['_token','img','new_password','confirm_password']));
        if($user){
            $user->attachRole(getRoleId('user'));
            return redirect()->route('manage-users')->with(['success' => config('constants.FLASH_REC_ADD_1')]);
        }
        return redirect()->route('manage-users')->with(['error' => config('constants.FLASH_REC_ADD_0')]);
    }

     public function editUser(Request $request)
    {
         $this->data['user_data']=User::whereId($request->id)->first();
        return view('manage_users',$this->data);
    }

    public function updateUser(Request $request)
    {
        if($this->core->checkUserExist($request->email,$request->id)){
            return redirect()->back()->with(['error' => config('constants.FLASH_EMAIL_ADDRESS_EXIST').' ('.$request->email.')']);
        }
        $userData = User::find($request->id);
        if($request->new_password!=''){
            $request->merge(['password'=>Hash::make($request->new_password)]);
        }
        if($request->hasFile('img')){
            $filename=$this->core->fileUpload($request->file('img'),'uploads/user_img');
            $this->core->unlinkImg($userData->image,'uploads/user_img');
            $request->merge(['image'=>$filename]);
        }

        $user = $userData->update($request->except(['_token','img','new_password','confirm_password']));
        if($user){
            // $user->attachRole(3);
            return redirect()->route('manage-users')->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->route('manage-users')->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }

    public function updateUserStatus(Request $request)
    {
         $user=User::find($request->id);
        if($request->type==1){
            $status = ($user->status==1) ? 0 : 1;
        } else {
            $status = 2;
        }
         $user->status=$status;
         $user->save();
        return response()->json(['status' => $status]);
    }

    public function sendVerificationMail(){
        $user=User::whereId(Auth::id())->first();
        $mailData['params']=['email'=>$user->email,'user_id'=>$user->id,'user_name'=>$user->name,'subject'=>'Please verify your Job4Gulf account','template'=>'verify_email'];
        $this->core->SendEmail($mailData);
        return redirect()->back()->with(['success' => 'We have sent a confirmation E-mail to '.$user->email.'. Please verify account.']);
    }

}

