<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,DB,Hash;
use App\User,App\UserAdditionalInfo,App\UserEducation;
use App\AppliedJob,App\SkillKeyword,App\Institute,App\Course;
use App\Designation;
use App\Industry;
use App\FunctionalArea;
use App\Degree,App\Specialization;
use App\Company;
use App\PostJob;
use App\Employer;
use App\Country;

class JobseekerController extends Controller
{
	public $data=[];
    private $core;
    public function __construct()
    {
        $this->middleware('auth');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function dasboard(){
        $user=loggedUserProfile();
        $recommended_jobs=[];
        $this->data['user']=$user;
        $this->data['applied_jobs']=Appliedjob::with('jobs')->whereApplyBy(Auth::id())->get();
        if($user->additional_info !=null){
	        $recommended_jobs=DB::select('
	            SELECT 
	                `p_job`.`id`,`p_job`.`job_title`,`p_job`.`min_experience`,`p_job`.`max_experience`,`p_job`.`industry_id`,
	                `emp`.`comp_name`,
	                `industries`.`name` as industry_name,
	                `countries`.`name` as country_name
	            FROM post_jobs p_job
	            LEFT JOIN employers emp ON `emp`.`user_id`=`p_job`.`user_id` 
	            LEFT JOIN industries ON `industries`.`id`=`p_job`.`industry_id` 
	            LEFT JOIN countries ON `countries`.`id`=`p_job`.`job_in_country` 
	            WHERE `p_job`.`industry_id`="'.$user->additional_info->current_industry.'"
	            OR `p_job`.`functional_area_id`="'.$user->additional_info->functional_area.'"
	            LIMIT 0,10');
    	}
        $this->data['recommended_jobs']=$recommended_jobs;
        
        return view('front_panel.jobseeker.dashboard',$this->data);
    }
    public function appliedJobs(){
        $this->data['applied_jobs']=Appliedjob::with('jobs')->whereApplyBy(Auth::id())->get();
        return view('front_panel.jobseeker.applied_jobs',$this->data);
    }
    public function recommendedJobs(){
        $user=loggedUserProfile();
        $recommended_jobs=[];
        if($user->additional_info !=null){
            $recommended_jobs=DB::connection('mysql')->table('post_jobs AS p_job')
                     ->select('p_job.id','p_job.job_title','p_job.min_experience','p_job.max_experience','p_job.industry_id','p_job.created_at','p_job.salary_min','p_job.salary_max',
                     'emp.comp_name',
                     'industries.name as industry_name',
                     'countries.name as country_name')
                     ->join('employers AS emp', 'emp.user_id','=','p_job.user_id')
                     ->join('industries', 'industries.id','=','p_job.industry_id')
                     ->join('countries', 'countries.id','=','p_job.job_in_country')
                     ->where('p_job.industry_id',$user->additional_info->current_industry)
                     ->orWhere('p_job.functional_area_id',$user->additional_info->functional_area)
                     ->simplePaginate('10');

	        /* $recommended_jobs=DB::connection('mysql')->select('
	            SELECT 
	                `p_job`.`id`,`p_job`.`job_title`,`p_job`.`min_experience`,`p_job`.`max_experience`,`p_job`.`industry_id`,
	                `emp`.`comp_name`,
	                `industries`.`name` as industry_name,
	                `countries`.`name` as country_name
	            FROM post_jobs p_job
	            LEFT JOIN employers emp ON `emp`.`user_id`=`p_job`.`user_id` 
	            LEFT JOIN industries ON `industries`.`id`=`p_job`.`industry_id` 
	            LEFT JOIN countries ON `countries`.`id`=`p_job`.`job_in_country` 
	            WHERE `p_job`.`industry_id`="'.$user->additional_info->current_industry.'"
	            OR `p_job`.`functional_area_id`="'.$user->additional_info->functional_area.'"
	            LIMIT 0,10'); */
        }
        $this->data['recommended_jobs']=$recommended_jobs;
        return view('front_panel.jobseeker.recommended_jobs',$this->data);
    }
    public function profile(){
        $this->data['user']=loggedUserProfile();
        $basicEducations=$masterEducations=[];
        $degreeList=Degree::with('courses:id,degree_id,name,education_type')->where('status',1)->orderBy('name','ASC')->get();
        foreach($degreeList as $k=>$val){
            if($val->courses!=null){
                foreach($val->courses as $c){
                    if($c->education_type==1){
                        $basicEducations[$val->name][$c->id]=$c->name;
                    } else {
                        $masterEducations[$val->name][$c->id]=$c->name;
                    }
                    
                }
            }
        }
        $this->data['basic_courses']=$basicEducations;
        $this->data['master_courses']=$masterEducations;
        $this->data['institutes']=Institute::where('status',1)->pluck('name','name');
        $this->data['industry_list']=$this->core->getIndustryList('pluck');

       // $this->data['industry']=$this->core->getIndustryList('pluck');
       $this->data['functional_area']=FunctionalArea::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        //$this->data['f_area']=FunctionalArea::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        $this->data['skill_keywords']=SkillKeyword::where('status',1)->select('name','id')->get();
        $this->data['country_codes']=Country::select('phonecode',
                DB::raw('CONCAT(sortname, " (+", phonecode,")") AS codecountry'))
                ->where('status',1)->where('phonecode','!=','')
                ->orderBy('sortname','ASC')
                ->pluck('codecountry','phonecode');

        return view('front_panel.jobseeker.update_profile',$this->data);
    }

    public function updateCandidateProfile(Request $request){
       
        $userId=Auth::id();
        $interestedIn='';
        $userEducation=[];
        $userData=[
            //'email'=>$request->email,
            'title'=>$request->title,
            'name'=>$request->name,
            'dob'=>dateConvert($request->dob,'Y-m-d'),
            'country_code'=>$request->country_code,
            'mobile'=>$request->phone,
            'country_id'=>$request->country_id,
            'city_id'=>$request->city,
            'nationality'=>$request->nationality,
            'gender'=>config('constants.genders')[$request->title],
        ]; 
        if($request->interested_in!=null){
            $interestedIn=join(',',$request->interested_in);
        }

        if($request->hasFile('user_img')){
            $filename=$this->core->fileUpload($request->file('user_img'),'uploads/user_img');
            $userData['avatar']=$filename;
        }  
        if($request->hasFile('resume_file')){
            $filename=$this->core->fileUpload($request->file('resume_file'),'uploads/user_resume');
            $userData['resume']=$filename;
        } 
        if($request->password!=''){
            $userData['password']=Hash::make($request->password);
        }
         DB::beginTransaction(); 
         
        //save user data
        $user=User::whereId($userId)->update($userData);        
        if($user){
         //user additional information
            $userAdditionalInfo=[
                'exp_level'=>$request->experience_level,
                'exp_year'=>$request->year_experiance,
                'exp_month'=>$request->month_experiance,
                'current_company'=>$request->current_company,
                'current_position'=>$request->current_position,
                'current_industry'=>$request->current_industry,
                'functional_area'=>$request->functional_area,
                'key_skills'=>$request->key_skills,
                'interested_in'=>$interestedIn,
                'salary'=>$request->salary,
                'salary_in'=>$request->currency,
                'resume_headline'=>$request->resume_headline,
                'resume'=>$request->resume
            ];

            $lastIdInfo=UserAdditionalInfo::updateOrCreate(['user_id'=>$userId],$userAdditionalInfo);

        //user education
            $userEducation=[
                'basic_course_id'=>$request->basic_course_id,
                'basic_specialization_id'=>$request->basic_specialization_id,
                'basic_comp_year'=>$request->basic_comp_year,
                'master_course_id'=>$request->master_course_id,
                'master_specialization_id'=>$request->master_specialization_id,
                'master_comp_year'=>$request->master_comp_year,
                'institute'=>$request->institute,
            ];
            $lastIdEducation=UserEducation::updateOrCreate(['user_id'=>$userId],$userEducation);

            DB::commit();            
            return redirect()->route('candidate-dashboard')->with(['success' => config('constants.FLASH_MSG_UPDATE_1')]);
        } else {
            DB::rollBack();
            return redirect()->back()->with(['error' => config('constants.FLASH_MSG_UPDATE_0')]);
        }
    }

    public function applyJob(Request $request){
        if(!Auth::check()){
            return redirect()->back()->with(['error' => config('constants.FLASH_SUCCESS_LOGIN_ALERT')]);
        }
        if(Auth::user()->role!=2){
            return redirect()->back()->with(['error' => config('constants.FLASH_JOB_APPLY_ONLY_CANDIDATE')]);
        }
        if(jobAppliedOrNot($request->job_id)>0){
            return redirect()->back()->with(['error' => config('constants.FLASH_ALREADY_APPLIED')]);
        }

        $job = PostJob::with('employer_info','industry','functional_area')->where('id',$request->job_id)->first();        
        if($job){
            AppliedJob::insert(['job_id'=>$request->job_id,'apply_by'=>Auth::user()->id]);
            
            //send email to JobSeeker
            $mailData['params']=['email'=>Auth::user()->email,'user_id'=>Auth::user()->id,'user_name'=>Auth::user()->name,'job_data'=>$job,'subject'=>'Applied Job : '.$job->job_title,'template'=>'applied_job'];
            $this->core->SendEmail($mailData);

            //send email to employer
            $mailData['params']=['email'=>$job->employer_info->user->email,'job_data'=>$job,'subject'=>'Application for Job : '.$job->job_title,'template'=>'employer_job_alert'];
            $this->core->SendEmail($mailData);
        
           if($job->job_type==3 && $job->employer_info->comp_website!=''){
                return redirect()->away($job->employer_info->comp_website);
            } else {
                return redirect()->back()->with(['success' => config('constants.FLASH_JOB_APPLY_MSG')]);
            }
        } else {
            return redirect()->back()->with(['error' => config('constants.FLASH_INVALID_PARAMS')]);
        }
    }
}
