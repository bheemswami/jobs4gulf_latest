<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{

	public function index(Request $request){
        if(Auth::user()){
           return redirect()->route('manage-certificates');
        }
        return view('login_admin');
    }
    public function userSignin(Request $request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
             if(Auth::user()->hasRole('Admin'))
                {
                return redirect()->route('manage-certificates')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);
             } else if(Auth::user()->hasRole('Manager'))
             {
                return redirect()->route('manage-certificates')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);
             }  else if(Auth::user()->hasRole('User'))
             {
                return redirect()->route('manage-certificates')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);
             }  else if(Auth::user()->hasRole('Person'))
             {
                return redirect()->route('manage-certificates')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);
             } else if(Auth::user()->hasRole('Volunteers'))
             {
                return redirect()->route('manage-certificates')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);
             }
           return redirect()->route('manage-certificates')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_INVALID_CREDENTIAL')]);
    }

    public function logout(Request $request){
        return redirect()->route('login');
    }
}
