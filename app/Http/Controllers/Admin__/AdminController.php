<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AdminController extends Controller
{
    public function index(){
        return view('admin::login');
    }
    public function dashboard(){
        return view('admin::dashboard');
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(){
        return view('admin::create');
    }
    public function store(Request $request){
    }
    public function show(){
        return view('admin::show');
    }
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function dashboard()
    {
        return view('admin::dashboard');
    }
}
