<?php

namespace App\Http\Controllers;
use Auth,DB;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use App\Nationals;
use App\Country,App\City,App\State;
use App\Industry,App\Degree,App\Course,App\Designation;
use App\SkillKeyword;
use App\PostJob,App\PostJobsCity,App\PostJobsKeyword,App\PostJobsQualification,App\PostJobsSpecialization,App\PostJobsNationality,App\PostJobsCandidateCountry,App\PostJobsCandidateCity;
use App\User,App\UserAdditionalInfo;
use App\Employer;
use App\FunctionalArea;
use App\SliderImage;
use App\PurchasePlan;
use App\AppliedJob;
use App\Mail\JobMail;
use Illuminate\Support\Facades\Mail;
class EmployerController extends Controller
{
     public $data=[];
    private $core;
    public function __construct()
    {
        $this->middleware('auth');
        //$this->excel = $excel;
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
     
    public function dashboard(){
       $this->data['user']=loggedEmployerProfile();
       $this->data['active_plan']=userPurchasePlans('active_plan');
        return view('front_panel.employer.dashboard',$this->data);
    }
    public function profileView(){
        $this->data['user']=loggedEmployerProfile();
        return view('front_panel.employer.profile_view',$this->data);
    }
    public function profileUpdateForm()
    {
        $this->data['user']=loggedEmployerProfile();
        $this->data['designations']=Designation::where('status',1)->pluck('name','id');
        $this->data['country_codes']=Country::select('phonecode',DB::raw('CONCAT(sortname, " (+", phonecode,")") AS codecountry'))->where('status',1)->where('phonecode','!=','')->orderBy('sortname','ASC')->pluck('codecountry','phonecode');
        $this->data['industry_list']=$this->core->getIndustryList('pluck');
        return view('front_panel.employer.profile_update',$this->data);
    }
    public function updateProfile(Request $request)
    {
        $userId=Auth::id();
        if($request->password!=''){
            $user['password']=Hash::make($request->password);
        }
        $user['name']=$request->cp_name;
        $user['title']=$request->cp_title;
        $user['email']=$request->email;
        $user['gender']=config('constants.genders')[$request->cp_title];

        DB::beginTransaction(); 
        $user = User::whereId($userId)->update($user);

        if($request->hasFile('comp_img')){
            $filename=$this->core->fileUpload($request->file('comp_img'),'uploads/employer_logo');
            $request->merge(['comp_logo'=>$filename]);
        }
        if(Employer::where('user_id',$userId)->update($request->except(['_token','password','comp_img','email'])))
        {
            DB::commit();  
            return redirect('employer-dashboard')->with(['success' => config('constants.FLASH_PROFILE_UPDATE_1')]);
        } else {
            DB::rollBack();
            return redirect('employer-dashboard')->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
        }
    }

    public function sliderImages()
    {
        $this->data['emp_data']=Employer::whereUserId(Auth::user()->id)->with('slider_images')->first();
        return view('front_panel.employer.slider_image_list',$this->data);
    }
    public function addSliderImage(Request $request)
    {
        if($request->hasFile('slider_image')){
            $filename=$this->core->fileUpload($request->file('slider_image'),'uploads/slider_image');
            $request->merge(['image'=>$filename]);
        }
        if(SliderImage::insert($request->except(['_token','slider_image'])))
        {
           return redirect()->back()->with(['success' => config('constants.FLASH_REC_ADD_1')]);
        } else {
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_ADD_0')]);
        }
    }

    public function deleteSliderImage(Request $request)
    {
        if($request->id>0){
            $rec = SliderImage::find($request->id);
            unlinkImg($rec->image,'uploads/slider_image');
        }
        if(SliderImage::whereId($request->id)->delete())
        {
           return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        } else {
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
        }
       
    }

    public function purchasedPlans()
    {
        $this->data['plan_list']=userPurchasePlans();
        return view('front_panel.employer.purchase_plans_list',$this->data);
    }
    public function jobPost()
    {
        $educations=[];
        $degreeList=Degree::where('status',1)->with('courses:id,degree_id,name')->orderBy('name','ASC')->get();
        foreach($degreeList as $k=>$val){
            if($val->courses!=null){
                foreach($val->courses as $c){
                    $educations[$val->name][$c->id]=$c->name;
                }
            }
        }
        $this->data['educations']=$educations;
        $this->data['skill_keywords']=SkillKeyword::where('status',1)->select('name','id')->get();
        $this->data['functional_area']=FunctionalArea::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        $this->data['country_codes']=getCountryCodes();
        $this->data['industry_list']=$this->core->getIndustryList('pluck');
        return view('front_panel.employer.job_post',$this->data);
    }
    public function saveJobPost(Request $request){
        $date = Carbon::now();
        if(!Auth::check()){
            return redirect()->back()->with(['error' => config('constants.FLASH_SUCCESS_LOGIN_ALERT')]);
        }
        
        // get current active job plan
        $tblColName = ($request->job_type==1) ? 'remaining_basic_jobs' : 'remaining_premium_jobs';
        $purchasePlanData = PurchasePlan::with('job_plan')->where('user_id',Auth::user()->id)->where($tblColName,'>',0)->where('plan_end_date','>=',date('Y-m-d'))->orderBy('plan_id','DESC')->first();
        
        // if not any active plan
        if(!$purchasePlanData){
            return redirect()->back()->with(['error' => config('constants.FLASH_NO_REMAINING_JOBS')]);
        }

        // reamining and daysOfLive jobs (basic/premium)
        if($request->job_type==1){
            $remainingJobs = $purchasePlanData->remaining_basic_jobs;
            $daysOfJob = $purchasePlanData->basic_job_live;
        } else {
            $remainingJobs = $purchasePlanData->remaining_premium_jobs;
            $daysOfJob = $purchasePlanData->premium_job_live;
        }
       
        // if jobs not availabel
        if($remainingJobs<=0){
            return redirect()->back()->with(['error' => config('constants.FLASH_NO_REMAINING_JOBS')]);
        }

        $jobData=[
            'user_id'=>Auth::user()->id,
            'job_type'=>$request->job_type,
            'expiry_date'=>$date->addDays($daysOfJob)->format('Y-m-d'),
            'job_title'=>$request->job_title,
            'job_description'=>$request->job_description,
            "total_vacancy" =>$request->total_vacancy,
            "salary_min" =>$request->salary_min,
            "salary_max" =>$request->salary_max,
            "show_salary" =>$request->show_salary,
            "job_in_country" =>$request->job_in_country,
            "industry_id" => $request->industry_id,
            "functional_area_id" =>$request->functional_area_id,
            "gender"=>$request->gender,
            "min_experience" =>$request->min_experience,
            "max_experience" =>$request->max_experience,
            'response_by'=>$request->response_by,
            'response_email'=>$request->response_email,
            'walkin_address'=>$request->walkin_address,
            'walkin_date_from'=>dateConvert($request->walkin_date_from,'Y-m-d'),
            'walkin_date_to'=>dateConvert($request->walkin_date_to,'Y-m-d'),
            'walkin_time'=>$request->walkin_start_time.' '.$request->time_meridiem,
            'cp_title'=>$request->job_cp_title,
            'cp_name'=>$request->job_cp_name,
            'cp_mobile'=>$request->job_cp_mobile,
            'dial_number'=>$request->job_dial_number,
        ];

        // save job data in database
        $postJobId=PostJob::insertGetId($jobData);

        $jobsCityArr=$keywordArr=$quilificationArr=$specializationArr=$nationalityArr=$candidateCountryArr=$candidateCityArr=[];
        if($postJobId){
            // save jobs city
            if($request->job_in_city!=''){
                $cities = explode(',', $request->job_in_city);
                foreach($cities as $val){
                    $jobsCityArr[]=['post_job_id'=>$postJobId,'city_id'=>$val];
                }
                PostJobsCity::insert($jobsCityArr);
            }  

            // save job keywords
            if($request->keywords!=''){
                $keywords = explode(',', $request->keywords);
                foreach($keywords as $val){
                    $keywordArr[]=['post_job_id'=>$postJobId,'skill_id'=>$val];
                }
                PostJobsKeyword::insert($keywordArr);
            }

            // save job qualifications
            if(is_array($request->quilification)){
                foreach($request->quilification as $val){
                    $quilificationArr[]=['post_job_id'=>$postJobId,'course_id'=>$val];
                }
                PostJobsQualification::insert($quilificationArr);
            }

            // save job specialization
            if(is_array($request->specialization)){
                foreach($request->specialization as $val){
                    $exp = explode('~', $val);
                    $specializationArr[]=['post_job_id'=>$postJobId,'course_id'=>$exp[1],'specialization_id'=>$exp[0]];
                }
                PostJobsSpecialization::insert($specializationArr);
            }

            // save job nationalities
            if(is_array($request->nationality_id)){
                foreach($request->nationality_id as $val){
                    $nationalityArr[]=['post_job_id'=>$postJobId,'nationality_id'=>$val];
                }
                PostJobsNationality::insert($nationalityArr);
            }

            // save candidate current country AND current city
            if(is_array($request->candidate_current_country)){
                foreach($request->candidate_current_country as $k=>$val){
                    $candidateCountryArr[]=['post_job_id'=>$postJobId,'country_id'=>$val];

                    if(isset($request->candidate_current_city[$k])){
                        foreach($request->candidate_current_city[$k] as $valCity){
                            $candidateCityArr[]=['post_job_id'=>$postJobId,'country_id'=>$val,'city_id'=>$valCity];
                        }
                    }
                }
                if(!empty($candidateCityArr)) PostJobsCandidateCity::insert($candidateCityArr);
                if(!empty($candidateCountryArr)) PostJobsCandidateCountry::insert($candidateCountryArr);
            }  

            //update remaining jobs count
            PurchasePlan::whereId($purchasePlanData->id)->decrement($tblColName);  

            //send mail
                $user_emails=$this->getMatchKeySkillUsers($request->keywords,$purchasePlanData->job_email_upto);
               
                //Mail::to($user_emails)->send(new JobMail(PostJob::with('employer_info','industry','functional_area')->whereId($postJobId)->first()));
                // $job_detail=PostJob::with('employer_info','industry','functional_area')->whereId($postJobId)->first();
                // Mail::send('email_templates.current_posted_job', ['job_detail'=>$job_detail], function ($m) use ($user_emails,$job_detail) {
                //     $m->from(config('constants.email_config')['from_email'], config('constants.email_config')['from_name']);
                //     $m->to($user_emails)->subject('Job | '.$job_detail->job_title);
                // });

            return redirect()->back()->with(['success' => config('constants.FLASH_JOB_POST_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_JOB_POST_0')]);
    }
    
    public function editJob(Request $request)
    {
        $job =PostJob::with([
            'job_qualifications:id,post_job_id,course_id',
            'job_specializations:id,post_job_id,course_id,specialization_id',
            'job_nationality:id,post_job_id,nationality_id',
            'candidate_country:id,post_job_id,country_id',
            'candidate_city:id,post_job_id,country_id,city_id',
            'skills:id,post_job_id,skill_id',
            'jobs_in_city'
        ])->whereUserId(Auth::user()->id)->whereId($request->id)->first();
       // dd($job);
        $this->data['job_detail']=$job;
        $educations=[];
        $degreeList=Degree::where('status',1)->with('courses:id,degree_id,name')->orderBy('name','ASC')->get();
        foreach($degreeList as $k=>$val){
            if($val->courses!=null){
                foreach($val->courses as $c){
                    $educations[$val->name][$c->id]=$c->name;
                }
            }
        }
        $this->data['educations']=$educations;

        if($job->candidate_country!=null){
            foreach($job->candidate_country as $key=>$value){
                $countryIds[]=$value->country_id;
            }
        }
        $this->data['skill_keywords']=SkillKeyword::where('status',1)->select('name','id')->get();
        $this->data['functional_area']=FunctionalArea::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        $this->data['country_city']=Country::whereIn('id',$countryIds)->with(['cities'])->get();
        $this->data['country_codes']=getCountryCodes();
        $this->data['industry_list']=$this->core->getIndustryList('pluck');
        return view('front_panel.employer.edit_job',$this->data);
    }
    public function updateJob(Request $request)
    {
        //dd($request->all());
        if(!Auth::check()){
            return redirect()->back()->with(['error' => config('constants.FLASH_SUCCESS_LOGIN_ALERT')]);
        }
        $postJobId=$request->id;
        $jobData=[
            //'job_type'=>$request->job_type,
            //'expiry_date'=>$date->addDays($purchasePlanData->days_of_live_job)->format('Y-m-d'),
            'job_title'=>$request->job_title,
            'job_description'=>$request->job_description,
            "total_vacancy" =>$request->total_vacancy,
            "salary_min" =>$request->salary_min,
            "salary_max" =>$request->salary_max,
            "show_salary" =>$request->show_salary,
            "job_in_country" =>$request->job_in_country,
            "industry_id" => $request->industry_id,
            "functional_area_id" =>$request->functional_area_id,
            "gender"=>$request->gender,
            "min_experience" =>$request->min_experience,
            "max_experience" =>$request->max_experience,
            'response_by'=>$request->response_by,
            'response_email'=>$request->response_email,
            'walkin_address'=>$request->walkin_address,
            'walkin_date_from'=>dateConvert($request->walkin_date_from,'Y-m-d'),
            'walkin_date_to'=>dateConvert($request->walkin_date_to,'Y-m-d'),
            'walkin_time'=>$request->walkin_start_time.' '.$request->time_meridiem,
            'cp_title'=>$request->job_cp_title,
            'cp_name'=>$request->job_cp_name,
            'cp_mobile'=>$request->job_cp_mobile,
            'dial_number'=>$request->job_dial_number,
        ];
        if(PostJob::whereId($request->id)->update($jobData)){
            $jobsCityArr=$keywordArr=$quilificationArr=$specializationArr=$nationalityArr=$candidateCountryArr=$candidateCityArr=[];
            // save jobs city
            PostJobsCity::where('post_job_id',$postJobId)->delete();
            if($request->job_in_city!=''){
                $cities = explode(',', $request->job_in_city);
                foreach($cities as $val){
                    $jobsCityArr[]=['post_job_id'=>$postJobId,'city_id'=>$val];
                }
                PostJobsCity::insert($jobsCityArr);
            }  

            // save job keywords
            PostJobsKeyword::where('post_job_id',$postJobId)->delete();
            if($request->keywords!=''){
                $keywords = explode(',', $request->keywords);
                foreach($keywords as $val){
                    $keywordArr[]=['post_job_id'=>$postJobId,'skill_id'=>$val];
                }
                PostJobsKeyword::insert($keywordArr);
            }

            // save job qualifications
             PostJobsQualification::where('post_job_id',$postJobId)->delete();
            if(is_array($request->quilification)){
                foreach($request->quilification as $val){
                    $quilificationArr[]=['post_job_id'=>$postJobId,'course_id'=>$val];
                }
                PostJobsQualification::insert($quilificationArr);
            }

            // save job specialization
             PostJobsSpecialization::where('post_job_id',$postJobId)->delete();
            if(is_array($request->specialization)){
                foreach($request->specialization as $val){
                    $exp = explode('~', $val);
                    $specializationArr[]=['post_job_id'=>$postJobId,'course_id'=>$exp[1],'specialization_id'=>$exp[0]];
                }
                PostJobsSpecialization::insert($specializationArr);
            }

            // save job nationalities
             PostJobsNationality::where('post_job_id',$postJobId)->delete();
            if(is_array($request->nationality_id)){
                foreach($request->nationality_id as $val){
                    $nationalityArr[]=['post_job_id'=>$postJobId,'nationality_id'=>$val];
                }
                PostJobsNationality::insert($nationalityArr);
            }

            // save candidate current country AND current city
            PostJobsCandidateCity::where('post_job_id',$postJobId)->delete();
            PostJobsCandidateCountry::where('post_job_id',$postJobId)->delete();
            if(is_array($request->candidate_current_country)){
                foreach($request->candidate_current_country as $k=>$val){
                    $candidateCountryArr[]=['post_job_id'=>$postJobId,'country_id'=>$val];

                    if(isset($request->candidate_current_city[$k])){
                        foreach($request->candidate_current_city[$k] as $valCity){
                            $candidateCityArr[]=['post_job_id'=>$postJobId,'country_id'=>$val,'city_id'=>$valCity];
                        }
                    }
                }
                if(!empty($candidateCityArr)) PostJobsCandidateCity::insert($candidateCityArr);
                if(!empty($candidateCountryArr)) PostJobsCandidateCountry::insert($candidateCountryArr);
            }    
            return redirect()->back()->with(['success' => config('constants.FLASH_JOB_POST_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_JOB_POST_0')]);
    }

    public function postededJobList()
    {
        $this->data['jobs']=PostJob::with('industry','jobs_in_city','job_applicants')->where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
        //dd($this->data);
        return view('front_panel.employer.all_posted_jobs',$this->data);
    }
    public function deleteJob(Request $request)
    {
        if(PostJob::whereId($request->job_id)->where('user_id',Auth::user()->id)->update(['status'=>0])){
            PostJobsCity::where('post_job_id',$request->job_id)->update(['status'=>0]);
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_0')]);
    }

    public function appliedCandidates(Request $request)
    {   
        $this->data['job_detail']=PostJob::with('employer_info','industry','jobs_in_city','job_plan')->whereId($request->jobid)->where('user_id',Auth::user()->id)->first();
        if(!$this->data['job_detail']){
            return redirect()->back()->with(['error' => config('constants.FLASH_NOT_ALLOW_URL')]);
        }
        $data = AppliedJob::with('user','user_education')->where('job_id',$request->jobid)->whereStatus(1);
        $this->data['total_candidates']=$data->get();

        if($request->folder_id){
            $data = $data->where('move_to',$request->folder_id);
        } else {
            $data = $data->where('move_to',1);
        }
        $this->data['candidates']=$data->paginate(3)->appends(request()->query());
        if ($request->ajax()) {
            return view('front_panel.employer.applied_job_candidates_list', $this->data)->render();  
        } else {
            return view('front_panel.employer.applied_job_candidates',$this->data);
        }
        
    }
    public function deleteAppliedCandidate(Request $request)
    {
        $applied_job =AppliedJob::where('job_id',$request->job_id)->where('apply_by',$request->user_id)->whereStatus(1);
        
        if($applied_job->count()==0){
            return redirect()->back()->with(['error' => config('constants.FLASH_NOT_ALLOW_URL')]);
        }
        if($applied_job->update(['status'=>0])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_REMOVE_1')]);  
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_REMOVE_0')]);
    }

    public function viewCandidateProfile(Request $request)
    {
        if(AppliedJob::where('job_id',$request->job_id)->where('apply_by',$request->user_id)->whereStatus(1)->count()==0){
            return redirect()->back()->with(['error' => config('constants.FLASH_NOT_ALLOW_URL')]);
        }
        $this->data['user']=User::with(['additional_info','education','country','city'])->where('id',$request->user_id)->first();
        return view('front_panel.employer.candidate_profile',$this->data);
    }

    public function cvMoveTo(Request $request)
    {
        if(count($request->ids)<=0){
            return redirect()->back()->with(['error' => config('constants.FLASH_INVALID_PARAMS')]);
        }
        $applied_job =AppliedJob::whereIn('id',$request->ids)->where('job_id',$request->job_id);
        if($applied_job->update(['move_to'=>$request->move_to])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }
   public function downloadCsvAppliedCandidates(Request $request)
    {
        $this->data['job_detail']=PostJob::with('employer_info','industry','jobs_in_city','job_plan')->whereId($request->job_id)->where('user_id',Auth::user()->id)->first();
        $this->data['candidates'] = AppliedJob::with('user','user_education')->where('job_id',$request->job_id)->whereStatus(1)->get();
        //return view('excel_view.job_applied_candidates',$this->data);
       $this->core->exportFiles($this->data,'job_applied_candidates');
       return true;
    }

    
    function getMatchKeySkillUsers($skills=null,$limit=0){
        if($skills == null){
            return false;
        }
        $user_emails=[];
        $rec = UserAdditionalInfo::with('user')->limit($limit)->whereStatus(1)->inRandomOrder()->select('id','user_id','key_skills');
        $key_skills = PostJobsKeyword::whereIn('id',explode(',',$skills))->select('id','skill_id')->get();
        
        if($key_skills && count($key_skills)>0){
            foreach($key_skills as $k=>$val){
                if($val->skill_keywords!=null){
                    if($k==0) 
                        $rec->whereRaw('FIND_IN_SET("'.$val->skill_keywords->name.'",key_skills)');
                    else 
                        $rec->orWhereRaw('FIND_IN_SET("'.$val->skill_keywords->name.'",key_skills)');
                }   
            }
        }
        if($rec->count()>0){
            $data_list = $rec->get();
            foreach($data_list as $val){
                if($val->user!=null){
                    $user_emails[]=$val->user->email;
                }
            }
        }
        return $user_emails;
    }

}
