<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function permissionList()
    {
        $data['permission']=Permission::get();
        return view('entrust_view.manage_permission',$data);
    }
   public function storePermission(Request $request)
    {
        $permission=new Permission;
        $flag=$permission->insert($request->except(['_token']));
        if($flag)
             return redirect()->back()->with(['success' => config('constants.FLASH_REC_ADD_1')]);
        else
             return redirect()->back()->with(['error' => config('constants.FLASH_REC_ADD_0')]);
    }
}