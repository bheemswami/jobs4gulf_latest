"use strict";

function fixselect(){
 	$('.select2-dropdown--above').attr('id','fix');
    var $fix = $('#fix');
    $fix.removeClass('select2-dropdown--above');
    $fix.addClass('select2-dropdown--below');
};

$(document).ready(function(){

		// Animate loader off screen
	$(".se-pre-con").fadeOut("slow");


	$('.jobform select').select2({
		placeholder: "Jobs",
	}).on('select2:open',fixselect());

	$('#register select').select2({
		placeholder: "Category",
	}).on('select2:open',fixselect());

	$('.fourplex-col-3 select').select2({
		placeholder: "Choose a category",
	}).on('select2:open',fixselect());

	$('body#find-a-job .fourplex-col-4 select').select2({
		placeholder: "Kind of jobs",
	}).on('select2:open',fixselect());

	$('body#how-it-works .fourplex-col-4 select').select2({
		placeholder: "Kind of jobs",
	}).on('select2:open',fixselect());


	$('body#find-a-candidate .fourplex-col-4 select').select2({
		placeholder: "Availability",
	}).on('select2:open',fixselect());


//Smooth scroll

	$("#jumboarrow").on("click",function(){
		$('html, body').animate({
			scrollTop: $("#mainbody").offset().top
			}, 500);
	});
/*------------------------------Mainbody Functions------------------------------*/


//Carousel 1
	var $carousel1 = $("#myCarousel1");
	$carousel1.carousel({
			interval:5000
		});
	$(".carousel1-left").on("click", function(e){
		$carousel1.carousel("prev");
		e.preventDefault();
	});
	$(".carousel1-right").on("click", function(e){
		$carousel1.carousel("next");
		e.preventDefault();
	});


//Carousel 2 - partners
	var $carousel2 = $("#myCarousel2");
	$carousel2.carousel({
			interval:2000,
		});





	$(".carousel2-left").on("click", function(e){
		$carousel2.carousel("prev");
		e.preventDefault();
	});
	$(".carousel2-right").on("click", function(e){
		$carousel2.carousel("next");
		e.preventDefault();
	});

//Carousel 3 - team
	var $carousel3 = $("#myCarousel3");
	$carousel3.carousel({
			interval:5000
		});
	$(".carousel3-left").on("click", function(e){
		$carousel3.carousel("prev");
		e.preventDefault();
	});
	$(".carousel3-right").on("click", function(e){
		$carousel3.carousel("next");
		e.preventDefault();
	});	

//Carousel 4 - company
	var $companycarousel = $("#company-carousekl");
	$companycarousel.carousel({
			interval:5000
		});
	$(".company-carousel-left").on("click", function(e){
		$companycarousel.carousel("prev");
		e.preventDefault();
	});
	$(".company-carousel-right").on("click", function(e){
		$companycarousel.carousel("next");
		e.preventDefault();
	});	

//Login popup
	var $overlay = $("#overlay");
	var $login = $("#login");
	var $register = $("#register");

	$("#signin-open").on("click", function(){
		$overlay.fadeIn();
		$login.fadeIn();
	});
	$("#login-close").on("click", function(){
		$overlay.fadeOut();
		$login.fadeOut();
	});

//Registration popup
	$("#register-open").on("click", function(){
		$overlay.fadeIn();
		$register.fadeIn();
	});
	$("#register-close").on("click", function(){
		$overlay.fadeOut();
		$register.fadeOut();
	});

//Tweet expander
	$(".expander").on("click",function(e){
		var text = $(this).prev();
		text.animate({
   			height: text[0].scrollHeight+'px'
			}, 200);
		$(this).animate({
			opacity:0
		});
		e.preventDefault();
	});

//Menu toggle
	var $mainnav = $("#mainnav");
	$("#nav-toggler").on("click",function(){
		$mainnav.css("width","320px");
		$overlay.fadeIn();
	});
	$("#overlay").on("click",function(){
		$mainnav.css("width","0");
		$(this).fadeOut();
	});

});