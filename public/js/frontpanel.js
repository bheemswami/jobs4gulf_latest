$(document).ready(function() {
    if($(".txt_editor")[0] != undefined){
        $('.txt_editor').summernote({
                height: 160,
        });
    }
    if($(".txt_editor_job")[0] != undefined){
        $('.txt_editor_job').summernote({
                height: 160,
                insert_img:false,
                insert_table:false
        });
    }
});
$('.pass-show').click(function(){
  var attr = $('#candidate_password,#emp_password').attr('type');
   if (attr === "password") {
        attr = "text";
    } else {
        attr = "password";
    }
  $('#candidate_password,#emp_password').attr({'type':attr});
});

function previewImage(input,elem,defaultImg='') {
    if (input.files && input.files[0] && $.inArray(input.files[0].type, ['image/jpeg','image/jpg','image/png']) != -1) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(elem).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        $(elem).attr('src',defaultImg);
    }
}
function printSelectedFileName(fieldVal,elem) {
    var fieldVal = fieldVal.replace("C:\\fakepath\\", "");
    if (fieldVal != undefined || fieldVal != "") {
        //console.log(fieldVal);
      $(elem).text(fieldVal);
    }
}
// **************************** Start owl-carousel js ****************************

if($("#bottom-slider")[0] != undefined){
  $("#bottom-slider").owlCarousel({
    loop:true,
    lazyLoad:true,
    items: 1,
    autoplay:true,
    autoplayTimeout:5000,
    autoplaySpeed: 1500,
    // dots:true,
    nav:true,
    navText:['',''],
    navContainer: '#slideNav',
    dotsContainer: '#slidedots',
    responsiveClass:true
  });
}
if($("#employers-slider")[0] != undefined){
  $("#employers-slider").owlCarousel({
    loop:true,
    // lazyLoad:true,
    items: 3,
    margin: 30,
    autoplay:true,
    // autoplayTimeout:5000,
    autoplaySpeed: 1500,
    // dots:true,
    nav:true,
    dots: false,
    // navText:['',''],
    // navContainer: '#slideNav',
    // dotsContainer: '#slidedots',
    responsiveClass:true
  });
}
if($("#condidate-slider")[0] != undefined){
  $("#condidate-slider").owlCarousel({
    loop:true,
    // lazyLoad:true,
    items: 3,
    margin: 30,
    autoplay:true,
    // autoplayTimeout:5000,
    autoplaySpeed: 1500,
    // dots:true,
    nav:false,
    dots: false,
    // navText:['',''],
    // navContainer: '#slideNav',
    // dotsContainer: '#slidedots',
    responsiveClass:true
  });
}

// **************************** end owl-carousel js ****************************

// **************************** start validation additional methods ****************************

$.validator.addMethod('checkBasicCompYear', function (value, element, param) {
    var dob = $(".dob_pick").val().split('-');
    if(parseInt(dob[2])>parseInt(value)){
         return false;
    }
    return true;
},'Qualification year must be greater than DOB year.');

$.validator.addMethod('checkMasterCompYear', function (value, element, param) {
    var year = $("#year_of_graduation").val();
    if(parseInt(year)>parseInt(value)){
         return false;
    }
    return true;
},'Master education year must be greater than Basic education year.'); 

$.validator.addMethod('checkMaxSalary', function (value, element, param) {
    var min_salary = $("#monthly_salary_min").val();
    if(parseInt(min_salary)>parseInt(value)){
         return false;
    }
    return true;
},'Maximum salary should not be less than the minimum salary.'); 

$.validator.addMethod("CheckDOB", function (value, element, param) {
    return value.match(/^\d\d?\-\d\d?\-\d\d\d\d$/);
}, "Please enter a date in dd-mm-yyyy format.");
$.validator.addMethod("CheckDOBDate", function (value, element, param) {
        var myDate = new Date(value);
        var today = new Date();
        if ( myDate > today ) { 
            return false;
        }
    return true;
}, "Date of birth cannot be future date.");

$.validator.addMethod("CheckEmail", function (value, element, param) {
    var radioValue = $("input[name='emp_registered']:checked").val();
    if(radioValue==0){
          return false;  
     } else {
         return true;
     }
    
}, "Please enter unique email id.");

// **************************** end validation additional methods ****************************


// **************************** start validation js ****************************

if($("#post-job-form")[0] != undefined){
    $("#post-job-form").validate({
        ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        rules: {
            job_type: {
                required: true,
            },
            job_title: {
                required: true,
                maxlength: 200,
            },
            job_description: {
                required: true,
            },
            total_vacancy: {
                required: true,
                digits:true,
            },
            salary_min: {
                required: true,
            },
            salary_max: {
                required: true,
                checkMaxSalary: true
            },
            job_in_country: {
                required: true,
            },
            job_in_city: {
                required: true,
            },
            'quilification[]': {required: true},
            'specialization[]': {required: true},
            industry_id: {required: true},
            functional_area_id: {required: true},
            keywords: {required: true},
            gender: {required: true},
            min_experience: {required: true},
            max_experience: {required: true},
            response_by: {required: true},
            response_email: {required: true},
            job_cp_name: {required: true},
            job_cp_mobile: {required: true},
            walkin_address: {required: true},            
            walkin_date_from: {required: true},            
            walkin_date_to: {required: true},            
            walkin_start_time: {required: true},            
            time_meridiem: {required: true},            
            
        },
        messages: {
            phone:{
                maxlength:'Please enter valid mobile number',
            }
        },
       errorPlacement: function(error, element) {
                var attrName = element.attr("name");
                if(attrName == "response_by"){
                    error.appendTo('#errorResponseBy');
                    return;
                }
                if(attrName == "gender"){
                    error.appendTo('#errorGender');
                    return;
                }
                if(attrName == "comp_img"){
                    error.appendTo('#errorCompImg');
                    return;
                }
                if(attrName == "job_in_city"){
                    error.appendTo('#errorJobInCity');
                    return;
                }                
                if(attrName == "keywords"){
                        error.appendTo('#errorKeywords');
                        return;
                } else {
                    error.insertAfter(element);
                }
           }
    });
}
if($("#job-update-form")[0] != undefined){
    $("#job-update-form").validate({
        ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        rules: {
            job_title: {
                required: true,
                maxlength: 200,
            },
            job_description: {
                required: true,
            },
            total_vacancy: {
                required: true,
                digits:true,
            },
            salary_min: {
                required: true,
            },
            salary_max: {
                required: true,
                checkMaxSalary: true
            },
            job_in_country: {
                required: true,
            },
            'job_in_city[]': {
                required: true,
            },
            basic_course_id: {required: true},
            basic_specialization_id: {required: true},
            industry_id: {required: true},
            functional_area_id: {required: true},
            key_skill: {required: true},
            gender: {required: true},
            exp_year_from: {required: true},
            exp_year_to: {required: true},
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            }
        },
        highlight: function(element) {
            if($.inArray($(element).attr('name'), ['job_in_city[]','functional_area_id','key_skill','industry_id','gender']) !== -1){
                $(element).parent().parent().css("margin-bottom",'30px');
            }
        },
        unhighlight: function(element) {
            $(element).parent().parent().css("margin-bottom",'15px');
        }
    });
}
if($("#post-job-free-form")[0] != undefined){
    $("#post-job-free-form").validate({
        ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        rules: {
            user_email_unique: {
                required: {
                    depends: function(element) {
                      return ($("input[name='emp_registered']:checked").val()==1) ? false : true;
                    }
                },
                email: true,
                remote: {
                    url: base_url+'api/check-email-exist',
                    type: "post",
                    data: {
                        email: function(){ return $("#user_email_unique").val(); }
                    }
                 }
            },
            user_email: {
                required: {
                    depends: function(element) {
                      return ($("input[name='emp_registered']:checked").val()==1) ? true : false;
                    }
                },
                email: true,
            },
            job_title: {
                required: true,
                maxlength: 200,
            },
            job_description: {
                required: true,
            },
            total_vacancy: {
                required: true,
                digits:true,
            },
            salary_min: {
                required: true,
            },
            salary_max: {
                required: true,
                checkMaxSalary: true
            },
            job_in_country: {
                required: true,
            },
            job_in_city: {
                required: true,
            },
            'quilification[]': {required: true},
            'specialization[]': {required: true},
            industry_id: {required: true},
            functional_area_id: {required: true},
            keywords: {required: true},
            gender: {required: true},
            min_experience: {required: true},
            max_experience: {required: true},
            response_by: {required: true},
            response_email: {required: true},
            job_cp_name: {required: true},
            job_cp_mobile: {required: true},
            walkin_address: {required: true},            
            walkin_date_from: {required: true},            
            walkin_date_to: {required: true},            
            walkin_start_time: {required: true},            
            time_meridiem: {required: true},            
            
            contact_person: {required: true},
            dial_code: {required: true},
            cp_title: {required: true},
            cp_name: {required: true},
            cp_designation: {required: true},
            comp_name: {required: true},
            comp_address: {required: true},
            comp_website: {url: true},
            comp_country: {required: true},
            comp_city: {required: true},
            comp_type: {required: true},
            comp_industry: {required: true},
            comp_about: {required: true},
            cp_mobile: {
                required: true,
                minlength: 6,
                maxlength:13,
                digits:true,
            },
            user_password: {
                required: true,
                minlength: 6,
            },
            comp_img: {
                required: true,
                accept: "image/jpg,image/jpeg,image/png,image/gif",                
            },
            terms_and_conditions: {
                required: true
            },
            
        },
        messages: {
            email: {
                required: "Please enter your email id.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            },
            user_email_unique: {
                required: "Please enter your email id.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id or <a id='signin_now'>signIn</a>."
            },
            terms_and_conditions: {
                required: 'Please accept terms and c.onditions.'
            },
             comp_img: {
                required: "Please select company logo.",
                accept: 'Please upload a image with valid extension (.jpg, .jpeg and .png)',
            },
            phone:{
                maxlength:'Please enter valid mobile number',
            }
        },
         errorPlacement: function(error, element) {
                var attrName = element.attr("name");
                if(attrName == "response_by"){
                    error.appendTo('#errorResponseBy');
                    return;
                }
                if(attrName == "gender"){
                    error.appendTo('#errorGender');
                    return;
                }
                if(attrName == "comp_img"){
                    error.appendTo('#errorCompImg');
                    return;
                }
                if(attrName == "job_in_city"){
                    error.appendTo('#errorJobInCity');
                    return;
                }
                if(attrName == "cp_designation"){
                    error.appendTo('#errorCpDesignation');
                    return;
                }
                if(attrName == "comp_type"){
                    error.appendTo('#errorCompType');
                    return;
                }
                if(attrName == "comp_city"){
                    error.appendTo('#errorCompCity');
                    return;
                }
                if(attrName == "keywords"){
                        error.appendTo('#errorKeywords');
                        return;
                } else {
                    error.insertAfter(element);
                }
           }
    });
}

if($("#canditate-registration-form")[0] != undefined){  
    $("#canditate-registration-form").validate({
        ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        rules: {
            email: {
                required: true,
                remote: {
                    url: base_url+'api/check-email-exist',
                    type: "post",
                    data: {
                        email: function(){ return $("#candidate_email").val(); }
                    }
                 }
            },
            plan_id: {required: true},
            name: {required: true},
            dob: {
                required: true,
                CheckDOB:true,
                CheckDOBDate:true,
            },            
            gender: {required: true},
            nationality: {required: true},
            country: {required: true},
            city: {required: true},
            country_code: {required: true},
            phone: {
                required: true,
                minlength: 6,
                maxlength:13,
                digits:true,
            },
            password: {
                required: true,
                minlength: 6,
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#candidate_password"
            },
            basic_course_id: {required: true},
            basic_specialization_id: {required: true},
            basic_comp_year: {
                required: true,
                checkBasicCompYear:true
            },
            basic_institute: {required: true},
            master_specialization_id :{required:true},
            master_comp_year: {
               checkMasterCompYear:true,
               required:true,
            },
            master_institute: {required: true},
            experience_level: {required: true},
            year_experiance: {required: true},
            month_experiance: {required: true},
            current_company: {required: true},
            current_position: {required: true},
            current_industry: {required: true},
            salary: {required: true},
            currency: {required: true},
            duration_from:{required:true},
            duration_to:{required:true},
            functional_area: {required: true},
            key_skills: {required: true},
            resume_file: {
                required: true,
                extension: "doc|docx|pdf|txt"
            },
            user_img: {
                accept: "image/jpg,image/jpeg,image/png",                
            }
            
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            },
            user_img: {
                accept: 'Please upload a image with valid extension (.jpg, .jpeg and .png)',
            },
            resume_file: {
                required: "Please upload your resume.",
               extension: 'Please upload a resume with valid extension (.doc, .docx, .pdf and .txt)',
            },
            phone:{
                maxlength:'Please enter valid mobile number',
            }
        },
        errorPlacement: function(error, element) {
                var attrName = element.attr("name");
                if(attrName == "plan_id"){
                    error.appendTo('#errorPlanId');
                    return;
                }
                if(attrName == "dob"){
                    error.appendTo('#errorDOB');
                    return;
                }
                if(attrName == "key_skills"){
                    error.appendTo('#errorKeySkills');
                    return;
                }
                if(attrName == "city"){
                    error.appendTo('#errorCity');
                    return;
                }
                if(attrName == "current_position"){
                    error.appendTo('#errorCurrentPosition');
                    return;
                }
                if(attrName=="experience_level"){
                    error.appendTo('#errorExperienceLevel');
                    return;
                }if(attrName=="basic_institute"){
                    error.appendTo('#errorbasicInstitute');
                    return;
                }if(attrName=="master_institute"){
                    error.appendTo('#errorInstitute');
                    return;
                }if(attrName=="resume_file"){
                    error.appendTo('#errorResumeFile');
                    return;
                }if(attrName=="user_img"){
                    error.appendTo('#errorProfile');
                    return;
                } else {
                    error.insertAfter(element);
                }
           },
           
    });
}

if($("#canditate-update-form")[0] != undefined){  
    $("#canditate-update-form").validate({
        ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        rules: {
            email: {
                required: true,
                // remote: {
                //     url: base_url+'api/check-email-exist',
                //     type: "post",
                //     data: {
                //         email: function(){ return $("#candidate_email").val(); }
                //     }
                //  }
            },
            name: {required: true},
            dob: {
                required: true,
               CheckDOB:true,
                CheckDOBDate:true,
            }, 
            
            gender: {required: true},
            nationality: {required: true},
            country: {required: true},
            city: {required: true},
            country_code: {required: true},
            phone: {
                required: true,
                minlength: 6,
                maxlength:13,
                digits:true,
            },
            password: {
                //required: true,
                minlength: 6,
            },
            confirm_password: {
                //required: true,
                minlength: 6,
                equalTo: "#candidate_password"
            },
            basic_course_id: {required: true},
            basic_specialization_id: {required: true},
            basic_comp_year: {
                required: true,
                checkBasicCompYear:true
            },
            master_comp_year: {
               checkMasterCompYear:true
            },
            institute: {required: true},
            experience_level: {required: true},
            year_experiance: {required: true},
            month_experiance: {required: true},
            current_company: {required: true},
            current_position: {required: true},
            current_industry: {required: true},
            salary: {required: true},
            currency: {required: true},
            functional_area: {required: true},
            key_skills: {required: true},
            resume_file: {
                //accept : "application/pdf,application/doc,application/docx",
                extension: "doc|docx|pdf|txt"
            },
            user_img: {
                accept: "image/jpg,image/jpeg,image/png",                
            },
            
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            },
            user_img: {
                accept: 'Please upload a image with valid extension (.jpg, .jpeg and .png)',
            },
            resume_file: {
                extension: 'Please upload a resume with valid extension (.doc, .docx, .pdf and .txt)',
            },
            phone:{
                maxlength:'Please enter valid mobile number',
            }
        },
         highlight: function(element) {
            if($.inArray($(element).attr('name'), ['dob','current_company','current_position','current_industry','functional_area','key_skills']) !== -1){
                $(element).parent().parent().css("margin-bottom",'30px');
            }
        },
        unhighlight: function(element) {
            $(element).parent().parent().css("margin-bottom",'15px');
        }
    });
}

//candidate login
if($(".form-1")[0] != undefined){
    $(".form-1").validate({
        rules: {
            candidate_username: {
                required: true,
                email: true,
            },
            candidate_password: {
                required: true,
                minlength: 6,
            }
        },
        submitHandler: function() { 
            $('.login_model_loader').show();
            var post_vals = { email: $('#cand_login_username').val(), password: $('#cand_login_password').val() ,'_token':csrf_token};
            $.ajax({
                url: base_url+'candidate-login',
                type: 'POST',
                dataType: 'json',
                data: post_vals,
                success: function(res) {
                    console.log(res);
                    if(res.status==1){
                        window.location.href = base_url+'candidate-dashboard';
                       //location.reload();
                    } else if(res.status==2){
                        $('.jobseeker-msg').html('Invalid Credentials !');
                    } else {
                        $('.jobseeker-msg').html('Network Connection Error ! Try Again...');
                    }
                    $('.login_model_loader').hide();
                },
                error: function(error) {
                    $('.login_model_loader').hide();
                }
            });
           
        }
    });
}

//employer login
if($(".form-2")[0] != undefined){
    $(".form-2").validate({
        rules: {
            employer_username: {
                required: true,
                email: true,
            },
            employer_password: {
                required: true,
                minlength: 6,
            }
        },
        submitHandler: function() { 
            $('.login_model_loader').show();
            var post_vals = { email: $('#emp_login_username').val(), password: $('#emp_login_password').val() ,'_token':csrf_token};
            $.ajax({
                url: base_url+'employer-login',
                type: 'POST',
                dataType: 'json',
                data: post_vals,
                success: function(res) {
                    if(res.status==1){
                        window.location.href = base_url+'employer-dashboard';
                    } else if(res.status==2){
                        $('.employer-msg').html('Invalid Credentials !');
                    } else {
                        $('.employer-msg').html('Network Connection Error ! Try Again...');
                    }
                    $('.login_model_loader').hide();
                },
                error: function(error) {
                    $('.login_model_loader').hide();
                }
            });
           
        }
    });
}
if($("#employer-registration-form")[0] != undefined){    
    $("#employer-registration-form").validate({
        ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        rules: {
            email: {
                required: true,
                remote: {
                    url: base_url+'api/check-email-exist',
                    type: "post",
                    data: {
                        email: function(){ return $("#emp_email").val(); }
                    }
                 }
            },
            password: {
                required: true,
                minlength: 6,
            },
            confirm_password:{
                required:true,
                minlength: 6,
                equalTo:"#emp_password"
            },
            cp_title: {required: true},
            cp_name: {required: true},
            cp_designation: {required: true},
            dial_code: {required: true},
            cp_mobile: {
                required: true,
                minlength: 6,
                maxlength:13,
                digits:true,
            },
            comp_type: {required: true},
            comp_name: {required: true},
            comp_about: {required: true},
            comp_website: {url: true},
            "comp_industry[]": {required: true},
            comp_country: {required: true},
            comp_city: {required: true},
            comp_address: {required: true},
            zip_code: {digits:true},
            comp_img: {
                required: true,
                accept: "image/jpg,image/jpeg,image/png,image/gif",                
            },
        },
        messages: {
            email: {
                required: "Please enter your email id.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            },
            comp_img: {
                required: "Please select company logo.",
                accept: 'Please upload a image with valid extension (.jpg, .jpeg and .png)',
            },
            cp_mobile:{
                maxlength:'Please enter valid mobile number',
            }
        },
        
        errorPlacement: function(error, element) {
                var attrName = element.attr("name");
                if(attrName == "comp_img"){
                    error.appendTo('#errorCompImg');
                    return;
                }
                if(attrName == "comp_city"){
                    error.appendTo('#errorCompCity');
                    return;
                }
                if(attrName == "comp_type"){
                    error.appendTo('#errorCompType');
                    return;
                }
                if(attrName == "cp_designation"){
                        error.appendTo('#errorDesignation');
                        return;
                }if(attrName == "comp_industry[]"){
                    error.appendTo('#errorCompIndustry');
                    return;
                }
                else {
                    error.insertAfter(element);
                }
           }
        // highlight: function(element) {
        //     if($.inArray($(element).attr('name'), ['comp_city','cp_designation']) !== -1){
        //         $(element).parent().parent().css("margin-bottom",'30px');
        //     }
        // },
        // unhighlight: function(element) {
        //     $(element).parent().parent().css("margin-bottom",'15px');
        // }
    });
}
if($("#employer-update-form")[0] != undefined){    
    $("#employer-update-form").validate({
        ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        rules: {
            email: {
                required: true,
                remote: {
                    url: base_url+'api/check-email-exist',
                    type: "post",
                    data: {
                        user_id: function(){ return $("#user_id").val(); },
                        email: function(){ return $("#emp_email").val(); }
                    }
                 }
            },
             password: {
                minlength: 6,
            },
            cp_title: {required: true},
            cp_name: {required: true},
            cp_designation: {required: true},
            dial_code: {required: true},
            cp_mobile: {
                required: true,
                minlength: 6,
                maxlength:13,
                digits:true,
            },
            comp_type: {required: true},
            comp_name: {required: true},
            comp_about: {required: true},
            comp_website: {url: true},
            comp_industry: {required: true},
            comp_country: {required: true},
            comp_city: {required: true},
            comp_address: {required: true},
            zip_code: {digits:true},
            comp_img: {
                accept: "image/jpg,image/jpeg,image/png,image/gif",                
            },
        },
        messages: {
            email: {
                required: "Please enter your email id.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            },
            comp_img: {
                required: "Please select company logo.",
                accept: 'Please upload a image with valid extension (.jpg, .jpeg and .png)',
            },
            cp_mobile:{
                maxlength:'Please enter valid mobile number',
            }
        },
        highlight: function(element) {
            if($.inArray($(element).attr('name'), ['comp_city','cp_designation']) !== -1){
                $(element).parent().parent().css("margin-bottom",'30px');
            }
        },
        unhighlight: function(element) {
            $(element).parent().parent().css("margin-bottom",'15px');
        }
    });
}
if($("#forget-password-form1")[0] != undefined){    
    $("#forget-password-form1").validate({
        rules: {
            email: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            }
        },
    });
}
if($("#forget-password-form2")[0] != undefined){    
    $("#forget-password-form2").validate({
        rules: {
            otp: {
                required: true,
            }
        },
    });
}
if($("#forget-password-form3")[0] != undefined){    
    $("#forget-password-form3").validate({
        rules: {
            password: {
                required: true,
                minlength: 6,
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#newPass"
            },
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            }
        },
    });
}
if($("#save-enquiry-form")[0] != undefined){    
    $("#save-enquiry-form").validate({
        rules: {
            email: {required: true},
            name: {required: true},
            mobile: {
                required: true,
                minlength: 6,
                digits:true,
            },
            query: {required: true,rangelength:[50,1000]}
        },
        messages: {
            email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
                remote: "Please enter unique email id."
            }
        },
    });
}
if($("#slider-img-form")[0] != undefined){    
    $("#slider-img-form").validate({
        rules: {
             slider_image: {
                required: true,
                accept: "image/jpg,image/jpeg,image/png,image/gif",                
            },
        },
        messages: {
             slider_image: {
                required: "Please select image.",
                accept: 'Please upload a image with valid extension (.jpg, .jpeg and .png)',
            },
        },
    });
}
if($("#save-service-enquiry-form")[0] != undefined){    
    $("#save-service-enquiry-form").validate({
        rules: {
             name: {required: true},
             email: {required: true},
             mobile: {required: true, minlength: 6,digits:true,},
             comments: {required: true}
        },
        messages: {
            
        },
    });
}
if($("#contact_form")[0] != undefined){    
    $("#contact_form").validate({
        rules: {
             name: {required: true},
             email: {required: true},
             subject: {required: true},
             message: {required: true},
             mobile:{required:true},
             type:{required:true},
             comp_name:{required:true},
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            var attrName = element.attr("name");
            if(attrName == "type"){
                error.appendTo('#errorType');
                return;
            }
            else {
                error.insertAfter(element);
            }
       }
    });
}

// ************************************************** End validation js *********************************************