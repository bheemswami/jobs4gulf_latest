
;(function($){
   
    function initCounter($this, e){
        $this.find('.digit').each(function(){
            var $display = $(this);
            var $digit = $display.find('span');

            $digit.html([0,1,2,3,4,5,6,7,8,9,0].reverse().join('<br/>'))
            $digit.css({ 
                top: '-' + (parseInt($display.height()) * (10 - parseInt($digit.attr('title')))) + 'px'
            });
        });

        animateDigit($this.find('.digit:last'), e);
    }

   
    function animateDigit($this, e){
        var $counter = $this.closest('.counter');
        var $display = $this;
        var $digit = $display.find('span');

        // If we've reached the end of the counter, tick the previous digit
        if(parseInt($digit.css('top')) == -1 * parseInt($display.height())){
            animateDigit($display.prevAll('.digit:first'), e);
        }

        $digit.animate({
            top: '+=' + $display.height() + 'px'
        }, 500, function(){
            // Repeat the animation on a semi-random interval
            if($display.index('.counter .digit') == $counter.find('.digit').length - 1){
                setTimeout(function(){
                    animateDigit($display, e);
                }, Math.max(550, Math.random() * 1000));
            }

            // If we've reached the end of the counter, loop back to the top
            if(parseInt($digit.css('top')) > -1 * parseInt($display.height())){
                $digit.css({
                    top: '-' + (parseInt($display.height()) * 10) + 'px'
                });
            }
        });
    }

    $(function(){
        // initCounter($('.counter'), $.Event('load'));
    });
})(jQuery);
