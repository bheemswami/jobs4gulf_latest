@extends('layouts.master')
@section('content')
    <!--PAGE CONTENT -->
            <div class="row">
                <div class="col-lg-12">
                    <h3> Manage Permissions</h3>
                </div>
            </div>  
            <hr />
            <div class="row collapse" id="permission_form_sec">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Permission
                        </div>
                        <div class="panel-body">
                          {{--  @permission(('add-permission')) --}}
                            {!! Form::open(array('url'=>'storePermission','class'=>'form-horizontal','id'=>'permissionForm')) !!}
                            {!!csrf_field() !!}
                              <div class="form-group">
                                  <div class="col-sm-6">
                                      {!! Form::label('Name',null,array('class'=>'')) !!}
                                      {!! Form::text('name',null,array('class'=>'form-control','placeholder'=>'Name')) !!}
                                  </div>
                             
                                  <div class="col-sm-6">
                                      {!! Form::label('Display Name',null,array('class'=>'')) !!}
                                      {!! Form::text('display_name',null,array('class'=>'form-control','placeholder'=>'Display Name')) !!}
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="col-sm-12">
                                      {!! Form::label('Description',null,array('class'=>'')) !!}
                                      {!! Form::textarea('description',null,array('class'=>"form-control",'placeholder'=>"Description")) !!}
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="col-sm-2 col-sm-offset-10">
                                      {{Form::submit('Add Permission',array('class'=>'btn btn-success add_btn','id'=>'subBtn')) }}
                                  </div>  
                              </div>
                            {!! Form::close() !!}
                            {{-- @endpermission --}}
                        </div>
                    </div>
                </div>
              </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            All Permissions
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                              {{-- @permission(('display-permission')) --}}
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                          <th>S.No</th>
                                          <th>Name</th>
                                          <th>Display Name</th>
                                          <th>Description</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php $i=1;?>
                                      @forelse($permission as $value)
                                          <tr>
                                              <td>{{$i++}}</td>
                                              <td>{{$value->name}}</td>
                                              <td>{{$value->display_name}}</td>
                                              <td>{{$value->description}}</td>
                                          </tr>
                                      @empty
                                      @endforelse
                                      </tbody>
                                </table>
                                {{-- @endpermission --}}
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>
    <!--END PAGE CONTENT -->

    <!-- RIGHT STRIP  SECTION -->
    @section('right_content')
    <div id="right">
        <div class="well well-small">
            <button class="btn btn-inverse btn-block" data-toggle="collapse" data-target="#permission_form_sec">Add New Permission</button>
         </div>        
    </div>
    @endsection
     <!-- END RIGHT STRIP  SECTION -->
@endsection

