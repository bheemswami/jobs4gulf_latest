@extends('layouts.master')
@section('content')
    <!--PAGE CONTENT -->
   
            <div class="row">
                <div class="col-lg-12">
                    <h3> Manage Roles</h3>
                </div>
            </div>  
            <hr />
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit Role
                        </div>
                        <div class="panel-body">
                            {{-- @permission(('role-edit')) --}}
                             {!! Form::model($role,array('url'=>array('updateRole',$role->id),'class'=>'form-horizontal','id'=>'editRoleForm')) !!}
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        {!! Form::label('Display Name',null,array('class'=>'')) !!}
                                        {!! Form::text('display_name',null,array('class'=>'form-control','placeholder'=>'Display Name')) !!}
                                    </div>
                                
                                    <div class="col-sm-6">
                                        {!! Form::label('Description',null,array('class'=>'')) !!}
                                        {!! Form::text('description',null,array('class'=>"form-control",'placeholder'=>"Description")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        {!! Form::label('Select Permissions',null,array('class'=>'')) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                     @php 
                                       foreach($permission as $val){
                                            $permissions[]= $val;
                                        }
                                    @endphp
                                    @foreach($all_permission as $value)
                                        {!! '<div class="col-sm-6">' !!} <input type="checkbox" name=permission[] value={{$value->id}} @php if(in_array($value->id, $permissions)){echo 'checked';}  @endphp >
                                        {{$value->display_name}} {!! "</div>" !!}
                                    @endforeach
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-10">
                                        {{Form::submit('Edit Role',array('class'=>'btn btn-success add_btn','id'=>'subBtn')) }}
                                    </div>  
                                </div>
                            {!! Form::close() !!}
                           {{--  @endpermission --}}
                        </div>
                    </div>
                </div>
              </div>
    <!--END PAGE CONTENT -->
@endsection

