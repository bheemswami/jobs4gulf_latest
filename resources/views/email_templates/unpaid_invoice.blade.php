<style>
.content{
    padding: 8px 31px !important;
    font-size: 13px  !important;
}
</style>
@php
$user = $params['user'];
$plan = $params['job_plan'];
$paypal_data = $params['paypal_data'];
@endphp
@include('email_templates.header')
    <tr>
        <td class="content">
            <table>
                <tr style="border-bottom: 2px solid #318ce7;">
                   <th class="text-center bg-danger" colspan="2" style="padding: 4px;color: #f31313;ont-weight: 600;font-size: 16px;">UNPAID INVOICE</th>
                </tr>
                <tr style="border-bottom: 2px solid #318ce7;">
                    <th class="text-left" style="padding: 16px;">Invoice No.: {{ $paypal_data->order_id }}</th>
                    <th class="text-right" style="padding: 16px;">Invoice Date: {{ date('d-m-Y') }}</th>
                </tr>
                 <tr>
                    <td class="text-left" style="padding-bottom: 20px;"><br/>
                        <b>Invoice To:</b><br/>
                        {{ ($user->employer_info!=null) ? $user->employer_info->comp_name : '' }}<br/>
                        {{ ($user->employer_info!=null) ? $user->employer_info->comp_address : '' }}<br/>
                    </td>
                </tr>
                <tr>
                    <td class="" colspan="2">
                        <table class="table table-condensed invoice_table" style="border-bottom: 1px solid #d4d7e1">
                            <tr style="border: 1px solid #d4d7e1">
                                <th style="border: 1px solid #d4d7e1 !important;padding: 5px;">Description</th>
                                <th style="border: 1px solid #d4d7e1 !important;padding: 5px;">Amount</th>
                            </tr>
                            <tr style="border: 1px solid #d4d7e1">
                                <td class="text-center" style="border: 1px solid #d4d7e1 !important;padding: 5px;">{{ $plan->name }}</td>
                                <td class="text-right" style="border: 1px solid #d4d7e1 !important;padding: 5px;">&#8377; {{ $plan->price }}</td>
                            </tr>
                            <tr style="border: 1px solid #d4d7e1">
                                <th class="text-right" style="border: 1px solid #d4d7e1 !important;padding: 5px;">Total :</th>
                                <th class="text-right" style="border: 1px solid #d4d7e1 !important;padding: 5px;">&#8377; {{ $plan->price }}</th>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 20px;"><h5>Transaction Details</h5></td>
                </tr>
                <tr>
                    <td class="" colspan="2">
                        <table class="table table-condensed invoice_table" style="border-bottom: 1px solid #d4d7e1">
                            <tr style="border: 1px solid #d4d7e1">
                                <th style="border: 1px solid #d4d7e1 !important;padding: 5px;">Payment URL</th>
                                <th style="border: 1px solid #d4d7e1 !important;padding: 5px;">Amount</th>
                            </tr>
                            <tr style="border: 1px solid #d4d7e1">
                                <td class="text-left" style="border: 1px solid #d4d7e1 !important;padding: 5px;">
                                    <span>Click to the following URL to process payment</span><br/>
                                    <a href="{!! $paypal_data->paypal_url !!}">{!! $paypal_data->paypal_url !!}</a>
                                </td>
                                <th class="text-right" style="border: 1px solid #d4d7e1 !important;padding: 5px;">&#8377; {{ $plan->price }}</th>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
   
@include('email_templates.footer')