<style>
.content{
    padding: 8px 31px !important;
    font-size: 13px  !important;
}
</style>
@include('email_templates.header')
    <tr>
        <td class="content">
            <span><strong>By : </strong> {{ @$params['are_you'] }} </span><br/>
            <span><strong>From : </strong> ({{ @$params['email_user'] }}) {{ ucfirst(@$params['name']) }} </span><br/>
            <span><strong>Mobile : </strong> {{ @$params['mobile'] }} </span><br/>
            <span><strong>Location : </strong> {{ @$params['location'] }} </span>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <strong>Message </strong>
        </td>
    </tr>
     <tr>
        <td class="content">
          {!! @$params['query'] !!}
        </td>
    </tr>
@include('email_templates.footer')