@include('email_templates.header')
@php
$jobseeker = loggedUserProfile();
@endphp
<style>
.content{
    padding: 8px 31px !important;
    font-size: 13px  !important;
}
</style>
 <tr>
    <th></th>
</tr>
<tr>
    <th><h6> Application for Job Number : " #{{$params['job_data']->id}} " Jobs4Gulf </h6></th>
</tr>
<tr>
    <td class="content">
        <table>
             <tr>
                <th class="text-left">Job Title&nbsp;:</th>
                <td>&nbsp;{{$params['job_data']->job_title}}</td>
            </tr>
            <tr>
                <th class="text-left">Experience&nbsp;:</th>
                <td>&nbsp;{{$params['job_data']->min_experience}}-{{$params['job_data']->max_experience}} years</td>
            </tr>
            <tr>
                <th class="text-left">No. of Vacancy&nbsp;:</th>
                <td>&nbsp;{{$params['job_data']->total_vacancy}}</td>
            </tr>
            @if($params['job_data']->functional_area!=null)
            <tr>
                <th class="text-left">Functional Area&nbsp;:</th>
                <td>&nbsp;{{ ($params['job_data']->functional_area!=null) ? $params['job_data']->functional_area->name : '' }}</td>
            </tr>
            @endif
            @if($params['job_data']->industry!=null)
            <tr>
                <th class="text-left">Industry&nbsp;:</th>
                <td>&nbsp;{{ ($params['job_data']->industry!=null) ? $params['job_data']->industry->name : '' }}</td>
            </tr>
            @endif
        </table>
    </td>
</tr>

<tr>
    <th><h6> Job Seeker Details: </h6></th>
</tr>
<tr>
    <td class="content">
        <table>
            @if($jobseeker->additional_info!=null && $jobseeker->additional_info->resume_headline!='')
            <tr>
                <th class="text-left">Cover Letter&nbsp;:</th>
                <td>&nbsp;{{ ($jobseeker->additional_info!=null) ? $jobseeker->additional_info->resume_headline : '' }}</td>
            </tr>
            @endif
            <tr>
                <th class="text-left">Name&nbsp;:</th>
                <td>&nbsp;{{ ucfirst($jobseeker->name) }}</td>
            </tr>
            <tr>
                <th class="text-left">Mobile&nbsp;:</th>
                <td>&nbsp;{{ ($jobseeker->country_code!='') ? '+'.$jobseeker->country_code.'-' : '' }}{{ $jobseeker->mobile }}</td>
            </tr>
            <tr>
                <th class="text-left">Date of Birth</th>
                <td>&nbsp;{{ dateConvert($jobseeker->dob,'d-m-Y') }}</td>
            </tr>
            <tr>
                <th class="text-left">Work Experience&nbsp;:</th>
                <td>&nbsp;
                    @if($jobseeker->additional_info!=null && $jobseeker->additional_info->exp_level==1)
                    {{ $jobseeker->additional_info->exp_year }} Year 
                    {{ $jobseeker->additional_info->exp_month }} Month
                    @else
                        Fresher
                    @endif
                </td>
            </tr>
            <tr>
                <th class="text-left">Key Skills&nbsp;:</th>
                <td>&nbsp;{{ ($jobseeker->additional_info!=null) ? $jobseeker->additional_info->key_skills : 'None' }}</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <th><h6>Educational Details</h6></th>
</tr>
<tr>
    <td class="content">
        <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Education</th>
                      <th>Course</th>
                      <th>Specialization</th>
                      <th>Year</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>Basic</th>
                      <td class="text-left">{{ ($jobseeker->education !=null) ? @$jobseeker->education->basic_course->name : 'None' }}</td>
                      <td class="text-left">{{ ($jobseeker->education !=null) ? @$jobseeker->education->basic_specialization->name : 'None' }}</td>
                      <td class="text-left">{{ ($jobseeker->education !=null) ? @$jobseeker->education->basic_comp_year : 'None' }}</td>
                    </tr>
                    @if($jobseeker->education !=null && $jobseeker->education->master_course!=null)
                    <tr>
                      <th>Master</th>
                      <td class="text-left">{{ ($jobseeker->education !=null) ? @$jobseeker->education->master_course->name : '' }}</td>
                      <td class="text-left">{{ ($jobseeker->education !=null) ? @$jobseeker->education->master_specialization->name : '' }}</td>
                      <td class="text-left">{{ ($jobseeker->education !=null) ? @$jobseeker->education->master_comp_year : '' }}</td>
                    </tr>
                    @endif
                  </tbody>
                </table>
    </td>
</tr>
<tr>
    <th><a href="{{route('view-candidate-profile',['job_id'=>$params['job_data']->id,'user_id'=>@$jobseeker->id])}}">Click Here</a> to View Seeker Profile.</th>
</tr>
@include('email_templates.footer')