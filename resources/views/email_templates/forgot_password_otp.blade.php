@include('email_templates.header')
 
                <tr>
                    <td class="content">

                        <h5>Hi {{ ucfirst($params['user_name']) }}</h5>
                        <p>We received a request to reset your Jobs4Gulf password.</p>
                        <p>You can enter the following password reset code. </p>

                        <table>
                            <tr>
                                <td align="center" style="background: #eee;">
                                    <p>{{$params['otp']}}</p>
                                </td>
                            </tr>
                        </table>
                       
                        <p>Regards,<br/>
                        <em>Team Jobs4Gulf</em></p>                        
                    </td>
                </tr>
            </table>

        </td>
    </tr>
   
@include('email_templates.footer')