@include('email_templates.header')
<style>
.content{
    padding: 8px 31px !important;
    font-size: 13px  !important;
}
</style>
<tr>
    <th><h6> Job Details</h6></th>
</tr>
<tr>
    <td class="content">
        <table>
             <tr>
                <th class="text-left">Company Name&nbsp;:</th>
                <td>&nbsp;{{ ($job_detail->employer_info!=null) ? $job_detail->employer_info->comp_name : '' }}</td>
            </tr>
            <tr>
                <th class="text-left">Job Title&nbsp;:</th>
                <td>&nbsp;{{$job_detail->job_title}}</td>
            </tr>
            <tr>
                <th class="text-left">Experience&nbsp;:</th>
                <td>&nbsp;{{$job_detail->min_experience}}-{{$job_detail->max_experience}} years</td>
            </tr>
            <tr>
                <th class="text-left">Monthly Salary&nbsp;:</th>
                <td>&nbsp;${{$job_detail->salary_min}}-${{$job_detail->salary_max}}</td>
            </tr>
            <tr>
                <th class="text-left">No. of Vacancy&nbsp;:</th>
                <td>&nbsp;{{$job_detail->total_vacancy}}</td>
            </tr>
            @if($job_detail->functional_area!=null)
            <tr>
                <th class="text-left">Functional Area&nbsp;:</th>
                <td>&nbsp;{{ ($job_detail->functional_area!=null) ? $job_detail->functional_area->name : '' }}</td>
            </tr>
            @endif
            @if($job_detail->industry!=null)
            <tr>
                <th class="text-left">Industry&nbsp;:</th>
                <td>&nbsp;{{ ($job_detail->industry!=null) ? $job_detail->industry->name : '' }}</td>
            </tr>
            @endif
            <tr>
                <th class="text-left">Key Skills&nbsp;:</th>
                <td>&nbsp;{{$job_detail->key_skill}}</td>
            </tr>
            <tr>
                <td colspan="2" class="text-center"><a href="{{route('job-details',['jobid'=>$job_detail->id])}}" class="button">Apply Job</a></td>
            </tr>
        </table>
    </td>
</tr>

<tr>
    <th><h6>Job Description</h6></th>
</tr>
<tr>
    <td class="content">{!! limit_words($job_detail->job_description,150) !!}
    
    </td>
</tr>


@include('email_templates.footer')