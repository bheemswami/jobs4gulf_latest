@extends('/layouts/front_panel_master')
@section('content')

@include('front_panel/includes/page_banner')

<section class="main-inner-page lite-greyBg">
    <div class="container">
      <div class="row">       
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 category-parent-box">
            <h2>About Us</h2>
            Jobs4Gulf is the leading online recruitment portal in the Middle East, used by over 7 million experienced professionals from all sectors and job categories. It serves as the primary source of both local and expatriate talent to over 8,000 of the largest employers and recruitment agencies across the region.
            Founded in 2005, the company has transformed the region’s online recruitment landscape through its innovative approaches as well as its relentless obsession with quality and excellence.
            Jobs4Gulf is run by a seasoned team of young professionals with diverse backgrounds across different industries, including technology, finance, sales and recruitment. The team also brings together a wealth of international experience across four continents.
            <br><br>
            <h3>Our Vision</h3>
            Jobs4Gulf is the leading online recruitment portal in the Middle East, used by over 7 million experienced professionals from all sectors and job categories. It serves as the primary source of both local and expatriate talent to over 8,000 of the largest employers and recruitment agencies across the region.
            Founded in 2005, the company has transformed the region’s
            <br><br>
            <h3>Our Mission</h3>
            Jobs4Gulf is the leading online recruitment portal in the Middle East, used by over 7 million experienced professionals from all sectors and job categories. It serves as the primary source of both local and expatriate talent to over 8,000 of the largest employers and recruitment agencies across the region.
            Founded in 2005, the company has transformed the region’s
          </div>
      </div>
    </div>
</section>

@endsection