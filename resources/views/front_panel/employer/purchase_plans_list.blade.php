
@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel.includes.employer_page_banner')
<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
         @include('front_panel.includes.employer_dashboard_sidebar')
        <div class="col-sm-9">
          <div class="tabs widget profile-area">
            <div class="tab-content">
              <div class="form-title">
                <h4 class="edu_detail">Your Job Plans</h4>
              </div>
              <table class="table table-bordered table-striped data_table">
                <thead>
                  <tr>
                      <th>Plan Name</th>
                      <th>Basic Jobs (Remaining/Total)</th>
                      <th>Premium Jobs (Remaining/Total)</th>
                      <th>Plan Validity(Days)</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                  
                  @forelse($plan_list as $k=>$val)
                    <tr>
                      <td>{{($val->job_plan!=null) ? $val->job_plan->name : ''}}</td>
                      <td>{{$val->remaining_basic_jobs}} / {{$val->total_basic_jobs}}</td>
                      <td>{{$val->remaining_premium_jobs}} / {{$val->total_premium_jobs}}</td>
                      <td>{{$val->plan_validity_days}}</td>
                      <td>{{dateConvert($val->plan_start_date,'d M Y')}}</td>
                      <td>{{dateConvert($val->plan_end_date,'d M Y')}}</td>
                      <td>
                        @if(strtotime($val->plan_start_date)>strtotime(date('Y-m-d')))
                          <span class="bg-warning">Upcoming</span>
                        @elseif(strtotime(date('Y-m-d'))>strtotime($val->plan_end_date))
                          <span class="bg-danger">Expired</span>
                        @else 
                          <span class="bg-success">Active</span>
                        @endif
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="7" class="bg-danger">No purchased plans</td>
                    </tr>
                    @endforelse
                    
                </tbody>
              </table>
             
            </div>
            <!-- tab-content --> 
          </div>
          <!-- tabs-widget -->              
        </div>
      </div>
    </div>
  </section>
</section>

@endsection