
@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel.includes.employer_page_banner')
<style>
.dataTables_filter { display: none; }
</style>
 @php
  $inbox_count=$shortlist_count=$unmatched_count=$onhold_count=$rejected_count=0; 
  if(count($total_candidates)>0){
    foreach($total_candidates as $k=>$val){
      if($val->move_to==1)
          $inbox_count=$inbox_count+1;
      if($val->move_to==2)
          $shortlist_count=$shortlist_count+1;
      if($val->move_to==3)
          $unmatched_count=$unmatched_count+1;
      if($val->move_to==4)
          $onhold_count=$onhold_count+1;
      if($val->move_to==5)
          $rejected_count=$rejected_count+1;
    } 
  }  
         
@endphp
<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
         @include('front_panel.includes.employer_dashboard_sidebar')
        <div class="col-sm-9">
          <img src="{{url('public/images/image_1153550.gif')}}" class="loader_image hide-elem"/>
          <div class="tabs widget profile-area">
            <div class="tab-content">
              <div class="form-title">
                <h4 class="edu_detail">Jobs Detail</h4>
              </div>
              <table class="table table-bordered">
                  <tr>
                    <th>Job Title</th>
                    <td>{{$job_detail->job_title}}</td>
                  </tr>
                  <tr>
                    <th>Job Type</th>
                    <td>{{($job_detail->job_plan!=null) ? $job_detail->job_plan->name : '' }}</td>
                  </tr>
                  <tr>
                    <th>Total Vacancy</th>
                    <td>{{$job_detail->total_vacancy}}</td>
                  </tr>
                  <tr>
                    <th>Experience</th>
                     <td>{{$job_detail->min_experience}}-{{$job_detail->max_experience}} Year</td>
                   </tr>
                   <tr>
                    <th>Salary</th>
                    <td>{{$job_detail->salary_min}}-{{$job_detail->salary_max}} {{$job_detail->currency}}</td>
                  </tr>
              </table>
              
              <div class="form-title">
                <h4 class="work_exp">Responses</h4>
              </div>
                <div class="col-sm-12">
                  <div class="col-sm-12 folder_list"> 
                      <ul>
                        <li class="folder_name folder_blank active" data-id="1">Inbox (<span id="inbox_count">{{ $inbox_count }}</span>)</li>
                        <li class="folder_name folder_blank" data-id="2">Shortlisted (<span id="shortlist_count">{{ $shortlist_count }}</span>)</li>
                        <li class="folder_name folder_blank" data-id="3">Unmatched (<span id="unmatched_count">{{ $unmatched_count }}</span>)</li>
                        <li class="folder_name folder_blank" data-id="4">On-Hold (<span id="onhold_count">{{ $onhold_count }}</span>)</li>
                        <li class="folder_name folder_blank" data-id="5">Rejected (<span id="rejected_count">{{ $rejected_count }}</span>)</li>
                    </ul>
                  </div>
                  <div id="load" style="position: relative;">

                      @include('front_panel.employer.applied_job_candidates_list')
                  </div>
                </div>
             
            </div>
            <!-- tab-content --> 
          </div>
          <!-- tabs-widget -->              
        </div>
      </div>
    </div>
  </section>
</section>
<script>
$(document).on('change','#download_cv',function(){
  if($(this).val()>0){
    if (confirm("Are you sure?")) {
        window.location.href="{{route('csv-applied-candidates',['job_id'=>$job_detail->id])}}";
    }
  }
});
$(document).on('change','#move_to',function(){
  if($(this).val()>0){
    if (confirm("Are you sure?")) {
        $('.response-form').submit();
    } 
  }
});

var url = base_url;
$('.folder_name').click(function(){
  $('.folder_name').removeClass('active');
  $(this).addClass('active');
  $('.loader_image').show();
  url="{{ route('applied-candidates',['jobid'=>$job_detail->id]) }}";
  var data = { 'folder_id':$(this).data('id') };
  ajaxCall(data);
});

$(document).on('click','.custom-pagination a',renderJobs);

function renderJobs(e){
  $('.loader_image').show();
  e.preventDefault();
     url = $(this).attr('href'); 
      ajaxCall();
  }
  function ajaxCall(data=NaN){
    $.ajax({
          url : url,
          data : data,
      }).done(function (data) {
          $('#load').html(data); 
      }).fail(function () {
        
      }).complete(function(){
            $('.loader_image').hide();
      });
  }
</script>
@endsection