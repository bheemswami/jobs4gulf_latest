<div class="col-xs-12 col-sm-12">
       
         {{ Form::open(array('url'=>route('cv-moveto'),'class'=>'response-form'))}}
                    {{ Form::hidden('job_id',$job_detail->id) }}
                        @if(count($candidates)>0)
                          <div class="col-sm-6">{{ Form::select('move_to',config('constants.folder_names'),null,['class'=>'form-control','id'=>'move_to','placeholder'=>'Move']) }}</div>
                          <div class="col-sm-6">{{ Form::select('download_cv',[1=>'CV `s'],null,['class'=>'form-control','id'=>'download_cv','placeholder'=>'Download']) }}</div>
                        @endif    
                   
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th class="nosort">S.No.</th>
                              <th class="nosort"><input type="checkbox" id="select_all_checkbox"></th> 
                              <th class="nosort">Name</th>
                              <th class="nosort"></th>
                            </tr>
                          </thead>
                          <tbody>
                            @forelse($candidates as $k=>$val)
                              @if($val->status==1)
                                <tr>
                                  <td>{{$k+1}}.</td>
                                  <td><input type="checkbox" name="ids[]" value="{{$val->id}}" class="data_checkbox"></td>
                                  <td>
                                    @if($val->user!=null)
                                        <b>{{ $val->user->title.' '.$val->user->name }}</b> <br/>
                                        @if($val->user->additional_info!=null)
                                            {{ ($val->user->additional_info->resume_headline!='') ? $val->user->additional_info->resume_headline.' -' : '' }}
                                            @if($val->user->additional_info->resume!='')
                                                <a title="Download Resume" href="{{url('public/uploads/user_resume/'.$val->user->additional_info->resume)}}" download><i class="fa fa-download" aria-hidden="true"></i></a><br/>
                                            @endif
                                            {{ ($val->user->additional_info->industry!=null) ? $val->user->additional_info->industry->name.' -' : '' }}
                                            {!! ($val->user->additional_info->exp_level==1) ? '<a>'.$val->user->additional_info->exp_year.'.'.$val->user->additional_info->exp_month.' Year</a>' : '<a>Fresher</a>' !!}
                                        @endif
                                        {!! ($val->user->city!=null) ? ' - <span class="textgreen">'.$val->user->city->name.'</span>' : '' !!}
                                        
                                    @endif
                                  </td>
                                  <td title="View Candidate Profile"><a href="{{route('view-candidate-profile',['job_id'=>$job_detail->id,'user_id'=>@$val->user->id])}}" target="_blank"><i class="fa fa-user" aria-hidden="true"></i></a></td>
                                  {{-- <td title="Remove Candidate"><a href="{{route('delete-applied-candidate',['job_id'=>$job_detail->id,'user_id'=>@$val->user->id])}}" onclick="return confirm('Are you sure ?');"><i class="fa fa-times fa-red" aria-hidden="true"></i></a></td> --}}
                                </tr>
                              @endif
                            @empty
                            <tr>
                              <td class="bg-danger" colspan="4">Record Not Found</td>
                            </tr>
                            @endforelse
                          </tbody>
                        </table>
                    {{ Form::close() }}
                  <div class="custom-pagination">
                      {{ $candidates->links() }}
                  </div>
        
        </div>
<script>
  
</script>