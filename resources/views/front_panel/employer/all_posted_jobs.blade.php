
@extends('/layouts/front_panel_master')
@section('content')

@include('front_panel.includes.employer_page_banner')

@php
$i=$j=1;
@endphp
<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
         @include('front_panel.includes.employer_dashboard_sidebar')
        <div class="col-sm-9">
          <div class="tabs widget profile-area">
            <ul class="nav nav-tabs widget">
            <li class="active"> <a data-toggle="tab" href="#active_jobs"> Active Jobs <span class="menu-active"><i class="fa fa-caret-up"></i></span>  </a></li>
            <li class=""> <a data-toggle="tab" href="#expired_jobs" id="expired_jobs_li"> Expired Jobs  <span class="menu-active"><i class="fa fa-caret-up"></i></span></a></li>    
            {{-- <li> <a data-toggle="tab" href="#deleted_jobs"> Deleted Jobs  <span class="menu-active"><i class="fa fa-caret-up"></i></span></a></li> --}}
          </ul>
            <div class="tab-content">
              <!-- Start Active Job Tab -->
              <div id="active_jobs" class="tab-pane active">
                  <div class="form-title">
                    <h4 class="edu_detail">Active Jobs</h4>
                  </div>
                  <table class="table table-bordered table-striped data_table">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Job Title</th>
                        <th>Job Type</th>
                        <th>Total Vacancy</th>
                        <th>Post Date</th>
                        <th>Expire Date</th>
                        <th class="nosort">Responses</th>
                      </tr>
                    </thead>
                    <tbody>
                      @forelse($jobs as $k=>$val)
                      
                        @if(strtotime($val->expiry_date)>=strtotime(date('Y-m-d')) && $val->status==1)
                          <tr>
                            <td>{{$i++}}.</td>
                            <td>
                              <span class="job_title">{{$val->job_title}}
                                <div class="action_btns hide-elem">
                                  <a href="{{route('job-edit',['id'=>$val->id])}}"><i class="fa fa-pencil" aria-hidden="true"></i> Edit |</a>
                                  <a href="{{route('job-details',['jobid'=>$val->id])}}" target="_blank"><i class="fa fa-info-circle" aria-hidden="true"></i> Details |</a>
                                  <a href="{{route('delete-job',['job_id'=>$val->id])}}" onclick="return confirm('Are you sure ?');"><i class="fa fa-times fa-red" aria-hidden="true"></i> Delete </a>
                                </div>
                              </span>
                            </td>
                            <td>{{($val->job_type==1) ? 'Basic(Free)' : 'Premium' }}</td>
                            <td>{{$val->total_vacancy}}</td>
                            <td>{{dateConvert($val->created_at,'d-m-Y')}}</td>
                            <td>{{dateConvert($val->expiry_date,'d-m-Y')}}</td>
                            <th title="Job Applied Candidates" class="text-center"><a href="{{route('applied-candidates',['jobid'=>$val->id])}}">{{$val->job_applicants->count()}}</a></th>
                          </tr>
                        @endif
                     @empty
                     
                     @endforelse
                    </tbody>
                  </table>
              </div>
              <!-- end Active Job Tab -->

              <!-- Start Expired Job Tab -->
              <div id="expired_jobs" class="tab-pane">
                  <div class="form-title">
                    <h4 class="edu_detail">Expired Jobs</h4>
                  </div>
                  <table class="table table-bordered table-striped data_table_posted_job__">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Job Title</th>
                        <th>Job Type</th>
                        <th>Total Vacancy</th>
                        <th>Experience</th>
                        <th>Salary</th>
                        <th>Post Date</th>
                        <th>Expire Date</th>
                        <th colspan="4">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @forelse($jobs as $k=>$val)
                        @if(strtotime($val->expiry_date)<=strtotime(date('Y-m-d')) && $val->status==1)
                          <tr>
                            <td>{{$j++}}.</td>
                            <td>{{$val->job_title}}</td>
                            <td>{{($val->job_plan!=null) ? $val->job_plan->name : '' }}</td>
                            <td>{{$val->total_vacancy}}</td>
                            <td>{{$val->min_experience}}-{{$val->max_experience}} Year</td>
                            <td>{{$val->salary_min}}-{{$val->salary_max}} {{$val->currency}}</td>
                            <td>{{dateConvert($val->created_at,'d-m-Y')}}</td>
                            <td>{{dateConvert($val->expiry_date,'d-m-Y')}}</td>
                            <td title="Job Details"><a href="{{route('job-details',['jobid'=>$val->id])}}"><i class="fa fa-info-circle" aria-hidden="true"></i></a></td>
                            <td title="Job Applied Candidates"><a href="{{route('applied-candidates',['jobid'=>$val->id])}}"><i class="fa fa-users" aria-hidden="true"></i></a></td>
                            <td title="Delete Job"><a href="{{route('delete-job',['job_id'=>$val->id])}}" onclick="return confirm('Are you sure ?');"><i class="fa fa-times fa-red" aria-hidden="true"></i></a></td>
                          </tr>
                        @endif
                     @empty
                     <tr>
                      <td class="text-center" colspan='12'>No data available in table</td>
                    </tr>
                     @endforelse
                    </tbody>
                  </table>
              </div>
              <!-- end Expired Job Tab -->
            </div>
          </div>           
        </div>
      </div>
    </div>
  </section>
</section>

@endsection