  
@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel.includes.employer_page_banner')

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
          @include('front_panel.includes.employer_dashboard_sidebar')     
            <div class="col-sm-9">
                <div class="tabs widget profile-area">
				          <div class="tab-content">
                    
      
                          <div class="row">          
                            <div class="col-xs-12 col-sm-12 mid-sec-top">
                              <div class="block pt-0">          
                                {{ Form::model($job_detail,array('url'=>route('job-update'),'class'=>'dasboard-form','id'=>'job-update-form','files'=>true))}} 
                                {{ Form::hidden('id') }}           
                                    <div class="form-section">
                                      <div class="form-title">
                                        <h4 class="jobdetail">Job Details</h4>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Title/Designation</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::text('job_title',null,array('class'=>"form-control",'id'=>"job_title",'placeholder'=>"")) }}                 
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Description</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::textarea('job_description',null,array('class'=>"form-control txt_editor_job",'id'=>"job_description",'placeholder'=>"")) }}
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">No. of vacancies</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::number('total_vacancy',null,array('class'=>"form-control",'id'=>"total_vacancy",'placeholder'=>"")) }}                 
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Monthly Salary</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                                            {{ Form::select('salary_min',config('constants.min_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_min",'placeholder'=>"in US$")) }}                 
                                            <span class="fieldlabel">Minimum</span>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                                            {{ Form::select('salary_max',config('constants.max_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_max",'placeholder'=>"in US$")) }}                 
                                            <span class="fieldlabel">Maximum</span>
                                          </div>
                                        </div>
                                      </div>
                                      @php 
                                        foreach($country as $val){
                                           $all_countries[$val->id]=$val->name;
                                          if($val->is_gulf==1){
                                            $gulf_countries[$val->id]=$val->name;
                                          }
                                        }
                                        @endphp
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Location</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 leftnopadding">
                                            {{ Form::select('job_in_country',['Gulf Country'=>$gulf_countries,'All Country'=>$all_countries],null,array('class'=>"form-control",'id'=>"job_country",'placeholder'=>"Select")) }}                 
                                          </div>
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 leftnopadding">
                                            {{ Form::select('job_in_state',[],null,array('class'=>"form-control",'id'=>"job_state",'placeholder'=>"Select",'multiple'=>true)) }}                 
                                          </div>
                                           <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 nopadding">
                                            {{ Form::select('job_in_city[]',[],null,array('class'=>"form-control",'id'=>"job_city",'placeholder'=>"Select",'multiple'=>true)) }}                 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
            
                                    <div class="form-section">
                                      <div class="form-title">
                                        <h4 class="filter">Filter Options For Better Results</h4>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Education Qualification</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                                            {{ Form::select('basic_course_id',$basic_courses,null,array('class'=>"form-control","id"=>"basic_course",'placeholder'=>"Select")) }} 
                                            <span class="fieldlabel">Basic Education</span>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                                            {{ Form::select('master_course_id',$master_courses,null,array('class'=>"form-control","id"=>"master_course",'placeholder'=>"Select")) }}                 
                                            <span class="fieldlabel">Master Education</span>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field spec-elem hide-elem">Specialization</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                                            <div class="basic-elem hide-elem">
                                              {{ Form::select('basic_specialization_id',[],null,array('class'=>"form-control","id"=>"basic_specialization",'placeholder'=>"Select your specialization")) }} 
                                            </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                                            <div class="master-elem hide-elem">
                                              {{ Form::select('master_specialization_id',[],null,array('class'=>"form-control","id"=>"master_specialization",'placeholder'=>"Select your specialization")) }} 
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::select('industry_id',$industry_list,$job_detail->industry_type,array('class'=>"form-control",'id'=>"industry_type",'placeholder'=>"Select")) }}
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Functional Area</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::select('functional_area_id',$func_area_list,null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select")) }}
                                        </div>
                                      </div>

                                       <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Key Skills</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                           {{ Form::textarea('key_skill',null,array('class'=>"form-control",'id'=>"key_skills",'placeholder'=>"")) }}
                                        </div>
                                      </div>
                                    </div>
                        
                                    <div class="form-section">
                                      <div class="form-title">
                                        <h4 class="personal_detail">Desired Candidates Profile</h4>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Gender</label>
                                        <div class="col-xs-12 col-sm-9 left-pad3">
                                                            
                                            <div class="radioStyle2">
                                              <label for="male">{{ Form::radio('gender','Male',false,array('class'=>"form-control",'id'=>"male")) }}Male</label>
                                            </div>                    
                                            <div class="radioStyle2">
                                              <label for="female">{{ Form::radio('gender','Female',false,array('class'=>"form-control",'id'=>"female")) }}Female</label>
                                            </div>
                                          
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Work Experience</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                                            {{ Form::select('min_experience',getDigitRange(0,30,'year'),null,array('class'=>"form-control",'id'=>"exp_year_from",'placeholder'=>"in Year")) }}                 
                                            <span class="fieldlabel">Minimum</span>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                                            {{ Form::select('max_experience',[],null,array('class'=>"form-control",'id'=>"exp_year_to",'placeholder'=>"in Year")) }}                 
                                            <span class="fieldlabel">Maximum</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Nationality</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                           {{ Form::select('nationality_id',$nationality,null,array('class'=>"form-control",'id'=>"nationality",'placeholder'=>"Select")) }}
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Current Location of the Candidate</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::select('current_location',$country,null,array('class'=>"form-control",'id'=>"current_location",'placeholder'=>"Select")) }} 
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-section">
                                      <div class="form-title">
                                        <h4 class="manageres">Manage Response</h4>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Response</label>
                                        <div class="col-xs-12 col-sm-9">
                                          <div class="row">                    
                                            <div class="radioStyle2">
                                              <label for="male">{{ Form::radio('reponce_manage','Male',false,array('class'=>"form-control")) }}On Response Manager </label>
                                            </div>                    
                                            <div class="radioStyle2">
                                              <label for="female">{{ Form::radio('reponce_manage','Female',false,array('class'=>"form-control")) }}On Response Manager and on Email</label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group row mt-20">
                                      <div class="col-sm-9">
                                        <button type="submit" class="newfclassub">Update Job</button>
                                      </div>
                                    </div>
                                  {{ Form::close() }}
                                  
                                </div>
                            </div>
                          </div>
                      
                   
                  </div>
  <!-- tab-content --> 
                </div>
<!-- tabs-widget -->              
            </div>
      </div>
    </div>
  </section>
</section>

<script>
  $(document).ready(function(){
    $('#exp_year_from').change();
    $('#basic_course').change();
    $('#master_course').change();
    globalVar.basic_s_id="{{$job_detail->basic_specialization_id}}";
    globalVar.master_s_id="{{$job_detail->master_specialization_id}}";
  })
  $('#exp_year_from').change(function(){
    var max_exp={{$job_detail->max_experience}};
    var array = JSON.parse('{!! json_encode(getDigitRange(0,30,'year')) !!}');
    var value = parseInt($(this).val());
    $('#exp_year_to').find('option').not(':first').remove();
    $.each(array,function(index,val){
      if(value <= index){
        if(index==max_exp)
          $('#exp_year_to').append('<option value="'+index+'" selected  >'+val+'</option>'); 
        else
          $('#exp_year_to').append('<option value="'+index+'">'+val+'</option>');
      }
    });
  });
</script>
<script>
  /* $('#functional_area').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'/api/func-area-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    }); */
  /* $('#nationality').selectize({
      maxItems: 1,
      valueField: 'id',
      labelField: 'name',
      searchField: 'name',
      create: false,
      options: [],
      render: {
          option: function(item, escape) {           
              return '<div>' +escape(item.name)+'</div>';
          }
      },
      load: function(query, callback) {
          if (!query.length) return callback();
          $.ajax({
              url: base_url+'api/nationality-suggestion',
              type: 'GET',
              dataType: 'json',
              data: {
                  q: query,
              },           
              success: function(res) {
                  callback(res);
              },
              error: function(error) {
                  callback();
              }
          });
      }
    });
    */
   /* $('#industry_type').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: [],
        onInitialize: function(){
          var selectize = this;
          selectize.setValue({{$job_detail->industry_id}});
        },
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'/api/industry-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });*/
    $('#key_skills').selectize({
      plugins: ['remove_button'],
      maxItems: 15,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
      options: {!! json_encode($skill_keywords) !!},
    
   });
  $('#current_location').selectize({
      maxItems: 1,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
      options: {!! json_encode($country) !!},
      onInitialize: function(){
          var selectize = this;
          selectize.setValue("{{$job_detail->current_location}}");
        },
     
  });
  var countryId='';
  var defaultCountry = {{$job_detail->job_in_country}};
  // $('#job_country').selectize({
  //       maxItems: 1,
  //       valueField: 'id',
  //       labelField: 'name',
  //       searchField: 'name',
  //       create: false,
  //       placeholder: 'Select Country',
  //       options: {!! json_encode($country) !!},
  //       onInitialize: function(){
  //         var selectize = this;
  //         selectize.setValue({{$job_detail->job_in_country}});
  //       },
  //       onItemAdd:function(value, $item){
  //         countryId=value;
  //         $.ajax({
  //               url: base_url+'api/state-suggestion',
  //               type: 'GET',
  //               dataType: 'json',
  //               data: {
  //                   country_id: countryId
  //               },           
  //               success: function(res) {
  //                 rePopulateState(res); 
  //                 rePopulateCity();
  //               },
  //               error: function(error) {
  //                 rePopulateState();
  //               },
  //           });
  //       },
  //       onDelete:function(value){
  //         rePopulateState();
  //       }
  // });
  $('#job_country').on('change',function(){
           countryId=$(this).val();
           $.ajax({
                url: base_url+'api/state-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    country_id: countryId
                },           
                success: function(res) {
                  rePopulateState(res); 
                  rePopulateCity();
                },
                error: function(error) {
                  rePopulateState();
                },
            });
  });
  $('#job_state').selectize({
    maxItems: 1,
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    create: false,
    placeholder: 'Select State',
    options: {!! json_encode($state) !!},
    onItemAdd:function(value, $item){
      var stateId=value;
      $.ajax({
            url: base_url+'api/city-suggestion',
            type: 'GET',
            dataType: 'json',
            data: {
                state_id: stateId
            },           
            success: function(res) {
                
                rePopulateCity(res);
            },
            error: function(error) {
               rePopulateCity();
            }
        });
    },
    onInitialize: function(){
      var selectize = this;
      selectize.setValue({{$job_detail->job_in_state}});
    },
    
    onDelete:function(value){
       rePopulateCity();
    }
    
});

//$("#job_state").val({{$job_detail->job_in_state}});
$('#job_state').selectize({
  plugins: ['remove_button'],
  maxItems: 1,
  valueField: 'id',
  labelField: 'name',
  searchField: 'name',
  create: false,
  options: [],

});
  $('#job_city').selectize({
    plugins: ['remove_button'],
    maxItems: 3,
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    create: true,
    options: {!! json_encode($city) !!},
    onInitialize: function(){
        var selectize = this;
        var skill='19183';
       // var splitVal=skill.split(',');
        //var selected_items=splitVal;
        selectize.setValue(skill);
    }
});
function rePopulateState(data=null){
  var selectize = $("#job_state")[0].selectize;
  if(countryId!=defaultCountry)  { 
        defaultCountry=0;
      selectize.clear();
      selectize.clearOptions();
    }
  selectize.renderCache['option'] = {};
  if(data!=null)
    selectize.addOption(data);
}
  function rePopulateCity(data=null){
  var city_data="{{$job_detail->jobs_in_city}}";
  var jobs_in_city=JSON.parse(city_data.replace(/&quot;/g,'"'));
  var selected_items=[];
  $.each(jobs_in_city,function(index,item){
    selected_items.push(item.city_id);
  });
  var selectize = $("#job_city")[0].selectize; 
  selectize.clear();
  selectize.clearOptions();
  selectize.renderCache['option'] = {};
  if(data!=null)
    selectize.addOption(data);
    selectize.setValue(selected_items);
}

$('#basic_course').change(function(e){
    $('.spec-elem').show();
    if($(this).val()>0){
      $('.basic-elem').show();
    } else {
      $('.basic-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationBasic, globalFunc.error, globalFunc.complete);
});
$('#master_course').change(function(e){
    $('.spec-elem').show();
    if($(this).val()>0){
      $('.master-elem').show();
    } else {
      $('.master-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationMaster, globalFunc.error, globalFunc.complete);
});
</script>
@endsection