@foreach($top_employers as $k=>$val)
    <div class="col-sm-6 col-md-6 col-lg-4 mt20 responsiveCard">
        <div class="cardBox" style="min-height:330px">
            <div class="row">
                <div class="col-xs-12 text-center"><img class="cardImg" src="{{checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')}}"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center cardCompanyName">{{ substr($val->comp_name,0,50) }} </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center cardCompanyLoc">{{(isset($val->country)) ? "(".$val->country->name .")": ''}}</div>
            </div>
            <div class="row  text-center">
                <div class="col-xs-12 text-center cardCompanyOpenings">
                    @if($val->job_post!=null && count($val->job_post)>0)
                        <a class="btn btn-success success-btn searchButton cardOpening" href="{{url('companies/'.$val->id)}}">{{ count($val->job_post) }}>OPENINGS</a>
                    {{-- <a href="{{url('companies/'.$val->id)}}">{{ count($val->job_post) }} OPENING</a> --}}
                @else
                    <a href="{{url('companies/'.$val->id)}}" class="btn btn-green" >NO OPENING</a>
                @endif
                    
                </div>
            </div>


        </div>
    </div>
@endforeach
<div class="row margnBtm30">
    <div class="col-lg-12 mt20">
        <div class="paginationContainer">      
            <div class="custom-pagination">
                {{$top_employers->links()}}
            </div>
        </div>
    </div>
</div>
<script>
    var search='{{$search_tag}}';
    var array = search.split(',');
    if(search)
    {
        $('#search_key').html('');
        $.each( array, function( key, value ) {
        if(value )
        $('#search_key').append('<span>'+value+'</span>|');
        });
    }
    
</script>
                             