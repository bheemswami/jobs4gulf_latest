
@extends('/layouts/front_panel_master')
@section('content')
@php
$defaultImages=['job-interview-tips.jpg','job-interview.jpg','Business-Photos-1.jpg'];
@endphp
  <div id="first-slider">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
        <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @if($employer_details->slider_images!=null)
                @forelse($employer_details->slider_images as $k=>$val)
                    <div class="item {{($k==0) ? 'active' : '' }} slide{{$k+1}}" style="background-image: url({{checkFile($val->image,'uploads/slider_image/','no_images.png')}}">
                      <div class="row">
                        <div class="container"></div>
                      </div>
                    </div>
                @empty
                  @foreach($defaultImages as $k=>$val)
                    <div class="item {{($k==0) ? 'active' : '' }} slide{{$k+1}}" style="background-image: url({{checkFile($val,'img/','no_images.png')}}">
                      <div class="row">
                        <div class="container"></div>
                      </div>
                    </div>
                  @endforeach
                @endforelse
            @endif
            
        </div>
        <!-- End Wrapper for slides-->
         <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
        </a>
    </div>
</div>


<section style="margin-top: 55px;margin-bottom: 20px;">
  <div class="taber ">
  <div class="container">
    <div class="row">
        <div class="col-lg-12">    
          <div class="ear">
            <div class="clearfix job-detail-inner">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 text-left">
                  <h3>{{ $employer_details->comp_name }}</h3>
                  <h5>Saicon Consultant is Deals in all Sectors of Recruitment as Well as Placement.</h5>
                  <p><b class="icon-user small vam blue-text mr5px"></b><span class="pl5px" itemprop="member">{{ ucfirst($employer_details->contact_person) }}</span></p>
                  <div class="post-details">
                  
                  </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <img src="{{checkFile($employer_details->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="com">
                </div>
            </div>
             <div class="haf post-details">
              <p> <span><i class="fa fa-map-marker" aria-hidden="true"></i> {{ ($employer_details->city!=null) ? $employer_details->city->name : '' }} - {{ ($employer_details->country!=null) ? $employer_details->country->name : '' }} </span>
                      @if($employer_details->comp_website!='')
                        <span><i class="fa fa-globe" aria-hidden="true"></i> <a target="blank" href="{{$employer_details->comp_website}}">Visit</a> </span>
                      @endif
                    <span><i class="fa fa-suitcase" aria-hidden="true"></i> {{ ($employer_details->job_post!=null) ? count($employer_details->job_post) : 0 }} Active Jobs</span>
                      </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="container">
<div class="row">
  <div class="col-md-6">
<h2 class="title-widget-sidebar note2 color-is" style="color:black; ">Company profile 
</h2>
{!! $employer_details->comp_profile !!}
</div>

<div class="col-md-6">
 <h2 class="title-widget-sidebar note2 color-is" style="color:black;">Contact Us </h2>
<div id="show_more" class="dn" style="display: block;">
    
    <ul class="fo ac-fl ac-w45 mt20px">
      <li class="mr15px" itemscope="" itemtype="http://schema.org/Organization">
      <p class="blue-text">{{ ($employer_details->city!=null) ? ucfirst($employer_details->city->name) : '' }} :</p>
      <p><b class="icon-user small vam blue-text mr5px"></b><span class="pl5px" itemprop="member">{{ ucfirst($employer_details->contact_person) }} {{ ($employer_details->designations!=null) ? ucfirst($employer_details->designations->name) : '' }}</span></p>
      <p itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress"> 
        <b class="icon-map-marker small vam blue-text mr5px"></b>
        <span class="pl5px" itemprop="streetAddress">{{ $employer_details->comp_address }} , {{ ($employer_details->city!=null) ? $employer_details->city->name : '' }} , {{ ($employer_details->country!=null) ? $employer_details->country->name : '' }}</span> </p>
        <a class="bdr p5px10px bgf7f7f7 dib mt2px blue-text" href="javascript:void(0)"> <i class="icon icon-phone hig vam"></i> {{ $employer_details->phone }} </a>
      
      </li>
   </ul>
</div>
                    
</div>

</div>
</div>
</section>

@if($employer_details->job_post!=null && count($employer_details->job_post)>0)
  <section style="margin-bottom: 211px;">
    <div class="container">
      <h2 class="title-widget-sidebar note2 color-is" style="color:black;">Careers</h2>
      <div class="row">
        @foreach($employer_details->job_post as $k=>$val)
        <div class="col-xs-12 col-md-6 related-block">
          <div class="clearfix nopadding lcop">
            <div class="job-box">
              <div class="row flexbox">
                <div class="col-xs-3 col-sm-2 left">
                  <div class="div-table">
                    <div class="table-cell-div">
                      <img src="{{checkFile($employer_details->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="center-block">
                     </div>
                  </div>
                </div>
                <div class="col-xs-9 col-sm-10 right">
                  <h2><a href="{{route('job-details',['jobid'=>$val->id])}}">{{$val->job_title}}</a></h2>
                  <h5>{{ ($val->employer_info!=null) ? $val->employer_info->comp_name : '' }}</h5>
                  <div class="post-details">
                    <p>{{$val->key_skill}}</p>
                    <p> 
                      <span><i class="fa fa-suitcase" aria-hidden="true"></i>{{$val->min_experience}}-{{$val->max_experience}} years</span> 
                      <span><i class="fa fa-map-marker" aria-hidden="true"></i>{{$val->job_in_city}} - {{ ($val->country!=null) ? $val->country->name : '' }}</span>
                    </p>
                  </div>
                </div>
              </div>
              <div class="job_optwrap">
                <p>Posted : {{dateConvert($val->created_at,'jS M Y')}}</p>
              </div>
            </div>
          </div>
        </div>  
        @endforeach
      </div>
    </div>
  </section>
@endif
<script>
(function( $ ) {

    //Function to animate slider captions 
  function doAnimations( elems ) {
    //Cache the animationend event in a variable
    var animEndEv = 'webkitAnimationEnd animationend';
    
    elems.each(function () {
      var $this = $(this),
        $animationType = $this.data('animation');
      $this.addClass($animationType).one(animEndEv, function () {
        $this.removeClass($animationType);
      });
    });
  }
  
  //Variables on page load 
  var $myCarousel = $('#carousel-example-generic'),
    $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
    
  //Initialize carousel 
  $myCarousel.carousel();
  
  //Animate captions in first slide on page load 
  doAnimations($firstAnimatingElems);
  
  //Pause carousel  
  $myCarousel.carousel('pause');
  
  
  //Other slides to be animated on carousel slide event 
  $myCarousel.on('slide.bs.carousel', function (e) {
    var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
    doAnimations($animatingElems);
  });  
    $('#carousel-example-generic').carousel({
        interval:3000,
        pause: "false"
    });
  
})(jQuery); 

</script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>
@endsection