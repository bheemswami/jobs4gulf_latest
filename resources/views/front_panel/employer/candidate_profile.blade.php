@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel.includes.employer_page_banner')

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">

      <div class="row">      	
        @include('front_panel.includes.employer_dashboard_sidebar') 
              
        <div class="col-sm-9">
          <div class="tabs widget profile-area">
				 
<div class="tab-content">
  <div id="profile-tab" class="tab-pane active">
    <div class="pd-20">       
            
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
              <h4 class="personal_detail">Personal Details</h4>
            </div>
            
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Full Name:</label>
                <div class="col-xs-6 controls">{{ $user->gender }} {{ ucfirst($user->name) }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">User Name:</label>
                <div class="col-xs-6 controls">{{ $user->email }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">City:</label>
                <div class="col-xs-6 controls">{{ ($user->city!=null) ? $user->city->name : 'None' }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Country:</label>
                <div class="col-xs-6 controls">{{ ($user->country!=null) ? $user->country->name : 'None' }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Date of Birth:</label>
                <div class="col-xs-6 controls">{{ dateConvert($user->dob,'d-m-Y') }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Phone:</label>
                <div class="col-xs-6 controls">{{ ($user->country_code!=null) ? '+'.$user->country_code.'-' : '' }}{{ ($user->mobile!=null) ? $user->mobile : 'None' }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
           
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
                <h4 class="edu_detail">Educational Details</h4>
            </div>
           
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">                
                <div class="col-xs-12 controls">
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Education</th>
                      <th>Course</th>
                      <th>Specialization</th>
                      <th>Year</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>Basic</th>
                      <td>{{ ($user->education !=null) ? @$user->education->basic_course->name : 'None' }}</td>
                      <td>{{ ($user->education !=null) ? @$user->education->basic_specialization->name : 'None' }}</td>
                      <td>{{ ($user->education !=null) ? @$user->education->basic_comp_year : 'None' }}</td>
                    </tr>
                    @if($user->education !=null && $user->education->master_course!=null)
                    <tr>
                      <th>Master</th>
                      <td>{{ ($user->education !=null) ? @$user->education->master_course->name : '' }}</td>
                      <td>{{ ($user->education !=null) ? @$user->education->master_specialization->name : '' }}</td>
                      <td>{{ ($user->education !=null) ? @$user->education->master_comp_year : '' }}</td>
                    </tr>
                    @endif
                  </tbody>
                </table>
                </div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Institute / University:</label>
                <div class="col-xs-6 controls">{{ ($user->education !=null) ? $user->education->institute : 'None' }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
          </div>
       
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
              <h4 class="work_exp">Additional Details</h4>
            </div>
            @if($user->additional_info!=null && $user->additional_info->exp_level==1)
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Work Experiance:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->exp_year }} Year {{ $user->additional_info->exp_month }} Month</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Employer Name:</label>
                  <div class="col-xs-6 controls">{{ ($user->additional_info!=null && $user->additional_info->company!=null) ? $user->additional_info->company->name : 'None' }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Employer's Industry:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->industry->name }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Position:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->current_position }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Monthly Salary:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->salary }} ({{ config('constants.currency')[$user->additional_info->salary_in] }})</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Functional Area / Department:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->function_area->name }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
            @endif
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Key Skills:</label>
                  <div class="col-xs-6 controls">{{ ($user->additional_info!=null) ? $user->additional_info->key_skills : 'None' }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
          </div>

          
      </div>
    </div>
      <!-- pd-20 --> 
  </div>
    <!-- home-tab -->
    
    
  </div>
  <!-- tab-content --> 
</div>
<!-- tabs-widget -->              
</div>
      </div>
    </div>
  </section>
</section>

@endsection