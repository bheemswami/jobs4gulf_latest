@extends('/layouts/front_panel_master')
@section('content')

<section id="inner-banner" class="dashboard-banner" style="background: url({{url('public/images/inner-banner.jpg')}}) no-repeat center top;">
<div class="overlay"></div>
 @include('front_panel/includes/search_job_section')
</section>


<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
          @include('front_panel.includes.employer_dashboard_sidebar',['user'=>$user])     
            <div class="col-sm-9">
                <div class="tabs widget profile-area">
                  <div class="tab-content">
                    <div class="row">          
                      <div class="col-xs-12 col-sm-12 mid-sec-top">
                        <div class="block pt-0"> 
                           {{ Form::model($user->employer_info,array('url'=>route('emp-save-profile'),'class'=>'dasboard-form','id'=>'employer-update-form','files'=>true))}}
                           {{ Form::hidden('user_id',null,['id'=>'user_id']) }}
                           <div class="form-section">
                             <div class="form-title">
                               <h4 class="logininfo">Login Information</h4>
                             </div>                             
                             <div class="form-group row">
                                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company E-mail ID</label>
                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                  {{ Form::email('email',$user->email,array('class'=>"form-control",'id'=>"emp_email",'placeholder'=>"Enter Company E-mail ID")) }}                 
                                </div>
                              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Password </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="input-group pwd-grp">
                    {{ Form::password('password',array('class'=>"form-control pwd",'id'=>"emp_password",'placeholder'=>"Enter Password")) }}
                    <span class="pass-show input-group-btn"><button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button></span>
                  </div>
                </div>
              </div>

            </div>
            
            <div class="form-section">
              <div class="form-title">
                <h4 class="contactinfo">Contact Person Information</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Person</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="form-row">
                    <div class="col-xs-12 col-sm-4 form-child-block">
                      {{ Form::select('cp_title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"cp_title",'placeholder'=>"Select Title")) }}
                    </div>
                    <div class="col-xs-12 col-sm-8 form-child-block">
                      {{ Form::text('cp_name',null,array('class'=>"form-control",'id'=>"cp_name",'placeholder'=>"Enter Contact Person Name")) }}
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Designation</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::select('cp_designation',$designations,null,array('class'=>"form-control hintable",'hint-class'=>"",'id'=>"designation",'placeholder'=>"Enter Designation")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Number</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                 <div class="form-row">
                   <div class="col-xs-12 col-sm-4 form-child-block">
                      {{ Form::select('dial_code',$country_codes,null,array('class'=>"form-control",'id'=>"dial_code",'placeholder'=>"Select Code")) }} 
                    </div>
                    <div class="col-xs-12 col-sm-8 form-child-block">
                      {{ Form::text('cp_mobile',null,array('class'=>"form-control",'id'=>"cp_mobile",'placeholder'=>"Enter Contact Number")) }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
                        
            <div class="form-section">
              <div class="form-title">
                <h4 class="factory">Company Information</h4>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Name</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('comp_name',null,array('class'=>"form-control",'id'=>"company_name",'placeholder'=>"Enter Company Name")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">About Company</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::textarea('comp_about',null,array('class'=>"form-control",'id'=>"company_about",'placeholder'=>"Enter About Company")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Company Website</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('comp_website',null,array('class'=>"form-control",'id'=>"company_website",'placeholder'=>"Enter Company Website URL")) }}
                </div>
              </div>

              <div class="form-group row">
               <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::select('comp_industry',$industry_list,null,array('class'=>"form-control",'id'=>"industry_type",'placeholder'=>"Select Industry Type")) }} 
                </div>
              </div>

              <div class="form-group row">  
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"> Company Logo</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <img src="{{url('public/images/default/no_logo.png')}}" class="user-default" style="width:15%">
                  <div class="form-field-note">
                    Upload only Image (.png/.jpg/.jpeg) formats.
                  </div><br>
                  <div class="upload-btn-wrapper">
                    <lable class="label-button">SELECT LOGO</lable> 
                    {{ Form::file('comp_img',array('class'=>"upload",'id'=>"comp_logo")) }}
                  </div>
                  <div class="upload-btn-wrapper">
                  <lable class="remove rmv_img">REMOVE LOGO</lable>
                  </div>
                </div>
              </div>

              <h2>Mailing Address</h2>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Country</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::select('comp_country',
                    ['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,
                    array('class'=>"form-control",'id'=>"country",'placeholder'=>"Select Country")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">City</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::select('comp_city',[],null,array('class'=>"form-control",'id'=>"city",'autocompletion'=>'off','placeholder'=>"Select City")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Address</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::textarea('comp_address',null,array('class'=>"form-control",'id'=>"company_address",'placeholder'=>"Enter Address")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">PO Box/ Zip Code</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('zip_code',null,array('class'=>"form-control",'id'=>"zip_code",'placeholder'=>"Enter PO Box/ Zip Code")) }}
                </div>
              </div>
            </div>

                           <div class="form-section">
                             <div class="form-title">
                               <h4 class="contactinfo">Social Links</h4>
                             </div>
                             <div class="form-group row">
                               <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Facebook Url</label>
                               <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                 {{ Form::text('fb_link',isset($user->employer_info) ? $user->employer_info->fb_link : '',array('class'=>"form-control",'id'=>"fb_link",'placeholder'=>"Enter Facebook Url")) }}
                               </div>
                             </div>

                             <div class="form-group row">
                               <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Twitter Url</label>
                               <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                 {{ Form::text('twitter_link',isset($user->employer_info) ? $user->employer_info->twitter_link : '',array('class'=>"form-control",'id'=>"twitter_link",'placeholder'=>"Enter Twitter Url")) }}
                                 
                               </div>
                             </div>

                              <div class="form-group row">
                               <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">G-Plus Url</label>
                               <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                 {{ Form::text('gplus_link',isset($user->employer_info) ? $user->employer_info->gplus_link : '',array('class'=>"form-control",'id'=>"gplus_link",'placeholder'=>"Enter Google Plus Url")) }}
                                 
                               </div>
                             </div>
                             
                           </div>
                           <div class="form-group row mt-20">
                             <div
                              class="col-sm-9 col-sm-offset-3 nopadding">
                               <button type="submit" class="newfclassub">Update</button>
                             </div>
                           </div>
                          {{ Form::close() }}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>            
            </div>
      </div>
    </div>
  </section>
</section>
<script>
globalVar.city_id='{{$user->employer_info->comp_city}}';
globalVar.countryId='{{$user->employer_info->country_id}}';
 $("#comp_logo").change(function(){
    previewImage(this,'.user-default','{{url('public/images/default/no_logo.png')}}');
  });
  $('.rmv_img').click(function () {
    $("#comp_logo").val(null);
    $(".user-default").attr('src','{{url('public/images/default/no_logo.png')}}');
  });
$(function(){
      $("#country").change();
 });
$('#country_code').change(function(){
      globalVar.countryId = $(this).val().split('~')[1];
      $('#country').val(globalVar.countryId);
      $('#country').change();
});
 $('#country').change(function(){

      globalVar.countryId = $(this).val();
      $("#country").val(globalVar.countryId);
      $('#city').find('option').not(':first').remove();
      $.ajax({
        url: base_url+'/api/city-suggestion',
        type: 'GET',
        dataType: 'json',
        data: {
            by_country: 1,
            country_id: globalVar.countryId,
        },           
        success: function(res) {
           $.each(res,function(key,val){
            if(globalVar.city_id==val.id){
              $('#city').append('<option value="'+val.id+'" selected>'+val.name+'</option>');
            } else {
              $('#city').append('<option value="'+val.id+'">'+val.name+'</option>');
            }
           });
        },
        error: function(error) {
            
        }
      });
  });

</script>
@endsection