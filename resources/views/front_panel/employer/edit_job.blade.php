  
@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel.includes.employer_page_banner')
@php 
  $gulfCountry=getGulfCountries();
  $otherCountry=getOtherCountries();
  $walkinTime = explode(' ', $job_detail->walkin_time);
  $total_country = count($job_detail->candidate_country);

  $qualifications=$specialization=$nationality=$jobsCity=$skills=$candidateCityElemIds=$candidateCity=[];
  
  if($job_detail->job_qualifications!=null){
    foreach($job_detail->job_qualifications as $k=>$val){
      $qualifications[]=$val->course_id;
    }
  }
  if($job_detail->job_specializations!=null){
    foreach($job_detail->job_specializations as $k=>$val){
      $specialization[]=$val->specialization_id.'~'.$val->course_id;
    }
  }
  if($job_detail->job_nationality!=null){
    foreach($job_detail->job_nationality as $k=>$val){
      $nationality[]=$val->nationality_id;
    }
  }
  if($job_detail->jobs_in_city!=null){
    foreach($job_detail->jobs_in_city as $k=>$val){
      $jobsCity[]=['id'=>$val->city->id,'name'=>$val->city->name];
    }
  }
  if($job_detail->skills!=null){
    foreach($job_detail->skills as $k=>$val){
      $skills[]=['id'=>$val->skill_keywords->id,'name'=>$val->skill_keywords->name];
    }
  }
  if($job_detail->candidate_city!=null){
    foreach($job_detail->candidate_city as $k=>$val){
      if(isset($candidateCity[$val->country_id])){
        $candidateCity[$val->country_id][]=$val->city_id;
      } else {
        $candidateCity[$val->country_id]=[$val->city_id];
      }
    }
  }

@endphp
<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
          @include('front_panel.includes.employer_dashboard_sidebar')     
            <div class="col-sm-9">
                <div class="tabs widget profile-area">
				          <div class="tab-content">
                          <div class="row">          
                            <div class="col-xs-12 col-sm-12 mid-sec-top">
                              <div class="block pt-0">          
                                {{ Form::model($job_detail,array('url'=>route('job-update'),'class'=>'dasboard-form','id'=>'job-update-form','files'=>true))}} 
                                {{ Form::hidden('id') }}           
                                    <div class="form-section">
                                  <div class="form-title">
                                    <h4 class="jobdetail">Job Details</h4>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Type</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      @if($job_detail->job_type==1)
                                        {{ Form::select('job_type',[1=>'Basic'],null,array('class'=>"form-control")) }}
                                      @elseif($job_detail->job_type==2)
                                        {{ Form::select('job_type',[2=>'Premium'],null,array('class'=>"form-control")) }}
                                      @else
                                        {{ Form::select('job_type',[1=>'Basic',2=>'Premium'],null,array('class'=>"form-control",'placeholder'=>'Select Job Type')) }}
                                      @endif
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Title/Designation</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::text('job_title',null,array('class'=>"form-control",'id'=>"job_title",'placeholder'=>"Enter Complete Designation")) }}                 
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Description</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::textarea('job_description',null,array('class'=>"form-control",'id'=>"job_description",'placeholder'=>"Enter Job Description")) }}
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Location</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 leftnopadding">
                                        {{ Form::select('job_in_country',
                                          ['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,
                                        array('class'=>"form-control",'id'=>"job_country",'placeholder'=>"Select Country")) }}
                                      </div>
                                     
                                       <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 nopadding">
                                        {{ Form::TEXT('job_in_city',null,array('class'=>"form-control",'id'=>"job_city",'autocompletion'=>'off','placeholder'=>"Select City")) }} 
                                         <span id="errorJobInCity"></span>                
                                      </div>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Monthly Salary</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                                        {{ Form::select('salary_min',config('constants.min_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_min",'placeholder'=>"in US$")) }}
                                         <span class="fieldlabel">Minimum</span>                 
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                                        {{ Form::select('salary_max',config('constants.max_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_max",'placeholder'=>"in US$")) }}
                                         <span class="fieldlabel">Maximum</span>                 
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"></label>  
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <lable class="checkbox-btn btn2">{{Form::checkbox('show_salary', 1, true)}}Do not Display the Salary range to Job Seekers</lable>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">No. of vacancies</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::number('total_vacancy',null,array('class'=>"form-control",'id'=>"total_vacancy",'placeholder'=>"Enter No. of Vacancies")) }}                 
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-section">
                                  <div class="form-title">
                                    <h4 class="filter">Job Classification</h4>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::select('industry_id',$industry_list,null,array('class'=>"form-control",'id'=>"job_industry",'placeholder'=>"Select Industry")) }}
                                    </div>
                                  </div>             

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Functional Area</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::select('functional_area_id',$functional_area,null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select Functional Area")) }}
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Keywords</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                       {{ Form::textarea('keywords',null,array('class'=>"form-control",'id'=>"key_skills",'placeholder'=>"Enter important keywords that can describe this job")) }}
                                    <span id="errorKeywords"></span>
                                    </div>
                                  </div>
                                </div>
                                            
                                <div class="form-section">
                                  <div class="form-title">
                                    <h4 class="personal_detail">Eligiblity Criteria</h4>
                                  </div>
                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Qualification</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::select('quilification[]',$educations,null,array('class'=>"form-control",'id'=>"qualifications",'multiple'=>"multiple")) }}
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Specialization</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::select('specialization[]',[],null,array('class'=>"form-control",'id'=>"specialization",'multiple'=>"multiple")) }}
                                    </div>
                                  </div>


                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Gender</label>
                                    <div class="col-xs-12 col-sm-9 left-pad3">
                                      <div class="row">
                                       <div class="radioStyle2" >
                                          <label for="male">{{ Form::radio('gender','Male',false,array('class'=>"form-control",'id'=>"male")) }}Male</label>
                                        </div>                    
                                        <div class="radioStyle2">
                                          <label for="female">{{ Form::radio('gender','Female',false,array('class'=>"form-control",'id'=>"female")) }}Female</label>
                                        </div>
                                        <div class="radioStyle2">
                                          <label for="anyone">{{ Form::radio('gender','No Preference',false,array('class'=>"form-control",'id'=>"anyone")) }}No Preference</label>
                                        </div>
                                        <span id="errorGender"></span>
                                     </div>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Work Experience</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                                        {{ Form::select('min_experience',getDigitRange(0,30,'year'),null,array('class'=>"form-control",'id'=>"exp_year_from",'placeholder'=>"in Year")) }}                 
                                        <span class="fieldlabel">Minimum</span>
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                                        {{ Form::select('max_experience',[],null,array('class'=>"form-control",'id'=>"exp_year_to",'placeholder'=>"in Year")) }}                 
                                        <span class="fieldlabel">Maximum</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Nationality</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                       {{ Form::select('nationality_id[]',config('constants.nationality'),null,array('class'=>"form-control",'id'=>"nationality",'multiple'=>"multiple")) }}
                                    </div>
                                  </div>

                                  <div class="current_location_section">
                                    @if($job_detail->candidate_country!=null)
                                      @foreach($country_city as $key=>$value)
                                          @php 
                                            $city=[];
                                            foreach($value->cities as $val){
                                              $city[$val->id]=$val->name;
                                            }
                                            $id = 'c_city'.$key;
                                            $candidateCityElemIds[] = ['elem_id'=>$id, 'country_id'=>$value->id]; 
                                          @endphp
                                          <div class="location_elem">
                                            <div class="form-group row" id="d_{{$key}}">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">@if($key==0) Current Location of the Candidate @endif</label>
                                              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                {{ Form::select('candidate_current_country['.$key.']',['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],$value->id,array('class'=>"form-control curr_loc",'data-id'=>$value->id,'placeholder'=>"Select Country")) }} 
                                              </div>
                                              
                                              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                {{ Form::select('candidate_current_city['.$key.'][]',$city,null,array('class'=>"form-control curr_city",'id'=>$id,'multiple'=>"multiple")) }} 
                                              </div>
                                              @if($key==0) 
                                                <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 add_more"><i class="fa fa-plus fa-icon-green" aria-hidden="true"></i></div>
                                              @else
                                                <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 rmv" data-id="d_{{$key}}"><i class="fa fa-minus fa-icon-red" aria-hidden="true"></i></div>
                                              @endif
                                            </div>
                                          </div>
                                      @endforeach
                                    @else 

                                      <div class="location_elem">
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Current Location of the Candidate</label>
                                          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            {{ Form::select('candidate_current_country[0]',['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,array('class'=>"form-control curr_loc",'placeholder'=>"Select Country")) }} 
                                          </div>
                                          
                                          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            {{ Form::select('candidate_current_city[0][]',[],null,array('class'=>"form-control curr_city",'id'=>'c_city0','multiple'=>"multiple")) }} 
                                          </div>
                                          <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 add_more"><i class="fa fa-plus fa-icon-green" aria-hidden="true"></i></div>
                                        </div>
                                      </div>

                                    @endif
                                  </div>

                                </div>

                                <div class="form-section">
                                  <div class="form-title">
                                    <h4 class="manageres">Manage Response</h4>
                                  </div>
                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Seeker will Respond you at</label>
                                    <div class="col-xs-12 col-sm-8">
                                      <div class="row">                    
                                        <div class="radioStyle2">
                                          <label for="responce_1">{{ Form::radio('response_by','Email',false,array('class'=>"form-control res_manage",'id'=>'responce_1')) }}Email </label>
                                        </div>                    
                                        <div class="radioStyle2">
                                          <label for="responce_2">{{ Form::radio('response_by','ContactDetails',false,array('class'=>"form-control res_manage",'id'=>'responce_2')) }}Contact Details</label>
                                        </div>
                                        <div class="radioStyle2">
                                          <label for="responce_3">{{ Form::radio('response_by','Walkin',false,array('class'=>"form-control res_manage",'id'=>'responce_3')) }}WalkIn</label>
                                        </div>
                                        <div class="radioStyle2">
                                          <label for="responce_4">{{ Form::radio('response_by','All',false,array('class'=>"form-control res_manage",'id'=>'responce_4')) }}All</label>
                                        </div>
                                      </div>
                                      <span id="errorResponseBy"></span>
                                    </div>
                                  </div>

                                  <div class="form-group row manage_sec_1 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Mailing E-Mail ID</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::text('response_email',null,array('class'=>"form-control",'id'=>"mailing_email",'placeholder'=>"Mailing E-Mail ID (Comma seperated for multiple)")) }}                 
                                    </div>
                                  </div>
                                  <div class="form-group row manage_sec_2 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Person</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="form-row">
                                        <div class="col-xs-12 col-sm-4 form-child-block">
                                          {{ Form::select('job_cp_title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"job_cp_title")) }} 
                                        </div>
                                        <div class="col-xs-12 col-sm-8 form-child-block">
                                          {{ Form::text('job_cp_name',null,array('class'=>"form-control",'hint-class'=>"fname_s",'id'=>"job_cp_name",'placeholder'=>"Enter Contact Person Name")) }}
                                        </div>
                                      </div>                  
                                    </div>
                                  
                                  </div>
                                  <div class="form-group row manage_sec_2 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Number</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="form-row">
                                        <div class="col-xs-12 col-sm-4 form-child-block">
                                          {{ Form::select('job_dial_number',$country_codes,'Male',array('class'=>"form-control",'id'=>"contact_code")) }} 
                                        </div>
                                        <div class="col-xs-12 col-sm-8 form-child-block">
                                          {{ Form::text('job_cp_mobile',null,array('class'=>"form-control",'id'=>"contact_number",'placeholder'=>"Enter Contact Person Number")) }}
                                        </div>
                                      </div>                  
                                    </div>
                                  </div>

                                  <div class="form-group row manage_sec_3 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Address</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                       {{ Form::textarea('walkin_address',null,array('class'=>"form-control",'id'=>"walkin_address",'placeholder'=>"Enter WalkIn Address")) }}
                                    </div>
                                  </div>
                                  <div class="form-group row manage_sec_3 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">WalkIn Date</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="form-row">
                                        <div class="col-xs-12 col-sm-6 form-child-block">
                                          {{ Form::text('walkin_date_from',null,array('class'=>"form-control","id"=>"wdf","readonly"=>true,'placeholder'=>'Select Date','autocomplete'=>'off')) }}
                                          <span id="errorWalkinDateFrom"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 form-child-block">
                                          {{ Form::text('walkin_date_to',null,array('class'=>"form-control","id"=>"wdt","readonly"=>true,'placeholder'=>'Select Date','autocomplete'=>'off')) }}
                                          <span id="errorWalkinDateTo"></span>
                                        </div>
                                      </div>                  
                                    </div>
                                  </div>

                                  <div class="form-group row manage_sec_3 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">WalkIn Starting Time</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="form-row">
                                        <div class="col-xs-12 col-sm-6 form-child-block">
                                          {{ Form::select('walkin_start_time',getTimeRange('9:00', '19:30'),$walkinTime[0],array('class'=>"form-control","id"=>"wst",'placeholder'=>'Select Time')) }}
                                        </div>
                                        <div class="col-xs-12 col-sm-6 form-child-block">
                                          {{ Form::select('time_meridiem',['AM'=>'AM','PM'=>'PM'],$walkinTime[1],array('class'=>"form-control")) }}
                                          <span id="errorWalkinDateTo"></span>
                                        </div>
                                      </div>                  
                                    </div>
                                  </div>
                                </div>

                                
                            <div class="form-group row mt-20">
                              <div class="col-sm-9">
                                <button type="submit" class="newfclassub">Post Job</button>
                              </div>
                            </div>
                                  {{ Form::close() }}
                                  
                                </div>
                            </div>
                          </div>
                      
                   
                  </div>
  <!-- tab-content --> 
                </div>
<!-- tabs-widget -->              
            </div>
      </div>
    </div>
  </section>
</section>

<script>
  globalVar.qIds={};
  globalVar.qIds={{json_encode($qualifications,true)}};
  globalVar.countryIdComp=0;
  globalVar.countryId=0;
  globalVar.jobCountryId=0;
  globalVar.currCityElem='';
  var loc = {{$total_country}};
  var countryOptions = '';
  var selected_items = [];
  var data={!! json_encode($candidateCityElemIds) !!};
    var dataSelectedCity={!! json_encode($candidateCity) !!};
  $(document).ready(function(){
    $('#exp_year_from').change();
    $('#job_country').change();
    manageResponse('{{ $job_detail->response_by }}');
    populateSkills();

// populate candidate country and cities
if(data.length>0){
    $.each(data,function(index,val){
      initCityMultiselect(val);
    });
} else {
  initCityMultiselect('','c_city0');
}
 
    //***** start create country list select box *****//
        var array1 = {!! json_encode($gulfCountry) !!};
        var array2 = {!! json_encode($otherCountry) !!};
        
        //countryOptions+='<select name="candidate_current_country[]" class="form-control curr_loc" placeholder="Select Country">';
          countryOptions+='<option>Select Country</option>';
          countryOptions+='<optgroup label="Gulf Country">';
            $.each(array1,function(index,val){
                countryOptions+='<option value="'+index+'">'+val+'</option>';
            });
          countryOptions+='</optgroup>';

          countryOptions+='<optgroup label="Other Country">';
            $.each(array2,function(index,val){
                countryOptions+='<option value="'+index+'">'+val+'</option>';
            });
          countryOptions+='</optgroup>';
        //countryOptions+='</select>';
    //***** end create country list select box *****//

    //***** start daterange picker *****//
      var dateFormat = "dd-mm-yy",
        from = $( "#wdf" ).datepicker({ defaultDate: "+1w", dateFormat: "dd-mm-yy", changeMonth: true, numberOfMonths: 1, minDate:'0' })
          .on( "change", function() { to.datepicker( "option", "minDate", getDate( this ) ); }),
        to = $( "#wdt" ).datepicker({ defaultDate: "+1w", dateFormat: "dd-mm-yy", changeMonth: true, numberOfMonths: 1 })
          .on( "change", function() { from.datepicker( "option", "maxDate", getDate( this ) ); });
     
        function getDate( element ) {
          var date;
          try {
            date = $.datepicker.parseDate( dateFormat, element.value );
          } catch( error ) {
            date = null;
          }
          return date;
        }
    //***** end daterange picker *****//

  });


  $('#exp_year_from').change(function(){
    var max_exp={{$job_detail->max_experience}};
    var array = JSON.parse('{!! json_encode(getDigitRange(0,30,'year')) !!}');
    var value = parseInt($(this).val());
    $('#exp_year_to').find('option').not(':first').remove();
    $.each(array,function(index,val){
      if(value <= index){
        if(index==max_exp)
          $('#exp_year_to').append('<option value="'+index+'" selected  >'+val+'</option>'); 
        else
          $('#exp_year_to').append('<option value="'+index+'">'+val+'</option>');
      }
    });
  });

  $('#key_skills').selectize({
      plugins: ['remove_button'],
      maxItems: 15,
      valueField: 'id',
      labelField: 'name',
      searchField: 'name',
      create: false,
      options: {!! json_encode($skill_keywords) !!},
    
  });
 
  $('.res_manage').click(function(e){
    $('.manage_sec_1,.manage_sec_2,.manage_sec_3').hide();
    manageResponse($(this).val());
  });

  $('#qualifications').multiselect({
     texts: {
        placeholder    : 'Select Qualification',
      },
      onOptionClick : function( element, option ){
        globalVar.qIds=$(element).val();
        specializationByCourse();
      },
  });

  $('#specialization').multiselect({
     texts: {
        placeholder: 'Select Specialization',
      },
  });

  $('#nationality').multiselect({
     texts: {
        placeholder    : 'Select Nationality',
      },
  });

  $('#job_country').change(function(){
        var currCountryId={{$job_detail->job_in_country}};
        globalVar.jobCountryId = parseInt($(this).val());
        var selectize = $('#job_city')[0].selectize; 
        selectize.clear();
        selectize.clearOptions();        
         
        if(currCountryId==globalVar.jobCountryId){
          populateCity();
        }

  });

  $('#job_city').selectize({
      plugins: ['remove_button'],
      maxItems: 5,
      valueField: 'id',
      labelField: 'name',
      searchField: 'name',
      create: false,
      closeAfterSelect: false,
     options: [{value: 3, text: 'Herat'}],
      render: {
          option: function(item, escape) {           
              return '<div>' +escape(item.name)+'</div>';
          }
      },
      load: function(query, callback) {
          console.log(globalVar.jobCountryId);
           $.ajax({
                url: base_url+'/api/city-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    country_id: globalVar.jobCountryId,
                    q: query
                },           
                success: function(res) {
                  callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
      }
  });

  
  $('.add_more').on('click',function(){
      var elem = $(this).siblings('div').children('.curr_city').attr('class');
      addMoreLocation();
  });

  $(document).on('click','.rmv',function(){
    $('#'+$(this).data('id')).remove();
  });

  //***** start default selected Qualifications *****//
    $("#qualifications").val({{json_encode($qualifications,true)}});
    $("#qualifications").multiselect("reload");
    specializationByCourse(1);
  //***** end default selected Qualifications *****//

  //***** start default selected Nationality *****//
    $("#nationality").val({{json_encode($nationality,true)}});
    $("#nationality").multiselect("reload");
    specializationByCourse(1);
  //***** end default selected Nationality *****//


  function specializationByCourse(flag=0){
    $.ajax({
        url: base_url+'api/specialization-list',
        type: 'POST',
        dataType: 'json',
        data: {
            course_ids: globalVar.qIds,
        },           
        success: function(res) {
            $('#specialization').find('option').remove();
            $('#specialization').find('optgroup').remove();
              if(res){
                var str='';
                  $.each(res,function(index,val){
                    if(val.specialization.length>0){
                      str+='<optgroup label="'+val.name+'">';
                        $.each(val.specialization,function(index2,val2){
                          str+='<option value="'+val2.id+'~'+val.id+'">'+val2.name+'</option>';
                        });
                      str+='</optgroup>';
                    }
                  });
                  $('#specialization').append(str);
                  $('#specialization').multiselect( 'reload' );

                  // default selected Specialization Start
                  //if(flag==1){
                    $("#specialization").val({!!json_encode($specialization,true)!!});
                    $("#specialization").multiselect("reload");
                  //}
                  // default selected Specialization End
            }
        },
        error: function(error) { }
    });
  }

  function addMoreLocation(){
      loc=loc+1;
      if(loc>10){
        alert('Allow Only 10 Current Location of the Candidate.');
      } else {
       var id ='c_city'+loc;
        var str=' <div class="form-group row" id="d_'+loc+'">\
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"></label>\
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">\
                    <select name="candidate_current_country['+loc+']" class="form-control curr_loc" placeholder="Select Country">\
                     '+countryOptions+'\
                     </select>\
                    </div>\
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">\
                      <select name="candidate_current_city['+loc+'][]" id="'+id+'" class="form-control curr_city" multiple></select>\
                    </div>\
                    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 rmv" data-id="d_'+loc+'"><i class="fa fa-minus fa-icon-red" aria-hidden="true"></i></div>\
                  </div>';
          $('.current_location_section').append(str);
          initCityMultiselect('',id);
      }
  }

   function initCityMultiselect(val,elem=null){
      if(elem==null){
          $('#'+val.elem_id).multiselect({
           texts: {
              placeholder : 'Select City',
            }
          });
          $('#'+val.elem_id).val(dataSelectedCity[val.country_id]);
          $('#'+val.elem_id).multiselect("reload");
      } else {
           $('#'+elem).multiselect({
         texts: { placeholder : 'Select City' }
        });
      }

  }
  function populateCity(){
    var city_data={!! json_encode($jobsCity) !!};
    selected_items=[];
    $.each(city_data,function(index,item){
      selected_items.push(item.id);
    });
    var selectize = $("#job_city")[0].selectize; 
    selectize.clear();
    selectize.clearOptions();
    selectize.renderCache['option'] = {};
    
    selectize.addOption(city_data);
    selectize.setValue(selected_items);      
  }

  function populateSkills(){
    var data={!! json_encode($skills) !!};
    selected_items=[];
    $.each(data,function(index,item){
      selected_items.push(item.id);
    });
    var selectize = $("#key_skills")[0].selectize; 

    selectize.addOption(data);
    selectize.setValue(selected_items);
      
  }

  function manageResponse(val){
    switch(val){
      case "Email":
        $('.manage_sec_1').show();
        break;
      case "ContactDetails":
        $('.manage_sec_2').show();
        break;
      case "Walkin":
        $('.manage_sec_3').show();
        break;
      case "All":
        $('.manage_sec_1,.manage_sec_2,.manage_sec_3').show();
        break;
    }
  }



$(document).on('change','.curr_loc',function(){
     var elem_id = $(this).parents('div').siblings('div').children('.curr_city').attr('id');
      cityListByCountry($(this).val(),'#'+elem_id);
     
});
function cityListByCountry(country_id=0,elem_id){
     $.ajax({
          url: base_url+'/api/city-suggestion',
          type: 'GET',
          dataType: 'json',
          data: {
              by_country: 1,
              country_id: country_id,
          },           
          success: function(res) {
               $(elem_id).find('option').remove();
               if(res){
                var str='';
                $.each(res,function(index,val){
                  str+='<option value="'+val.id+'">'+val.name+'</option>';
                });
                $(elem_id).append(str);
                $(elem_id).multiselect( 'reload' );
                $(elem_id).val(dataSelectedCity[country_id]);
                $(elem_id).multiselect("reload");
            }
          },
          error: function(error) { }
      });
  }
</script>
@endsection