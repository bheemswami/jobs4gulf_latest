
@extends('/layouts/front_panel_master')
@section('content')

<section id="inner-banner" class="dashboard-banner" style="background: url({{url('public/images/inner-banner.jpg')}}) no-repeat center top;">
<div class="overlay"></div>

</section>

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
         @include('front_panel.includes.employer_dashboard_sidebar')
        <div class="col-sm-9">
          <div class="tabs widget profile-area">
            <div class="tab-content">
              <h4>Applied Jobs</h4>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Job Title</th>
                    <th>Total Vacancy</th>
                    <th>Experience</th>
                    <th>Salary</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($jobs as $k=>$val)
                  <tr>
                    <td>{{++$k}}.</td>
                    <td>{{$val->job_title}}</td>
                    <td>{{$val->total_vacancy}}</td>
                    <td>{{$val->min_experience}}-{{$val->max_experience}} Year</td>
                    <td>{{$val->salary_min}}-{{$val->salary_max}} {{$val->currency}}</td>
                    <td>
                      <a href="{{route('job-edit',['id'=>$val->id])}}" class="btn btn-info btn-xs">Edit</a> || 
                      <a href="{{route('job-details',['jobid'=>$val->id])}}" class="btn btn-info btn-xs">Detail</a>
                    </td>
                  </tr>
                 @empty
                 @endforelse
                </tbody>
              </table>
             
            </div>
            <!-- tab-content --> 
          </div>
          <!-- tabs-widget -->              
        </div>
      </div>
    </div>
  </section>
</section>

@endsection