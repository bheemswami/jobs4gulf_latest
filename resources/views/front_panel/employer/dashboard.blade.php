
@extends('/layouts/front_panel_master')
@section('content')

@include('front_panel.includes.employer_page_banner')

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
{{-- {{dd($user)}} --}}
      @if($user->email_verified==0)
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
           <a href="{{ route('send-verify-mail') }}">Verify</a> your email to increase your visibility and search-ability.
        </div>
      @endif
     {{--  @if($user->mobile_verified==0)
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
           Verify your mobile number. increase your visibility and searchability. Click : <a href="">Verify Number</a>
        </div>
      @endif --}}
      
      <div class="row">       
        @include('front_panel.includes.employer_dashboard_sidebar')   
              
        <div class="col-sm-9">
          <div class="tabs widget profile-area">
            <div class="tab-content">
              <div id="profile-tab" class="tab-pane active">
                <div class="pd-20"> 
                  <div class="row">                     
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
                        <div class="form-title">
                          <h4 class="contactinfo">Active Job Plan{{ ($active_plan!=null) ? ' : '.$active_plan->job_plan->name : '' }}</h4>
                        </div>
                        @if($active_plan!=null)     
                          @php
                              $basic_jobs = getClassName($active_plan->remaining_basic_jobs, $active_plan->total_basic_jobs);
                              $premium_jobs = getClassName($active_plan->remaining_premium_jobs, $active_plan->total_premium_jobs);
                              $plan_validity = getClassName(dateDiff('',$active_plan->plan_end_date)->days, $active_plan->plan_validity_days);
                          @endphp

                          <div class="col-sm-12 mb-20">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-3 controls">
                                <span class=" btn-sm btn-success">Plan Validity : {{ $active_plan->plan_validity_days }} Days</span>
                              </div>
                              <div class="col-xs-3 controls">
                                <span class=" btn-sm btn-danger">Remaining : {{ dateDiff('',$active_plan->plan_end_date)->days }} Days</span>
                              </div>
                            
                              <div class="col-xs-3 controls">
                                <span class=" btn-sm btn-info">Start Date : {{ dateConvert($active_plan->plan_start_date,'d M Y') }}</span>
                              </div>
                              <div class="col-xs-3 controls">
                                <span class=" btn-sm btn-danger">End Date : {{ dateConvert($active_plan->plan_end_date,'d M Y') }}</span>
                              </div>
                              </div>
                          </div>

                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-3">                             
                                <span class=" btn-sm btn-info">Total Basic Jobs : {{ $active_plan->total_basic_jobs }}</span>
                              </div>

                              <div class="col-xs-9">
                                <div class="progress">
                                  <div class="progress-bar {{ $basic_jobs['class_name'] }}" role="progressbar" style="width:{{ $basic_jobs['persent'] }}%">
                                    {{ $active_plan->remaining_basic_jobs }} Remaining Basic Jobs
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-3">
                               <span class=" btn-sm btn-info">Total Premium Jobs : {{ $active_plan->total_premium_jobs }}</span>
                              </div>

                              <div class="col-xs-9">
                                <div class="progress">
                                  <div class="progress-bar {{ $premium_jobs['class_name'] }}" role="progressbar" style="width:{{ $premium_jobs['persent'] }}%">
                                    {{ $active_plan->remaining_premium_jobs }} Remaining Premium Jobs
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        @else
                           <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <div class="col-xs-12 text-center">
                                <span class=" btn-sm btn-danger">You don't have any active plan.<a href="{{ route('job-plans') }}" target="_blank">Click here</a> to purchase.</span>
                              </div>
                            </div>
                          </div>
                        @endif
                      </div>
                  </div>
            </div>
            <!-- tab-content --> 
          </div>
          <!-- tabs-widget -->              
        </div>
      </div>
    </div>
  </section>
</section>
@php
function getClassName($val_1,$val_2){
    $persentage = ($val_2>0) ? ($val_1/$val_2)*100 : 0;
        if($persentage>=70){
            $class_name = 'progress-bar-success';
        } else if($persentage>=20){
            $class_name = 'progress-bar-warning';
        } else {
            $class_name = 'progress-bar-danger';
        }
    return ['class_name'=>$class_name,'persent'=>$persentage];
}
@endphp
@endsection