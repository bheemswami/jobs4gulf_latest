@extends('/layouts/front_panel_master')
@section('content')
<style>
  body{
    background-color: white !important;
  }
.padding14px {
    height: auto;
}
.cardBox3:hover {
    border-top: none;
    background-color: white; 
}
.label {
     padding: none;
     font-size: 16px;
    font-weight: 700;
     color: #000;
     text-align: left;
}
.ms-options-wrap > .ms-options > ul label{
    font-weight: 400 !important;
    font-size: 14px !important;
}
</style>
@php 
    $gulfCountry=getGulfCountries();
    $otherCountry=getOtherCountries();
@endphp
 {{ Form::model(session()->get('tmpPostJobData'),array('url'=>route('save-free-job'),'class'=>'dasboard-form','id'=>'post-job-free-form','files'=>true))}}
<!-- <div class="marginSearchHeader">
    <div class="row padding14px">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-7 postAJobHeader">
                <a href="javascript:;">Dashboard</a>
                <a  href="javascript:;">Slider Image</a>
                <a  href="javascript:;">Purchased plans</a>
                <a  href="javascript:;">Jobs</a>
                <a  href="javascript:;">Profile</a>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-3">
            <i class="fa fa-search imgSearch" aria-hidden="true"></i>
            <input type="text" placeholder="Search Candidate" class="postAJobBtn">
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <div class="input-group add-on">
                <div class="input-group-btn">
                    <button class="btn btn-success success-btn searchButton postAJobBtn newfclassub" type="submit">Post a job</button>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="jobseeker-banner" >  
    <div class="centered">Post A Job<br>
          <span>Let The Top Employers From Gulf Reach You,Rgister On Job4gulf Now!</span>
        </div>
      </div>
<div class="container mt25">
    <!-- <div class="col-lg-12">
        <div class="md-stepper-horizontal orange">
            <div class="md-step active">
                <div class="md-step-circle"><span></span></div>
                <div class="md-step-title">Job Detail</div>
                <div class="md-step-bar-left"></div>
                <div class="md-step-bar-right"></div>
            </div>
            <div class="md-step active">
                <div class="md-step-circle"><span></span></div>
                <div class="md-step-title">Job Classification</div>
                <div class="md-step-bar-left"></div>
                <div class="md-step-bar-right"></div>
            </div>
            <div class="md-step ">
                <div class="md-step-circle"><span></span></div>
                <div class="md-step-title">Eligiblity Criteria</div>
                <div class="md-step-bar-left"></div>
                <div class="md-step-bar-right"></div>
            </div>
            <div class="md-step">
                <div class="md-step-circle"><span></span></div>
                <div class="md-step-title">Manage Response</div>
                <div class="md-step-bar-left"></div>
                <div class="md-step-bar-right"></div>
            </div>
        </div>

    </div> -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Job Detail
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                {{-- <div class="form-group marginT20">
                    <label class="inputHeading">Job Type</label>
                    <select class="form-control resp jobTypeSelect"  required>
                        <option value="" class="placeholder" hidden>Select Job Type</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>
 --}}
                <div class="form-group marginT20">
                    <label class="inputHeading">Job Title</label>
                    {{-- <input type="text" placeholder="Job Title" class="form-control registerHereSubTextInput" id="passwordcnf"> --}}
                    {{ Form::text('job_title',null,array('class'=>"form-control registerHereSubTextInput",'id'=>"job_title ",'placeholder'=>"Job Title")) }} 
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading">Job Description</label>
                    {{ Form::textarea('job_description',null,array('class'=>"form-control",'id'=>"job_description",'placeholder'=>"Enter Job Description","rows"=>"5")) }}
                    {{-- <textarea class="form-control" placeholder="Enter Job Description" rows="5"></textarea> --}}
                </div>

                <div class="form-group marginT20">
                    <div class="row noPadding nomrgn">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddingryt5">
                            <label class="inputHeading">Country</label>
                            {{ Form::select('job_in_country',
                            ['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,
                        array('class'=>"form-control resp jobTypeSelect",'id'=>"job_country",'placeholder'=>"Select Country")) }}
                    
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">City</label>
                            {{ Form::text('job_in_city',null,array('class'=>"form-control resp jobTypeSelect",'id'=>"job_city",'autocompletion'=>'off','placeholder'=>"Enter City")) }} 
                         <span id="errorJobInCity"></span>  
                        </div>
                    </div>
                </div>

                <div class="form-group marginT20">
                    <div class="row noPadding nomrgn">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddingryt5">
                            <label class="inputHeading">Monthly Salary</label>
                            {{ Form::select('salary_min',config('constants.min_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_min",'placeholder'=>"in US$")) }}
                             <span class="fieldlabel">Minimum</span> 
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">&nbsp;</label>
                           {{ Form::select('salary_max',config('constants.max_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_max",'placeholder'=>"in US$")) }}
                         <span class="fieldlabel">Maximum</span>                 
                        </div>
                    </div>
                </div>
                 <div class="form-group marginT20 mrgnBtm50">
                    <lable class="checkbox-btn btn2">{{Form::checkbox('show_salary', 1, true)}}Do not Display the Salary range to Job Seekers</lable>
                </div>

                <div class="form-group marginT20 mrgnBtm50">
                    <label class="inputHeading">No. of Vacancies</label>
                    {{ Form::number('total_vacancy',null,array('class'=>"form-control registerHereSubTextInput",'id'=>"total_vacancy",'placeholder'=>"Enter Vacancy number")) }} 
                    {{-- <input type="nunber" placeholder="Enter Vacancy number" class="form-control registerHereSubTextInput"> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Job Classification
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                <div class="form-group marginT20">
                    <label class="inputHeading">Industry Type</label>
                    {{ Form::select('industry_id',$industry_list,null,array('class'=>"form-control",'id'=>"job_industry",'placeholder'=>"Select Industry")) }}
                    {{-- <select class="form-control" name="industry_id"><option selected="selected" value="">Select Industry</option><option value="2">Accounting</option><option value="3">Advertising</option><option value="4">Agriculture / Dairy / Fishing</option><option value="5">Architecture / Interior Design</option><option value="10">Event Management</option><option value="1">Fresh Graduate</option><option value="7">General merchandisers</option><option value="6">Oil and gas</option><option value="9">Petroleum refining</option><option value="8">Utilities</option></select> --}}
                </div>
                <div class="form-group marginT20">
                    <label class="inputHeading">Functional Area</label>
                    {{ Form::select('functional_area_id',$functional_area,null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select Functional Area")) }}
                    {{-- <select class="form-control" name="functional_area_id"><option selected="selected" value="">Select Functional Area</option><option value="2">Accounting/Auditing/Tax/Financial Services</option><option value="1">Freshers/Trainee</option><option value="3">IT- Software</option><option value="4">Packaging /Delivery/ Logistics Operations</option><option value="5">Photography/TV / films / Radio12</option></select> --}}
                </div>

                <div class="form-group marginT20 mrgnBtm50">
                    <label class="inputHeading">Keywords</label>
                    {{ Form::textarea('keywords',null,array('class'=>"form-control registerHereSubTextInput",'id'=>"key_skills",'placeholder'=>"Enter important keywords that can describe this job")) }}
                    <span id="errorKeywords"></span>
                    {{-- <input type="text" placeholder="Enter important keywords that can describe this job" class="form-control registerHereSubTextInput"> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Eligiblity Criteria
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                <div class="form-group marginT20">
                    <label class="inputHeading">Select Qualification</label>
                    {{ Form::select('quilification[]',$educations,null,array('class'=>"form-control multiselect-ui",'id'=>"qualifications",'multiple'=>"multiple")) }}
                    {{-- <select class="form-control multiselect-ui" id="qualifications" multiple="multiple" name="quilification[]"><optgroup label="Bachelors"><option value="1">Bachelor of Architecture</option><option value="2">Bachelor of Arts</option><option value="3">Bachelor of Business Administration</option><option value="4">Bachelor of Commerce</option><option value="5">Bachelor of Dental Surgery</option><option value="6">Bachelor of Education</option><option value="7">Bachelor of Hotel Management</option><option value="8">Bachelor of Laws (LLB)</option><option value="9">Bachelor of Pharmacy</option><option value="10">Bachelor of Science</option><option value="11">Bachelor of Technology/Engineering</option><option value="12">Bachelor of Veterinary Science</option><option value="13">Bachelors in Computer Application</option><option value="18">Chartered Accountant</option><option value="19">CA Inter</option><option value="20">Chartered Financial Analyst</option></optgroup><optgroup label="Diploma"><option value="15">Diploma</option></optgroup><optgroup label="Doctorate"><option value="22">Doctor of Medicine (MD)</option><option value="23">Doctor of Surgery (MS)</option></optgroup><optgroup label="Master Education"><option value="14">MBBS</option><option value="26">Master of Architecture</option><option value="27">Master of Arts</option><option value="28">Master of Commerce</option><option value="29">Master of Education</option><option value="30">Master of Laws (LLM)</option><option value="31">Master of Pharmacy</option><option value="32">Master of Science</option><option value="33">Master of Technology/Engineering</option><option value="34">Master of Veterinary Science</option><option value="35">Masters in Computer Application</option><option value="36">MBA/PG Diploma in Business Mgmt</option></optgroup><optgroup label="School"><option value="16">Intermediate School</option><option value="17">Secondary School</option></optgroup></select> --}}
                </div>
                <div class="form-group marginT20">
                    <label class="inputHeading">Specialization</label>
                    {{ Form::select('specialization[]',[],null,array('class'=>"form-control",'id'=>"specialization",'multiple'=>"multiple")) }}
                    {{-- <select class="form-control" id="functional_area" name="functional_area_id"><option selected="selected" value="">Select Functional Area</option><option value="2">Accounting/Auditing/Tax/Financial Services</option><option value="1">Freshers/Trainee</option><option value="3">IT- Software</option><option value="4">Packaging /Delivery/ Logistics Operations</option><option value="5">Photography/TV / films / Radio12</option></select> --}}
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading">Gender</label>
                    <div class="genderCheckBoxes">
                        <span for="male">{{ Form::radio('gender','Male',false,array('class'=>"",'id'=>"male")) }}Male</span>
                        <span for="female">{{ Form::radio('gender','Female',false,array('class'=>"",'id'=>"female")) }}Female</span>
                        <span for="anyone">{{ Form::radio('gender','No Preference',false,array('class'=>"",'id'=>"anyone")) }}No Preference</span>
                        <span id="errorGender"></span>
                    </div>
                </div>
    
                <div class="form-group marginT20">
                    <div class="row noPadding nomrgn">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddingryt5">
                            <label class="inputHeading">Work Experience</label>
                            {{ Form::select('min_experience',getDigitRange(0,30,'year'),null,array('class'=>"form-control",'id'=>"exp_year_from",'placeholder'=>"in Year")) }}                 
                        <span class="fieldlabel">Minimum</span>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">&nbsp;</label>
                            {{ Form::select('max_experience',[],null,array('class'=>"form-control",'id'=>"exp_year_to",'placeholder'=>"in Year")) }}                 
                            <span class="fieldlabel">Maximum</span>             
                        </div>
                    </div>
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading">Nationality</label>
                    {{ Form::select('nationality_id[]',config('constants.nationality'),null,array('class'=>"form-control multiselect-ui",'id'=>"nationality",'multiple'=>"multiple")) }}
                    {{-- <select class="form-control multiselect-ui" id="nationality" multiple="multiple" name="nationality_id[]"><optgroup label="Top"><option value="13">Bahraini</option><option value="54">Egyptian</option><option value="55">Emirati (UAE)</option><option value="91">Jordanian</option><option value="95">Kuwaiti</option><option value="99">Lebanese</option><option value="134">Omani</option><option value="136">Palestinian</option><option value="143">Qatari</option><option value="152">Saudi Arabian</option><option value="172">Syrian</option><option value="180">Tunisian</option></optgroup><optgroup label="All"><option value="0">Afghan</option><option value="1">Albanian</option><option value="2">Algerian</option><option value="3">American</option><option value="4">Andorran</option><option value="5">Angolan</option><option value="6">Antiguans</option><option value="7">Argentinean</option><option value="8">Armenian</option><option value="9">Australian</option><option value="10">Austrian</option><option value="11">Azerbaijani</option><option value="12">Bahamian</option><option value="14">Bangladeshi</option><option value="15">Barbadian</option><option value="16">Barbudans</option><option value="17">Batswana</option><option value="18">Belarusian</option><option value="19">Belgian</option><option value="20">Belizean</option><option value="21">Beninese</option><option value="22">Bhutanese</option><option value="23">Bolivian</option><option value="24">Bosnian</option><option value="25">Brazilian</option><option value="26">British</option><option value="27">Bruneian</option><option value="28">Bulgarian</option><option value="29">Burkinabe</option><option value="30">Burmese</option><option value="31">Burundian</option><option value="32">Cambodian</option><option value="33">Cameroonian</option><option value="34">Canadian</option><option value="35">Cape Verdean</option><option value="36">Central African</option><option value="37">Chadian</option><option value="38">Chilean</option><option value="39">Chinese</option><option value="40">Colombian</option><option value="41">Comoran</option><option value="42">Congolese</option><option value="43">Costa Rican</option><option value="44">Croatian</option><option value="45">Cuban</option><option value="46">Cypriot</option><option value="47">Czech</option><option value="48">Danish</option><option value="49">Djibouti</option><option value="50">Dominican</option><option value="51">Dutch</option><option value="52">East Timorese</option><option value="53">Ecuadorean</option><option value="56">Equatorial Guinean</option><option value="57">Eritrean</option><option value="58">Estonian</option><option value="59">Ethiopian</option><option value="60">Fijian</option><option value="61">Filipino</option><option value="62">Finnish</option><option value="63">French</option><option value="64">Gabonese</option><option value="65">Gambian</option><option value="66">Georgian</option><option value="67">German</option><option value="68">Ghanaian</option><option value="69">Greek</option><option value="70">Grenadian</option><option value="71">Guatemalan</option><option value="72">Guinea-Bissauan</option><option value="73">Guinean</option><option value="74">Guyanese</option><option value="75">Haitian</option><option value="76">Herzegovinian</option><option value="77">Honduran</option><option value="78">Hungarian</option><option value="79">I-Kiribati</option><option value="80">Icelander</option><option value="81">Indian</option><option value="82">Indonesian</option><option value="83">Iranian</option><option value="84">Iraqi</option><option value="85">Irish</option><option value="86">Israeli</option><option value="87">Italian</option><option value="88">Ivorian</option><option value="89">Jamaican</option><option value="90">Japanese</option><option value="92">Kazakhstani</option><option value="93">Kenyan</option><option value="94">Kittian and Nevisian</option><option value="96">Kyrgyz</option><option value="97">Laotian</option><option value="98">Latvian</option><option value="100">Liberian</option><option value="101">Libyan</option><option value="102">Liechtensteiner</option><option value="103">Lithuanian</option><option value="104">Luxembourger</option><option value="105">Macedonian</option><option value="106">Malagasy</option><option value="107">Malawian</option><option value="108">Malaysian</option><option value="109">Maldivan</option><option value="110">Malian</option><option value="111">Maltese</option><option value="112">Marshallese</option><option value="113">Mauritanian</option><option value="114">Mauritian</option><option value="115">Mexican</option><option value="116">Micronesian</option><option value="117">Moldovan</option><option value="118">Monacan</option><option value="119">Mongolian</option><option value="120">Moroccan</option><option value="121">Mosotho</option><option value="122">Motswana</option><option value="123">Mozambican</option><option value="124">Namibian</option><option value="125">Nauruan</option><option value="126">Nepalese</option><option value="127">New Zealander</option><option value="128">Nicaraguan</option><option value="129">Nigerian</option><option value="130">Nigerien</option><option value="131">North Korean</option><option value="132">Northern Irish</option><option value="133">Norwegian</option><option value="135">Pakistani</option><option value="137">Panamanian</option><option value="138">Papua New Guinean</option><option value="139">Paraguayan</option><option value="140">Peruvian</option><option value="141">Polish</option><option value="142">Portuguese</option><option value="144">Romanian</option><option value="145">Russian</option><option value="146">Rwandan</option><option value="147">Saint Lucian</option><option value="148">Salvadoran</option><option value="149">Samoan</option><option value="150">San Marinese</option><option value="151">Sao Tomean</option><option value="153">Scottish</option><option value="154">Senegalese</option><option value="155">Serbian</option><option value="156">Seychellois</option><option value="157">Sierra Leonean</option><option value="158">Singaporean</option><option value="159">Slovakian</option><option value="160">Slovenian</option><option value="161">Solomon Islander</option><option value="162">Somali</option><option value="163">South African</option><option value="164">South Korean</option><option value="165">Spanish</option><option value="166">Sri Lankan</option><option value="167">Sudanese</option><option value="168">Surinamer</option><option value="169">Swazi</option><option value="170">Swedish</option><option value="171">Swiss</option><option value="173">Taiwanese</option><option value="174">Tajik</option><option value="175">Tanzanian</option><option value="176">Thai</option><option value="177">Togolese</option><option value="178">Tongan</option><option value="179">Trinidadian/Tobagonian</option><option value="181">Turkish</option><option value="182">Tuvaluan</option><option value="183">Ugandan</option><option value="184">Ukrainian</option><option value="185">Uruguayan</option><option value="186">Uzbekistani</option><option value="187">Venezuelan</option><option value="188">Vietnamese</option><option value="189">Welsh</option><option value="190">Yemenite</option><option value="191">Zambian</option><option value="192">Zimbabwean</option></optgroup></select> --}}
                </div>

                <div class="form-group marginT20 mrgnBtm50">
                    <div class="row noPadding nomrgn current_location_section">
                        <div>
                            <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5 paddingryt5">
                                <label class="inputHeading">Current Location</label>
                                {{ Form::select('candidate_current_country[0]',['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,array('class'=>"form-control resp curr_loc jobTypeSelect",'placeholder'=>"Select Country")) }} 
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5 paddinglft5">
                                <label class="inputHeading ">&nbsp;</label>
                                {{ Form::select('candidate_current_city[0][]',[],null,array('class'=>"form-control resp curr_city jobTypeSelect",'id'=>'c_city0','multiple'=>"multiple")) }} 
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 add_more"><i class="fa fa-plus fa-icon-green" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Manage Response
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                <div class="form-group marginT20">
                    <label class="inputHeading">Seeker will Respond you at</label>
                    <div class="genderCheckBoxes">
                         <span for="responce_1">{{ Form::radio('response_by','Email',false,array('class'=>"res_manage",'id'=>'responce_1')) }}Email</span>
                         <span for="responce_2">{{ Form::radio('response_by','ContactDetails',false,array('class'=>"res_manage",'id'=>'responce_2')) }}Contact Details</span>
                         <span for="responce_3">{{ Form::radio('response_by','Walkin',false,array('class'=>"res_manage",'id'=>'responce_3')) }}WalkIn</span>
                         <span for="responce_4">{{ Form::radio('response_by','All',false,array('class'=>"res_manage",'id'=>'responce_4')) }}All</span>
                    </div>
                </div>

                <div class="form-group marginT20 manage_sec_1 hide-elem">
                   <label class="inputHeading">Mailing E-Mail ID</label>
                   {{ Form::text('response_email',null,array('class'=>"form-control",'id'=>"mailing_email",'placeholder'=>"Mailing E-Mail ID (Comma seperated for multiple)")) }}                 
                </div>
                <div class="form-group marginT20 manage_sec_2 hide-elem">
                    <div class="row noPadding nomrgn">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading required-field">Contact Person</label>
                          {{ Form::select('job_cp_title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"job_cp_title")) }} 
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading required-field">&nbsp;</label>
                          {{ Form::text('job_cp_name',null,array('class'=>"form-control",'hint-class'=>"fname_s",'id'=>"job_cp_name",'placeholder'=>"Enter Contact Person Name")) }}
                        </div>
                    </div>            
                </div>

                <div class="form-group marginT20 manage_sec_2 hide-elem">
                    <div class="row noPadding nomrgn">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading required-field">Contact Number</label>
                            {{ Form::select('job_dial_number',$country_codes,'Male',array('class'=>"form-control",'id'=>"contact_code")) }} 
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading required-field">&nbsp;</label>
                            {{ Form::text('job_cp_mobile',null,array('class'=>"form-control",'id'=>"contact_number",'placeholder'=>"Enter Contact Person Number")) }}
                        </div>
                    </div>            
                </div>

                <div class="form-group marginT20 manage_sec_3 hide-elem">
                    <label class="inputHeading required-field">Address</label>
                    {{ Form::textarea('walkin_address',null,array('class'=>"form-control",'id'=>"walkin_address",'placeholder'=>"Enter WalkIn Address")) }}
                </div>
                <div class="form-group row manage_sec_3 hide-elem">                    
                    <div class="row noPadding nomrgn">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading required-field">WalkIn Date</label>
                            {{ Form::text('walkin_date_from',null,array('class'=>"form-control","id"=>"wdf","readonly"=>true,'placeholder'=>'Select Date','autocomplete'=>'off')) }}
                          <span id="errorWalkinDateFrom"></span>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">&nbsp</label>
                            {{ Form::text('walkin_date_to',null,array('class'=>"form-control","id"=>"wdt","readonly"=>true,'placeholder'=>'Select Date','autocomplete'=>'off')) }}
                            <span id="errorWalkinDateTo"></span>
                        </div>
                    </div>                  
                </div>

                <div class="form-group row manage_sec_3 hide-elem">
                    <div class="row noPadding nomrgn">
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading required-field">WalkIn Starting Time</label>
                            {{ Form::select('walkin_start_time',getTimeRange('9:00', '19:30'),null,array('class'=>"form-control","id"=>"wst",'placeholder'=>'Select Time')) }}
                        </div>
                       <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">&nbsp;</label>
                            {{ Form::select('time_meridiem',['AM'=>'AM','PM'=>'PM'],null,array('class'=>"form-control")) }}
                          <span id="errorWalkinDateTo"></span>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </div>

    <!-- Employer Reg Section Start -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Register Here
            </div>

             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                <div class="registerHereSubText">Account Information</div>
               
                <div class="form-group marginT20">
                    <label class="inputHeading">Registered Employer</label>
                    <div class="genderCheckBoxes">
                        <span for="">{{ Form::radio('emp_registered',0,true,array('class'=>"emp_registered_rbtn")) }}No</span>
                        <span for="">{{ Form::radio('emp_registered',1,false,array('class'=>"emp_registered_rbtn")) }}Yes</span>
                    </div>
                </div>

                <div class="form-group marginT20 emp_no_registered">
                    <label class="inputHeading required-field">Email ID</label>
                    {{ Form::email('user_email_unique',null,array('class'=>"form-control",'id'=>"user_email_unique",'placeholder'=>"Enter Company E-mail ID")) }}                
                </div>

                <div class="form-group marginT20 emp_yes_registered hide-elem">
                    <label class="inputHeading required-field">Email ID</label>
                    {{ Form::email('user_email',null,array('class'=>"form-control",'id'=>"emp_email",'placeholder'=>"Enter Company E-mail ID")) }}                
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading required-field">Password </label>
                    <div class="input-group pwd-grp">
                        {{ Form::password('user_password',array('class'=>"form-control pwd",'id'=>"emp_password",'placeholder'=>"Enter Password")) }}
                        <span class="pass-show input-group-btn"><button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button></span>
                    </div>
                </div>
                <div class="emp_no_registered">     
                    <div class="registerHereSubText">Contact Person Information</div>
                    <div class="form-group marginT20">
                        <div class="row noPadding nomrgn">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                                <label class="inputHeading required-field">Contact Person</label>
                                {{ Form::select('cp_title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"cp_title",'placeholder'=>"Select Title")) }}
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                                <label class="inputHeading">&nbsp;</label>
                                {{ Form::text('cp_name',null,array('class'=>"form-control",'id'=>"cp_name",'placeholder'=>"Enter Contact Person Name")) }}
                            </div>
                        </div>            
                    </div>

                    <div class="form-group marginT20">
                        <label class="inputHeading required-field">Designation</label>
                        {{ Form::text('cp_designation',null,array('class'=>"form-control hintable",'hint-class'=>"",'id'=>"designation",'placeholder'=>"Enter Designation")) }}
                        <span id="errorCpDesignation"></span>
                    </div>

                    <div class="form-group marginT20">
                        <div class="row noPadding nomrgn">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                                <label class="inputHeading required-field">Contact Number</label>
                                {{ Form::select('dial_code',$country_codes,null,array('class'=>"form-control",'id'=>"dial_code",'placeholder'=>"Select Code")) }} 
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                                <label class="inputHeading">&nbsp;</label>
                                {{ Form::text('cp_mobile',null,array('class'=>"form-control",'id'=>"cp_mobile",'placeholder'=>"Enter Contact Number")) }}
                            </div>
                        </div>
                    </div>
       
                   <div class="registerHereSubText">Company Information</div>
                    <div class="form-group marginT20">
                        <label class="inputHeading">Member Type</label>
                        <div class="genderCheckBoxes">
                            <span for="">{{ Form::radio('comp_type','1',false,array('class'=>"")) }}Consultant</span>
                            <span for="">{{ Form::radio('comp_type','2',false,array('class'=>"")) }}Employer</span>
                        </div>
                    </div>

                    <div class="form-group marginT20">
                        <label class="inputHeading required-field">Company Name</label>
                        {{ Form::text('comp_name',null,array('class'=>"form-control",'id'=>"company_name",'placeholder'=>"Enter Company Name")) }}
                    </div>

                    <div class="form-group marginT20">
                        <label class="inputHeading required-field">About Company</label>
                        {{ Form::textarea('comp_about',null,array('class'=>"form-control",'id'=>"company_about",'placeholder'=>"Enter About Company")) }}
                    </div>

                    <div class="form-group marginT20">
                        <label class="inputHeading">Company Website</label>
                        {{ Form::text('comp_website',null,array('class'=>"form-control",'id'=>"company_website",'placeholder'=>"Enter Company Website URL")) }}
                    </div>

                    <div class="form-group marginT20">
                        <label class="inputHeading required-field">Industry Type</label>
                        {{ Form::select('comp_industry',$industry_list,null,array('class'=>"form-control",'id'=>"industry_type",'placeholder'=>"Select Industry Type")) }} 
                    </div>

                    <div class="form-group row marginT20">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <img class="profileCompanyImg user-default" src="{{url('public/images/demoUser.png')}}">
                        </div>
                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                           <div class="profileDesc2">
                               <span class="profileSpan">Upload only Image (.png/.jpg/.jpeg) formats. </span>
                                <div class="profilePic2-button">
                                    {{ Form::file('comp_img',array('class'=>"upload file-input",'id'=>"comp_logo")) }}
                                 <label for="comp_logo"> Upload Your Logo </label>
                               </div>
                               <span id="errorCompImg"></span>
                           </div>
                       </div>
                   </div>

                   <div class="registerHereSubText">Mailing Address</div>
                    <div class="form-group marginT20">
                        <label class="inputHeading required-field">Country</label>
                         {{ Form::select('comp_country',
                          ['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,
                          array('class'=>"form-control",'id'=>"comp_country",'placeholder'=>"Select Country")) }}
                    </div>

                    <div class="form-group marginT20">
                        <label class="inputHeading required-field">City</label>
                         {{ Form::text('comp_city',null,array('class'=>"form-control",'id'=>"comp_city",'autocompletion'=>'off','placeholder'=>"Enter City")) }}
                         <span id="errorCompCity"></span>
                    </div>

                    <div class="form-group marginT20">
                        <label class="inputHeading required-field">Address</label>
                         {{ Form::textarea('comp_address',null,array('class'=>"form-control",'id'=>"company_address",'placeholder'=>"Enter Address")) }}  
                    </div>

                    <div class="form-group marginT20">
                        <label class="inputHeading">PO Box/ Zip Code</label>
                         {{ Form::text('zip_code',null,array('class'=>"form-control",'id'=>"zip_code",'placeholder'=>"Enter PO Box/ Zip Code")) }}
                    </div>
                </div>
           </div>
           <div class="col-md-3 col-lg-3"></div>
        </div>
    </div>
    <!-- Employer Reg Section End -->
</div>
<div class="registerHereSubText3 marginT60 text-center">
    <span class="submitResume2-button text-center">
        <button type="submit">Post Job</button>
    </span>
</div>
{{Form::close()}}
<script type="text/javascript">
$(function() {
    $('.multiselect-ui').multiselect({
        minHeight: 50,  
        maxHeight: 400,
        //includeSelectAllOption: true
    });
});

</script>
<script>
  globalVar.qIds={};
  globalVar.countryIdComp=0;
  globalVar.countryId=0;
  globalVar.jobCountryId=0;
  globalVar.currCityElem='';
 
</script>
@include('front_panel/includes/add_job_js_code')
@endsection
