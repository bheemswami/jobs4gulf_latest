@extends('/layouts/front_panel_master')
@section('content')
<style>
    body{
        background-color: #FFF !important;
    }
</style>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-content">
            <div class="height-padding"></div>
            <div class="head-text text-center">
                <h2>Find Your Dream job in Gulf</h2>
                <h2>Join Top Companies Currently Hiring</h2>
                <h3>Search through 1,15,000 Jobs</h3>
            </div>
            @php
                if(!isset($form_class)){
                    $form_class='';
                }
                $country = getAllCountries();
                foreach($country as $val){
                    $all_countries[$val->id]=$val->name;
                    if($val->is_gulf==1){
                        $gulf_countries[$val->id]=$val->name;
                    }
                }
            @endphp
            @php
                $searchData=getHomePageSearchList();
                $countryList=$searchData['country_list'];
                $industryList=$searchData['industry_list'];
                $functionList=$searchData['function_list'];
            @endphp
            <div class="form-box">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {{ Form::open(array('url'=>route('search-jobs'),'class'=>$form_class." form-inline",'method' =>'GET','id'=>'search-job'))}}
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 nopadding">
                            <div class="form-group">
                                <label for="skill">Enter Keywords</label>
                                {{ Form::text('key_skills',null,['id'=>'skill','class'=>'form-control type-it rounded searchInput job-title','placeholder'=>'Enter skills, designations or company name']) }}
                                {{-- <input type="text" class="form-control type-it rounded searchInput" id="skill" placeholder="Enter skills, designations or company name"> --}}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 nopadding">
                            <div class="form-group">
                                <label for="city">Location</label>
                                {{ Form::select('country_ids',['Gulf Country'=>$gulf_countries,'All Country'=>$all_countries],null,['id'=>'country','class'=>'form-control type-it rounded searchInput roundedNA','placeholder'=>'City or country']) }}
                                {{-- <input type="text" class="form-control type-it rounded searchInput roundedNA" id="country" placeholder="City or country"> --}}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 nopadding">
                            <div class="form-group">
                                <label for="year">Experience</label>
                                {{ Form::select('exp_year',getDigitRange(0,5),null,['id'=>'exp year','class'=>'form-control type-it rounded searchInput roundedNA','placeholder'=>'Exp (Years)']) }}
                                {{-- <input type="text" class="form-control type-it rounded searchInput roundedNA" id="year" placeholder="Years"> --}}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 nopadding">
                            <label class="searchBlank" for="search">&nbsp;</label><br class="searchBlank">
                            <button type="submit" id="search" class="btn btn-success success-btn">Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="form-box">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPad1024">
                        <div class="second-navbar">
                                <ul class="optionUl">
                                    <li class="active"><a href="#">Browse job</a></li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Functions<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        @php $counter=1; @endphp
                                        @foreach($functionList as $val)
                                            @if($counter<=6)
                                                <li class="second_dropdown"><a title="{{$val->name}}" href="{{url('search-jobs?function_list_ids='.$val->id)}}" >{{substr($val->name,0,20)}}</a></li>
                                            @endif
                                            @php $counter++; @endphp
                                        @endforeach
                                        <li class="second_dropdown"><a href="{{url('search-jobs')}}" >More</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Location<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        @php $counter=1; @endphp
                                        @foreach($countryList as $val)
                                            @if($counter<=6)
                                                <li class="second_dropdown"><a title="{{$val->name}}" href="{{url('search-jobs?country_ids='.$val->id)}}" >{{substr($val->name,0,20)}}</a></li>
                                            @endif
                                            @php $counter++; @endphp
                                        @endforeach
                                        <li class="second_dropdown"><a href="{{url('search-jobs')}}" >More</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Industry<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        @php $counter=1; @endphp
                                        @foreach($industryList as $val)
                                            @if($counter<=6)
                                                <li class="second_dropdown"><a title="{{$val->name}}" href="{{url('search-jobs?industry_ids='.$val->id)}}" >{{substr($val->name,0,20)}}</a></li>
                                            @endif
                                            @php $counter++; @endphp
                                        @endforeach
                                        <li class="second_dropdown"><a href="{{url('search-jobs')}}" >More</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{route('consultant')}}">Best Consultant Zone</a></li>
                                    <li> <a href="{{route('walkins')}}">Walk-In Zone</a></li>
                                </ul>
                            </div>
                </div>
            </div>
            <div class="form-box">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="header-button text-center">
                        <a href="{{route('candidate-register')}}">Upload CV! It's Free</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="paddingJobs">
                    <div class="page-heading discover">
                        <span onclick="javascript:;">
                            Discover Our Hiring Partner
                        </span>
                        <a href="{{route('top-employers')}}" class="pull-right">View All</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center mrgnBtm50px">
                        <div class="width80">
                            <div class="owl-carousel" id="premium">
                            @foreach($top_employers as $k=>$val)
                            <div class="item top-candidate">
                                <span>
                                    <img src="{{checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="sliderImg">
                                </span>
                                <h4 class="sliderHeader">{{ substr($val->comp_name,0,50) }} </h4>
                                <div class="job_location">
                                    {{(isset($val->city) ? $val->city->name."-" : '' )}} {{(isset($val->country)) ? $val->country->name : ''}}
                                </div>
                                @if($val->job_post!=null && count($val->job_post)>0)
                                    <a href="{{url('companies/'.$val->id)}}">{{ count($val->job_post) }} OPENING</a>
                                @else
                                    <a href="#" class="disableAnchor" >NO OPENING</a>
                                @endif
                                </div>  
                            @endforeach
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row noPadding noMrgn bckclrWhoru">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mrgnTpwhoru">
                <div class="col-md-6">
                    <div class="card text-center">
                        <img class="imgWhoru" class="card-img-top" src="{{url('public/images/Mask Group 2@2x.png')}}" class="img-responsive" alt="Card image cap">
                         <div class="card-body">
                            <h3 class="card-title">Are you a recruiter?</h3>
                            <div class="col-md-9 col-md-offset-3">
                            <ul>
                           <li>  Advertise your Jobs to get qualified candidates</li>
                             <li>  CVs direct to your inbox</li>
                              <li>  Get customized recruiters packages as per your needs</li>
                            
                            </ul>
                            </div>
                            <div class="upload_cv">
                                <a href="{{route('free-job-post')}}">Post a Job Free ! &nbsp;&nbsp;</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card text-center">
                        <img class="imgWhoru" class="card-img-top" src="{{url('public/images/Mask Group 3@2x.png')}}" class="img-responsive" alt="Card image cap">
                         <div class="card-body">
                            <h3 class="card-title">Are you job seeker?</h3>
                             <div class="col-md-9 col-md-offset-3">
                            <ul class="clr-grn">
                            <li>Upload your CV to get your next dream job in Gulf</li>
                           <li> Get relevant jobs in your inbox</li>
                            <li> Apply to over 5000 Jobs in Gulf.</li>
                         </ul>
                     </div>
                            <div class="post_new_job">
                            <a href="{{route('candidate-register')}}">Upload CV ! It's Free</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="paddingJobs">
                    <div class="page-heading discover">
                        <span>Premium Jobs</span>
                    </div>
                </div>
            </div>
        </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        @php 
                            $pJobs = getPremiumJobs();
                            $pJobIds=[];
                        @endphp
                        {{-- {{dd($pJobs)}} --}}
                        @foreach($pJobs as $k=>$val)
                            @php 
                            $pJobIds[]=$val->id;
                            @endphp
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left premium-job-pannel">
                                <a href="{{route('job-details',['jobid'=>$val->id])}}">
                                <div class="col-xs-12 col-md-3 text-center">
                                    @if($val->employer_info!=null)
                                        <img src="{{checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="premium-job-logo">
                                    @else 
                                        <img src="{{url('public/images/default/company_logo.png')}}" class="premium-job-logo">
                                    @endif
                                    {{-- <img src="{{url('public/images/social-1.png')}}" class="premium-job-logo"> --}}
                                </div>
                                <div class="col-xs-12 col-md-6 premium-job-details">
                                    <p class="premium-job-heading">{{ ($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt.Ltd' }}
                                        </p>
                                   <h4>{{$val->job_title}}</h4>
                                    
                                    <p class="location"><img src="{{url('public/images/location-icon.png')}}"> {{ getCityNames($val->jobs_in_city) }} - {{ ($val->country!=null) ? $val->country->name : '' }} </p>
                                </div>
                                <div class="col-xs-12 col-md-6 premium-job-details">
                                    @php $counter=1; @endphp
                                    @foreach($val->skills as $data)      
                                        @if(isset($data->skill_keywords) && $counter<4)          
                                            <button type="button" class="skill-btn"> {{$data->skill_keywords->name}}</button>
                                            @php $counter++; @endphp
                                        @endif
                                    @endforeach
                                    {{-- <button type="button" class="skill-btn">Design</button>
                                    <button type="button" class="skill-btn">UI/UX</button>
                                    <button type="button" class="skill-btn">Web Development</button> --}}
                                </div>
                                <div class="col-xs-12 col-md-3 pricing">
                                    @if($val->salary_min!=null)
                                    <p><img src="{{url('public/images/doller.png')}}"> 
                                        {{$val->currency}}{{$val->salary_min}}- {{$val->currency}}{{$val->salary_max}}
                                    </p>
                                    @else
                                        <h5 class="no-salary-text">As per market standard</h5>
                                    @endif
                                </div>
                                </a>
                            </div>
                        @endforeach
                        <div class="col-xs-12 col-lg-12 col-md-12 view-all text-center">
                            <a href="{{route('search-jobs')}}" class="">View all</a>
                        </div>
                    </div>
                </div>
            </div> 

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 knowMoreAbtus">
                <div class="paddingJobs">
                    <!-- <div class="col-md-8 heightDiv">
                        <div class="knowAbtMoreHeader">
                            Know more about us
                        </div>
                        <div class="knowAbtMoredesc">
                            Check out our about us sectiona and see how are helping enterprises and job seekers
                            to get the best resource possible.
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img src="{{url('public/images/knowMoreAbtUs.png')}}" class="knowMoreBtusImg">
                    </div> -->
                </div>

            </div>
        </div>

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="paddingJobs">
                    <div class="page-heading discover">
                        <span>Latest Jobs</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="paddingJobs">
                        @php 
                        $rJobs = getRecentJobs($pJobIds);
                    @endphp
                    @foreach($rJobs as $k=>$val)
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 mrgn-btm">
                        <a href="{{route('job-details',['jobid'=>$val->id])}}">
                            <div class="carrer_pannel">
                                <div class="row">
                                    <div class="col-xs-4 align_centered full-width">
                                        @if($val->employer_info!=null)
                                            <img src="{{checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="logoImgJobs">
                                        @else 
                                            <img src="{{url('public/images/default/company_logo.png')}}" class="logoImgJobs">
                                        @endif
                                    </div>
                                    <div class="col-xs-8 align_centered full-width skills_btn pdtop text-right">
                                        @php $counter=1; @endphp
                                        @foreach($val->skills as $data)      
                                            @if(isset($data->skill_keywords) && $counter<4)          
                                                <span> {{$data->skill_keywords->name}}</span>
                                                @php $counter++; @endphp
                                            @endif
                                        @endforeach                 
                                    </div>
                                </div>
                                <div class="row mt-top">
                                    <div class="col-xs-7 full-width">
                                        <div class="job_name">{{($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt.Ltd' }}</div>
                                        <div class="job_designation">{{$val->job_title}}</div>
                                        <div class="job_location">
                                            <img src="public/images/location-icon.png">
                                            {{ getCityNames($val->jobs_in_city) }} - {{ ($val->country!=null) ? $val->country->name : ''}}
                                        </div>
                                    </div>
                                    <div class="col-xs-5 full-width mt-top-5">
                                        <span class="job_amount pull-right nomargin">
                                            <img src="public/images/doller.png">
                                            @if($val->salary_min!=null)
                                                {!! $val->currency . $val->salary_min ."-". $val->currency .$val->salary_max !!}
                                            @else
                                                <h5 class="no-salary-text">As per market standard</h5>
                                            @endif
                                        </span>
                                        <span class="job_date">
                                            {!! countDays($val->created_at) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                    @endforeach
                <div class="col-xs-12 col-lg-12 col-md-12 view-all text-center">
                    <a href="{{route('search-jobs')}}" class="">View all</a>
                </div>

        </div>
    </div>
</div>

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 questionBkclr">
                <!-- <div class="paddingJobs">
                    <div class="col-md-4 text-center">
                        <img src="{{url('public/images/questnImg.png')}}" class="questionsImg">
                    </div>
                    <div class="col-md-8">
                        <div class="questionsMoreHeader">
                            Have a question? Contact us now
                        </div>
                        <div class="knowAbtMoredesc">
                            We're here to help. Check out our FAQs, send us an email or call us at <br>1(800)555-5555
                        </div>
                    </div>
                </div> -->
            </div>
        </div>

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-12">
                <div class="page-heading discover">
                    <span>Popular Categories</span>
                </div>
            </div>
            <div class="padding10">
                @foreach($industries as $val)
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                        <div class="popular-block text-center ">
                            {{--  uploads/industry_img --}}
                    <img class="industry_icon" src="{{checkFile($val->icon,'uploads/industry_img/','gas-pump-1.svg')}}" data-hovericon="{{checkFile($val->icon_hover,'uploads/industry_img/','gas-pump.svg')}}" data-hoverouticon="{{checkFile($val->icon,'uploads/industry_img/','gas-pump-1.svg')}}">                       
                    <a href="{{url('search-jobs?industry_ids='.$val->id)}}"><h5 class="popular-block-heading">{{$val->name}}</h5></a>
                            <p class="popular-block-description">({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }}) Jobs</p>                         

                        </div>
                    </div>
                @endforeach
                
                {{-- <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                         <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Data Science</h5>
                        <p class="popular-block-description">3020 Jobs</p>
                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                       <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Sales Manager</h5>
                        <p class="popular-block-description">2820 Jobs</p>
                       
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                       <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Marketing Manager</h5>
                        <p class="popular-block-description">2340 Jobs</p>
                       
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                        <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">                       
                         <h5 class="popular-block-heading">Cloud Computing</h5>
                        <p class="popular-block-description">2018 Jobs</p>
                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                        <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Data Science</h5>
                        <p class="popular-block-description">3020 Jobs</p>
                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                        <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Sales Manager</h5>
                        <p class="popular-block-description">2820 Jobs</p>
                       
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                        <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Marketing Manager</h5>
                        <p class="popular-block-description">2340 Jobs</p>
                      
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                        <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Cloud Computing</h5>
                        <p class="popular-block-description">2018 Jobs</p>
                      
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                        <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Data Science</h5>
                        <p class="popular-block-description">3020 Jobs</p>
                      
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                       <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Sales Manager</h5>
                        <p class="popular-block-description">2820 Jobs</p>
                      
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                    <div class="popular-block text-center">
                        <img class="industry_icon" src="{{url('public/images/gas-pump-1.svg')}}" data-hovericon="{{url('public/images/gas-pump.svg')}}" data-hoverouticon="{{url('public/images/gas-pump-1.svg')}}">
                        <h5 class="popular-block-heading">Marketing Manager</h5>
                        <p class="popular-block-description">2340 Jobs</p>
                      
                    </div>
                </div> --}}

                <div class="col-lg-12 view-all text-center">
                    <a href="javascript:;" class="">View all</a>
                </div>
            </div>
        </div>
        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="page-headingTc discover">
                    <span>Premium Candidates</span>
                </div>
                <div class="page-heading2">
                    <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Choose from the list of most popular sectors.</span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 candidateBox">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="width90">
                            <div class="owl-carousel" id="candidates">

                                <div class="item cursorPointer candidate">
                                        <img src="{{url('public/images/profile1.png')}}" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                        <img src="{{url('public/images/profile2.png')}}" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                        <img src="{{url('public/images/profile3.png')}}" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                        <img src="{{url('public/images/profile1.png')}}" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                        <img src="{{url('public/images/profile1.png')}}" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                    <img src="{{url('public/images/profile3.png')}}" class="candiddateProfilePic">
                                    <div>Ayush Morya</div>
                                    <p>Delhi</p>
                                    <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                    <img src="{{url('public/images/profile3.png')}}" class="candiddateProfilePic">
                                    <div>Ayush Morya</div>
                                    <p>Delhi</p>
                                    <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                 <div class="item cursorPointer candidate">
                                    <img src="{{url('public/images/profile3.png')}}" class="candiddateProfilePic">
                                    <div>Ayush Morya</div>
                                    <p>Delhi</p>
                                    <label class="mrgnBtm25">UI UX Designer</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script>
    /* $('.closemodal').click(function() {
        $('#alertModal').modal('hide');
    });
    $(function() {
        // for key skill //
        $( "#skills" ).autocomplete({
        source: '{{url("listing/get_key")}}'
        });
    });
    $(function() {
        // for location //
        $( "#location" ).autocomplete({
        source: '{{url("listing/country_state_city")}}'
        });
    }); */
</script>
@endsection


     