@extends('/layouts/front_panel_master')
@section('content')

<div class="row topEmployerBanner3">
        <div class="centered_1">Contact Us<br>
          <span>Be a premium candidate in jobs4gulf active jobseker's database and increase your cv by 70%.</span>
        </div>
    </div>
<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registerHeaderBox2">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding10px">
            <div class="contactUsHeader">Send Us A Message</div>
            {{ Form::open(array('url'=>route('send-contact-message'),'class'=>'contact-form-wrapper','id'=>'contact_form'))}}
            <div class="areYou checkbox" >Are You:
                <label><input type="radio" name="type" value="Candidate" class="radio-type">Job Seeker/Candidate</label>
                <label><input type="radio" name="type" value="Employer" class="radio-type">Employer/Corporate</label>
            </div>
            <span id="errorType"></span>
            <div class="form-group marginT20">
                <input type="text" class="form-control registerHereSubTextInput" name="name" placeholder="Name">
                {{-- <input type="text" placeholder="Name" class="form-control registerHereSubTextInput" > --}}
            </div>
            <div class="form-group marginT20">
                <input type="email" class="form-control registerHereSubTextInput" name="email" placeholder="Email">
                {{-- <input type="text" placeholder="Email" class="form-control registerHereSubTextInput" > --}}
            </div>
            <div class="form-group marginT20">
                <input type="text" placeholder="Mobile no." class="form-control registerHereSubTextInput" name="mobile" >
            </div>
            <div class="form-group marginT20">
                <input type="text" placeholder="Location" name="location" class="form-control registerHereSubTextInput" >
            </div>
            <div class="form-group marginT20 hide-elem" id="comp-div">
                <input type="text" placeholder="Company Name" name="comp_name" class="form-control registerHereSubTextInput" >
            </div>
            <div class="form-group marginT20">
                <textarea name="message" class="form-control" placeholder="Enter your query in brief" rows="5"></textarea>
            </div>
            <div class="areYou2 checkbox" >
                <label><input type="checkbox" value="1" checked>Interested to forward my query to more corporates.</label>
            </div>
            <div>
                <button class="btn btn-success success-btn searchButton2" type="submit">Submit</button>
            </div>
            {{Form::close()}}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 marginBtm50">
           <div class="row padding10px contactInfo contactUsBlock ">
               <div class="contactUsHeader">Contact Info</div>
               <div class="row noPadding1">
                   <div class="col-xs-6 noPadding1 marginTp30 padding15px">
                       <div class="jobLocation2"><img class="imgNumber" src="{{url('public/images/phoneBlack.png')}}">Contact Number:</div>
                   </div>
                   <div class="col-xs-6 noPadding1 marginTp30 padding15px">
                       <div class="detailsWalkin">{{$settings->mobile}}</div>
                   </div>
               </div>
               <div class="row noPadding1">
                   <div class="col-xs-6 noPadding1 padding15px">
                       <div class="jobLocation2"><img class="imgEmailAddress" src="{{url('public/images/messageBlack.png')}}">Email Address:</div>
                   </div>
                   <div class="col-xs-6 noPadding1 padding15px">
                       <div class="detailsWalkin">{{$settings->email}}</div>
                   </div>
               </div>
               
               <div class="row noPadding1">
                   <div class="col-xs-6 noPadding1 padding15px">
                       <div class="jobLocation2"><img class="imgAddress" src="{{url('public/images/placeholder@2x.png')}}">Address:</div>
                   </div>
                   <div class="col-xs-6 noPadding1 padding15px">
                       <div class="detailsWalkin">{!!$settings->address!!}</div>
                   </div>
               </div>

           </div>
        </div>
        <div class="col-xs-12 text-center icons">
            @if($settings->fb_link!='')
                <a href="{{$settings->fb_link}}" target="_blank"><img src="{{url('public/images/fb.png')}}"></a>
            @endif
            @if($settings->twitter_link!='')
                <a href="{{$settings->twitter_link}}" target="_blank"><img src="{{url('public/images/twitter.png')}}"></a>
            @endif
            @if($settings->gplus_link!='')
                <a href="{{$settings->gplus_link}}" target="_blank"><img src="{{url('public/images/google.png')}}"></a>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pricingHeader text-center">
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6 ">
            <div class="pricingHeader4">
                Frequently asked <br> questions
                <div class="pricingHeader5">Need more help? Contact us at <br>1800-204-1477</div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
            <div class="pricingHeader9">
                <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click','.radio-type',function(){

        if($(this).val()=="Employer"){
            $("#comp-div").removeClass("hide-elem");
        }
        else if($(this).val()=="Candidate"){
            $("#comp-div").addClass("hide-elem");
        }
    });
</script>
@endsection