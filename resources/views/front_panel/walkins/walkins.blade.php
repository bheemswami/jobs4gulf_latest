@extends('/layouts/front_panel_master') 
@section('content')

<div class="row topEmployerBanner2">
        <div class="centered_1">Walk-In Zone<br>
          <span>Check Out The Upcoming Walk-In Interviews Of Gulf Top Companies.<br> Just Appear With Your Resume And Grab Your Opportunity.</span>
        </div>
    </div>
    <div class="marginSearchHeader">
        <div class=" row padding14px" style="height:auto;">
            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 searchHeading">You search for</div>
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 searchedFeilds">
                    <div id='search_key'></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        
        <script>
            function toggleLeftNav(id){
                $("#"+id).toggle();
            }
        </script>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20">
                <div class="leftSearchBox">
                    <div class="paddingSearchBox">
                        <label class="LeftSearchMainHeading">Refine Search</label>
                        <form method="post" action="" id="filter_form">
                            <!--For Country-->
                            <div class="borderBtm mrgntp10">
                                <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                                <label class="LeftSearchSubHeading">Walk in Date <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('date')" aria-hidden="true"></i></label>
                                <div id="date" class="row nomrgn ">
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                            <label class="searchField">
                                                <input type="checkbox" name="this_week" value="this_week" class="change-checkbox searchCheckBox"/>
                                                <span class="optionDiv">This Week</span>
                                            </label>
                                            <label class="searchField">
                                                <input type="checkbox" class="searchCheckBox change-checkbox" name="this_week" value="next_week">
                                                <span class="optionDiv">Next Week</span>
                                            </label>
                                            <label class="searchField">
                                                <input type="checkbox" class="searchCheckBox change-checkbox" name="this_week" value="this_month">
                                                <span class="optionDiv">This Month</span>
                                            </label>
                                            <label class="searchField">
                                                <input type="checkbox" name="post_date" value="" class="change-checkbox" id="post_date"/>
                                                <span class="optionDiv">Select Date</span>
                                                <input type="text" id="datepicker"  placeholder="Select Date" readonly="readonly" class="searchCheckBox hide-elem"/>
                                                <span class="optionDiv2" id="show-post-date"></span>
                                            </label>
                                            {{-- <label class="searchField">
                                                <input type="checkbox" class="searchCheckBox change-checkbox" name="post_date" value="" id="post_date">
                                                <span class="optionDiv">Select Date</span>
                                                <span class="optionDiv2">
                                                    <input type="text" id="datepicker"  placeholder="Select Date" readonly="readonly" />
                                                </span>
                                            </label> --}}
                                        </div>
                                        {{-- <div class="showMore">Show more</div> --}}
                                    </div>
                                </div>
                            </div>
                            <!--For City-->
                            <div class="borderBtm mrgntp10">
                                <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                                <label class="LeftSearchSubHeading">Job by City <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('city')" aria-hidden="true"></i></label>
                                <div id="city" class="row nomrgn" style="display: none">
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                            @php $counter=1; @endphp
                                            @foreach($city_list as $k=>$val)
                                                
                                                @if($counter<10)
                                                    <label class="searchField">
                                                        <input type="checkbox" name="city_ids" value="{{$val->id}}" class="change-checkbox searchCheckBox" />
                                                        <span class="optionDiv">{{$val->name}}</span>
                                                        <span class="optionDiv2">({{count($val->walkins)}})</span>
                                                    </label>
                                                @else
                                                <div class="hide-elem city-list">
                                                    <label class="searchField">
                                                        <input type="checkbox" name="city_ids" value="{{$val->id}}" class="change-checkbox searchCheckBox" />
                                                        <span class="optionDiv">{{$val->name}}</span>
                                                        <span class="optionDiv2">({{count($val->walkins)}})</span>
                                                    </label>
                                                </div>
                                                @endif
                                                @php $counter++; @endphp
                                            @endforeach
                                        </div>
                                        <div class="show-city-list showMore">Show more</div>
                                    </div>
                                </div>
                            </div>

                            <!--For City-->
                            <div class=" borderBtm mrgntp10">
                                <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                                <label class="LeftSearchSubHeading">Job by Industry <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('category')" aria-hidden="true"></i></label>
                                <div id="category" class="row nomrgn" style="display: none">
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                            @php $counter=1; @endphp
                                            @foreach($industry_list as $k=>$val)
                                            @if($counter<5)
                                                <label class="searchField">
                                                    <input type="checkbox" class="change-checkbox searchCheckBox" name="industry_ids" value="{{$val->id}}">
                                                    <span class="optionDiv">{{$val->name}}</span>
                                                    <span class="optionDiv2">({{count($val->walkins)}})</span>
                                                </label>
                                            @else
                                                <div class="hide-elem industry-list">
                                                    <label class="searchField">
                                                        <input type="checkbox" class="change-checkbox searchCheckBox" name="industry_ids" value="{{$val->id}}">
                                                        <span class="optionDiv">{{$val->name}}</span>
                                                        <span class="optionDiv2">({{count($val->walkins)}})</span>
                                                    </label>
                                                </div>
                                            @endif
                                            @php $counter++; @endphp
                                            @endforeach
                                        </div>
                                        <div class="show-industry-list showMore">Show more</div>
                                    </div>
                                </div>
                            </div>

                            <!--For City-->
                            <div class=" mrgntp10">
                                <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                                <label class="LeftSearchSubHeading">Employer Type <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('industry')" aria-hidden="true"></i></label>
                                <div id="industry" class="row nomrgn" style="display: none">
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                            <label class="searchField">
                                                <input type="checkbox" class="change-checkbox searchCheckBox" name="employer_type" value="1">
                                                <span class="optionDiv">Company Jobs</span>
                                            </label>
                                            <label class="searchField">
                                                <input type="checkbox" class="change-checkbox searchCheckBox" name="employer_type" value="2">
                                                <span class="optionDiv">Consultancy Jobs</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding" id="load">
                @include('front_panel/walkins/walkins_list')
            </div>

        </div>
        
    </div>

<script type="text/javascript">
    var url = '{{route('walkins')}}';
    $(document).on('click','.custom-pagination a',renderData);
    $(document).on('click','.change-checkbox',function(){
       setValues();
    });

    function setValues(){
         var form = $('#filter_form').serializeArray();
        console.log(form);
        var industry_ids = [];
        var city_ids = [];
        var employer_type = [];
        var duration = [];
        var this_week='';
        var next_week='';
        var this_month='';
        var post_date='';
        $.each(form,function(index,value){
          if(value.name=='city_ids'){
            city_ids.push(value.value);
          } else if(value.name=='industry_ids'){
            industry_ids.push(value.value);
          } else if(value.name=='employer_type'){
            employer_type.push(value.value);
          } else if(value.name=='this_week'){
            this_week=value.value;
          }  else if(value.name=='next_week'){
            next_week=value.value;
          }  else if(value.name=='this_month'){
            this_month=value.value;
          }  else if(value.name=='post_date'){
            post_date=value.value;
          }
        });
        var data = {'city_ids':city_ids.join(),'industry_ids':industry_ids.join(),'employer_type':employer_type.join(),this_week:this_week,next_week:next_week,this_month:this_month,post_date:post_date,'page':1};
        ajaxCall(data);
    }
    function renderData(e){
      e.preventDefault();
      url = $(this).attr('href');
      ajaxCall();
      window.history.pushState("", "", url);
    }
    function ajaxCall(data=NaN){
        $.ajax({url: url,data:data, success: function(result){
        $('#load').html(result); 
    }});
    /* $.ajax({
          url : url,
          data : data,
      }).done(function (data) {
        //console.log(data);
          $('#load').html('').html(data); 
      }).fail(function () {
        
      }).complete(function(){
    }); */
    }

    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      showOn: "button",
      buttonImage: "{{url('public/images/calendar_11.png')}}",
      buttonImageOnly: true,
      buttonText: "Select date",
      altField: "#post_date",
      dateFormat: "dd-mm-yy",
      onSelect: function (date) {setValues();
        $("#show-post-date").text(date);
      }
    });

$(document).on('click','.show-city-list',function(){
      $(".city-list").show();
      $(this).removeClass('show-city-list');
      $(this).addClass('hide-city-list');
      $(this).html('Show Less');

  })
  $(document).on('click','.hide-city-list',function(){
      $(".city-list").hide();
      $(this).addClass('show-city-list');
      $(this).removeClass('hide-city-list');
      $(this).html('Show more');
  })

  $(document).on('click','.show-industry-list',function(){
      $(".industry-list").show();
      $(this).removeClass('show-industry-list');
      $(this).addClass('hide-industry-list');
      $(this).html('Show Less');

  })
  $(document).on('click','.hide-industry-list',function(){
      $(".industry-list").hide();
      $(this).addClass('show-industry-list');
      $(this).removeClass('hide-industry-list');
      $(this).html('Show more');
  })
  
</script>
@endsection