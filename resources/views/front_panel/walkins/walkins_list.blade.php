<div class="col-sm-12"> 
    @forelse ($walkins as $key=>$vals)
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt20">
            <div class="cardBox2">
                <div class="row walkinHeader">
                    <div class="col-xs-12">
                        <h3>
                        {{ $vals->title }}</h3>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="row text-center">
                            <div class="img_back_div">
                            <a href="{{url('walkins-detail/'.$vals->id)}}">
                                <img class="cardImg4 img-thumbnail" src="{{checkFile($vals->comp_logo,'uploads/walkin_comp_logo/','job_role_default.png')}}">
                            </a>
                        </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                        <div class="row walkinHeader2">
                            <h4>
                            {{ $vals->comp_name }}</h4>
                        </div>
                        @php  $interview_city=[]; $date_from=$date_to=''; @endphp
                        @if($vals->walkin_interview!=null)
                            @foreach($vals->walkin_interview as $k=>$v)
                                @php
                                    if($v->city!=null){
                                        $interview_city[]= $v->city->name;         
                                    }
                                @endphp
                                @if($k==0)
                                    @php $date_to=$v->date_to;
                                        $date_from=$v->date_from;
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                        <div class="row marginTPBtm10">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 noPadding1">
                                <div class="jobLocation"><img src="{{url('public/images/calendarBlck.png')}}">Walk-In Date:</div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-8 noPadding1">
                                <div class="detailsWalkin">
                                    @if($date_from!='' && $date_to!='')
                                        {{dateConvert($date_from,'dS')}}-{{dateConvert($date_to,'dS M Y')}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row marginTPBtm10">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 noPadding1">
                                <div class="jobLocation"><img src="{{url('public/images/clockBlack.png')}}">Walk-In Time:</div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-8 noPadding1">
                                <div class="detailsWalkin">
                                    @if($date_from!='' && $date_to!='')
                                        {{dateConvert($date_from,'h:i A')}} to {{dateConvert($date_to,'h:i A')}}
                                    @else
                                        N/A
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row marginTPBtm10">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4  noPadding1">
                                <div class="jobLocation"><img src="{{url('public/images/locationBlack.png')}}">Walk-In Location:</div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-8 noPadding1">
                                <div class="detailsWalkin">{{join(',',$interview_city)}}</div>
                            </div>
                        </div>
                        <div class="row walkinHeader3">Position:</div>

                        <div class="row walkinHeader4">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPadding1">
                                @if($vals->walkin_position!=null)
                                    @php $designations=[];  @endphp
                                    @foreach($vals->walkin_position as $d)
                                        @php 
                                        if($d->designation!=null)
                                            $designations[]= $d->designation->name; 
                                        @endphp
                                    @endforeach
                                    {{ limit_text(join(',',$designations),140) }}
                                @else
                                    N/A
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <div class="col-xs-12 col-md-12 nopadding">
            <div class=" job-box">
            <div class="row flexbox">
                
                <div class="col-xs-12 col-sm-12">
                
                <img src="{{url('public/images/default/nojobsfound.png')}}" class="center-block" width="240px" style="margin-top: 5%;">
                <div class="post-details">
                    
                </div>
                </div>
            </div>
            
            </div>
        </div> 
    @endforelse                       
    <div class="row margnBtm30">
            <div class="col-lg-12 mt20">
                <div class="paginationContainer">
                    <div class="custom-pagination">
                        {{$walkins->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>

var search='{{$search_tag}}';
var array = search.split(',');

if(search)
{
  $('#search_key').html('');
  $.each( array, function( key, value ) {
    if(value )
    $('#search_key').append('<span>'+value+'</span>|');
  });
}
</script>