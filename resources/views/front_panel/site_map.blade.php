@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel/includes/page_banner')

<section class="main-inner-page lite-greyBg">
    <div class="container contact">
      <div class="row">       
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 category-parent-box card">
            <h2>Site Map</h2>
            
            <div class="col-lg-4">
                      <strong>Global</strong>
                      <ul class="sm_UL">
                          <li>
                              <a href="{{url('/')}}">Home</a>
                              <a href="{{route('about-us')}}" target="_blank">About Us</a>
                              <a href="{{route('contact-us')}}">Contact Us</a>
                          </li>
                      </ul>
                       <br>
                     <strong>Job Seeker</strong>
                      <ul class="sm_UL">
                          <li>
                              <a class="registerRR" href="{{route('candidate-register')}}">Post Your CV</a>
                              <a href="{{route('search-jobs')}}">Search Jobs</a>
                          </li>
                      </ul>
                       <br>
                       <strong>Jobs by Country</strong>
                      <ul class="sm_UL">
                          <li>
                              @foreach($countries as $val)
                                  <a href="{{url('search-jobs?country_ids='.$val->id)}}" >Jobs for {{$val->name}}</a>
                              @endforeach                                    
                              <a href="{{route('jobs-by',['country'])}}">View All Countries</a>
                          </li>
                      </ul>
                      <br>
                      <strong>Jobs by Industry</strong>
                      <ul class="sm_UL">
                        <li>
                         @foreach($industries as $val)
                          <a href="{{url('search-jobs?industry_ids='.$val->id)}}" > {{$val->name}}</a>
                        @endforeach
                       <a href="{{route('jobs-by',['industry'])}}">View All Industries</a>
                     </li>
                      </ul>
                      
                      
                  </div>

                 
          </div>
      </div>
    </div>
</section>

@endsection