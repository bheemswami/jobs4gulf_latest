
@extends('/layouts/front_panel_master')
@section('content')

@php
$paramCountryIds=$paramCityIds=$paramIndustryIds=$paramfunctionlist=$paramexp=[];
$jobDate='';
if(isset(Request::all()['country_ids'])){
  $paramCountryIds=explode(',',Request::all()['country_ids']);
}
if(isset(Request::all()['city_ids'])){
  $paramCityIds=explode(',',Request::all()['city_ids']);
}
if(isset(Request::all()['industry_ids'])){
  $paramIndustryIds=explode(',',Request::all()['industry_ids']);
}
if(isset(Request::all()['function_list_ids'])){
  $paramfunctionlist=explode(',',Request::all()['function_list_ids']);
}
if(isset(Request::all()['exp'])){
  $paramexp=explode(',',Request::all()['exp']);
}
if(isset(Request::all()['job_date'])){
  $jobDate=Request::all()['job_date'];
}
@endphp

<div class=" marginSearchHeader">
    <div class=" row padding14px" style="height:auto;">
        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 searchHeading">You search for</div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 searchedFeilds">
                  <div id='search_key'>  </div>
            </div>
            
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <div class="input-group add-on">
                    <div class="input-group-btn">
                            <button data-toggle="modal" data-target="#modifySearch"  class="btn btn-success success-btn searchButton" id="modalSearch" type="submit">Modify Search</button>
                    </div>
                </div>
            </div>
    </div>

</div>
<div class="container">
@include('front_panel/includes/search_job_section')
 <div class="row">
    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20 subHeaderResp">
        <span class="subHeaderLabel">Home / Job Search </span>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 mt20 ">

            <div class=" col-xs-6 col-sm-6 col-md-6 col-lg-6 subHeaderLabel2 nomrgn noPadding">
                <span class="subHeaderLabel3 noLftMargn">
                    <img src="{{url('public/images/listViewSelected.png')}}" class="list-icon" data-name="list">
                    <img src="{{url('public/images/gridViewUnselected.png')}}" class="grid-icon" data-name="grid">
                </span>
                <span class="subHeaderLabel3 responsiveSubHeader">View</span>

                 <span class="dropdownCustom">
                     <i class="fa fa-angle-down dropdownCustom" aria-hidden="true"></i>
                     <div class="dropdown-content-custom">
                        <p class="active">Revelance</p>
                        <p>Most Recent</p>
                        <p>Top Rated</p>
                        <p>Most Applied</p>
                     </div>
                 </span>
                <span>Recent</span>
                <span class="responsiveSubHeader">Sort by :</span>

           </div>
        </div>
            <!-- <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 mt20 ">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 subHeaderLabel nomrgn noPadding">13444 Jobs Available</div>

                <div class=" col-xs-6 col-sm-6 col-md-6 col-lg-6 subHeaderLabel2 nomrgn noPadding">
                    <span class="subHeaderLabel3 noLftMargn">
                        <img src="images/listViewUnselected.png">
                        <img src="images/gridViewSelected.png">
                    </span>
                    <span class="subHeaderLabel3 responsiveSubHeader">View</span>

                     <span class="dropdownCustom">
                         <i class="fa fa-angle-down dropdownCustom" aria-hidden="true"></i>
                         <div class="dropdown-content-custom">
                            <p class="active">Revelance</p>
                            <p>Most Recent</p>
                            <p>Top Rated</p>
                            <p>Most Applied</p>
                         </div>
                     </span>
                    <span>Recent</span>
                    <span class="responsiveSubHeader">Sort by :</span>

               </div>
            </div> -->
        </div>


        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20">
                <div class="leftSearchBox">
                    <div class="paddingSearchBox">
                        <label class="LeftSearchMainHeading">Refine Search</label>
                        {{Form::open(array('url'=>route('search-jobs'),'method'=>'GET','class'=>'','id'=>'filter_form'))}}
                        <!--For Country-->
                        <div class="borderBtm mrgntp10">
                            <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                            <label class="LeftSearchSubHeading">Jobs by Location <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('country')" aria-hidden="true"></i></label>
                            <div id="country" class="row nomrgn ">
                                <div class="col-xs-12"> 
                                    <div class="checkbox">
                                        @foreach($country_list as $k=>$val)
                                            <label class="searchField">
                                            @if(in_array($val->id, $paramCountryIds))
                                            <input type="checkbox" name="country_ids" value="{{$val->id}}" class="searchCheckBox change-checkbox" checked/> 
                                            @else
                                            <input type="checkbox" name="country_ids" value="{{$val->id}}" class="searchCheckBox change-checkbox"/> 
                                            @endif
                                            <span class="optionDiv">{{$val->name}}</span>
                                            <span class="optionDiv2">({{count($val->job_post)}})</span></label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                       {{--  <div class="borderBtm mrgntp10">
                            <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                            <label class="LeftSearchSubHeading">City <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('city')" aria-hidden="true"></i></label>
                            <div id="city" class="row nomrgn" style="display: none">
                                <div class="col-xs-12">
                                    <div class="checkbox">
                                        @foreach($city_list as $k=>$val)
                                            <label class="searchField">
                                            @if(in_array($val->id, $paramCityIds))
                                                <input type="checkbox" name="city_ids" value="{{$val->id}}" class="searchCheckBox checkbox" checked/> 
                                            @else
                                                <input type="checkbox" name="city_ids" value="{{$val->id}}" class="searchCheckBox checkbox"/> 
                                            @endif
                                            <span class="optionDiv">{{$val->name}}</span>
                                            @php
                                            $total_jobs=0;
                                            if($val->active_jobs!=null){                        
                                                foreach($val->active_jobs as $vals){
                                                    if($vals->jobs!=null ){
                                                        $total_jobs=$total_jobs+1;
                                                    }  
                                                }
                                            }
                                            @endphp
                                            <span class="optionDiv2">({{$total_jobs}})</span></label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div> --}}
              
                        <div class=" borderBtm mrgntp10">
                            <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                            <label class="LeftSearchSubHeading">Jobs By Industry <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('category')" aria-hidden="true"></i></label>
                            <div id="category" class="row nomrgn" style="display: none">
                                <div class="col-xs-12">
                                    <div class="checkbox">
                                        @foreach($industry_list as $k=>$val)
                                            <label class="searchField">
                                            @if(in_array($val->id, $paramIndustryIds))
                                            <input type="checkbox" name="industry_ids" value="{{$val->id}}" class="change-checkbox" checked/> 
                                            @else
                                            <input type="checkbox" name="industry_ids" value="{{$val->id}}" class="change-checkbox"/> 
                                            @endif
                                            <span class="optionDiv">{{$val->name}}</span><span class="optionDiv2">({{count($val->job_post)}})</span></label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div> 
                        
                        
                        <div class="borderBtm mrgntp10">
                            <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                            <label class="LeftSearchSubHeading">Jobs By Function <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('function_A')" aria-hidden="true"></i></label>
                            <div id="function_A" class="row nomrgn" style="display: none">
                                <div class="col-xs-12">
                                    <div class="checkbox">
                                        @foreach($function_list as $k=>$val)
                                            <label class="searchField">
                                            @if(in_array($val->id, $paramfunctionlist))
                                            <input type="checkbox" name="function_list_ids" value="{{$val->id}}" class="change-checkbox" checked/> 
                                            @else
                                            <input type="checkbox" name="function_list_ids" value="{{$val->id}}" class="change-checkbox"/> 
                                            @endif
                                            <span class="optionDiv">{{$val->name}}</span><span class="optionDiv2">({{count($val->job_post)}})</span></label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php
                        if(isset(Request::all()['exp_year']))
                            $k=(int)Request::all()['exp_year'];
                        else
                            $k=5;
                        @endphp
                        <div class="borderBtm mrgntp10">
                            <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                            <label class="LeftSearchSubHeading">Jobs By Experience <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('mexp')" aria-hidden="true"></i></label>
                            <div id="mexp" class="row nomrgn" style="display: none">
                                <div class="col-xs-12">
                                    <div class="checkbox">
                                        @for($i=0; $i<=$k;$i++)
                                            <label class="searchField">
                                            @if(in_array($i, $paramexp))
                                                <input type="checkbox" name="exp" value="{{$i}}" class="change-checkbox" checked/> 
                                            @else
                                                <input type="checkbox" name="exp" value="{{$i}}" class="change-checkbox"/> 
                                            @endif
                                            <span class="optionDiv">{{$i}}</span>
                                            <span class="optionDiv2">(@if(isset($exp[$i])) {{@$exp[$i]}} @else 0 @endif )</span></label>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="borderBtm mrgntp10">
                                <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                                <label class="LeftSearchSubHeading">Job Freshness <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('jdate')" aria-hidden="true"></i></label>
                                <div id="jdate" class="row nomrgn" style="display: none">
                                    <div class="col-xs-12">
                                        <div class="">
                                            <label class="searchField">
                                            <input type="radio" name="job_date" value="0" class="change-checkbox" {{(isset($jobDate) && $jobDate=="0") ? "checked" : ''}} /> 
                                            <span class="optionDiv">Today</span>
                                            <span class="optionDiv2"></span>
                                            </label>
                                            <label class="searchField">
                                            <input type="radio" name="job_date" value="7" class="change-checkbox" {{(isset($jobDate) && $jobDate=="7") ? "checked" : ''}} /> 
                                            <span class="optionDiv">Upto 7 days</span>
                                            <span class="optionDiv2"></span>
                                            </label>
                                            <label class="searchField">
                                            <input type="radio" name="job_date" value="15" class="change-checkbox" {{(isset($jobDate) && $jobDate=="15") ? "checked" : ''}}/> 
                                            <span class="optionDiv">Upto 15 days</span>
                                            <span class="optionDiv2"></span>
                                            </label>
                                            <label class="searchField">
                                            <input type="radio" name="job_date" value="30" class="change-checkbox" {{(isset($jobDate) && $jobDate=="30") ? "checked" : ''}}/> 
                                            <span class="optionDiv">Upto 30 days</span>
                                            <span class="optionDiv2"></span>
                                            </label>
                                            <label class="searchField">
                                            <input type="radio" name="job_date" value="60" class="change-checkbox"  {{(isset($jobDate) && $jobDate=="60") ? "checked" : ''}} /> 
                                            <span class="optionDiv">Upto 60 days</span>
                                            <span class="optionDiv2"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            
            <div id="load">
                <div class="list-view">
                    @include('front_panel.job.job_listing_paginate')
                </div>
                <div class="grid-view hide-elem">
                    @include('front_panel.job.job_grid_paginate')
                </div>
            </div>
        </div>
<script>
    var view="list";
    function toggleLeftNav(id){
        $("#"+id).toggle();
    }
  var url = base_url;
  $(document).on('click','.custom-pagination a',renderJobs);

  $(document).on('click','.checkbox,.change-checkbox',function(e){
     
    var form = $('#filter_form').serializeArray();
    var industry_ids = [];
    var city_ids = [];
    var country_ids = [];
    var function_list_ids = [];
    var exp = [];
    var job_date='';
    $.each(form,function(index,value){
      if(value.name=='country_ids'){
        country_ids.push(value.value);
      }else if(value.name=='city_ids'){
        city_ids.push(value.value);
      }else if(value.name=='industry_ids'){
        industry_ids.push(value.value);
      }else if(value.name=='function_list_ids'){
        function_list_ids.push(value.value);
      }
      else if(value.name=="job_date"){
        job_date=value.value;
      }
      else if(value.name=='exp'){
        exp.push(value.value);
      }
    });
    url=base_url+'search-jobs?country_ids='+country_ids+'&city_ids='+city_ids+'&industry_ids='+industry_ids+'&function_list_ids='+function_list_ids+'&exp='+exp;
    window.history.pushState("", "", url);
    var data = {'country_ids':country_ids.join(),'city_ids':city_ids.join(),'industry_ids':industry_ids.join(),'function_list_ids':function_list_ids.join(),'exp':exp.join(),'job_date':job_date};
    ajaxCall(data);
  });
  function renderJobs(e){
      e.preventDefault();
      url = $(this).attr('href');
      ajaxCall();
      window.history.pushState("", "", url);
  }
  function ajaxCall(data=NaN){
    $.ajax({url: url,data:data, success: function(result){
        $(".list-view").html(result['list']); 
        $('.grid-view').html(result['grid']); 
    }});
    /* $.ajax({
          url : url,
          data : data,
      }).done(function (data) {
          $('#load').html(data); 
          
      }).fail(function () {
        
      }).complete(function(){
            });
 */
     //window.history.pushState("", "", url);
  }
  function showView(view2){
      if(view2=="grid")
      {
        $(".list-view").addClass("hide-elem");
        $('.grid-view').removeClass("hide-elem");
        $(".grid-icon").attr('src',"{{url('public/images/gridViewSelected.png')}}");
        $(".list-icon").attr('src',"{{url('public/images/listViewUnselected.png')}}");
      }
      else{
        $(".list-view").removeClass("hide-elem");
        $('.grid-view').addClass("hide-elem");
        $(".list-icon").attr('src',"{{url('public/images/listViewSelected.png')}}");
        $(".grid-icon").attr('src',"{{url('public/images/gridViewUnselected.png')}}");
      }
      ajaxCall(data);
  }  
    $(document).on('click','.grid-icon,.list-icon',function(){
        view=$(this).data('name');
        showView(view);
    });
</script>
@endsection
