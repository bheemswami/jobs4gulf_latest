@extends('/layouts/front_panel_master')
@section('content')

<div class="jobseeker-banner" >  
    <div class="centered">Register As Job Seeker<br>
          <span>Let The Top Employers From Gulf Reach You,Rgister On Job4gulf Now!</span>
        </div>
      </div>

<div class="row noPadding nomrgn">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pricingHeader text-center">
    <div class="row pricingHeading1 text-center">
        <span class="plansHeader">Supercharge your Company Productivity</span>
    </div>

    <div class="row pricingHeading2 text-center">
        Job4Gulf is perfect for Individuals, Small, Medium and Large Companies
    </div>
    {{-- <div class="row pricingHeading3 text-center">
        <span>Why Choose Jobs4Gulf ?</span>
    </div>
    
    <div class="container text-center marginBtm100">
       <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mrgnTpBtm5">
           <div class="row text-center">
               <img class="pricingImg" src="{{url('public/images/pricing1.png')}}">
           </div>
           <div class="row text-center pricingHeading4">
               More Candidates
           </div>
           <div class="row text-center pricingHeading5">
               149,000+ new registration <br> every month
           </div>
       </div>
       <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mrgnTpBtm5">
           <div class="row text-center">
               <img class="pricingImg" src="{{url('public/images/pricing2.png')}}">
           </div>
           <div class="row text-center pricingHeading4">
               More Response
           </div>
           <div class="row text-center pricingHeading5">
               3.5 million applications <br> a Month
           </div>
       </div>
       <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mrgnTpBtm5">
           <div class="row text-center">
               <img class="pricingImg" src="{{url('public/images/pricing3.png')}}">
           </div>
           <div class="row text-center pricingHeading4">
               More Relevance
           </div>
           <div class="row text-center pricingHeading5">
               Target the best candidates <br> quickly
           </div>
       </div>
       <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mrgnTpBtm5">
           <div class="row text-center">
               <img class="pricingImg" src="{{url('public/images/pricing4.png')}}">
           </div>
           <div class="row text-center pricingHeading4">
               Our Dedicated Support
           </div>
           <div class="row text-center pricingHeading5">
               We finally found a host that truly <br> understood the unique
           </div>
       </div>
    </div> --}}

</div>
</div>
<div class="container">
    <div class="row marginTpBtm20">
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <!--Header-->
           <div class="row brdrBtm2">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingHeader8 ">
                   Features
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingHeader6 text-center">
                    <div>BASICS</div>
                    <div class="pricingHeader7">Free 2 Jobs</div>
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingHeader6  text-center">
                   <div>PREMIUM</div>
                   <div>Starts at $1999</div>
                   <img src="{{url('public/images/popular.png')}}" class="popularImg">
               </div>
           </div>
           @php $counter=1; @endphp
           @foreach($features as $k=>$val)
    
            <div class="row {{($counter%2==0) ? 'colorBlue' : ''}}">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                    {!! $val->feature_title !!}
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                    @if($val->val_type==0)
                        {{ $val->basic_benefits}}
                    @else
                        {!! ($val->basic_benefits==1) ? '<img src="'.url("public/images/checked@2x.png").'"  width="18px">' : '<img src="'.url("public/images/cross.png").'"  width="12px">' !!}
                    @endif
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                    @if($val->val_type==0)
                        {{ $val->premium_benefits}}
                    @else
                        {!! ($val->premium_benefits==1) ? '<img src="'.url("public/images/checked@2x.png").'"  width="18px">' : '<img src="'.url("public/images/cross.png").'"  width="12px">' !!}
                    @endif
               </div>
           </div>
           @php $counter++; @endphp
           @endforeach
           {{-- <div class="row colorBlue">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                   Post Job Validity
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                   90 Days
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                   180 Days
               </div>
           </div> --}}
           {{-- <div class="row">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                   Applications Straight To Your Inbox
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                   90 Days
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                   180 Days
               </div>
           </div>
           <div class="row colorBlue">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                   Included In Job Alert Emails To Candidates
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                   90 Days
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                   180 Days
               </div>
           </div>
           <div class="row">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                   Your Job Add Refreshed To The Top Of The Search Listings In Relevant Search On Days 7, 14 & 21
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                   <img src="{{url('public/images/checked@2x.png')}}" width="18px">
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                   <img src="{{url('public/images/cross.png')}}"  width="12px">
               </div>
           </div>
           <div class="row colorBlue">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                   Included In Job Alert Emails To Candidates
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                   <img src="{{url('public/images/checked@2x.png')}}" width="18px">
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                   <img src="{{url('public/images/cross.png')}}"  width="12px">
               </div>
           </div>
           <div class="row">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                   Bulk Download Of CVs In Excel Format
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                   <img src="{{url('public/images/checked@2x.png')}}" width="18px">
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                   <img src="{{url('public/images/cross.png')}}"  width="12px">
               </div>
           </div>
           <div class="row colorBlue">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                   Link To Your Own Application Form From Your Ad
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                   <img src="{{url('public/images/cross.png')}}"  width="12px">
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                   <img src="{{url('public/images/checked@2x.png')}}" width="18px">
               </div>
           </div>
           <div class="row">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                   Bulk Download Of CVs In Excel Format
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                   <img src="{{url('public/images/checked@2x.png')}}" width="18px">
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                   <img src="{{url('public/images/cross.png')}}"  width="12px">
               </div>
           </div> --}}
       </div>
    </div>
    <div class="row marginTpBtm20_2">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                    <button type="button" class="btn btn-success colorGreen2">Post a job</button>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                    <button type="button" class="btn btn-success colorNavy">Get a Quote</button>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row noPadding nomrgn">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pricingHeader2 text-center">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 ">
            <div class="pricingHeader3">Thousands of Companies <br> Trust Job4Gulf as a best <br> source for Human Resource.</div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 marginTpBtm120">
            <span><img src="{{url('public/images/layer0.png')}}"></span>
            <span><img src="{{url('public/images/lifehacker@2x.png')}}"></span>
            <span><img src="{{url('public/images/logoTechCrunch2x@2x.png')}}"></span>
            <span><img src="{{url('public/images/tnw-logo2x@2x.png')}}"></span>
        </div>
    </div>
</div>
<div class="row noPadding nomrgn">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pricingHeader text-center">
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 ">
            <div class="pricingHeader4">
                Frequently asked <br> questions
                <div class="pricingHeader5">Need more help? Contact us at <br>1800-204-1477</div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="pricingHeader9">
                    <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                    <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                    <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                    <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                    <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                    <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                    <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                    <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>
            </div>
        </div>
    </div>
</div>

{{--  --}}

@endsection