<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding">
        @forelse($jobs as $k=>$val)
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mt20">
                <div class="cardBox2">
                    <div class="row">
                        <div class="col-xs-3 paddingRyt0">
                            @if($val->employer_info!=null)
                                <img src="{{checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="cardImg2">
                            @else 
                                <img src="{{url('public/images/default/company_logo.png')}}" class="cardImg2">
                            @endif
                        </div>
                        <div class="col-xs-9">
                            <div class="jobName"><a href="{{route('job-details',['jobid'=>$val->id])}}">{{$val->job_title}}</a></div>
                            <div class="jobDesignation">{{ ($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt. Ltd.' }}</div>
                            <div class="jobLocation"><img src="{{url('public/images/location-icon.png')}}">{{ ($val->country!=null) ? $val->country->name : '' }}</div>
                        </div>
                    </div>
                    <div class="row skillsRow">
                        <div class="col-xs-12 skills">
                                @php $counter=1; @endphp
                                @foreach($val->skills as $data)      
                                    @if(isset($data->skill_keywords) && $counter<4)          
                                        <span> {{$data->skill_keywords->name}}</span>
                                        @php $counter++; @endphp
                                    @endif
                                @endforeach
                            {{-- <span>{{$val->min_experience}}-{{$val->max_experience}} years</span> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 mrgnTp12">
                            <span class="jobAmount">
                                @if($val->salary_min!=null)
                                    <img src="{{url('public/images/doller.png')}}">{{$val->currency}}{{$val->salary_min}} - {{$val->currency}}{{$val->salary_max}}
                                @else
                                    <h5 class="no-salary-text">As per market standard</h5>
                                @endif
                            </span>
                            <span class="jobDate">{!! countDays($val->created_at) !!}</span>
                        </div>
                    </div>

                </div>
            </div>
          @empty 
              <div class="col-xs-12 col-md-12 nopadding">
              <div class=" job-box">
                <div class="row flexbox">
                  
                  <div class="col-xs-12 col-sm-12">
                    
                    <img src="{{url('public/images/default/nojobsfound.png')}}" class="center-block" width="240px" style="margin-top: 5%;">
                    <div class="post-details">
                      
                    </div>
                  </div>
                </div>
                
              </div>
            </div> 
          @endforelse
          </div>
        <div class="row margnBtm30">
            <div class="col-lg-12 mt20">
                <div class="paginationContainer">
                    <div class="custom-pagination">
                        {{ $jobs->appends(['view' =>'grid'])->links() }}
                    </div>
                </div>
            </div>
        </div>
        
<script>
var search='{{$search_tag}}';
var array = search.split(',');
if(search)
{
  $('#search_key').html('');
  $.each( array, function( key, value ) {
    if(value )
    $('#search_key').append('<span>'+value+'</span>|');
  });
}

</script>