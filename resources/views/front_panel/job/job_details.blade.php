@extends('/layouts/front_panel_master')
@section('content')
<div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 jobDetailBox">
            <div class="row paddingJobDetailBox">
                <div class="col-lg-3 text-center">
                    @if($job_detail->employer_info!=null)
                        <img src="{{checkFile($job_detail->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="jobDetailImg">
                    @else 
                        <img src="{{url('public/images/default/company_logo.png')}}" class="jobDetailImg">
                    @endif
                </div>
                <div class="col-lg-9">
                    <div class="jobLabel1">{{($job_detail->employer_info!=null) ? $job_detail->employer_info->comp_name : '' }}</div>
                    <div class="jobLabel2">{{$job_detail->job_title}}</div>
                    <div class="jobLabel3">Lorem ipsum dolor sit amet, eu cibo modus eruditi nam. Cu cetero appetere explicari eam. Usu ne electram disputationi. Erat blandit cotidieque vix ei, cu stet clita democritumnobis dicunt virtute ne sed</div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img class="jobDetailVacancy" src="{{url('public/images/vacancy.png')}}">No. of Vacancy
                               </div>
                               <div class="vacancyNumber">
                                    {{$job_detail->total_vacancy}}
                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img class="jobDetailMoney" src="{{url('public/images/doller.png')}}">Salary Range
                               </div>
                               <div class="vacancyNumber">
                                   {{$job_detail->currency . $job_detail->salary_min}} - {{$job_detail->currency . $job_detail->salary_max}}
                                   {{-- $4000 - $8000 --}}
                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img src="{{url('public/images/location-icon.png')}}" class="jobDetailLocation">Location
                               </div>
                               <div class="vacancyNumber">
                                    {{ ($job_detail->country!=null) ? $job_detail->country->name : '' }}
                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img class="jobDetailExp" src="{{url('public/images/breifcase.png')}}">Experience
                               </div>
                               <div class="vacancyNumber">
                                    {{$job_detail->min_experience}}-{{$job_detail->max_experience}} Years
                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img class="jobDetailApplicants" src="{{url('public/images/document.png')}}">Jobs Applicants
                               </div>
                               <div class="vacancyNumber">
                                    {{count($job_detail->job_applicants)}}
                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img src="{{url('public/images/postedOn.png')}}" class="jobDetailPOstedOn">Posted on
                               </div>
                               <div class="vacancyNumber">
                                    {{dateConvert($job_detail->created_at,'jS M Y')}}
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 jobDescritionCard1">
            <div class="paddingJobDetailBox">
                <div class="jobDescriptionLabel">
                    Job Description
                </div>
                <div class="jobDescription">
                    <p>{!! $job_detail->job_description !!}</p>
                </div>
                <div class="jobDescriptionLabel mrgnTp30">
                    Job Summary
                </div>
                <div class="jobDescription">
                    <span><strong>Industry : </strong></span>
                    <span>{{ ($job_detail->industry!=null) ? $job_detail->industry->name : '' }}</span><br/>
                    <span><strong>Functional Area : </span></strong>
                    <span>{{ ($job_detail->functional_area!=null) ? $job_detail->functional_area->name : ''}}</span><br/>
                    <span><strong>Gender </span>:</strong>
                    <span>{{$job_detail->gender}}</span><br/>
                </div>
                <div class="jobDescriptionLabel mrgnTp30">
                    You can Contact
                </div>
                <div class="jobDescription">
                    @if($job_detail->response_email!='')
                        @php
                            $emails=[];
                            $expEmails = explode(',', $job_detail->response_email);
                        @endphp
                        @if(!empty($expEmails))
                            <strong><span>Email</span>:</strong>
                            @foreach($expEmails as $val)
                                @php $emails[]='<a href="mailto:'.$val.'">'.$val.'</a>';@endphp
                            @endforeach
                        @endif     
                        <span>{!! join(', ',$emails) !!}</span><br/>  
                    @endif
    
                    @if($job_detail->cp_name!='')
                        <strong><span>Person </span>:</strong>
                        <span>{{ $job_detail->cp_title }} {{ $job_detail->cp_name }}</span><br/>
                    @endif

                    @if($job_detail->cp_mobile!='')
                        <strong><span>Contact Number </span>:</strong>
                        <span>+{{ $job_detail->dial_number }}-{{ $job_detail->cp_mobile }}</span><br/>
                    @endif
                    @if($job_detail->walkin_address!='')
                        <strong><span>Address</span>:</strong>
                        <span>{{ $job_detail->walkin_address }}</span><br/>
                    @endif
                </div>
                <div class="jobDescriptionLabel mrgnTp30">
                    Skills
                </div>
                <div class="row skillsRow">
                    <div class="col-xs-12 skills">
                        @if($job_detail->skills!=null)
                            @foreach($job_detail->skills as $val)
                                @if($val->skill_keywords!=null)
                                    <span>{{$val->skill_keywords->name}}</span>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="jobSkillsDesc">
                    Repeatedly dreamed alas opossum but dramatically despite expeditiously that jeepers loosely yikes that as or eel underneath kept and slept compactly far purred sure abidingly up above fitting to strident wiped set way
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 jobDescritionCard2">
            <div class="paddingJobDetailBox text-center">
                <div class="applyNowBox">
                    @if(Auth::check())
                        @if(Auth::user()->role=='2') 
                          @if(jobAppliedOrNot($job_detail->id)==0)  
                            <a class="btn btn-success success-btn searchButton cardOpening" onclick="return confirm('Are you sure you want to apply?')" href="{{url('apply-job/'.$job_detail->id)}}"><img src="{{url('public/images/ap_now@2x.png')}}">Apply Now</a>
                          @else
                              <a class="btn btn-success success-btn searchButton cardOpening" href="#"><img src="{{url('public/images/ap_now@2x.png')}}">Already Applied
                          @endif
                        @endif
                    @else
                        <a class="btn btn-success success-btn searchButton cardOpening" data-toggle="modal" data-target="#loginModal">Apply Now</a>    
                    @endif
                </div>
                <div class="applicationEndStatus">
                    Application end in 5m 13d 22h 10min
                </div>
            </div>
            <div class="mrgnTp20">
                <div id="map"></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 similarJobsHeader">
            <div class="jobDescriptionLabel">
                Similar Jobs You May Like
            </div>
        </div>
        @php $rJobs = getRelatedJobs($job_detail->id);@endphp
        <!-- related jobs -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 jobDescritionCard3">
            @foreach($rJobs as $val)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <a href="{{route('job-details',['jobid'=>$val->id])}}">
                    <div class="jobDetailCardBox2">
                        <div class="row">
                            <div class="col-xs-3 paddingRyt0">
                                @if($val->employer_info!=null)
                                    <img src="{{checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="cardImg2">
                                @else 
                                    <img src="{{url('public/images/default/company_logo.png')}}" class="cardImg2">
                                @endif
                                {{-- <img class="cardImg2" src="{{url('public/images/ola.png')}}"> --}}
                            </div>
                            <div class="col-xs-9">
                                <div class="jobName">{{($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt. Ltd.' }}</div>
                                <div class="jobDesignation">{{$val->job_title}}</div>
                                <div class="jobLocation"><img src="{{url('public/images/location-icon.png')}}">{{ getCityNames($val->jobs_in_city) }} - {{ ($val->country!=null) ? $val->country->name : '' }}</div>
                            </div>
                        </div>
                        <div class="row skillsRow">
                            <div class="col-xs-12 skills">
                                @if($val->skills!=null)
                                    @php $counter=1; @endphp
                                    @foreach($val->skills as $records)
                                        @if($records->skill_keywords!=null && $counter<4)
                                            <span>{{$records->skill_keywords->name}}</span>
                                        @endif
                                        @php $counter++; @endphp
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 mrgnTp12">
                                <span class="jobAmount"><img src="{{url('public/images/doller.png')}}">
                                    {{$val->currency . $val->salary_min}} - {{$val->currency . $val->salary_max}}
                                </span>
                                <span class="jobDate">{!! countDays($val->created_at)!!}</span>
                            </div>
                        </div>
        
                    </div>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 jobDescritionCard2 margintp10">
            <div class="paddingJobDetailBox2">
                <div class="latestNewsBox">
                    @php 
                        $latestNews = getLatestNews();
                    @endphp  
                    <div class="jobDescriptionLabel2">
                        Latest News
                    </div>
                    @forelse($latestNews as $val)
                        <div class="jobDescriptionLabel3">
                            <p>{!! $val->title !!}</p>
                        </div>
                    @empty
                        <div class="jobDescriptionLabel3">
                            <p>No Latest News</p>
                        </div>
                        <li><i class="fa fa-flash"></i> <p>No Latest News</p></li>
                    @endforelse
                </div>
            </div>
        </div>
    </div>


<script>
    function myMap() {
        var myCenter = new google.maps.LatLng(12.893265, 77.622374);
        var mapCanvas = document.getElementById("map");
        var mapOptions = {center: myCenter, zoom: 14};
        var map = new google.maps.Map(mapCanvas, mapOptions);
        var marker = new google.maps.Marker({position:myCenter});
        marker.setMap(map);

        // Zoom to 9 when clicking on marker
        google.maps.event.addListener(marker,'click',function() {
            map.setZoom(9);
            map.setCenter(marker.getPosition());
        });
    }

    function placeMarker(map, location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        var infowindow = new google.maps.InfoWindow({
            content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
        });
        infowindow.open(map,marker);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
        
@endsection