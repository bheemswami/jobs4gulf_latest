@extends('/layouts/front_panel_master')
@section('content')
<style>
body{
    background-color: #FFF !important;
}
</style>
<div class="container">
    <div class="row mrginTpBtm20">
        <div class="col-xs-6 jobs_country_head">
            <h4>Jobs By {{$title or ''}}</h4>
        </div>
        <div class="col-xs-6">
            {{Form::open(array('url'=>route('jobs-by',$type)))}}
            <div class="input-group add-on">
                <input class="form-control searchInputTE" placeholder="Search"  type="text" name="search">
                <div class="input-group-btn">
                    <button class="btn btn-success success-btn searchButton" type="submit">Search</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
    <script>
        function toggleLeftNav(id){
            $("#"+id).toggle();
        }
    </script>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding ">
            @if($title=='Country')
                @foreach($list as $val)
                    <div class="col-sm-3 col-md-3 col-lg-3 mt20">
                        <div class="cardBox">
                            <div class="row">
                                <div class="col-xs-12 text-center margin_btm10">
                                    @if($val->icon!='')
                                        <img class="flagimg" alt="image" src="{{$val->icon}}">
                                    @else
                                        <img class="flagimg" alt="image" src="{{checkFile($val->icon,'uploads/country_img/','small-logo.png')}}">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyName"><a href="{{url('search-jobs?country_ids='.$val->id)}}" >{{$val->name}}</a></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyLoc">({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }})</div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if($title=='City')
                @foreach($list as $val)
                    <div class="col-sm-3 col-md-3 col-lg-3 mt20">
                        <div class="cardBox">
                            <div class="row">
                                <div class="col-xs-12 text-center margin_btm10">
                                    @if($val->icon!='')
                                        <img class="flagimg" alt="image" src="{{checkFile($val->icon,'uploads/city_img/','small-logo.png')}}">
                                    @else
                                        <img class="flagimg" alt="image" src="{{checkFile($val->icon,'uploads/city_img/','small-logo.png')}}">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyName"><a href="{{url('search-jobs?city_ids='.$val->id)}}" >{{$val->name}}</a></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyLoc">({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }})</div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if($title=='Industry')
                @foreach($list as $val)
                    <div class="col-sm-3 col-md-3 col-lg-3 mt20">
                        <div class="cardBox">
                            <div class="row">
                                <div class="col-xs-12 text-center margin_btm10">
                                    @if($val->icon!='')
                                        <img class="flagimg" alt="image" src="{{checkFile($val->icon,'uploads/industry_img/','small-logo.png')}}">
                                    @else
                                        <img class="flagimg" alt="image" src="{{checkFile($val->icon,'uploads/industry_img/','small-logo.png')}}">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyName"><a href="{{url('search-jobs?industry_ids='.$val->id)}}" >{{$val->name}}</a></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyLoc">({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }})</div>
                            </div>
                        </div>
                    </div>
                @endforeach
                    
            @endif
        </div>
    </div>
    <div class="row margnBtm30">
        <div class="col-lg-12 mt20">
            <div class="paginationContainer">
                {{ $list->links() }}
            </div>
        </div>
    </div>
</div>
@endsection