@extends('/layouts/front_panel_master')
@section('content')


<div id="first-slider">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
        <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
           
            <!-- Item 1 -->
             <div class="item active slide1" style="background-image: url({{url('public/img/job-interview-tips.jpg')}});">
                <div class="row"><div class="container">
                   
                </div></div>
             </div>  
            <!-- Item 2 -->
          <div class="item  slide2" style="background-image: url({{url('public/img/job-interview.jpg')}});">

                <div class="row"><div class="container">
                   
                </div></div>
             </div>  
            <!-- End Item 4 -->
     <div class="item  slide3" style=" background-image: url({{url('public/img/Business-Photos-1.jpg')}});">

                <div class="row"><div class="container">
                   
                </div></div>
             </div> 
        </div> 
        <!-- End Wrapper for slides-->
         <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
        </a>
    </div>
</div>


<section style="margin-top: 55px;margin-bottom: 20px;">
<div class="container">
<div class="row">
<div class="col-md-4">
 <h2 class="title-widget-sidebar note2 color-is" style="color:black;margin-bottom: 42px;">CONTACT US </h2>
<img src="" style=" margin-top: -20px;margin-bottom: 20px;width: 68px;height: 68px;" alt="Company Logo">
  <div class="conDiv">
      <span class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
      <span>
        No Data Found (Contact Us)
      </span>
  </div>
                    
</div>
<div class="col-md-5">
<h2 class="title-widget-sidebar note2 color-is" style="color:black;margin-bottom: 42px;">ABOUT US </h2>
<p style="text-align: justify;"> No Data Found (About Us)</p>

<!--button type="button" class="btn btn-primary">Read More</button-->
<button class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="float: right;">Read More</button>
</div>
<div class="modal fade" id="myModal1" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4> ABOUT US </h4>
      </div>
      <div class="modal-body">
     <p>About Us Content</p>
      </div>
    
    </div>
  </div>
</div>
<div class="col-md-3">
<a href=""><img src="" class="img-responsive"></a>
<img style="width: 284px;margin-top: 47px;" src="{{url('public/img/Business-Free-Download-PNG.png')}}">
<button type="button" class="btn btn-primary">Drop yOur CV for Further Opening</button>
<!-- <button type="button" class="btn btn-primary">Careers</button> -->
</div>

</div>
</div>
</section>
<section style="    margin-bottom: 211px;">
<div class="container">
<div class="row">
<h2 class="title-widget-sidebar note2 color-is" style="color:black;    margin-bottom: 42px;">CAREERS
</h2>

<div class="col-md-12">
  <div class="JOBS">
      <div class="media" style="background: #f5f5f5;">
        <div class="media-bodyw">
          <h4>Job_title (min_experience - max_experience years)</h4>
          <p>Company Name</p>
            <ul class="detail-li">
              <li><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp; <p>Posted by</p></li>
              <li><p>Nakshatra Devaraj</p></li>
            </ul>
        </div>
      </div>
      <br>
      <li style="list-style-type: none;">
        <div class="media">           
          <div class="media-body">
            <p>Job Description &nbsp;&nbsp;<a href="#">Read More</a></p>
          </div>
          <div class="media-right align-self-center">
              <div class="Viewbt">
                <a href="{{route('job-details')}}"><button type="button" class="btn btn-primary">View Deatil</button></a>
                <button type="button" class="btn btn-primary">Apply</button>
              </div>
          </div>
        </div>      
      </li>

      <div class="media" style="margin-top: 10px;background: #ffffff;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;">
        <div class="media-bodyy">
          <ul class="media-fa">
            <li><i class="fa fa-briefcase" aria-hidden="true"></i><p>&nbsp;&nbsp; min_experience - max_experience yrs</p></li>
            <li><i class="fa fa-map-marker" aria-hidden="true"></i><p>&nbsp;&nbsp; city_name / country_name</p></li>
            <li><i class="fa fa-map-marker" aria-hidden="true"></i><p>&nbsp;&nbsp; employment_status</p></li>
            <li><i class="fa fa-inr" aria-hidden="true"></i><p>&nbsp;&nbsp; annual_salary_min - annual_salary_max Per Month</p></li>
            <!-- <li><i class="fa fa-map-marker" aria-hidden="true"></i><p>&nbsp;&nbsp;  10 Applications</p></li> -->
          </ul>
          <ul class="detail-liq">
            <li><p>{{time_elapsed_string(date('d-m-Y H:i:s'))}}</p> </li>
          </ul>
        </div>       
      </div>
  </div>
</div>

</div>
</div>
</section>

<script>
(function( $ ) {

    //Function to animate slider captions 
  function doAnimations( elems ) {
    //Cache the animationend event in a variable
    var animEndEv = 'webkitAnimationEnd animationend';
    
    elems.each(function () {
      var $this = $(this),
        $animationType = $this.data('animation');
      $this.addClass($animationType).one(animEndEv, function () {
        $this.removeClass($animationType);
      });
    });
  }
  
  //Variables on page load 
  var $myCarousel = $('#carousel-example-generic'),
    $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
    
  //Initialize carousel 
  $myCarousel.carousel();
  
  //Animate captions in first slide on page load 
  doAnimations($firstAnimatingElems);
  
  //Pause carousel  
  $myCarousel.carousel('pause');
  
  
  //Other slides to be animated on carousel slide event 
  $myCarousel.on('slide.bs.carousel', function (e) {
    var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
    doAnimations($animatingElems);
  });  
    $('#carousel-example-generic').carousel({
        interval:3000,
        pause: "false"
    });
  
})(jQuery); 

</script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>

@endsection