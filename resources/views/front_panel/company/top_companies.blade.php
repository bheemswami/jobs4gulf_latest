@extends('/layouts/front_panel_master')
@section('content')

<section id="inner-banner" style="background: url({{url('public/images/inner-banner.jpg')}}) no-repeat center top;">
  <div class="overlay">
  <div class="container text-center table-div text-white">
    <div class="banner-content table-cell-div">Experience dynamic hiring with our diverse solutions, designed to meet your recruitment needs effectively. To proceed further complete the registration form given below. If you are an existing user Login Here. Job seekers <a href="" class="">click here</a> to login.</div>
  </div>
</div>
  </section>

<section class="main-inner-page lite-greyBg">
  <section id="search-section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-2">
          <img src="{{url('public/images/left-side-img.jpg')}}" class="img-responsive center-block">
        </div>
        
        <div class="col-xs-12 col-sm-8 mid-sec-top">
          <div class="block pt-0">          
            <div class="section-title text-center">
                <img src="{{url('public/images/user-icon1.png')}}" width="60px">
                <h2><span class="textgreen">Post Job</h2>
            </div>
            {{ Form::open(array('url'=>route('save-candidate'),'class'=>'form','id'=>'registration-form'))}}
            <div class="form-section">
              <div class="form-title">
                <h4>Job Details</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Title/Designation</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::text('job_title',null,array('class'=>"form-control",'id'=>"job_title",'placeholder'=>"")) }}                 
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Description</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::textarea('job_description',null,array('class'=>"form-control",'id'=>"job_description",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">No. of vacancies</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::number('total_vacancy',null,array('class'=>"form-control",'id'=>"total_vacancy",'placeholder'=>"")) }}                 
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Monthly Salary</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::number('monthly_salary',null,array('class'=>"form-control",'id'=>"monthly_salary",'placeholder'=>"")) }}                 
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Location</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::text('job_location',null,array('class'=>"form-control",'id'=>"job_location",'placeholder'=>"")) }}                 
                </div>
              </div>

            </div>
            
            <div class="form-section">
              <div class="form-title">
                <h4>Filter Options For Better Results</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Education Qualification</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::text('education',null,array('class'=>"form-control",'id'=>"",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::select('industry_type',[],null,array('class'=>"form-control",'id'=>"industry_type",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Functional Area</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::select('functional_area',[],null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"")) }}
                </div>
              </div>

               <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Key Skills</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::textarea('key_skills',null,array('class'=>"form-control",'id'=>"key_skills",'placeholder'=>"")) }}
                </div>
              </div>
            </div>
                        
            <div class="form-section">
              <div class="form-title">
                <h4>Desired Candidates Profile</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Gender</label>
                <div class="col-xs-12 col-sm-9">
                  <div class="row">                    
                    <div class="radioStyle2">
                      <label for="male">{{ Form::radio('gender','Male',false,array('class'=>"form-control",'id'=>"male")) }}Male</label>
                    </div>                    
                    <div class="radioStyle2">
                      <label for="female">{{ Form::radio('gender','Female',false,array('class'=>"form-control",'id'=>"female")) }}Female</label>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Work Experience</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::number('exp_year',null,array('class'=>"form-control",'id'=>"exp_year",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Nationality</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('nationality',null,array('class'=>"form-control",'id'=>"nationality",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Current Location of the Candidate</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('current_location',null,array('class'=>"form-control",'id'=>"current_location",'placeholder'=>"")) }}
                </div>
              </div>
            </div>
             <div class="form-section">
              <div class="form-title">
                <h4>Manage Response</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Gender</label>
                <div class="col-xs-12 col-sm-9">
                  <div class="row">                    
                    <div class="radioStyle2">
                      <label for="male">{{ Form::radio('reponce_manage','Male',false,array('class'=>"form-control",'id'=>"")) }}On Response Manager </label>
                    </div>                    
                    <div class="radioStyle2">
                      <label for="female">{{ Form::radio('reponce_manage','Female',false,array('class'=>"form-control",'id'=>"")) }}On Response Manager and on Email</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row mt-20">
              <div class="col-sm-9 col-sm-offset-3 nopadding">
                <button type="submit" class="newfclassub">Post This Job</button>
              </div>
            </div>
          </form>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-2">
          <img src="{{url('public/images/left-side-img.jpg')}}" class="img-responsive center-block">
        </div>
        
        <div class="col-xs-12">
          <img src="{{url('public/images/bottom-adv.jpg')}}" class="img-responsive center-block">
        </div>
      </div>
    </div>
  </section>
</section>

@endsection