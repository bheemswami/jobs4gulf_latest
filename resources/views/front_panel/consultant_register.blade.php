@extends('/layouts/front_panel_master')
@section('content')

<section id="inner-banner" style="background: url({{url('public/images/inner-banner.jpg')}}) no-repeat center top;">
  <div class="overlay">
  <div class="container text-center table-div text-white">
    <div class="banner-content table-cell-div">Experience dynamic hiring with our diverse solutions, designed to meet your recruitment needs effectively. To proceed further complete the registration form given below. If you are an existing user Login Here. Job seekers <a href="" class="">click here</a> to login.</div>
  </div>
</div>
  </section>

<section class="main-inner-page lite-greyBg">
  <section id="search-section">
    <div class="container">
      <div class="row">
       
        <div class="col-xs-12 col-sm-10 mid-sec-top">
           <div class="section-title text-center">
                <img src="{{url('public/images/user-icon1.png')}}" width="60px">
                <h2><span class="textgreen">Become an Consultant</h2>
            </div>
          <div class="block pt-0">          
           
            {{ Form::open(array('url'=>route('save-consultant'),'class'=>'form','id'=>'registration-consultant-form','files'=>true))}}
            {{ Form::hidden('comp_type',1) }}
            <div class="form-section">
              <div class="form-title">
                <h4 class="logininfo">Login Information</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Email ID</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::email('email',null,array('class'=>"form-control",'id'=>"emp-email",'placeholder'=>"This Email Will Be Your Username")) }}                 
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Password</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::password('password',array('class'=>"form-control",'id'=>"emp-password",'placeholder'=>"Please enter your password")) }}
                </div>
              </div>
            </div>
            
            <div class="form-section">
              <div class="form-title">
                <h4 class="contactinfo">Contact Information</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Person</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::text('contact_person',null,array('class'=>"form-control",'id'=>"contact_person",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Designation</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::text('designation',null,array('class'=>"form-control hintable",'hint-class'=>"",'id'=>"designation",'placeholder'=>"Enter Current Position")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Number</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                 <div class="form-row">
                   <div class="col-xs-12 col-sm-4 form-child-block">
                      {{ Form::select('country_code',$country_codes,null,array('class'=>"form-control",'id'=>"country_code",'placeholder'=>"Select")) }} 
                    </div>
                    <div class="col-xs-12 col-sm-8 form-child-block">
                      {{ Form::text('phone',null,array('class'=>"form-control",'id'=>"phone",'placeholder'=>"")) }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
                        
            <div class="form-section">
              <div class="form-title">
                <h4 class="factory">Company Information</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Name</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('comp_name',null,array('class'=>"form-control",'id'=>"company_name",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Address</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::textarea('comp_address',null,array('class'=>"form-control",'id'=>"company_address",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Website</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('comp_website',null,array('class'=>"form-control",'id'=>"company_website",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Country</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                 <div class="form-row">
                   <div class="col-xs-12 col-sm-4 form-child-block">
                      {{ Form::select('comp_country',$country,null,array('class'=>"form-control",'id'=>"country",'placeholder'=>"Select")) }} 
                    </div>
                    <div class="col-xs-12 col-sm-8 form-child-block">
                      {{ Form::select('comp_city',[],null,array('class'=>"form-control",'id'=>"city",'placeholder'=>"Select")) }} 
                    </div>
                  </div>
                </div>
              </div>

                <div class="form-group row">
                 <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    {{ Form::select('comp_industry',[],null,array('class'=>"form-control",'id'=>"industry_type",'placeholder'=>"Select")) }} 
                  </div>
                </div>

                <div class="form-group row">
                 <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Key Skills</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                     {{ Form::text('skills',null,array('class'=>"form-control hintable",'hint-class'=>"keyskills_s",'id'=>"key_skills",'data-toggle'=>"tooltip",'placeholder'=>"Key Skills")) }}
                  </div>
                </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Profile</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::textarea('comp_profile',null,array('class'=>"form-control",'id'=>"company_profile",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">About Us</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::textarea('about',null,array('class'=>"form-control",'id'=>"company_about",'placeholder'=>"")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Logo</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="box">
                    {{ Form::file('comp_img',array('class'=>"upload inputfile inputfile-6",'id'=>"logo")) }}
                    <label for="logo"><strong>Choose file</strong><span>No file choosen</span></label>
                </div>
                  <div class="form-field-note">
                    Upload only Image (.png/.jpg/.jpeg) formats.
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-9 col-sm-offset-3">
                <label class="checkbox-btn full-width mr-0">
                  <input type="checkbox" name="terms_and_conditions" value="true" class="" checked="checked"> I have read the <a href="#">Terms and Conditions</a> and agree to be bound by them.
                </label>
              </div>
            </div>

            <div class="form-group row mt-20">
              <div class="col-sm-9 col-sm-offset-3 nopadding">
                <button type="submit" class="newfclassub">Register</button>
              </div>
            </div>
          </form>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-2">
          <img src="{{url('public/images/left-side-img.jpg')}}" class="img-responsive center-block">
        </div>
        
        
      </div>
    </div>
  </section>
</section>
<script>
 $(function(){
  
    $('#company_name').selectize({
        maxItems: 1,
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        create: true,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'/api/company-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });
    $('#designation').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'/api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });

    $('#industry_type').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: {!! $industry_list !!},
    });

    $('#country').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: {!! json_encode($country) !!},
        onItemAdd:function(value, $item){
          var countryId=value;
          $.ajax({
                url: base_url+'/api/city-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    country_id: countryId
                },           
                success: function(res) {                    
                    rePopulateCity(res);
                },
                error: function(error) {
                   rePopulateCity();
                }
            });
        },
        onDelete:function(value){
           rePopulateCity();
        }
    });
  
    $('#city').selectize({
      plugins: ['remove_button'],
      maxItems: 1,
      valueField: 'id',
      labelField: 'name',
      searchField: 'name',
      create: false,
      options: [],
    });
    $('#key_skills').selectize({
      plugins: ['remove_button'],
      maxItems: 15,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
      options: {!! json_encode($skill_keywords) !!},
    
  });
function rePopulateCity(data=null){
  var selectize = $("#city")[0].selectize; 
  selectize.clear();
  selectize.clearOptions();
  selectize.renderCache['option'] = {};
  if(data!=null)
    selectize.addOption(data);
}
  });
</script>
@endsection