@php 
$settings = getSettings();
//session()->put('CALL_US',$settings->mobile);
@endphp
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Jobs4Gulf</title>
    <script>
      var base_url="{{url('/').'/'}}";
      var csrf_token="{{ csrf_token() }}";
      var globalFunc = {};
      var globalVar = {};
      var currentThis = '';
      var minChars = 2;
    </script>
    <link rel="stylesheet" href="{{URL::asset('public/plugins/selectize/selectize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/owlcarousel-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/style-new.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/media.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/bootstrap-multiselect.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/plugins/Font-Awesome/css/font-awesome.css')}}" />
    <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/css/slick.css')}}"  />        
    <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/css/slick-theme.css')}}"  /> 
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/plugins/bootstrap/css/consultant-detail-bootstrap.css')}}"  /> 
    <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/css/consultant-detail.css')}}"  /> 

    <script src="{{URL::asset('public/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{URL::asset('public/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('public/js/owl.carousel.min.js')}}"></script>
    <script src="{{URL::asset('public/js/bootstrap-multiselect.js')}}"></script>
    <script src="{{URL::asset('public/js/custom.js')}}"></script>
    <script src="{{URL::asset('public/js/smoothscroll.js')}}"></script>
    <script src="{{URL::asset('public/js/jquery.validate.min.js')}}" type="text/javascript"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{URL::asset('public/plugins/selectize/selectize.js')}}"></script>
</head>

<body>

<!-- Header Start -->

<!-- Header End -->
  
  @include('layouts.flash_msg')
  @yield('content')
<!-- Start Modal Candidate Login-->
  <div class="modal fade" id="userLoginModal" tabindex="-1" role="dialog" aria-labelledby="userLoginModalLabel" aria-hidden="true">
    <div class="modal-dialog login-modal" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Jobseeker Login</h4>
          </div>
        <div class="modal-body flexbox">
            <div class="col-xs-12 col-sm-4 hidden-xs" style="background: url('{{url('public/images/login-img.jpg')}}') no-repeat center top;margin-top: -1px; background-size:cover;">
             
              <div class="socialLogin">
                <p class="txtSignUp">Log In With</p>
                <a class="socialBtn fbLogin ico" href="#" id="fbLogin"><img src="{{url('public/images/facebook.png')}}">
                </a>
                <a class="socialBtn gpLogin ico" href="#" id="gpLogin"><img src="{{url('public/images/gplus.png')}}">
                </a>
              </div>

            </div>
            <div class="col-xs-12 col-sm-8 right">
            <div id="success_error_msg" class="jobseeker-msg"></div>
              <form class="login-form form-1" id="login-form" autocomplete="on">
                <div class="form-group clearfix">
                  <label class="col-xs-12 nopadding">Username</label>
                  <div class="col-xs-12 nopadding">
                    <input type="email" name="candidate_username" value="role2@gmail.com" id="cand_login_username" class="form-control" placeholder="Enter Your Username" data-toggle="tooltip" title="Please enter your E-mail ID" data-validation="email" data-validation-error-msg="Please enter email ID" />
                  </div>
                </div>
                <div class="form-group clearfix">
                  <label class="col-xs-12 nopadding">Password</label>
                  <div class="col-xs-12 nopadding">
                    <input type="password" name="candidate_password" value="12345678" id="cand_login_password" class="form-control" placeholder="Enter Your Password" data-toggle="tooltip" name="password" data-validation="required" data-validation-error-msg="Please enter your password"/>
                  </div>
                </div>
                <div class="form-group clearfix">
                  <button type="submit">Login</button>
                </div>
                <a href="{{route('forgot-password')}}">Forgot Password</a>
              </form>
            </div>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal Candidate Login-->

<!-- Start Modal Employer Login-->
  <div class="modal fade" id="employerLoginModal" tabindex="-1" role="dialog" aria-labelledby="employerLoginModalLabel" aria-hidden="true">
    <div class="modal-dialog login-modal" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Employer Login</h4>
          </div>
        <div class="modal-body flexbox">
            <div class="col-xs-12 col-sm-4 hidden-xs" style="background: url('{{url('public/images/login-img.jpg')}}') no-repeat center top;margin-top: -1px; background-size:cover;">
             
              <div class="socialLogin">
                <p class="txtSignUp">Log In With</p>
                <a class="socialBtn fbLogin ico" href="#" id="fbLogin"><img src="{{url('public/images/facebook.png')}}">
                </a>
                <a class="socialBtn gpLogin ico" href="#" id="gpLogin"><img src="{{url('public/images/gplus.png')}}">
                </a>
              </div>

            </div>
            <div class="col-xs-12 col-sm-8 right">
            <div id="success_error_msg" class="employer-msg"></div>
              <form class="login-form form-2" id="login-form" autocomplete="on">
                <div class="form-group clearfix">
                  <label class="col-xs-12 nopadding">Username</label>
                  <div class="col-xs-12 nopadding">
                    <input type="email" name="employer_username" value="bheem@gmail.com" id="emp_login_username" class="form-control" placeholder="Enter Your Username" data-toggle="tooltip" title="Please enter your E-mail ID" data-validation="email" data-validation-error-msg="Please enter email ID" />
                  </div>
                </div>
                <div class="form-group clearfix">
                  <label class="col-xs-12 nopadding">Password</label>
                  <div class="col-xs-12 nopadding">
                    <input type="password" name="employer_password" value="12345678" id="emp_login_password" class="form-control" placeholder="Enter Your Password" data-toggle="tooltip" name="password" data-validation="required" data-validation-error-msg="Please enter your password"/>
                  </div>
                </div>
                <div class="form-group clearfix">
                  <button type="submit">Login</button>
                </div>
                <a href="{{route('forgot-password')}}">Forgot Password</a>
              </form>
            </div>
        </div>
      </div>
    </div>
  </div>
<!-- End Modal Employer Login-->
@php
  $countryList=getFooterData('country');
  $industryList=getFooterData('industry');
@endphp
<footer id="footer" class="inner-footer">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
        <h3>Jobs By</h3>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">

          <h4>Location</h4>
          <ul>            
            @foreach($countryList as $val)
              <li><a href="{{url('search-jobs?country_ids='.$val->id)}}" >Jobs in {{$val->name}}</a></li>
            @endforeach
            <li class="footer-menu-more"><a href="{{route('jobs-by',['country'])}}">More</a></li>
          </ul>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
          <h4>Category</h4>
          <ul>
            @foreach($industryList as $val)
              <li><a href="{{url('search-jobs?industry_ids='.$val->id)}}" > {{$val->name}}</a></li>
            @endforeach
           <li class="footer-menu-more"><a href="{{route('jobs-by',['industry'])}}">More</a></li>
          </ul>
        </div>
        
      </div>

      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
        <h3>Users</h3>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
          <h4>Job Seekers</h4>
          <ul>
          @if(Auth::check() && Auth::user()->role==2)
              <li><a href="#">{{ucfirst(Auth::user()->name)}}</a>
              <li><a href="{{route('logout')}}">Logout</a></li>
          @else 
              <li><a href="#" data-toggle="modal" data-target="#userLoginModal">Login</a></li>
              <li><a href="{{route('candidate-register')}}">Register</a></li>
          @endif 
          </ul>         
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
          <h4>Employers</h4>
          <ul>
            @if(Auth::check() && Auth::user()->role==2)
              <li><a href="#">{{ucfirst(Auth::user()->name)}}</a>
              <li><a href="{{route('logout')}}">Logout</a></li>
            @else 
                <li><a href="#" data-toggle="modal" data-target="#employerLoginModal">Login</a></li>
                <li><a href="{{route('employer-register')}}">Register</a></li>
            @endif 
            <li><a href="{{route('our-plans')}}">Our Plans</a></li>
          </ul>
        </div>
        
      </div>
      
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 nopadding right">
        <h3>Jobs4Gulf</h3>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
          <h4>Quick Links</h4>
          <ul>
            <li><a href="{{route('about-us')}}">About Us</a></li>
            <li><a href="{{route('contact-us')}}">Contact Us</a></li>
            <li><a href="{{route('site-map')}}">Site Map</a></li>
          </ul>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding footer-social">
            <h4>Follow Us</h4>
            <ul>
              @if($settings->fb_link!='') <li><a target="_blank" href="{{$settings->fb_link}}"><img src="{{url('public/images/facebook.png')}}"></a></li> @endif
              @if($settings->twitter_link!='')<li><a target="_blank" href="{{$settings->twitter_link}}"><img src="{{url('public/images/twitter.png')}}"></a></li> @endif
              @if($settings->gplus_link!='')<li><a target="_blank" href="{{$settings->gplus_link}}"><img src="{{url('public/images/gplus.png')}}"></a></li> @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="clearfix footer-bottom">
        <div class="text-center"> All rights reserved © {{date('Y')}} Jobs4Gulf</div>
      </div>
    </div>
  </div>
</footer>
<script src="{{URL::asset('public/js/custom-file-input.js')}}"></script>
<script src="{{URL::asset('public/js/frontpanel.js')}}"></script>
<script src="{{URL::asset('public/js/global_custom.js')}}"></script>
<script src="{{URL::asset('public/js/slick.js')}}" type="text/javascript"></script>
<!-- Plugin JavaScript -->
    <script src="{{URL::asset('public/plugins/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Custom scripts for this template -->
    <script src="{{URL::asset('public/js/consultant-detail.js')}}"></script>
 <script type="text/javascript">
        $(document).ready(function(){
            $(".lazy").slick({
            lazyLoad: 'ondemand', // ondemand progressive anticipated
            infinite: true,
            dots: true,
            arrows : false,
            autoplay: true,
          });
        })
    $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 619;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Click here for more details...";
    var lesstext = "Show less";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="readmorecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="readmorelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".readmorelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>
</body>

</html>