@forelse($consultants as $k=>$val)
    <div class="col-sm-6 col-md-6 col-lg-4 mt20">
        <div class="cardBox" style="min-height:282px;">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <img class="cardImg" src="{{checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')}}"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center cardCompanyName"><a href="consultant-detail/{{$val->id}}"{{ substr($val->comp_name,0,50) }}</a></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center cardCompanyLoc2">{{$val->country->name}}</div>
            </div>
            <div class="row  text-center">
                <div class="col-xs-12 text-center cardCompanyOpenings2">
                @if($val->job_post!=null && count($val->job_post)>0)
                        <a  class="btn btn-green" href="consultant-detail/{{$val->id}}">{{ count($val->job_post) }}>OPENINGS</a>
                    {{-- <a href="{{url('companies/'.$val->id)}}">{{ count($val->job_post) }} OPENING</a> --}}
                @else
                    <a href="consultant-detail/{{$val->id}}" class="btn-green" >NO OPENING</a>
                @endif
                    {{-- <a href="consultant-detail/{{$val->id}}" class="btn-green" type="submit">{{count($val->job_post)}} OPENING</a> --}}
                </div>
            </div>


        </div>
    </div>
@empty
    <div class="col-xs-12 col-md-12 nopadding">
              <div class=" job-box">
                <div class="row flexbox">
                  
                  <div class="col-xs-12 col-sm-12">
                    
                    <img src="{{url('public/images/default/nojobsfound.png')}}" class="center-block" width="240px" style="margin-top: 5%;">
                    <div class="post-details">
                      
                    </div>
                  </div>
                </div>
                
              </div>
            </div>  
@endforelse         
<div class="row margnBtm30">
    <div class="col-lg-12 mt20">
        <div class="paginationContainer">
            {{$consultants->links()}}
        </div>
    </div>
</div>
<script>

var search='{{$search_tag}}';
var array = search.split(',');
if(search)
{
    $('#search_key').html('');
    $.each( array, function( key, value ) {
        if(value)
            $('#search_key').append('<span>'+value+'</span>|');
    });
}
</script>
                             