@extends('/layouts/front_panel_master')
@section('content')
<style>
    body{
        background-color: white !important;
    }
    .leftSearchBox {
        border-radius: 5px;
        box-shadow: rgba(0,0,0,0.1) 0 0 20px;
    }
    .cardBox3:hover {
        border-top: none !important;
        background-color:white !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20">
            <div class="leftSearchBox">
                <div class="paddingSearchBox">
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu">Job</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a  href="{{route('applied-jobs')}}"><img class="imgBreifcae" src="{{url('public/images/job4.png')}}">Applied Jobs</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a class="activeTab" href="{{route('last-activity')}}"><img class="imgBreifcae" src="{{url('public/images/job1.png')}}">Last Activity</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="{{route('recommended-jobs')}}"><img class="imgBreifcae" src="{{url('public/images/job2.png')}}">Recommended Jobs</a>
                    </div>
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu2">Account</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a  href="{{route('candidate-dashboard')}}"><img class="imgBreifcae" src="{{url('public/images/job3.png')}}">Profile</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding mt20 marginBtm100" >
                <div class="col-lg-12 nopadding nomrgn recommendedJobsHeader recommendedJobsHeader2">
                    Activity
                </div>
                <div class="col-lg-12 stepper d-flex flex-column">
                    <div class="d-flex mb-1 active">
                        <div class="d-flex flex-column pr-4 align-items-center pd35">
                            <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1 roundCirclePadding"></div>
                            <div class="line h-100"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard9">
                            <div class="activityHeader">
                                <img class="imgBreifcae" src="{{url('public/images/job4.png')}}"> Someone has give you a surprise
                            </div>
                            <div class="activityDesc">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            </div>
                            <div class="pdlt20">
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/calendar@2x.png')}}">8 July 2018</span>
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/clockGreen.png')}}">3:00 PM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 stepper d-flex flex-column">
                    <div class="d-flex mb-1">
                        <div class="d-flex flex-column pr-4 align-items-center pd35">
                            <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1 roundCirclePadding"></div>
                            <div class="line h-100"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard9">
                            <div class="activityHeader">
                                <img class="imgBreifcae" src="{{url('public/images/job4.png')}}"> Someone has give you a surprise
                            </div>
                            <div class="activityDesc">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            </div>
                            <div class="pdlt20">
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/calendar@2x.png')}}">8 July 2018</span>
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/clockGreen.png')}}">3:00 PM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 stepper d-flex flex-column">
                    <div class="d-flex mb-1">
                        <div class="d-flex flex-column pr-4 align-items-center pd35">
                            <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1 roundCirclePadding"></div>
                            <div class="line h-100"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard9">
                            <div class="activityHeader">
                                <img class="imgBreifcae" src="{{url('public/images/job4.png')}}"> Someone has give you a surprise
                            </div>
                            <div class="activityDesc">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            </div>
                            <div class="pdlt20">
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/calendar@2x.png')}}">8 July 2018</span>
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/clockGreen.png')}}">3:00 PM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 stepper d-flex flex-column">
                    <div class="d-flex mb-1">
                        <div class="d-flex flex-column pr-4 align-items-center pd35">
                            <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1 roundCirclePadding"></div>
                            <div class="line h-100"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard9">
                            <div class="activityHeader">
                                <img class="imgBreifcae" src="{{url('public/images/job4.png')}}"> Someone has give you a surprise
                            </div>
                            <div class="activityDesc">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            </div>
                            <div class="pdlt20">
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/calendar@2x.png')}}">8 July 2018</span>
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/clockGreen.png')}}">3:00 PM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 stepper d-flex flex-column">
                    <div class="d-flex mb-1">
                        <div class="d-flex flex-column pr-4 align-items-center pd35">
                            <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1 roundCirclePadding"></div>
                            <div class="line h-100"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard9">
                            <div class="activityHeader">
                                <img class="imgBreifcae" src="{{url('public/images/job4.png')}}"> Someone has give you a surprise
                            </div>
                            <div class="activityDesc">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            </div>
                            <div class="pdlt20">
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/calendar@2x.png')}}">8 July 2018</span>
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/clockGreen.png')}}">3:00 PM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 stepper d-flex flex-column lastColumn">
                    <div class="d-flex mb-1">
                        <div class="d-flex flex-column pr-4 align-items-center pd35">
                            <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1 roundCirclePadding"></div>
                            <div class="line h-100"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard9">
                            <div class="activityHeader">
                                <img class="imgBreifcae" src="{{url('public/images/job4.png')}}"> Someone has give you a surprise
                            </div>
                            <div class="activityDesc">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                            </div>
                            <div class="pdlt20">
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/calendar@2x.png')}}">8 July 2018</span>
                                <span class="activityDate"><img class="imgActivityDate" src="{{url('public/images/clockGreen.png')}}">3:00 PM</span>
                            </div>
                        </div>
                    </div>
                </div>
    
    
    
    
    
    
    
            </div>
    </div>
</div>



@endsection