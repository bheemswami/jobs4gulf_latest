
@extends('/layouts/front_panel_master')
@section('content')

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registerHereBox">
  <div class="col-md-3 col-lg-3"></div>
  <div class="col-md-6 col-lg-6">
    {{ Form::model($user,array('url'=>route('candidate-profile-update'),'class'=>'dasboard-form','id'=>'canditate-update-form','files'=>true))}}            
      <div class="text-center registerHereText">REGISTER HERE</div>
      
      <div class="registerHereSubText">Account Information</div>
      <div class="form-group marginT20">
           {{ Form::email('email',null,array('class'=>"form-control registerHereSubTextInput ic_email",'id'=>"candidate_email",'placeholder'=>"Enter your email address*")) }}
      </div>
      <div class="form-group marginT20">
           {{ Form::password('password',array('class'=>"form-control registerHereSubTextInput pwd ic_psswrd",'id'=>"candidate_password",'placeholder'=>"Enter Password*")) }}
          {{-- <input type="password" placeholder="Create Password*" class="form-control registerHereSubTextInput" id="password"> --}}
          
      </div>
      <div class="form-group marginT20">
           {{ Form::password('confirm_password',array('class'=>"form-control registerHereSubTextInput pwd ic_psswrd",'id'=>"confirm_password",'placeholder'=>"Confirm Password*")) }}
          {{-- <input type="password" placeholder="Confirm Password*" class="form-control registerHereSubTextInput" id="passwordcnf"> --}}
      </div>

      <div class="registerHereSubText2">Your Personnel Details</div>

      <div class="row marginT20">
          <div class="col-xs-2 pdlt0">
               {{ Form::select('name_title',config('constants.name_titles'),null,array('class'=>"form-control resp",'id'=>"name_title","style"=>"padding: 0px 12px;")) }} 
              {{-- <select class="form-control resp" id="select" style="padding: 0px 12px;">
                  <option>MR.</option>
                  <option>MRS.</option>
                  <option>MS.</option>
              </select> --}}
          </div>
          <div class="col-xs-10 pdrt0">
              {{-- <input type="text" placeholder="Enter Full Name" class="form-control registerHereSubTextInput" id="name"> --}}
              {{ Form::text('name',null,array('class'=>"form-control registerHereSubTextInput ic_user",'hint-class'=>"fname_s",'id'=>"name",'data-toggle'=>"tooltip",'placeholder'=>"Enter Full Name*")) }}
           </div>
      </div>
      <div class="form-group marginT20">
           {{ Form::text('dob',dateConvert($user->dob,'d-m-Y'),array('class'=>"form-control dob_pick registerHereSubTextInput ic_dob",'id'=>"datepicker","readonly"=>true,'autocomplete'=>'off',"placeholder"=>"Date of Birth*")) }}
          {{-- <input type="password" placeholder="Date of Birth" class="form-control registerHereSubTextInput" id="dob"> --}}
          <span id="errorDOB"></span>
      </div>
      
      <div class="form-group marginT20">
           <input type="hidden" name="check" id="check" value="0" >
           {{Form::hidden('country_code',null,array('id'=>'country_code'))}}
           {{-- <input type="hidden" name="country_code" id="country_code" value="0" > --}}
           {{Form::text('phone',$user->mobile,array('id'=>'contact_number','class'=>'form-control registerHereSubTextInput contact_number','placeholder'=>"Phone No.*"))}}
           {{-- <input type="text" name="phone" id="contact_number" class="form-control registerHereSubTextInput contact_number" placeholder="Phone No.*" > --}}
      </div>
      <div class="row marginT20">
          <div class="col-xs-6">
               {{ Form::select('country_id',['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,array('class'=>"form-control ic_cuntry",'id'=>"country",'placeholder'=>"Select Country*")) }} 
          </div>
          <div class="col-xs-6">
               {{ Form::select('city',[],null,array('class'=>"form-control ic_city",'id'=>"city",'placeholder'=>"Enter City*")) }}
               <span id="errorCity"></span>
          </div>
      </div>
      <div class="form-group marginT20">
         <!--  <input type="password" placeholder="Select Nationality" class="form-control registerHereSubTextInput"> -->
         {{ Form::select('nationality',config('constants.nationality'),null,array('class'=>"form-control ic_nation",'id'=>"nationality",'placeholder'=>"Select Nationality*")) }} 
      </div>
      

       <div class="registerHereSubText2">Your Education Details</div>
       <div class="form-group marginT20">
           {{ Form::select('basic_course_id',$basic_courses,$user->education->basic_course_id,array('class'=>"form-control registerHereSubTextInput ic_bsic_edu","id"=>"basic_course",'placeholder'=>"Basic Education*")) }} 
       </div>
       <div class="basic-elem hide-elem">
           <div class="form-group marginT20">
               {{ Form::select('basic_specialization_id',[],$user->education->basic_specialization_id,array('class'=>"form-control ic_special","id"=>"basic_specialization",'placeholder'=>"Specialization*")) }} 
           </div>
           <div class="form-group marginT20">
               {{ Form::select('basic_comp_year',getDigitRange(1960,date('Y')),$user->education->basic_comp_year,array('class'=>"form-control ic_dob",'id'=>"year_of_graduation",'placeholder'=>"Completion Year*")) }} 
           </div>
           <div class="form-group marginT20">
               {{ Form::text('basic_institute',$user->education->basic_institute,array('class'=>"form-control institutes registerHereSubTextInput",'placeholder'=>"Insitiute University*")) }} 
               {{-- <input type="password" placeholder="Insitiute University" class="form-control registerHereSubTextInput"> --}}
               <span id="errorbasicInstitute"></span>
           </div>
       </div>
       <div class="form-group marginT20">
           {{ Form::select('master_course_id',$master_courses,$user->education->master_course_id,array('class'=>"form-control registerHereSubTextInput ic_master_edu","id"=>"master_course",'placeholder'=>"Master Education")) }} 
          {{-- <input type="password" placeholder="Master Education" class="form-control registerHereSubTextInput"> --}}
       </div>
       <div class="master-elem hide-elem">
           <div class="form-group marginT20">
               {{ Form::select('master_specialization_id',[],$user->education->master_specialization_id,array('class'=>"form-control ic_special","id"=>"master_specialization",'placeholder'=>"Specialization*")) }} 
             </div>

           <div class="form-group marginT20">
               {{ Form::select('master_comp_year',getDigitRange(1960,date('Y')),$user->education->master_comp_year,array('class'=>"form-control ic_dob",'id'=>"year_of_postgraduation",'placeholder'=>"Completion Year*")) }} 
           </div>
           <div class="form-group marginT20">
               {{ Form::text('master_institute',$user->education->master_institute,array('class'=>"form-control institutes registerHereSubTextInput",'placeholder'=>"Insitiute University*")) }} 
               {{-- <input type="password" placeholder="Insitiute University" class="form-control registerHereSubTextInput"> --}}
               <span id="errorInstitute"></span>
           </div>

       </div>
      

      <div class="registerHereSubText2">Your Current Employment Details</div>
      <div class="registerHereSubText3">Experience Level*</div>
      <div class="radio row marginT20 radioDiv">
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> 
              <label class="radioText">
                    {{ Form::radio('experience_level',1,($user->additional_info->exp_level==1) ? true : false,array('class'=>"exp_level")) }}I have work Experience
                   {{-- <input onclick="openMoreMenu('exp')" type="radio" name="experience_level" value='1' class="exp_level">I have work Experience --}}
               </label>
           </div>
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
               <label class="radioText">
                    {{ Form::radio('experience_level',0,($user->additional_info->exp_level==0) ? true : false,array('class'=>"exp_level")) }}I am a Fresher
                   {{-- <input onclick="openMoreMenu('freshr')" type="radio" name="experience_level" value='0' class="exp_level">I am a Fresher --}}
               </label>
           </div>
          <span id="errorExperienceLevel"></span>
      </div>
      <!--//For Exp-->
      <div id="experienced" class="hide-elem">
          <div class="registerHereSubText3">Total Work Experience</div>
          <div class="row marginT20">
              <div class="col-xs-6">
                   {{ Form::select('year_experiance',getDigitRange(0,30),$user->additional_info->exp_year,array('class'=>"form-control ic_dob",'id'=>"year_experiance",'placeholder'=>"Select Years*")) }}
              </div>
              <div class="col-xs-6">
                   {{ Form::select('month_experiance',getDigitRange(0,12),$user->additional_info->exp_month,array('class'=>"form-control ic_dob",'id'=>"month_experiance",'placeholder'=>"Select Months*")) }}
              </div>
          </div>
          <div class="form-group marginT20">
               {{ Form::text('current_company',$user->additional_info->current_company,array('class'=>"form-control ic_company",'hint-class'=>"",'id'=>"current_company",'placeholder'=>"Enter Company Name*")) }}
          </div>
          <div class="row marginT20">
              <div class="col-xs-12">
                   {{ Form::text('current_position',$user->additional_info->current_position,array('class'=>"form-control",'id'=>"current_position",'placeholder'=>"Select Current Position*")) }}
                   <span id="errorCurrentPosition"></span>
              </div>
          </div>
           <div class="row marginT20">
              <div class="col-xs-12">
                  {{ Form::select('current_industry',$industry_list,$user->additional_info->current_industry,array('class'=>"form-control ic_industry",'id'=>"current_industry",'placeholder'=>"Select Current Employer Industry*")) }}
              </div>
           </div>
           @php
               $currencyList=config('constants.currency');
               sort($currencyList);
           @endphp
           <div class="row marginT20">
               <div class="col-xs-6">
                   {{ Form::text('salary',$user->additional_info->salary,array('class'=>"form-control ic_salary",'id'=>"salary",'placeholder'=>"Enter Salary*")) }}
               </div>
               <div class="col-xs-6">
                   {{ Form::select('currency',$currencyList,$user->additional_info->salary_in,array('class'=>"form-control ic_salary",'id'=>"currency",'placeholder'=>"Select Currency*")) }}
               </div>
           </div>

           <div class="row marginT20">
               <div class="col-xs-6">
                   {{ Form::selectYear('duration_from',2000,date('Y'),$user->additional_info->duration_from,array('class'=>"form-control ic_dob",'placeholder'=>"Working Duration from*")) }}
               </div>
               <div class="col-xs-6">
                   {{ Form::select('duration_to',['Currently Working'=>'Currently Working','2018'=>'2018','2017'=>'2017'],$user->additional_info->duration_to,array('class'=>"form-control ic_dob",'placeholder'=>"Working Duration to*")) }}
               </div>
           </div>
          <div class="row marginT20">
              <div class="col-xs-12">
                   {{ Form::select('functional_area',$functional_area,$user->additional_info->exp_month,array('class'=>"form-control ic_function",'id'=>"functional_area",'placeholder'=>"Select Functional Area*")) }}
              </div>
          </div>
          {{-- <div class="form-group marginT20">
               {{ Form::text('key_skills',null,array('class'=>"form-control registerHereSubTextInput",'hint-class'=>"keyskills_s",'id'=>"key_skills",'data-toggle'=>"tooltip",'placeholder'=>"Enter Key Skills")) }}
               <span id="errorKeySkills"></span>
          </div> --}}

      </div>
      <div id="fresher" class="fresher_level_fields hide-elem">
          <div class="form-group marginT20">
              {{-- <input type="text" placeholder="Enter Key Skils" class="form-control registerHereSubTextInput"> --}}
              {{ Form::text('key_skills',null,array('class'=>"form-control",'hint-class'=>"keyskills_s",'id'=>"key_skills",'data-toggle'=>"tooltip",'placeholder'=>"Enter Key Skills*")) }}
               <span id="errorKeySkills"></span>
          </div>

      </div>
      <!--/////-->

      <div class="form-group marginT20">
           {{ Form::text('resume_headline',$user->additional_info->resume_headline,array('class'=>"form-control ic_headline",'id'=>"resume_headline",'placeholder'=>"Enter Resume Headline"/* ,'data-toggle'=>"popover","data-content"=>"CV Headline is a one line description of your CV. A good headline can kelp you get shortlisted." */ )) }}
          {{-- <input type="password" placeholder="Insitiute University" class="form-control registerHereSubTextInput"> --}}
      </div>
      <div class="form-group row marginT20">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><img class="profileImg" src="{{url('public/images/demoUser.png')}}"></div>
          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              <div class="profileDesc">
                  <span class="profileSpan">A Profile with Photo has a 40% higher chance of getting viewed by Employers</span>
                  <div class="profilePic2-button">
                     <input type="file" id="upload_profile_photo" class="file-input" name="user_img">
                     <label for="upload_profile_photo">
                     Upload Your Photo
                     </label> 
                  </div>
                  
                  <spna id="errorProfile"></spna>
              </div>
          </div>
      </div>
      <div class="form-group marginT20">
           {{ Form::select('perferred_location',getGulfCountries(),$user->additional_info->perferred_location,array('class'=>"form-control registerHereSubTextInput ic_location",'placeholder'=>"Select Preferred Location")) }} 
        </div>
      {{-- <div class="registerHereSubText3">Intrested In</div>
      <div class="radio row  radioDiv">
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> <label class="radioText"><input type="radio" name="optradio">Job Alerts</label></div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> <label class="radioText"><input type="radio" name="optradio">SMS Contact by Rec.</label></div>
      </div> --}}
       <div class="registerHereSubText3 uploadDiv">
           <span class="labelUploadPic">Upload Resume*</span>
           <span class="uploadResume-button">  
               <label for="upload_resume">
               <img src="{{url('public/images/uploadResume.png')}}">
               </label> 
               <input type="file" id="upload_resume" class="file-input" name="resume_file" required>
               <span id="curr_selected_file"></span>
           </span>
           <span id="errorResumeFile"></span>
       </div>
       <div class="registerHereSubText3 marginT60 text-center">
           <span class="submitResume-button text-center">
               <button type="submit">Submit Resume</button>
           </span>
      </div>
      {{Form::close()}}
  </div>
  <div class="col-md-3 col-lg-3"></div>

</div>

<script>
globalVar.city_id='{{$user->city_id}}';
globalVar.basic_s_id='{{$user->education->basic_specialization_id}}';
globalVar.master_s_id='{{$user->education->master_specialization_id}}';
globalVar.countryId='{{$user->country_id}}';
 var defaultImg = '{{url('public/images/deuser.png')}}';

function openMoreMenu(str){
        $('#experienced').hide();
        $('#fresher').hide();
        if(str=='exp')
            $('#experienced').show();
        if(str=='freshr')
            $('#fresher').show();
        $('.fresher_level_fields').show();
    }

    $(document).ready(function() {
        $("#contact_number").intlTelInput();
        $("#contact_number").change();
        var initialCountry = $("#contact_number").intlTelInput("getSelectedCountryData").dialCode;
        console.log(initialCountry);
        var initialCountry="{{$user->country_code}}";
        $('#country_code').val(initialCountry);
        $("#contact_number").on("countrychange", function(e, countryData) {
            $('#country_code').val(countryData.dialCode);
        });
    });
$("input[name=resume_file]").change(function () {
    printSelectedFileName($(this).val(),'#curr_selected_file');
});
$("#upload_profile_photo").change(function(){
    previewImage(this,'.profileImg','{{url('public/images/deuser.png')}}');
});


  $(function(){
      $("#country").change();
      $("#basic_course").change();
      $("#master_course").change();
      expLevelElem({{$user->additional_info->exp_level}});

    $('#current_position').selectize({
        maxItems: 1,
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        create: true,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });
   
  });

/* $('#country_code').change(function(){
      globalVar.countryId = $(this).val().split('~')[1];
      $('#country').val(globalVar.countryId);
      $('#country').change();
}); */
$('#country').change(function(){
    globalVar.countryId = $(this).val();
    $("#country").val(globalVar.countryId);
    $('#city').find('option').not(':first').remove();
    $.ajax({
        url: base_url+'api/city-suggestion',
        type: 'GET',
        dataType: 'json',
        data: {
            by_country: 1,
            country_id: globalVar.countryId,
        },           
        success: function(res) {
           $.each(res,function(key,val){
            if(globalVar.city_id==val.id){
              $('#city').append('<option value="'+val.id+'" selected>'+val.name+'</option>');
            } else {
              $('#city').append('<option value="'+val.id+'">'+val.name+'</option>');
            }
           });
        },
        error: function(error) {
            
        }
    });
});


$('#key_skills').selectize({
    plugins: ['remove_button'],
    maxItems: 15,
    valueField: 'name',
    labelField: 'name',
    searchField: 'name',
    create: true,
    options: {!! json_encode($skill_keywords) !!},
    onInitialize: function(){
        var selectize = this;
        var skill='{{$user->additional_info->key_skills}}';
        var splitVal=skill.split(',');
        var selected_items=splitVal;
        selectize.setValue(splitVal);
    }
});
  
$('.exp_level').click(function(e){
    expLevelElem($(this).val());
});
function expLevelElem(input_val){
    if(input_val==0){
      $('.exp_level_fields').hide();
    } else {
      $('#experienced').show();
    }
    $('.fresher_level_fields').show();
}

$('#basic_course').change(function(e){
    if($(this).val()>0){
      $('.basic-elem').show();
    } else {
      $('.basic-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationBasic, globalFunc.error, globalFunc.complete);
});
$('#master_course').change(function(e){
    if($(this).val()>0){
      $('.master-elem').show();
    } else {
      $('.master-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationMaster, globalFunc.error, globalFunc.complete);
});

$( "#datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true,
    yearRange: "-58:+0",
    showOn: "both",
    buttonImage: "{{url('public/images/icons/calendar.png')}}",
    buttonImageOnly: true,
    buttonText: "Select date",
    dateFormat: "dd-mm-yy",
    maxDate:'0'
});


</script>
@endsection