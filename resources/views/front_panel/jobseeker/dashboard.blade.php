@extends('/layouts/front_panel_master')
@section('content')
<style>
    body{
        background-color: white !important;
    }
    .leftSearchBox {
        border-radius: 5px;
        box-shadow: rgba(0,0,0,0.1) 0 0 20px;
    }
</style>
<div class="container">
    @if($user->email_verified==0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <a href="{{ route('send-verify-mail') }}">Verify</a> your email to increase your visibility and search-ability.
        </div>
    @endif
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20">
            <div class="leftSearchBox">
                <div class="paddingSearchBox">
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu">Job</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="{{route('applied-jobs')}}"><img class="imgBreifcae" src="{{url('public/images/job4.png')}}">Applied Jobs</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="{{route('last-activity')}}"><img class="imgBreifcae" src="{{url('public/images/job1.png')}}">Last Activity</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="{{route('recommended-jobs')}}"><img class="imgBreifcae" src="{{url('public/images/job2.png')}}">Recommended Jobs</a>
                    </div>
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu2">Account</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a class="activeTab" href="{{route('candidate-dashboard')}}"><img class="imgBreifcae" src="{{url('public/images/job3.png')}}">Profile</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding mt20" >
            <div class="col-lg-12 nopadding nomrgn recommendedJobsHeader profileLabel">
                Profile
            </div>
            <div class="col-lg-12">
                <div class="col-lg-12 profileHeader">Your Personal Details <a class="profileEdit" href="{{route('candidate-profile')}}">Edit</a></div>
                <div class="col-lg-12 paddingProfileDetails">
                    <div class="row nopadding">
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 text-center">
                            <img class="profileImage" alt="example image" src="{{checkFile($user->avatar,'uploads/user_img/','user_img.png')}}">
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-8 mt10">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="profileDetails">Name:</div>
                                    <div class="profileDetails">Username:</div>
                                    <div class="profileDetails">Designation:</div>
                                    <div class="profileDetails">Phone:</div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="profileDetails">{{ $user->title }} {{ ucfirst($user->name) }}</div>
                                    <div class="profileDetails">{{ $user->email }}</div>
                                    <div class="profileDetails">{{ ($user->city!=null) ? $user->city->name : '' }}, {{ ($user->country!=null) ? $user->country->name : '' }}</div>
                                    <div class="profileDetails">{{ ($user->country_code!='') ? '+'.$user->country_code.'-' : '' }}{{ $user->mobile }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 mt40 profileHeader">Your Education Details<a class="profileEdit" href="{{route('candidate-profile')}}">Edit</a></div>
                <div class="col-lg-12 mt30 profileHeader">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 profileDetails">
                        Basic:
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 profileDetails">
                        <div>{{ ($user->education !=null) ? 'Course : '. @$user->education->basic_course->name : '' }}</div>
                        <div>{{ ($user->education !=null) ? 'Specialization : '. @$user->education->basic_specialization->name : '' }}</div>
                        <div>{{ ($user->education !=null) ? 'Year : '. @$user->education->basic_comp_year : '' }}</div>
                        <div>{{ ($user->education !=null) ? 'Institute : '. @$user->education->basic_institute : '' }}</div>
                    </div>

                    @if($user->education !=null && $user->education->master_course!=null)
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 profileDetails">
                            Master:
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 profileDetails">
                            <div>{{ ($user->education !=null) ? 'Course : '.@$user->education->master_course->name : '' }}</div>
                            <div>{{ ($user->education !=null) ? 'Specialization : '.@$user->education->master_specialization->name : '' }}</div>
                            <div>{{ ($user->education !=null) ? 'Year : '. @$user->education->master_comp_year : '' }}</div>
                            <div>{{ ($user->education !=null) ? 'Institute : '.@$user->education->master_institute : '' }}</div>
                        </div>
                    @endif

                    {{-- <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                        <div>Bachelor of Architecture (2005),</div>
                        <div>MBBS (2016)</div>
                    </div> --}}
                </div>

                <div class="col-lg-12 mt71 profileHeader">Your Current Employment Details<a class="profileEdit" href="{{route('candidate-profile')}}">Edit</a></div>
                <div class="col-lg-12 mt30 mb50 profileHeader">
                    @if($user->additional_info!=null && $user->additional_info->exp_level)
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            Work Experiance:
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            <div> {{ $user->additional_info->exp_year }} Year {{ $user->additional_info->exp_month }} Month</div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            Employer Name:
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            <div> {{ ($user->additional_info!=null) ? $user->additional_info->current_company : 'None' }}</div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            Employer's Industry:
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            <div> {{ $user->additional_info->industry->name }}</div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                                Position:
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            <div> {{ $user->additional_info->current_position }}</div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                                Monthly Salary:
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            <div> {{ $user->additional_info->salary }} ({{ config('constants.currency')[$user->additional_info->salary_in] }})</div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            Work Duration:
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            <div> {{ $user->additional_info->duration_from }}- {{$user->additional_info->duration_to }}</div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            Functional Area / Department:
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                            <div>{{ $user->additional_info->function_area->name }}</div>
                        </div>
                    @endif
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                        Key Skills:
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 profileDetails">
                        <div> {{ ($user->additional_info!=null) ? $user->additional_info->key_skills : '' }}</div>
                    </div>
                </div>

            </div>

        </div>

    </div>


</div>



@endsection