@extends('/layouts/front_panel_master')
@section('content')
<style>
    body{
        background-color: white !important;
    }
    .leftSearchBox {
        border-radius: 5px;
        box-shadow: rgba(0,0,0,0.1) 0 0 20px;
    }
    .cardBox3:hover {
        border-top: none !important;
        background-color:white !important;
    }
    .responsiveCard {
        padding-left: 5px !important;
        padding-right: 5px !important;
        padding-top: 0px; 
        padding-bottom: 0px; 
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20">
            <div class="leftSearchBox">
                <div class="paddingSearchBox">
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu">Job</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="{{route('applied-jobs')}}"><img class="imgBreifcae" src="{{url('public/images/job4.png')}}">Applied Jobs</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="{{route('last-activity')}}"><img class="imgBreifcae" src="{{url('public/images/job1.png')}}">Last Activity</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a class="activeTab" href="{{route('recommended-jobs')}}"><img class="imgBreifcae" src="{{url('public/images/job2.png')}}">Recommended Jobs</a>
                    </div>
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu2">Account</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a  href="{{route('candidate-dashboard')}}"><img class="imgBreifcae" src="{{url('public/images/job3.png')}}">Profile</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding mt20" >
                <div class="col-lg-12 nopadding nomrgn recommendedJobsHeader">
                    Recommended Jobs
                </div>
                <div>
                    @forelse($recommended_jobs as $k=>$val)
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard">
                            <a href="{{route('job-details',['jobid'=>$val->id])}}">
                            <div class="cardBox2">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-xs-3 paddingRyt0">
                                                <img class="cardImg2" src="{{url('public/images/atlas-1.png')}}">
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="jobName">{{$val->comp_name}}</div>
                                                <div class="jobDesignation">{{$val->job_title}}</div>
                                                <div>
                                                    <span class="jobDesignation">Exp Level: ( {{$val->min_experience}} - {{$val->max_experience}} yrs. )</span>
                                                    <span class="jobLocation"><img src="{{url('public/images/location-icon.png')}}">{{$val->country_name}}</span>
                                                </div>
                                                </div>
                                        </div>
                                        <div class="row skillsRow">
                                            <div class="col-xs-3 paddingRyt0">
                                            </div>
                                            <div class="col-xs-9 skills noPadding">
                                                <span>Design</span>
                                                <span>UI/UX</span>
                                                <span>Web Development</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-xs-12 mrgnTp8">
                                                <span class="jobAmountGrid jobAmountGrid2"><img src="{{url('public/images/doller.png')}}">${{$val->salary_min}} - ${{$val->salary_max}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="jobDateGrid">{!!countDays($val->created_at)!!}</span>
                            </div>
                            </a>
                        </div>
                    @empty
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard">
                        <div class="cardBox2">
                            <p>No jobs available for now, Click here to <a href="{{route('search-jobs')}}" target="_blank">explore more</a></p>
                        </div>
                    </div>
                   @endforelse
                </div>
                <div class="row margnBtm30">
                        <div class="col-lg-12 mt20">
                            <div class="paginationContainer">
                                {{$recommended_jobs->links()}}
                            </div>
                        </div>
            
                    </div>
            </div>
    </div>
</div>



@endsection