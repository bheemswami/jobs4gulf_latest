@extends('/layouts/front_panel_master')
@section('content')
<style>
    body{
        background-color: white !important;
    }
    .leftSearchBox {
        border-radius: 5px;
        box-shadow: rgba(0,0,0,0.1) 0 0 20px;
    }
    .cardBox3:hover {
        border-top: none !important;
        background-color:white !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20">
            <div class="leftSearchBox">
                <div class="paddingSearchBox">
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu">Job</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a class="activeTab" href="{{route('applied-jobs')}}"><img class="imgBreifcae" src="{{url('public/images/job4.png')}}">Applied Jobs</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="{{route('last-activity')}}"><img class="imgBreifcae" src="{{url('public/images/job1.png')}}">Last Activity</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="{{route('recommended-jobs')}}"><img class="imgBreifcae" src="{{url('public/images/job2.png')}}">Recommended Jobs</a>
                    </div>
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu2">Account</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a  href="{{route('candidate-dashboard')}}"><img class="imgBreifcae" src="{{url('public/images/job3.png')}}">Profile</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding mt20" >
            <div class="col-lg-12 nopadding nomrgn recommendedJobsHeader">
                Applied Jobs
            </div>
            @php $total_ojobs=$total_cjobs=0; @endphp
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard">
                <div class="row cardBox3">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                        Opened Jobs
                    </div>
                    <!--Header-->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 marginTp30 noPaddingResp tableHeader2">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 noPaddingResp3 nomrgn">Job Title</div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  noPaddingResp3 nomrgn">Total Vacancy</div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn">Experience</div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn">Salary</div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn">Apply Date</div>
                    </div>
                    <!--Content-->
                    @forelse($applied_jobs as $k=>$val)
                    @if($val->jobs!=null && strtotime($val->jobs->expiry_date)>=strtotime(date('Y-m-d')))
                        @php $total_ojobs=$total_ojobs+1; @endphp
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp tableHeader3">
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 noPaddingResp3 nomrgn">
                                <div class="jobName9">{{$val->jobs->job_title}}</div>
                                <div><a class="jobActionEdit" href="javascript:;">Edit job</a>|<a class="jobActionDelete" href="javascript:;">Delete job</a></div>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  noPaddingResp3 nomrgn jobDetails1">{{$val->jobs->total_vacancy}}</div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn jobDetails1">{{$val->jobs->min_experience}}-{{$val->jobs->max_experience}} Year</div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn jobDetails1">{{$val->jobs->salary_min}}-{{$val->jobs->salary_max}} {{$val->jobs->currency}}</div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn jobDetails1">{{dateConvert($val->created_at,'d M Y')}}</div>
                        </div>
                    @endif
                  @empty
                  @endforelse
                </div>
            </div>
           {{--  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt20 mb20">
                <div class="paginationContainer">
                    <span class="active">1</span>
                    <span>2</span>
                    <span>3</span>
                    <span>4</span>
                    <span>5</span>
                    <span>6</span>
                    <span>Next</span>
                </div>
            </div> --}}


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
                <div class="row cardBox3">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                        Closed Jobs
                    </div>
                    <!--Header-->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 marginTp30 noPaddingResp tableHeader2 ">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 noPaddingResp3 nomrgn">Job Title</div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  noPaddingResp3 nomrgn">Total Vacancy</div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn">Experience</div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn">Salary</div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn">Apply Date</div>
                    </div>
                    <!--Content-->
                    @forelse($applied_jobs as $k=>$val)
                        @if($val->jobs!=null && strtotime($val->jobs->expiry_date)<strtotime(date('Y-m-d')))
                            @php $total_cjobs=$total_cjobs+1; @endphp
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp tableHeader3">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 noPaddingResp3 nomrgn">
                                    <div class="jobName9">{{$val->jobs->job_title}}</div>
                                    <div><a class="jobActionEdit" href="javascript:;">Edit job</a>|<a class="jobActionDelete" href="javascript:;">Delete job</a></div>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  noPaddingResp3 nomrgn jobDetails1">{{$val->jobs->total_vacancy}}</div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn jobDetails1">{{$val->jobs->min_experience}}-{{$val->jobs->max_experience}} Year</div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn jobDetails1">{{$val->jobs->salary_min}}-{{$val->jobs->salary_max}} {{$val->jobs->currency}}</div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  noPaddingResp3 nomrgn jobDetails1">{{dateConvert($val->created_at,'d M Y')}}</div>
                            </div>
                        @endif  
                    @empty
                        {{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp tableHeader3 lastEntry">
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 noPaddingResp3 nomrgn">
                               <div class="jobName9">Graphic Designer</div>
                               <div><a class="jobActionEdit" href="javascript:;">Edit job</a>|<a class="jobActionDelete" href="javascript:;">Delete job</a></div>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 noPaddingResp3 nomrgn jobDetails1">12</div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 noPaddingResp3 nomrgn jobDetails1">2-8 Years</div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 noPaddingResp3 nomrgn jobDetails1">1000-4000</div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 noPaddingResp3 nomrgn jobDetails1">02/25/18</div>
                       </div> --}}
                    @endforelse
                </div>
            </div>
            {{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt20 mb20">
                <div class="paginationContainer">
                    <span class="active">1</span>
                    <span>2</span>
                    <span>3</span>
                    <span>4</span>
                    <span>5</span>
                    <span>6</span>
                    <span>Next</span>
                </div>
            </div> --}}
        </div>
    </div>
</div>



@endsection