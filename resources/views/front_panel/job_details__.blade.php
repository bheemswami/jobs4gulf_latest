@extends('/layouts/front_panel_master')
@section('content')


<div class="gulf">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <h2>Job Details</h2>
        </div>
    </div>
  </div>
</div>
      


<div class="clearfix"></div>
<div class="taber ">
  <div class="container">
    <div class="row">
        <div class="col-lg-9 cod">    
      <div class="ear">
          <img class="com" src="" alt="logo">
          <h3>job_title</h3>
          <h5>company_name</h5>
          <div class="clearfix"></div>
          <hr>
          <ul class="fc">
          <li><i class="fa fa-pencil"></i> min_experience to max_experience years</li>
          <li><i class="fa fa-map-marker"></i> country_name</li>
          </ul>         
          <p class="xix">company_profile<i class="fa fa-plus"></i> </p>
          <hr>

        <div class="haf">
          <div class="ols">
            <p>details->key_skill</p>
          </div>
        </div>
      </div>
    
    
            <div class="tab" role="tabpanel">
      
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Job Description</a></li>
                    <li role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">Desired Skills and Experience</a></li>
                </ul>
        
                <div class="tab-content tabs">
                    <div role="tabpanel" class="tab-pane fade in active" id="Section1">
                       <div class="xop">
                          Work with the Director of Regulatory Compliance to create a regulatory audit model for the internal review of rates, forms and underwriting.

Develop and maintain a proactive compliance audit program to identify and resolve potential compliance exposures. Assist with external market conduct audit as necessary.

Coordinate reviews of previous Market Conduct Examinations findings to assess current regulatory/market conduct exposures.

Conduct DOI complaint trend analysis based on feedback from the Regulatory Compliance Team which is responsible for tracking, distributing, researching and responding to the Department of Insurance, the Better Business Bureau, and direct written consumer complaints.

Manage the execution of the compliance audit function, which includes state compliance audits, targeted audits, and the post-release audits of all forms.

Analyze compliance risks and exposures both internal and external and prioritize and develop an internal audit plan to assess compliance risks on an annual basis.

Actively participate in weekly Defect Roundtable meetings to identify operational risks and exposures. Conduct root cause analysis of production live site issues to ensure proper controls are in place to mitigate risk. Research and interpret state and federal legislation, state bulletin/circulars and other regulatory material to determine impact to business operations.

Ensure customer and corporate compliance with state regulations, which includes analysis of information, coordination, and consultation with customers and internal departments.

Participate in the establishment of departmental goals and implements procedures and performance standards to achieve these goals; manages, coordinates, monitors and evaluates the activities of assigned staff. Responsible for the employment, promotion, associate performance evaluation, training, motivation, counseling, and discipline of assigned associates.
            </div>
            
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section2">
                        <div class="xop">
            <ul>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Work with the Director of Regulatory Compliance to create a regulatory audit model for the internal review of rates, forms and underwriting. </p></li>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Develop and maintain a proactive compliance audit program to identify and resolve potential compliance exposures. Assist with external market conduct audit as necessary. </p></li>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Coordinate reviews of previous Market Conduct Examinations findings to assess current regulatory/market conduct exposures. </p></li>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Conduct DOI complaint trend analysis based on feedback from the Regulatory Compliance Team which is responsible for tracking, distributing, researching and responding to the Department of Insurance, the Better Business Bureau, and direct written consumer complaints. </p></li>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Manage the execution of the compliance audit function, which includes state compliance audits, targeted audits, and the post-release audits of all forms. </p></li>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Analyze compliance risks and exposures both internal and external and prioritize and develop an internal audit plan to assess compliance risks on an annual basis. </p></li>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Actively participate in weekly Defect Roundtable meetings to identify operational risks and exposures. Conduct root cause analysis of production live site issues to ensure proper controls are in place to mitigate risk. Research and interpret state and federal legislation, state bulletin/circulars and other regulatory material to determine impact to business operations. </p></li>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Ensure customer and corporate compliance with state regulations, which includes analysis of information, coordination, and consultation with customers and internal departments. </p></li>
              <li><i class="fa fa-snowflake-o" aria-hidden="true"></i><p>Participate in the establishment of departmental goals and implements procedures and performance standards to achieve these goals; manages, coordinates, monitors and evaluates the activities of assigned staff. Responsible for the employment, promotion, associate performance evaluation, training, motivation, counseling, and discipline of assigned associates. </p></li>
            </ul>
            </div>
                    </div>
                </div>
            </div>
        </div>
    
    
    <div class="col-lg-3">
    
    <div class="latest">
      <h4 class="lat">Latest news</h4>
        <ul>
        <li><i class="fa fa-flash"></i> <p>Every day, thousands of new job vacancies are listed. Location: United Arab Emirates </p></li>
        <li><i class="fa fa-flash"></i> <p> Every day, thousands of new job vacancies are listed. Location: United Arab Emirates</p></li>
        <li><i class="fa fa-flash"></i> <p> Every day, thousands of new job vacancies are listed. Location: United Arab Emirates</p></li>
        <li><i class="fa fa-flash"></i> <p> Every day, thousands of new job vacancies are listed. Location: United Arab Emirates</p></li>
      
        </ul>   
    </div>
    
    
    
    <div class="emp">
      <h4 class="lat">Top Employers</h4>
        <ul>
        <li><img src="{{url('public/images/top1.png')}}"></li>
        <li><img src="{{url('public/images/top2.png')}}"></li>
        <li><img src="{{url('public/images/top3.png')}}"></li>
        <li><img src="{{url('public/images/top1.png')}}"></li>
        <li><img src="{{url('public/images/top2.png')}}"></li>
        <li><img src="{{url('public/images/top3.png')}}"></li>
      
        </ul>   
    </div>
    
    <div class="ads">
        
        <img class="img-thumbnail" src="{{url('public/images/ads.jpg')}}" >
    
        </div>
    
    
    </div>
    
    </div>
</div>
</div>
  

<div class="refer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <ul>
          <li><a href="#" data-toggle="modal" data-target="#myModal">Apply Now</a></li>       
          <li><a href="#"></a>Save Job</li>
          <li><a href="#"></a>Refer a Friend</li>
          </ul>
      </div>
    </div>
  </div>
</div>


<div class="carr waz">
    <div class="container">
      <div class="row">
      <div class="lcop">
      
      <h5 class="allx xgx">Related Jobs</h5>
        <div class="col-md-6">
        
          <div class="ear">
          <img class="com" src="{{url('public/images/com.png')}}">
          <h3>PHP Developer</h3>
          <h5>Aditz Technology Private Limited</h5>
          <div class="clearfix"></div>
          <hr>
          <ul class="fc">
          <li><i class="fa fa-pencil"></i> 0 to 4 years</li>
          <li><i class="fa fa-map-marker"></i> Chennai</li>
          </ul>
          
          <p class="xix">To create and develop php web applications.Integration and development in open street map and google map...<i class="fa fa-plus"></i> </p>
<hr>

<div class="haf">

<div class="ols">
<p>Skills : Core PHP , Codeignitor, Laravel, MVC...</p>
</div>


<div class="dls">
<a href="#">Apply Now</a>
</div>

</div>
    </div>
        </div>
        
        
        <div class="col-md-6">
        
          <div class="ear">
          <img class="com" src="{{url('public/images/j3.png')}}">
          <h3>Java Developer</h3>
          <h5>Cisco Technology Private Limited</h5>
          <div class="clearfix"></div>
          <hr>
          <ul class="fc">
          <li><i class="fa fa-pencil"></i> 0 to 4 years</li>
          <li><i class="fa fa-map-marker"></i> Chennai</li>
          </ul>
          
          <p class="xix">To create and develop php web applications.Integration and development in open street map and google map...<i class="fa fa-plus"></i> </p>
<hr>
<div class="haf">

<div class="ols">
<p>Skills : Core PHP , Codeignitor, Laravel, MVC...</p>
</div>


<div class="dls">
<a href="#">Apply Now</a>
</div>

</div>
    </div>
        </div>
        
        <div class="col-md-6">
        
          <div class="ear">
          <img class="com" src="{{url('public/images/j5.png')}}">
          <h3>Web Designers</h3>
          <h5>NIT Technology Private Limited</h5>
          <div class="clearfix"></div>
          <hr>
          <ul class="fc">
          <li><i class="fa fa-pencil"></i> 0 to 3 years</li>
          <li><i class="fa fa-map-marker"></i> Trivandrum</li>
          </ul>
          
          <p class="xix">To create and develop php web applications.Integration and development in open street map and google map...<i class="fa fa-plus"></i> </p>
<hr>
<div class="haf">

<div class="ols">
<p>Skills : Core PHP , Codeignitor, Laravel, MVC...</p>
</div>


<div class="dls">
<a href="#">Apply Now</a>
</div>

</div>
    </div>
        </div>
        
        <div class="col-md-6">
        
          <div class="ear">
          <img class="com" src="{{url('public/images/j6.png')}}">
          <h3>PHP Developer</h3>
          <h5>Azandoo Technology Private Limited</h5>
          <div class="clearfix"></div>
          <hr>
          <ul class="fc">
          <li><i class="fa fa-pencil"></i> 0 to 3 years</li>
          <li><i class="fa fa-map-marker"></i> Hyderabad</li>
          </ul>
          
          <p class="xix">To create and develop php web applications.Integration and development in open street map and google map...<i class="fa fa-plus"></i> </p>
<hr>
<div class="haf">

<div class="ols">
<p>Skills : Core PHP , Codeignitor, Laravel, MVC...</p>
</div>


<div class="dls">
<a href="#">Apply Now</a>
</div>

</div>
    </div>
        </div>
        
        

    </div>
    </div>
  </div>

    </div>

    
    
    <div class="acds">
        <div class="container">
          <div class="row">
            
          </div>
        </div>
    </div>
      

<div class="clearfix"></div>
@endsection