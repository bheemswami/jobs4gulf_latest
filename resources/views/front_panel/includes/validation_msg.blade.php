
@if($errors->all())
<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 banner-form">
	<ul>
		  @foreach($errors->all() as $val)
		  	<li>{{ $val }}</li>
		  @endforeach
		</ul>
  </div>
@endif
