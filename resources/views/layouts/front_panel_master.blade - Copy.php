  @php 
  $settings = getSettings();
  //session()->put('CALL_US',$settings->mobile);
  @endphp
  <html>

<head>
    <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta property="og:url" content="@yield('og_url')" />
        <meta property="og:image" content="@yield('og_image')" />
        <meta property="og:title" content="@yield('og_title')" />
        <title>{{$settings->meta_title}} @yield('page_title')</title>
        <script>
          var base_url="{{url('/').'/'}}";
          var csrf_token="{{ csrf_token() }}";
          var globalFunc = {};
          var globalVar = {};
          var currentThis = '';
          var minChars = 2;
        </script>
    
    <link rel="shortcut icon" type="image/png" href="http://localhost/job4gulf_old/public/images/favicon.png">
    <script src="{{URL::asset('public/js/jquery-3.3.1.min.js')}}"></script> 
    <script src="{{URL::asset('public/build/js/intlTelInput.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('public/js/bootstrap.js')}}"></script>
    <!-- owl-javascript -->
    {{-- <script src="{{URL::asset('public/js/jquery.min.js')}}"></script> --}}
    {{-- <script src="{{URL::asset('public/js/intlInputPhone.js')}}"></script> --}}
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <script src="{{URL::asset('public/js/owl.carousel.js')}}"></script>
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{URL::asset('public/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    
    <link rel="stylesheet" href="{{URL::asset('public/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/css/home/homeResponsive.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/css/media.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/css/styleNav.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/css/reponsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/bootstrap-multiselect.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/plugins/Font-Awesome/css/font-awesome.css')}}" />
    <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/css/slick.css')}}"  />        
    <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/css/slick-theme.css')}}"  /> 
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="{{URL::asset('public/css/home/homeNav.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/plugins/selectize/selectize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('public/plugins/multiSelect/jquery.multiselect.css')}}" />

    {{-- <link rel="stylesheet" href="{{URL::asset('public/css/intlInputPhone.css')}}"> --}}
    <link rel="stylesheet" href="{{URL::asset('public/build/css/intlTelInput.css')}}">
    
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{URL::asset('public/js/jquery.validate.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript" src="http://rawgithub.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
    <script src="http://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="http://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
      
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
    <script src="{{URL::asset('public/plugins/selectize/selectize.js')}}"></script>
    <script src="{{URL::asset('public/plugins/multiSelect/jquery.multiselect.js')}}"></script>

    <script src="{{URL::asset('public/js/custom.js')}}"></script>
    <script src="{{URL::asset('public/js/smoothscroll.js')}}"></script>
    
    
</head>

<body>
    <!-- first section -->

    <div id="loginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Login model content-->
            <div class="modal-content ">
                <div class="modal-body paddingBody">
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-default active marginNegative cornerRight">Candidate Login</button>
                        <button type="button"  data-toggle="modal" data-target="#loginModal_employers" class="btn btn-default marginNegative cornerLeft">Employer Login</button>
                    </div>
                    <div class="form-group marginTp30">
                        <label class="fontLogin">Login</label>
                    </div>
                    <div id="success_error_msg" class="jobseeker-msg text-center"></div>
                        <form class="login-form form-1" id="login-form" autocomplete="on">
                            <div class="form-group">
                                <label for="usr">Enter Email</label>
                                <input type="email" class="form-control loginBtn" name="candidate_username" id="cand_login_username">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Enter Password</label>
                                <input type="password" class="form-control loginBtn error" id="cand_login_password" name="candidate_password">
                            </div>
                            <div class="form-group marginTp30">
                                <a class="forgetPass" href="javascript:;">Forgot Password?</a>
                                <button type="submit" class="btn btn-success right">Login</button>
                            </div>
                        </form>
                    <div class="form-group text-center marginTp30 dontHaveAccount">
                        Don't have a account yet?<a href="javascript:void(0);"> Create New</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="loginModal_employers" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Login model content-->
            <div class="modal-content ">
                <div class="modal-body paddingBody">
                    <div class="form-group text-center">
                        <button type="button" data-toggle="modal" data-target="#loginModal_employers" class="btn btn-default  marginNegative cornerRight">Candidate Login</button>
                        <button type="button" class="btn btn-default active marginNegative_1 cornerLeft">Employer Login</button>
                    </div>
                    <div class="form-group marginTp30">
                        <label class="fontLogin">Login</label>
                    </div>
                    <div id="success_error_msg" class="employer-msg text-center"></div>
                    <form class="login-form form-2" id="" autocomplete="on">
                        <div class="form-group">
                            <label for="usr">Enter Email</label>
                            <input type="email" name="employer_username" value="" id="emp_login_username" class="form-control loginBtn" placeholder="Enter Username" title="Please Enter Username" required/>
                            {{-- <input type="email" class="form-control loginBtn" id="usr"> --}}
                        </div>
                        <div class="form-group">
                            <label for="pwd">Enter Password</label>
                            <input type="password" name="employer_password" value="" id="emp_login_password" class="form-control loginBtn" placeholder="Enter Password" required/>
                            {{-- <input type="password" class="form-control loginBtn error" id="pwd"> --}}
                        </div>
                        <div class="form-group marginTp30">
                            <a class="forgetPass" href="javascript:;">Forgot Password?</a>
                            <button type="submit" class="btn btn-success right">Login</button>
                        </div>
                    </form>
                    <div class="form-group text-center marginTp30 dontHaveAccount">
                        Don't have a account yet?<a href="javascript:void(0);"> Create New</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
		
    <nav class="navbar navbar-default">
        <div class="container" style="padding: 10px;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{-- <a href="{{url('/')}}"><img src="{{checkFile($settings->logo,'/uploads/site_logo/','logo.png')}}" class="img-responsive"></a> --}}
                <a class="navbar-brand" href="{{url('/')}}"><img class="brandLogo" src="{{url('public/images/job 4 gulf@2x.png')}}"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Browse Jobs</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Search Job</a></li>
                            <li><a href="#">Job by Country</a></li>
                            <li><a href="#">Job by City</a></li>
                            <li><a href="#">Job by Industry</a></li>
                        </ul>
                    </li>
                    <li><a href="">Search Consultants</a></li>
                    <li><a href="">Walkings</a></li>
                    @if(Auth::check() && Auth::user()->role==2)
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> {{ucfirst(Auth::user()->name)}} </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('candidate-dashboard')}}">Dashboard</a></li>
                                <li><a href="{{route('logout')}}">Logout</a></li>
                            </ul>
                        </li>
                    @else 
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Job Seeker </a>
                            <ul class="dropdown-menu">
                                <li><a data-toggle="modal" data-target="#loginModal" href="javascript:;">Login</a></li>
                                <li><a href="{{route('candidate-register')}}">Register</a></li>
                            </ul>
                        </li>
                    @endif
                    @if(Auth::check() && Auth::user()->role==3)
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> {{ucfirst(Auth::user()->name)}} </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{route('employer-dashboard')}}">Dashboard</a></li>
                                <li class="no-vertical-line"><a href="{{route('emp-profile-view')}}">Profile</a></li>
                                <li><a href="{{route('logout')}}">Logout</a></li>
                            </ul>
                        </li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Employers </a>
                            <ul class="dropdown-menu">
                                <li><a data-toggle="modal" data-target="#loginModal_employers" href="javascript:;">Login</a></li>
                                <li><a href="{{route('employer-register')}}">Register</a></li>
                                <li><a href="#">Our Plans</a></li>
                            </ul>
                        </li>
                    @endif
                    <li><a href="javascript:;">Contact us</a></li>
                    {{-- @if(Auth::check())
                        <img class="userimg" src="{{checkFile(Auth::user()->avatar,'uploads/user_img/','user_img.png')}}">
                    @endif --}}
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    @include('layouts.flash_msg')
    @yield('content')
    @php
        $countryList=getFooterData('country');
        $industryList=getFooterData('industry');
    @endphp
<!--seven section-->
    <section id="seven-section" class="banner-4">
        <div class="container">
            <div class="row footer-menu-text">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
                    <h4 class="move-left">Jobs By</h4>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                        <h5 class="hr-line">Location</h5>
                        <ul>
                            @foreach($countryList as $val)
                                <li><a href="{{url('search-jobs?country_ids='.$val->id)}}" >Jobs in {{$val->name}}</a></li>
                            @endforeach
                            {{-- <li><a href="#">Jobs in Afghanistan</a></li>
                            <li><a href="#">Jobs in Albania</a></li>
                            <li><a href="#">Jobs in Algeria</a></li>
                            <li><a href="#">Jobs in American Samoa</a></li>
                            <li><a href="#">Jobs in Angola</a></li>
                            <li><a href="#">Jobs in Anguilla</a></li> --}}
                            <li><a href="{{route('jobs-by',['country'])}}">more</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                        <h5 class="hr-line">Category</h5>
                        <ul>
                            @foreach($industryList as $val)
                                <li><a href="{{url('search-jobs?industry_ids='.$val->id)}}" > {{$val->name}}</a></li>
                            @endforeach
                            {{-- <li><a href="#">Oil And Gas Jobs</a></li>
                            <li><a href="#">Government Jobs</a></li>
                            <li><a href="#">Hospitality Jobs</a></li>
                            <li><a href="#">Shipping Jobs</a></li>
                            <li><a href="#">HR Jobs</a></li>
                            <li><a href="#">Logistics Jobs</a></li>
                            <li><a href="#">Banking Jobs</a></li>
                            <li><a href="#">Engineering Jobs</a></li>
                            <li><a href="#">Construction Jobs</a></li>
                            <li><a href="#">Media Jobs</a></li>
                            <li><a href="#">Sales Jobs</a></li> --}}
                            <li><a href="{{route('jobs-by',['industry'])}}">more</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
                    <h4 class="move-left">Users</h4>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                        <h5 class="hr-line">Job Seekers</h5>
                        <ul>
                            @if(Auth::check() && Auth::user()->role==2)
                                <li><a href="#">{{ucfirst(Auth::user()->name)}}</a>
                                <li><a href="{{route('logout')}}">Logout</a></li>
                            @else 
                                <li><a data-toggle="modal" data-target="#loginModal" href="javascript:;">Login</a></li>
                                <li><a href="{{route('candidate-register')}}">Register</a></li>
                            @endif 
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                        <h5 class="hr-line">Employers</h5>
                        <ul>
                            @if(Auth::check() && Auth::user()->role==3)
                                <li><a href="#">{{ucfirst(Auth::user()->name)}}</a>
                                <li><a href="{{route('logout')}}">Logout</a></li>
                            @else 
                                <li><a data-toggle="modal" data-target="#loginModal_employers" href="javascript:;">Login</a></li>
                                <li><a href="{{route('employer-register')}}">Register</a></li>
                            @endif
                            <li><a href="#">Our Plans</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
                    <h4 class="move-left">Jobs4Gulf</h4>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                        <h5 class="hr-line">Quick Links</h5>
                        <ul>
                            <li><a href="{{route('about-us')}}">About Us</a></li>
                            <li><a href="{{route('contact-us')}}">Contact Us</a></li>
                            <li><a href="{{route('site-map')}}">Site Map</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding footer-social">
                            <h5 class="hr-line">Follow Us</h5>
                            <ul>
                                <li><a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a target="_blank" href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a target="_blank" href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a target="_blank" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="copyright">
                <p class="text-center">All rights reserved &#9400; {{date('Y')}} Jobs4Gulf </p>
            </div>
        </div>
    </section>
<!-- seven-secton-end -->
<!-- vendors -->
<script src="{{URL::asset('public/js/frontpanel.js')}}"></script>
<script src="{{URL::asset('public/js/global_custom.js')}}"></script>
    <script>
        $('.popular-block').mouseover(function(){
            var imgAttr=$(this).children('img');
            var hoverImgSrc = imgAttr.data('hovericon');
            imgAttr.attr('src',hoverImgSrc);

        });
        $('.popular-block').mouseout(function(){
            var imgAttr=$(this).children('img');
            var outImgSrc = imgAttr.data('hoverouticon');
            imgAttr.attr('src',outImgSrc);
        });
    </script>
<script>
    $("#top-candidates").owlCarousel({
        loop:true,
        // lazyLoad:true,
        items: 4,
        margin: 30,
//            autoplay:true,
        // autoplayTimeout:5000,
//            autoplaySpeed: 100,
        // dots:true,
        // nav:true,
        // dots: true,
        //  navText:['next','pre'],
        // navContainer: '#slideNav',
        // dotsContainer: '#slidedots',
        responsiveClass:true
      });
      $(document).ready(function(){

      $("#premium").owlCarousel({
        loop:true,
        items: 4,
          autoplay: true,
          autoplayHoverPause: true,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:3,
              nav:false
          },
          1000:{
              items:4,
              nav:true,
              loop:true
          }
      },
        margin: 30,
         autoplayTimeout:1500,
        dots:true,
        nav:true,
        dots: true,
          navText:['<img src="{{url('public/images/Group 636.png')}}">','<img src="{{url('public/images/Group 637.png')}}">'],
        // navContainer: '#slideNav',
        // dotsContainer: '#slidedots',
        responsiveClass:true
      });

          $("#candidates").owlCarousel({
        loop:true,
        items: 4,
        autoplay: true,
        autoplayHoverPause: true,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:3,
              nav:false
          },
          1000:{
              items:4,
              nav:true,
              loop:true
          }
      },
        margin: 30,
         autoplayTimeout:1500,
        dots:true,
        nav:true,
        dots: true,
          navText:['<img src="{{url('public/images/Group 636.png')}}">','<img src="{{url('public/images/Group 637.png')}}">'],
        // navContainer: '#slideNav',
        // dotsContainer: '#slidedots',
        responsiveClass:true
      });



      $("#jobPannel").owlCarousel({
        loop:true,
        items: 4,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:3,
              nav:false
          },
          1000:{
              items:2,
              nav:true,
              loop:false
          }
      },
        margin: 30,
         autoplayTimeout:500,
        dots:true,
        nav:true,
        dots: true,
          navText:['<img src="{{url('public/images/Group 636.png')}}">','<img src="{{url('public/images/Group 637.png')}}">'],
        // navContainer: '#slideNav',
        // dotsContainer: '#slidedots',
        responsiveClass:true
      });
      });
</script>
<style>

</style>
</body>

</html>