<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\LatestNews;
class LatestNewsController extends Controller
{
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
    }
    public function index()
    {
        $this->data['news_list']=LatestNews::where('status', '!=' ,2)->get();
        return view('admin::latest_news.all_news',$this->data);
    }
    public function addNews(){
        return view('admin::latest_news.add_edit_news');
    }
    public function updateNews(Request $request){
        $this->data['news_data']=LatestNews::whereId($request->id)->first();
        return view('admin::latest_news.add_edit_news',$this->data);
    }
    public function saveNews(Request $request) {
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        $res = LatestNews::updateOrCreate(['id'=>$request->id],$request->except(['_token']));
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteNews(Request $request){
        if(LatestNews::whereId($request->id)->delete()){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
