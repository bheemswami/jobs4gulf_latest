<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;
class DashboardController extends Controller
{
	private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
        $this->core->adminSessionCheck();
    }
    public function index(){
    	$this->data['counts']=DB::select(DB::raw("SELECT     		
    		(SELECT COUNT(*) FROM users WHERE status = 1) as user_count"));
    
        return view('admin::dashboard',$this->data);

    }
}
