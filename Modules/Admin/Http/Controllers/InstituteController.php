<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Institute;
class InstituteController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['institutes']=Institute::where('status', '!=' ,2)->get();
        return view('admin::institute.all_institute',$this->data);
    }
    public function addInstitute(){
        return view('admin::institute.add_edit_institute');
    }
    public function updateInstitute(Request $request){
        $this->data['institute_data']=Institute::whereId($request->id)->first();
        return view('admin::institute.add_edit_institute',$this->data);
    }
    public function saveInstitute(Request $request) {
        $res = Institute::updateOrCreate(['id'=>$request->id],$request->except(['_token']));
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteInstitute(Request $request){
        if(Institute::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
