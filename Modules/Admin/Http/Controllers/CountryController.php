<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Country;
class CountryController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['countries']=Country::where('status', '!=' ,2)->get();
        return view('admin::country.all_country',$this->data);
    }
    public function addCountry(){
        $this->data['country_data']='';
        return view('admin::country.add_edit_country',$this->data);
    }
    public function updateCountry(Request $request){
        
        $this->data['country_data']=Country::whereId($request->id)->first();
        return view('admin::country.add_edit_country',$this->data);
    }
    public function saveCountry(Request $request) {
        
        if(!isset($request->is_gulf))
        $request->merge(['is_gulf'=>0]);
        
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
       if($request->hasFile('img')){
            if($request->id>0){
                $rec = Country::find($request->id);
                unlinkImg($rec->icon,'uploads/country_img');
            }
            $filename=$this->core->fileUpload($request->file('img'),'uploads/country_img',1);
            $request->merge(['icon'=>$filename]);
        }
        $res = Country::updateOrCreate(['id'=>$request->id],$request->except(['_token','img']));
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteCountry(Request $request){
        if(Country::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
