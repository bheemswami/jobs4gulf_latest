<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Specialization;
class SpecializationController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['specializations']=Specialization::with('course')->where('status', '!=' ,2)->get();
        return view('admin::specialization.all_specialization',$this->data);
    }
    public function addSpecialization(){
        return view('admin::specialization.add_edit_specialization');
    }
    public function updateSpecialization(Request $request){
        $this->data['specialization_data']=Specialization::whereId($request->id)->first();
        return view('admin::specialization.add_edit_specialization',$this->data);
    }
    public function saveSpecialization(Request $request) {
        $res = Specialization::updateOrCreate(['id'=>$request->id],$request->except(['_token']));
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteSpecialization(Request $request){
        if(Specialization::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
