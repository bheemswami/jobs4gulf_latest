@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All City</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>
                        <th>Icon</th>
                        <th>Country Name </th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cities as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td> <img class="img-circle dr_img" src="{{checkFile($val->icon,'/uploads/city_img/','small-logo.png')}}"/> </td>
                            <td> {{@$val->country->name}}</td>
                            <td> {{$val->name}}</td>
                           <td> 
                                @if($val->status==1)
                                    <span class="label label-success"> Active</span> 
                                @else
                                    <span class="label label-default"> Inactive</span>
                                @endif
                            </td>
                            <td>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('update-city',[$val->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                               <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-city',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
