@extends('admin::layouts.master')
@section('content')
  @php 
      $flag=0;
      $heading='Add';
      if(isset($designation_data) && !empty($designation_data)){
          $flag=1;
          $heading='Update';
      }
  @endphp
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>{{ $heading }} Designation</h4>
          </div>
          @if($flag==1)
                  {{ Form::model($designation_data,array('url'=>route('save-designation'),'class'=>'designation-form','files' => true))}}
                      {{Form::hidden('id',null)}}
              @else
                  {{ Form::open(array('url'=>route('save-designation'),'class'=>'designation-form','files' => true))}}
              @endif
          <div class="form-parentBlock-inner"> 
              
              {{ csrf_field() }}
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Name</label>
                {{Form::text('name',null,['class'=>"form-control",'placeholder'=>'Enter Name'])}}
              </div>
              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Status</label>
                {{Form::select('status',config('constants.status'),1,['class'=>"form-control",'placeholder'=>'--Select--'])}}
              </div>
       </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}
@stop