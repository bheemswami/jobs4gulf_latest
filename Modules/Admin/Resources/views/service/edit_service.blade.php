@extends('admin::layouts.master')
@section('content')

    {{ Form::model($service_data,array('url'=>route('save-service'),'class'=>'service-form','files' => true))}}  
    {{ Form::hidden('id',null)}}           
    {{ csrf_field() }}
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Service Detail</h4>
          </div>
          <div class="form-parentBlock-inner">              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Title</label>
                {{Form::text('title',null,['class'=>"form-control",'placeholder'=>'Enter Name'])}}
              </div> 

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Icon</label>
                <div class="box">
                  {{Form::file('service_icon',['class'=>"form-control",'id'=>'file-1'])}}
                </div>
                    <img class="icon-medium" src="{{checkFile($service_data->icon,'/uploads/service_img/','no-img.png')}}"/>
              </div> 
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Image</label>
                <div class="box">
                  {{Form::file('service_image',['class'=>"form-control",'id'=>'file-2'])}}
                </div>
                  <img class="icon-medium" src="{{checkFile($service_data->image,'/uploads/service_img/','no-thumbnail.jpg')}}"/>
              </div>              
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">Description</label>
                {{Form::textarea('description',null,['class'=>"form-control"])}}
              </div>

             
          </div>
     </div>

   
     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Key Features</h4>
          </div>
          <div class="form-parentBlock-inner">
            <div class="key_feature_elem">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                  {{Form::text('new_title',null,['rows'=>'2','class'=>"form-control",'id'=>'new_title','placeholder'=>'Enter New Feature'])}}
                </div>
                <img src="{{url('public/images/plus_24.png')}}" class="add-new-row">
              </div>
             <table class="table table-bordered features hide-elema" cellspacing="0" width="100%">
              <tr>
               <th>Features</th>
               <th>Action</th>
              </tr> 
              @if($service_data->features!=null)
                @foreach($service_data->features as $k=>$val)             
                  <tr>
                    <td><input type="text" name="key_features[]" value="{{$val->title}}" class="form-control"/></td>
                    <td><img src="{{url('public/images/error_img.png')}}" class="rmv-row"></td>
                  </tr>
                @endforeach
              @endif
            </table
          </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}

      <script>
        $('.add-new-row').click(function(){
          if($('#new_title').val()!=''){
            $('.features').append('<tr>\
              <td><input type="text" name="key_features[]" value="'+$('#new_title').val()+'" class="form-control"></td>\
              <td><img src="{{url('public/images/error_img.png')}}" class="rmv-row"></td>\
            </tr>');
          }
          $('#new_title').val('');
        });
        $(document).on('click','.rmv-row',function(){
          $(this).parents('tr').remove();
        });
      </script>
@stop