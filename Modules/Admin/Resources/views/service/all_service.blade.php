@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All Services</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th>Icon</th>                       
                        <th>Image</th>                       
                        <th>Title</th>
                        <th>Description</th>
                        {{-- <th>Show Button</th> --}}
                        {{-- <th>Status</th> --}}
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($service_list as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td><img class="img-circle dr_img" src="{{checkFile($val->icon,'/uploads/service_img/','no-img.png')}}"/></td>
                            <td><img class="img-circle dr_img" src="{{checkFile($val->image,'/uploads/service_img/','no-thumbnail.jpg')}}"/></td>
                            <td> {{$val->title}}</td>
                            <td> {{$val->description}}<hr/>
                                @if($val->features!=null)
                                <span><strong>Key Features:</strong></span>
                                <ul>
                                    @foreach($val->features as $key=>$value)
                                        <li><i class="fa fa-arrow-right"></i>&nbsp;&nbsp;{{$value->title}}</li>
                                    @endforeach
                                </ul>
                                @endif
                            </td>
                            {{-- <td> 
                                @if($val->show_btn==1)
                                    <span class=""> Enquiry Button</span> 
                                @else
                                    <span class=""> Job Plan Button</span>
                                @endif
                            </td> --}}
                           {{--  <td> 
                                @if($val->status==1)
                                    <span class="label label-success"> Active</span> 
                                @else
                                    <span class="label label-default"> Inactive</span>
                                @endif
                            </td> --}}
                            <td>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('update-service',[$val->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                               {{-- <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-service',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
