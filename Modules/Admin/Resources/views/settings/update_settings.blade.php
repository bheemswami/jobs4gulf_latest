@extends('admin::layouts.master')
@section('content')
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Update Settings</h4>
          </div>
          {{ Form::model($settings,array('url'=>route('save-settings'),'class'=>'settings-form','files' => true))}}
          {{Form::hidden('id',null)}}
          {{ csrf_field() }}
          <div class="form-parentBlock-inner">

            <div class="row">
              <label for="" class="col-sm-2">Meta Title</label>
              <div class="col-sm-6 form-group">
                {{Form::text('meta_title',null,['class'=>"form-control",'placeholder'=>'Enter Meta Title'])}}
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2">E-mail</label>
              <div class="col-sm-6 form-group">
                {{Form::text('email',null,['class'=>"form-control",'placeholder'=>'Enter E-mail Address'])}}
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2">Mobile</label>
              <div class="col-sm-6 form-group">
                {{Form::text('mobile',null,['class'=>"form-control",'placeholder'=>'Enter Mobile Number'])}}
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2">Phone</label>
              <div class="col-sm-6 form-group">
                {{Form::text('phone',null,['class'=>"form-control",'placeholder'=>'Enter Phone Number'])}}
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2">Facebook Link</label>
              <div class="col-sm-6 form-group">
                {{Form::text('fb_link',null,['class'=>"form-control",'placeholder'=>'Enter Facebook Link'])}}
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2">Twitter Link</label>
              <div class="col-sm-6 form-group">
                {{Form::text('twitter_link',null,['class'=>"form-control",'placeholder'=>'Enter Twitter Link'])}}
              </div>
            </div>
             <div class="row">
              <label for="" class="col-sm-2">LinkedIn Link</label>
              <div class="col-sm-6 form-group">
                {{Form::text('in_link',null,['class'=>"form-control",'placeholder'=>'Enter LinkedIn Link'])}}
              </div>
            </div>
             <div class="row">
              <label for="" class="col-sm-2">G&#43; Link</label>
              <div class="col-sm-6 form-group">
                {{Form::text('gplus_link',null,['class'=>"form-control",'placeholder'=>'Enter Google Plus Link'])}}
              </div>
            </div>
             <div class="row">
              <label for="" class="col-sm-2">Youtube Link</label>
              <div class="col-sm-6 form-group">
                {{Form::text('youtube_link',null,['class'=>"form-control",'placeholder'=>'Enter Youtube Link'])}}
              </div>
            </div>
             <div class="row">
              <label for="" class="col-sm-2">SMTP Username</label>
              <div class="col-sm-6 form-group">
                {{Form::text('smtp_username',null,['class'=>"form-control",'placeholder'=>'Enter SMTP Username'])}}
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2">SMTP Password</label>
              <div class="col-sm-6 form-group">
                {{Form::text('smtp_password',null,['class'=>"form-control",'placeholder'=>'Enter SMTP Password'])}}
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2">Site Logo</label>
              <div class="box col-sm-6 form-group">
                  {{Form::file('site_logo',['class'=>"form-control",'id'=>'file-1'])}}
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2"></label>
              <div class="col-sm-6 form-group">
                <img class="icon-medium" src="{{checkFile($settings->logo,'/uploads/site_logo/','small-logo.png')}}"/>                
              </div>
            </div>
            <div class="row">
              <label for="" class="col-sm-2">Address</label>
              <div class="box col-sm-6 form-group">
                 {{Form::textarea('address',null,['class'=>"form-control",'placeholder'=>'Enter Company Address'])}}
              </div>
            </div>

            <div class="row">
              <label for="" class="col-sm-2">Jobseeker Terms</label>
              <div class="box col-sm-6 form-group">
                 {{Form::textarea('seeker_terms',null,['class'=>"form-control txt_editor",'placeholder'=>'Enter Jobseeker Terms & Conditions'])}}
              </div>
            </div>

            <div class="row">
              <label for="" class="col-sm-2">Employer Terms</label>
              <div class="box col-sm-6 form-group">
                 {{Form::textarea('employer_terms',null,['class'=>"form-control txt_editor",'placeholder'=>'Enter Employer Terms & Condition'])}}
              </div>
            </div>
       </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}
@stop