@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="col-sm-2">
             <button name="jobs" type="button" value="all" class="btn btn-sm btn-info jobs_btn">All Posted Jobs</button>
        </div>
        <div class="col-sm-2">
             <button name="jobs" type="button" value="1" class="btn btn-sm btn-success jobs_btn">Approved Jobs</button>
        </div>
        <div class="col-sm-2">
             <button name="jobs" type="button" value="0" class="btn btn-sm btn-danger jobs_btn">Un-Approve Jobs</button>
        </div>
    </div>

    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All Posted Jobs</h4>
        </div>
        {{ Form::open(array('url'=>route('is-approve-job'),'class'=>'form-horizontal'))}}
        <div class="form-parentBlock-inner"> 
            <table id="jobs-datatable" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                    <th>S.No</th>
                    <th><input type="checkbox" id="select_all_checkbox"></th>
                    <th>Job Title</th>
                    <th>Job Type</th>
                    <th>Total Vacancy</th>
                    <th>Post Date</th>
                    <th>Expire Date</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
            </table>
            <div class="col-lg-12">
                <button name="status" type="submit" value="1" class="btn btn-sm btn-success">Approve Selected</button>
                <button name="status" type="submit" value="0" class="btn btn-sm btn-danger">Un-Approve Selected</button>
            </div>
        </div>
         {{ Form::close() }}
    </div>
  <script>
    var jobType=0;
    $('.jobs_btn').click(function(){
      jobType = $(this).val();
      $('#jobs-datatable').dataTable().fnDestroy();
      loadTable();
    });
  $(function() {
    loadTable();
  });

  function loadTable(){
    $('#jobs-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('all-posted-jobs') !!}?ajax=1&is_approve='+jobType,
        columns: [
            { data: 'sno', name: 'sno' ,"searchable": true},
            { data: 'checkbox', name: 'checkbox' ,"searchable": true},
            { data: 'job_title', name: 'job_title' ,"searchable": true},
            { data: 'job_type', name: 'job_type' ,"searchable": true},
            { data: 'total_vacancy', name: 'total_vacancy',"searchable": true },
            { data: 'post_date', name: 'post_date',"searchable": true },
            {data: 'expire_date', name: 'expire_date',"searchable": false},
            {data: 'is_approved', name: 'is_approved',"searchable": false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
  }
  </script>
@stop
