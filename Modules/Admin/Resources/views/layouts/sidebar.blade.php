<div class="main-menu menu-fixed nano">
  <ul id="main-menu" class="nano-content" style="">
  
    <li class="root-level"><a href="{{route('dashboard')}}"><i class="fa fa-desktop"></i>Dashboard</a></li>
    <li class="root-level"><a href="{{route('all-posted-jobs')}}"><i class="fa fa-desktop"></i>Posted Jobs</a></li>
    <li class="root-level has-sub">
      <a><i class="fa fa-male"></i>Users</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('employers')}}"><i class="fa fa-arrow-right"></i>All Employers</a></li>
          <li><a href="{{route('consultants')}}"><i class="fa fa-arrow-right"></i>All Consultants</a></li>  
          <li><a href="{{route('candidates')}}"><i class="fa fa-arrow-right"></i>All Candidates</a></li>     
      </ul>
    </li>
    <li class="root-level has-sub">
      <a><i class="fa fa-globe"></i>Job Plans</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-jobplan')}}"><i class="fa fa-arrow-right"></i>Add Job Plan</a></li> 
          <li><a href="{{route('all-jobplans')}}"><i class="fa fa-arrow-right"></i>Job Plans</a></li> 
      </ul>
    </li> 

    <li class="root-level has-sub">
      <a><i class="fa fa-google-wallet"></i>Walkins</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-walkin')}}"><i class="fa fa-arrow-right"></i>Add Walkin</a></li>
          <li><a href="{{route('all-walkins')}}"><i class="fa fa-arrow-right"></i>Walkin List</a></li> 
          <li><a href="{{route('all-applied-walkins')}}"><i class="fa fa-arrow-right"></i>Applied Walkin List</a></li> 
      </ul>
    </li>

    <li class="root-level has-sub">
      <a><i class="fa fa-newspaper-o"></i>Latest News</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('all-news')}}"><i class="fa fa-arrow-right"></i>All News</a></li>
          <li><a href="{{route('add-news')}}"><i class="fa fa-arrow-right"></i>Add News</a></li>       
      </ul>
    </li>
    
    <li class="root-level has-sub">
      <a><i class="fa fa-globe"></i>Country</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-country')}}"><i class="fa fa-arrow-right"></i>Add Country</a></li>
          <li><a href="{{route('all-countries')}}"><i class="fa fa-arrow-right"></i>Country List</a></li>       
      </ul>
    </li>
    <li class="root-level has-sub">
      <a><i class="fa fa-location-arrow"></i>City</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-city')}}"><i class="fa fa-arrow-right"></i>Add City</a></li>
          <li><a href="{{route('all-cities')}}"><i class="fa fa-arrow-right"></i>City List</a></li>       
      </ul>
    </li>
     <li class="root-level has-sub">
      <a><i class="fa fa-book"></i>Degree</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-degree')}}"><i class="fa fa-arrow-right"></i>Add Degree</a></li>
          <li><a href="{{route('all-degries')}}"><i class="fa fa-arrow-right"></i>Degree List</a></li>       
      </ul>
    </li>
    <li class="root-level has-sub">
      <a><i class="fa fa-book"></i>Course</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-course')}}"><i class="fa fa-arrow-right"></i>Add Course</a></li>
          <li><a href="{{route('all-courses')}}"><i class="fa fa-arrow-right"></i>Course List</a></li>       
      </ul>
    </li>
    <li class="root-level has-sub">
      <a><i class="fa fa-book"></i>Specialization</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-specialization')}}"><i class="fa fa-arrow-right"></i>Add Specialization</a></li>
          <li><a href="{{route('all-specializations')}}"><i class="fa fa-arrow-right"></i>Specialization List</a></li>       
      </ul>
    </li>
    <li class="root-level has-sub">
      <a><i class="fa fa-book"></i>Designation</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-designation')}}"><i class="fa fa-arrow-right"></i>Add Designation</a></li>
          <li><a href="{{route('all-designations')}}"><i class="fa fa-arrow-right"></i>Designation List</a></li>       
      </ul>
    </li>
    <li class="root-level has-sub">
      <a><i class="fa fa-industry"></i>Industry</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-industry')}}"><i class="fa fa-arrow-right"></i>Add Industry</a></li>
          <li><a href="{{route('all-industries')}}"><i class="fa fa-arrow-right"></i>Industry List</a></li>       
      </ul>
    </li>
    <li class="root-level has-sub">
      <a><i class="fa fa-user-md"></i>Functional Area</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-functional-area')}}"><i class="fa fa-arrow-right"></i>Add Functional Area</a></li>
          <li><a href="{{route('all-functional-area')}}"><i class="fa fa-arrow-right"></i>Functional Area List</a></li>       
      </ul>
    </li>

    <li class="root-level has-sub">
      <a><i class="fa fa-building"></i>Company</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-company')}}"><i class="fa fa-arrow-right"></i>Add Company</a></li>
          <li><a href="{{route('all-companies')}}"><i class="fa fa-arrow-right"></i>Company List</a></li>       
      </ul>
    </li>

    <li class="root-level has-sub">
      <a><i class="fa fa-university"></i>Institute</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('add-institute')}}"><i class="fa fa-arrow-right"></i>Add Institute</a></li>
          <li><a href="{{route('all-institutes')}}"><i class="fa fa-arrow-right"></i>Institute List</a></li>       
      </ul>
    </li>
  
    <li class="root-level has-sub">
      <a><i class="fa fa-cogs"></i>Settings</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('settings')}}"><i class="fa fa-arrow-right"></i>Site Settings</a></li>
          <li><a href="{{route('page-banners')}}"><i class="fa fa-arrow-right"></i>Page Banners</a></li>       
      </ul>
    </li>    
    <li class="root-level has-sub">
      <a><i class="fa fa-external-link-square"></i>More</a>
      <ul class="sub_menu_ul">
          <li><a href="{{route('all-transactions')}}"><i class="fa fa-arrow-right"></i>Transaction History</a></li>      
          <li><a href="{{route('all-services')}}"><i class="fa fa-arrow-right"></i>Services</a></li> 
          <li><a href="{{route('all-enquiry')}}"><i class="fa fa-arrow-right"></i>Enquires</a></li>    
          {{-- <li><a href="{{route('advertise-features')}}"><i class="fa fa-arrow-right"></i>Job Advertise Features</a></li>     --}}
      </ul>
    </li>
  </ul>
</div>

<script>
   $(document).ready(function() {
        var permalink_nodomain = window.location.href;
        console.log(permalink_nodomain);
        $('.root-level a[href="' + permalink_nodomain + '"]').parents('li').addClass('active').addClass('opened');
        $('.root-level a[href="' + permalink_nodomain + '"]').parents('.sub_menu_ul').addClass('visible');
        $('.root-level a[href="' + permalink_nodomain + '"]').parent('.sub_menu_ul li').addClass('active_child');
    });
  </script>