@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All Candidates</h4>
        </div>
       
         {{ Form::open(array('url'=>route('update-candidate-status'),'class'=>'form-horizontal','files' => true))}}
      
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th><input type="checkbox" name="id" id="select_all_checkbox"></th>                        
                        <th>Image </th>
                        <th>Name </th>
                        <th>Email </th>
                        <th>Mobile </th>
                        <th>Gender </th>
                        <th>Country</th>
                        <th>City</th>
                        <th>Resume</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($candidates as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td><input type="checkbox" name="ids[]" value="{{$val->id}}" class="data_checkbox"></td>
                            <td><img width="60px" src="{{checkFile($val->avatar,'uploads/user_img/','user_img.png')}}"></td>
                            <td>{{  $val->name }}</td>
                            <td>{{ $val->email }}</td>
                            <td>{{ $val->mobile }}</td>
                            <td>{{ $val->gender }}</td>
                            <td>{{ ($val->country!=null) ? $val->country->name : '-' }}</td>
                            <td>{{ ($val->city!=null) ? $val->city->name : '-' }}</td>
                            <td>
                                @if($val->additional_info!=null)
                                    <table class="table table-bordered"  width="100%">
                                    <tr>
                                        <td colspan="2"> {{$val->additional_info->resume_headline }} </td>
                                    </tr>
                                    @if($val->additional_info->resume!='')
                                        <tr>
                                            <td><a href="{{url('public/uploads/user_resume/'.$val->additional_info->resume)}}" target=_blank><i class="fa fa-2x fa-eye" aria-hidden="true"></i></a></td>
                                            <td><a href="{{url('public/uploads/user_resume/'.$val->additional_info->resume)}}" download><i class="fa fa-2x fa-download" aria-hidden="true"></i></a></td>
                                        </tr>
                                    @endif
                                    </table>
                                @endif
                            </td>
                            <td> 
                                @if($val->status==1)
                                    <span class="label label-success"> Approved</span> 
                                @else
                                    <span class="label label-default"> Inactive</span>
                                @endif
                            </td>
                           
                            <td>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('view-candidate',[$val->id])}}" title="View"><i class="fa fa-pencil"></i>View</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="block-title">
                <h4>Selected Users</h4>
            </div><p></p>
            <div class="col-lg-12">
                <button name="status" type="submit" value="1" class="btn btn-sm btn-success">Approve</button>
                <button name="status" type="submit" value="0" class="btn btn-sm btn-danger">Deactive</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@stop
