@extends('admin::layouts.master')
@section('content')

  @php 
      $flag=0;
      $heading='Add';
      if(isset($industry_data) && !empty($industry_data)){
          $flag=1;
          $heading='Update';
      }
  @endphp
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>{{ $heading }} Industry</h4>
          </div>
          @if($flag==1)
                  {{ Form::model($industry_data,array('url'=>route('save-industry'),'class'=>'industry-form','files' => true))}}
                      {{Form::hidden('id',null)}}
              @else
                  {{ Form::open(array('url'=>route('save-industry'),'class'=>'industry-form','files' => true))}}
              @endif
          <div class="form-parentBlock-inner"> 
              
              {{ csrf_field() }}
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 form-group">
                <label for="">Name</label>
                {{Form::text('name',null,['class'=>"form-control",'placeholder'=>'Enter Name'])}}
              </div>
              
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 form-group">
                <label for="">Status</label>
                {{Form::select('status',config('constants.status'),1,['class'=>"form-control",'placeholder'=>'--Select--'])}}
              </div>

              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 form-group">
                <label for="">Icon</label>
                <div class="box">
                  {{Form::file('img',['class'=>"form-control",'id'=>'file-1'])}}
                </div>                
                  @if($flag==1)
                    <img class="icon-medium" src="{{checkFile($industry_data->icon,'uploads/industry_img/','no-img.png')}}"/>
                  @endif
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 form-group">
                <label for="">Icon</label>
                <div class="box">
                  {{Form::file('img_hover',['class'=>"form-control",'id'=>'file-1'])}}
                </div>                
                  @if($flag==1)
                    <img class="icon-medium" src="{{checkFile($industry_data->icon_hover,'uploads/industry_img/','no-img.png')}}"/>
                  @endif
              </div>
       </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}
@stop