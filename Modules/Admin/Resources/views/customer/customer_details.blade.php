@extends('admin::layout.master')
@section('content')
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Customer Details</h4>
          </div>
          <div class="form-parentBlock-inner"> 
            <table class="table table-bordered" cellspacing="0" width="100%">
              <tr>
                <td rowspan="3"><img class="service_icon" src="{{checkFile($customer_data->avatar,'/uploads/user_img/')}}"/></td>
              </tr>
              <tr>
                <th>Name :</th>
                <td>{{$customer_data->first_name}} {{$customer_data->last_name}}</td>                
              </tr>
              <tr>               
                <th>Status :</th>
                <td>{!!($customer_data->status==1) 
                  ? '<button class="btn btn-success btn-sm">Active</button>' 
                  : '<button class="btn btn-danger btn-sm">Inactive</button>'!!}
                </td>
              </tr>
              <tr>               
                <th>Email :</th>
                <td colspan="2">{{ $customer_data->email }}</td>
              </tr>
              <tr>               
                <th>Phone Number :</th>
                <td colspan="2">{{ $customer_data->phone }}</td>
              </tr>             
              <tr> 
                <td colspan="3" class="text-right">
                  <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('update-customer',[$customer_data->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                 </td>
              </tr>
            </table>
          </div>
     </div>

     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Customer Address</h4>
          </div>
          <div class="form-parentBlock-inner"> 
            <table class="table table-bordered" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th>S.No.</th>
                <th>Address</th>
              </tr>
              </thead>
              @if($customer_data->address_list!=null)
                @foreach($customer_data->address_list as $k=>$val)
                    <tr>
                        <td>{{$k+1}}</td>
                        <td>{!! ($val->is_default==1) ? '<span class="btn-success btn-xs">Default</span> ':'' !!} {{$val->address_line_1}}</td>
                    </tr>               
                @endforeach
                @endif
            </table>
          </div>
     </div>
@stop
