@extends('admin::layout.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>Customers</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="example" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th> S.no. </th>
                        <th> Avatar </th>
                        <th> Name </th>
                        <th> Email</th>
                        <th> Phone Number</th>
                        <th> Status </th>
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td> <img class="img-circle dr_img" src="{{checkFile($val->image,'/uploads/user_img/')}}"/> </td>
                            <td> {{$val->first_name}} {{$val->last_name}} </td>
                            <td> {{$val->email}}</td>
                            <td> {{$val->phone}}</td>
                            <td> 
                                @if($val->status==1)
                                    <span class="label label-success"> Active</span> 
                                @else
                                    <span class="label label-default"> Inactive</span>
                                @endif
                            </td>
                            <td>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('view-customer',[$val->id])}}" title="View"><i class="fa fa-eye"></i>View</a>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('update-customer',[$val->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                               <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-customer',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
