@extends('admin::layout.master')
@section('content')
<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <style>
      #locationField, #controls {
        position: relative;
        width: 480px;
      }
      #autocomplete {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 99%;
      }
      .label {
        text-align: right;
        font-weight: bold;
        width: 100px;
        color: #303030;
      }
      #address {
        border: 1px solid #000090;
        background-color: #f0f0ff;
        width: 480px;
        padding-right: 2px;
      }
      #address td {
        font-size: 10pt;
      }
      .field {
        width: 99%;
      }
      .slimField {
        width: 80px;
      }
      .wideField {
        width: 200px;
      }
      #locationField {
        height: 20px;
        margin-bottom: 2px;
      }
    </style>
  @php 
      $flag=0;
      $heading='Add';
      if(isset($customer_data) && !empty($customer_data)){
          $flag=1;
          $heading='Update';
      }
  @endphp
      @if($flag==1)
          {{ Form::model($customer_data,array('url'=>route('save-customer'),'class'=>'update-customer-form','files' => true))}}
          {{ Form::hidden('id',null)}}
      @else
          {{ Form::open(array('url'=>route('save-customer'),'class'=>'add-customer-form','files' => true))}}
      @endif
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>{{ $heading }} Customer</h4>
          </div>
         
          <div class="form-parentBlock-inner"> 
              
              {{ csrf_field() }}
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Firstname</label>
                {{Form::text('first_name',null,['class'=>"form-control",'placeholder'=>'Enter Fisrt Name'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Lastname</label>
                {{Form::text('last_name',null,['class'=>"form-control",'placeholder'=>'Enter Last Name'])}}
              </div>
              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Email</label>
                {{Form::text('email',null,['class'=>"form-control",'placeholder'=>'Enter Email'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Phone Number</label>
                {{Form::text('phone',null,['class'=>"form-control",'placeholder'=>'Enter Phone Number'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Password</label>
                {{Form::password('new_password',['class'=>"form-control",'placeholder'=>'Enter Password','id'=>'password'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Confirm Password</label>
                {{Form::password('confirm_password',['class'=>"form-control",'placeholder'=>'Enter Password'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Avatar</label>
                <div class="box">
                  {{Form::file('img',['class'=>"inputfile inputfile-6 ",'id'=>'file-1'])}}
                          <label for="file-1"><strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a file&hellip;</strong><span></span></label>
                      </div>
              </div>
             
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                 @if($flag==1)
                    <img class="service_icon" src="{{checkFile($customer_data->image,'/uploads/user_img/')}}"/>
                  @endif
              </div>
          </div>
     </div>
      <div class="row form-parentBlock address-elem">      
          <div class="block-title">
            <h4>Customer Address</h4>
          </div>

          <div class="form-parentBlock-inner"> 
            @if($flag==1 && $customer_data->address_list!=null)
              <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 form-group">
                  <ul class="address_list">
                    @foreach($customer_data->address_list as $k=>$val) 
                      <li>
                        <span><img src="{{url('images/negative_16.png')}}" class="minus-icon-16 remove-address-li" ></span>
                        {{$val->address_line_1}}
                        {{ Form::hidden('address_ids[]',$val->id)}}
                      </li>                  
                    @endforeach
                  </ul>
              </div>
            @endif
              <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 form-group">
                {{Form::text('address[]',null,['class'=>"form-control autocomplete",'placeholder'=>'Enter Address','id'=>'','onFocus'=>"" ])}}
              </div>
              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-group">
               <img src="{{url('images/plus_24.png')}}" class="plus-icon add-more-address" >
              </div>              
          </div>
         
      </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}

<script>
  $(document).on('click','.add-more-address',function(){
    $('.address-elem').append('<div class="form-parentBlock-inner">\
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 form-group">\
          {{Form::text('address[]',null,['class'=>"form-control autocomplete",'placeholder'=>'Enter Address','id'=>'','onFocus'=>"" ])}}\
        </div>\
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 form-group">\
         <img src="{{url('images/negative_24.png')}}" class="minus-icon remove-address" >\
        </div>\
    </div>');
    initAutocomplete();
  });
  $(document).on('click','.remove-address',function(){
     $(this).parents('.form-parentBlock-inner').remove();
  });
  $(document).on('click','.remove-address-li',function(){
     $(this).parents('li').remove();
  });

function initAutocomplete() {
          var defaultBounds = new google.maps.LatLngBounds(
             new google.maps.LatLng(-33.8902, 151.1759),
             new google.maps.LatLng(-33.8474, 151.2631));

             var input = document.getElementsByClassName('autocomplete');
             var options = {
                 bounds: defaultBounds,
                 types: ['geocode']
             };
             for (i = 0; i < input.length; i++) {
                 autocomplete = new google.maps.places.Autocomplete(input[i], options);
             }
      }

</script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAWSoQNi9Fc1hxBa-tGfwmi5wMis04OstQ&libraries=places&callback=initAutocomplete" async defer></script> 
@stop