@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All Walkins</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th>Title </th>
                        <th>Company Name</th>
                        <th>Company Industry</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($walkins as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td> {{$val->title}}</td>
                            <td> {{$val->comp_name}}</td>
                            <td>{{$val->industry->name}}</td>
                            <td>
                                <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('view-walkin',[$val->id])}}" title="View"><i class="fa fa-list"></i>View</a>
                                <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('edit-walkin',[$val->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                                <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-walkin',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
