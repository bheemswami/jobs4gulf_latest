@extends('admin::layouts.master')
@section('content')

    {{ Form::open(array('url'=>route('save-walkin'),'class'=>'company-form','files' => true))}}             
    {{ csrf_field() }}
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Walkin Detail</h4>
          </div>
          <div class="form-parentBlock-inner">              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Title</label>
                {{Form::text('title',null,['class'=>"form-control",'placeholder'=>'Enter Name'])}}
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Position</label>
                {{Form::select('designation_ids[]',$designations,null,['class'=>"form-control",'multiple'=>true])}}
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Job Description</label>
                {{Form::textarea('job_description',null,['class'=>"form-control"])}}
              </div>

             
          </div>
     </div>

     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Company Info</h4>
          </div>
          <div class="form-parentBlock-inner"> 
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Select Employer</label>
                {{Form::select('emp_id',['Add New','Employers'=>$employers['employers'],'Consultants'=>$employers['consultants']],null,['class'=>"form-control",'id'=>'employers','placeholder'=>'Select Employer'])}}
              </div> 
              <div class="new_info_company hide-elem">            
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                  <label for="">Company Name</label>
                  {{Form::text('comp_name',null,['class'=>"form-control",'placeholder'=>'Enter Name'])}}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                  <label for="">Company Industry</label>
                  {{Form::select('comp_industry',$industries,null,['class'=>"form-control",'placeholder'=>'Select'])}}
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                  <label for="">Country</label>
                  {{Form::select('country_id',['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,['class'=>"form-control",'placeholder'=>'Select'])}}
                </div>

                
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                  <label for="">Company Logo</label>
                  <div class="box">
                    {{Form::file('logo',['class'=>"form-control",'id'=>'file-1'])}}
                  </div>
                </div> 
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                  <label for="">About Us</label>
                  {{Form::textarea('about_us',null,['class'=>"form-control"])}}
                </div> 
              </div>
          </div>
     </div>

     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Interview Details</h4>
          </div>
          <div class="form-parentBlock-inner">
            <div class="Interview_elem">                
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 form-group">
                  <label for="">DateTime (From)</label>
                  <input type="text" class="form-control cal_date_time" id="date_time_from">
                </div> 
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 form-group">
                  <label for="">DateTime (To))</label>
                  <input type="text" class="form-control cal_date_time" id="date_time_to">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 form-group">
                  <label for="">Location</label>
                  {{Form::select('',$cities,null,['class'=>"form-control",'id'=>'location'])}}
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                  <label for="">Address</label>
                  {{Form::textarea('',null,['rows'=>'2','class'=>"form-control",'id'=>'address'])}}
                </div>
                <img src="{{url('public/images/plus_24.png')}}" class="add-new-row">
              </div>
             <table class="table table-bordered intervies hide-elem" cellspacing="0" width="100%">
              <tr>
                <th>Date From</th>
                <th>Date To</th>
                <th>Location</th>
                <th>Address</th>
                <th>Action</th>
              </tr> 
              </table
          </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}

      <script>
        var city_list={!! json_encode($cities) !!};
        var r=0;
        $('.add-new-row').click(function(){
          $('.intervies').show();
          $('.intervies').append('<tr>\
            <td><input type="hidden" name="interview_detail['+r+'][date_from]" value="'+$('#date_time_from').val()+'">'+$('#date_time_from').val()+'</td>\
            <td><input type="hidden" name="interview_detail['+r+'][date_to]" value="'+$('#date_time_to').val()+'">'+$('#date_time_to').val()+'</td>\
            <td><input type="hidden" name="interview_detail['+r+'][location]" value="'+$('#location').val()+'">'+city_list[$('#location').val()]+'</td>\
            <td><input type="hidden" name="interview_detail['+r+'][address]" value="'+$('#address').val()+'">'+$('#address').val()+'</td>\
            <td><img src="{{url('public/images/error_img.png')}}" class="rmv-row"></td>\
          </tr>');
          //$('#date_time_from,#date_time_to,#location,#address').val('');
          r++;
        });
        $(document).on('click','.rmv-row',function(){
          $(this).parents('tr').remove();
        });

        $('#employers').change(function(){
          if($(this).val()==0){
            $('.new_info_company').show();
          } else {
            $('.new_info_company').hide();
          }
        })
      </script>
@stop