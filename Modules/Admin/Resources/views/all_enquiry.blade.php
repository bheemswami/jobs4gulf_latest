@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>Service Enquiry</h4>
        </div>
        <div class="form-parentBlock-inner"> 
        <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th>Name</th>                       
                        <th>Email</th>                       
                        <th>Designation</th>
                        <th>Mobile</th>
                        <th>Landline</th>
                        <th>Company</th>
                        <th>Location</th>
                        <th>Product Interested In</th>
                        <th>Comments</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $j=1; @endphp
                    @foreach($enquiry as $index=>$val)
                        @if($val->type=='1')
                        <tr>
                            <td>{{$j++}}. </td>
                            <td>{{$val->name}}</td>                       
                            <td>{{$val->email}}</td>                       
                            <td>{{$val->designation}}</td>
                            <td>{{$val->mobile}}</td>
                            <td>{{$val->landline}}</td>
                            <td>{{$val->company}}</td>
                            <td>{{$val->location}}</td>
                            <td>{{$val->pro_interest_in}}</td>
                            <td>{{$val->comments}}</td>
                            <td><button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-enquiry',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button></td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>Normal Enquiry</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th>Name</th>                       
                        <th>Email</th>                       
                        <th>Designation</th>
                        <th>Mobile</th>
                        <th>Landline</th>
                        <th>Company</th>
                        <th>Location</th>
                        <th>Product Interested In</th>
                        <th>Comments</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $j=1; @endphp
                    @foreach($enquiry as $index=>$val)
                        @if($val->type=='0')
                        <tr>
                            <td>{{$j++}}. </td>
                            <td>{{$val->name}}</td>                       
                            <td>{{$val->email}}</td>                       
                            <td>{{$val->designation}}</td>
                            <td>{{$val->mobile}}</td>
                            <td>{{$val->landline}}</td>
                            <td>{{$val->company}}</td>
                            <td>{{$val->location}}</td>
                            <td>{{$val->pro_interest_in}}</td>
                            <td>{{$val->comments}}</td>
                            <td>
                            <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-enquiry',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
