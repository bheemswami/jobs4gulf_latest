@extends('admin::layouts.master')
@section('content')

    {{ Form::open(array('url'=>route('save-advertise-feature'),'class'=>'plan-form','files' => true))}}  
    {{ csrf_field() }}
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Add Advertise Plan Feature</h4>
          </div>
          <div class="form-parentBlock-inner">              
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-12 form-group">
                <label for="">Feature Title</label>
                {{Form::text('feature_title',null,['class'=>"form-control"])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Value Type</label>
                {{Form::select('val_type',['Text','Boolean'],null,['id'=>'f_val_type','class'=>"form-control"])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Feaure Benifit (Basic)</label>
                <div class="basic_inputbox"> {{Form::text('basic_benefits',null,['class'=>"form-control",'placeholder'=>'Enter Benifit'])}} </div>
                <div class="basic_checkbox hide-elem"> 
                      <label class="radio-inline">{{ Form::radio('basic_plan_value',1,false,array('class'=>"")) }} Yes</label>
                      <label class="radio-inline">{{ Form::radio('basic_plan_value',0,false,array('class'=>"")) }} No</label>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Feaure Benifit (Premium)</label>
                <div class="premium_inputbox"> {{Form::text('premium_benefits',null,['class'=>"form-control",'placeholder'=>'Enter Benifit'])}} </div>
                <div class="premium_checkbox hide-elem"> 
                      <label class="radio-inline">{{ Form::radio('premium_plan_value',1,false,array('class'=>"")) }} Yes</label>
                      <label class="radio-inline">{{ Form::radio('premium_plan_value',0,false,array('class'=>"")) }} No</label>
                </div>
              </div>
              <div class="form-action col-sm-12">   
                <button type="submit" class="btn btn-primary submit-form">Submit</button>
              </div>
          </div>
     </div>

      {{Form::close()}}
   
     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Key Features</h4>
          </div>
          <div class="form-parentBlock-inner">
            <table class="table table-bordered features hide-elema" cellspacing="0" width="100%">
              <tr>
                <th>S.no.</th>  
                <th>Features</th>
                <th>Benefit(Basic)</th>
                <th>Benefit(Premium)</th>
                <th>Action</th>
              </tr> 
              
                @foreach($features as $k=>$val)             
                  <tr>
                    <td>{{$k+1}}. </td>
                    <td>{!! $val->feature_title !!}</td>
                    <td>
                      @if($val->val_type==0)
                        {{ $val->basic_benefits}}
                      @else
                        {{ ($val->basic_benefits==1) ? 'Yes' : 'No' }}
                      @endif
                    </td>
                    <td>
                      @if($val->val_type==0)
                        {{ $val->premium_benefits}}
                      @else
                        {{ ($val->premium_benefits==1) ? 'Yes' : 'No' }}
                      @endif
                    </td>
                    <td>
                      <a class="btn btn-danger btn-sm btn-icon icon-left" href="{{route('delete-advertise-feature',[$val->id])}}"  title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-times"></i>Delete</a>
                    </td>
                  </tr>
                @endforeach
            </table>
          </div>
     </div>
        

<script>
$('#f_val_type').change(function(){
    if($(this).val()==1){
      $('.basic_checkbox,.premium_checkbox').show();
      $('.basic_inputbox,.premium_inputbox').hide();
    } else {
      $('.basic_checkbox,.premium_checkbox').hide();
      $('.basic_inputbox,.premium_inputbox').show();

    }
})
</script>
@stop