@extends('admin::layouts.master')
@section('content')
@php $i=1; @endphp
    {{ Form::open(array('url'=>route('create-jobplan'),'class'=>'plan-form','files' => true))}}  
    {{ csrf_field() }}
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Plan Detail</h4>
          </div>
          <div class="form-parentBlock-inner">              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Plan Name</label>
                {{Form::text('name',null,['class'=>"form-control",'placeholder'=>'Enter Name'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">No. of Basic Jobs</label>
                {{Form::number('basic_jobs',null,['class'=>"form-control",'min'=>0,'placeholder'=>'Enter No. of Basic Jobs'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">No. of Premium Jobs</label>
                {{Form::number('premium_jobs',null,['class'=>"form-control",'min'=>0,'placeholder'=>'Enter No. of Premium Jobs'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Plan Price</label>
                {{Form::number('price',null,['class'=>"form-control",'placeholder'=>'Enter Price'])}}
              </div> 
             
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Status</label>
                {{Form::select('status',config('constants.status'),null,['class'=>"form-control"])}}
              </div>

             
          </div>
     </div>

   
     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Key Features</h4>
          </div>
          <div class="form-parentBlock-inner">
            <table class="table table-bordered features hide-elema" cellspacing="0" width="100%">
              <tr>
                <th>S.no.</th>  
                <th><input type="checkbox" id="select_all_checkbox"></th>     
                <th>Features</th>
                <th>Benefit(Unit)</th>
              </tr> 
              @foreach(config('constants.job_features_category') as $key=>$category)  
              <tr>
                <th colspan="4">{{ $category }} Fetaures</th>  
              </tr>  
                @foreach(config('constants.jobplan_features') as $k=>$val)
                  @if($key==$val['cat_id'])             
                    <tr>
                      <td>{{$i++}}. </td>
                      <td><input type="checkbox" name="key_features[]" value="{{$val['id']}}" class="data_checkbox"></td>
                      <td>{!! $val['name'] !!}</td>
                      <td>
                        @if($val['allow_unit']==1)
                            @if($val['id']==8)
                              {{ Form::select("units[".$val['id']."]",[0=>'Single Email-id',1=>'Multiple Email-id'],null,['class'=>"form-control"]) }}
                            @else
                              {{ Form::number("units[".$val['id']."]",null, ['class'=>"form-control"]) }}
                            @endif
                        @endif
                      </td>
                    </tr>
                  @endif
                @endforeach
              @endforeach
            </table>
          </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}
@stop