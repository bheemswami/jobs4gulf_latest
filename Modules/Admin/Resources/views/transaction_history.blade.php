@extends('admin::layouts.master')
@section('content')
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>All Transactions</h4>
          </div>
          <div class="form-parentBlock-inner">
              <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                      <th>S.No.</th>
                      <th>Date</th>
                      <th>Payment Id</th>
                      <th>Payment Status</th>
                      <th>Plan Price</th>
                      <th>Quantity</th>
                      <th>Total Amount</th>
                      <th>Currency</th>
                      <th>Payment Method</th>
                      <th>Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($transactions as $k=>$val)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>{{dateConvert($val->created_at,'d-m-Y h:i A')}}</td>
                      <td>{{$val->payment_id}}</td>
                      <td>{{$val->payment_state}}</td>
                      <td>{{$val->plan_price}}</td>
                      <td>{{$val->quantity}}</td>
                      <td>{{$val->plan_price*$val->quantity}}</td>
                      <td>{{$val->currency}}</td>
                      <td>{{ucfirst($val->payer_payment_method)}}</td>
                      <td>
                        <table class="table table-bordered" cellspacing="0" width="100%">
                          <tr>
                            <th>Purchase By :</th>
                            <td>{{ ($val->user!=null) ? $val->user->name : ''}}</td>
                          </tr>
                          <tr>
                            <th>Plan Name :</th>
                            <td>{{ ($val->job_plan!=null) ? $val->job_plan->name : ''}}</td>
                          </tr>
                          <tr>
                           <td colspan="2">{{$val->transaction_for}}</td>
                          </tr>
                        </table>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="7" class="bg-danger">No Transaction History</td>
                    </tr>
                    @endforelse
                    </tbody>
                  </table>
                
          </div>
     </div>
@stop
