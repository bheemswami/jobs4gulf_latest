@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All Latest News</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>
                        <th>News </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($news_list as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td> {{$val->title}}</td>
                            <td>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('update-news',[$val->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                               <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-news',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop
