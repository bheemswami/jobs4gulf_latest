@extends('admin::layouts.master')
@section('content')
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Employer Job Plans</h4>
          </div>
          <div class="form-parentBlock-inner">
               @include('admin::includes.purchase_plans')
                
          </div>
     </div>

     <div class="row form-parentBlock" style="position: relative;"> 
      <img src="{{url('public/images/image_1153550.gif')}}" class="hide-elem loader_image">
        {{ Form::open(array('url'=>route('save-job-plan'),'class'=>'assign-plan-form'))}}  
        {{ Form::hidden('user_id',Request::segment(3))}}           
          <div class="block-title">
            <h4>Assign Job Plan</h4>
          </div>
         <div class="form-parentBlock-inner">              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Select Plan</label>
                {{Form::select('plan_id',$job_plans,null,['class'=>"form-control",'id'=>'plans','placeholder'=>'--Select Plan--'])}}
              </div>

              
               <div class="form-parentBlock-inner plan_details hide-elem">
                  <table class="table table-bordered features hide-elema" cellspacing="0" width="100%">
                     <tr>
                      <th>Plan Name</th>
                      <td id="plan_name">N/A</td>
                      <th>Plan Price</th>
                      <td id="plan_price">0</td>
                    </tr>
                    
                    <tr>
                      <th>No. of Basic Jobs</th>
                      <td id="basic_jobs">0</td>
                      <th>No. of Premium Jobs</th>
                      <td id="premium_jobs">0</td>
                    </tr>
                    <tr>
                      <th>Plan Features</th>
                      <td colspan="3">
                        <table id="plan_features" class="table table-bordered" cellspacing="0" width="100%">
                          
                        </table>
                      </td>
                    </tr>

                    <tr>
                      <td><button type="submit" class="btn btn-primary submit-form">Apply Plan</button></td>
                      <td><button type="button" class="btn btn-success" id="gen_url">Generate Payment Url</button></td>
                      <td colspan="2"><input type="text" value="" readonly="readonly" id="payment_url" class="form-control">
                        <button type="button" class="btn btn-success hide-elem" id="send_invoice">Send Unpaid Invoice</button></td>
                    </tr>

                  </table>
                </div>
          </div>
          {{ Form::close() }}
     </div>
<script>
 globalVar.currPlanId=0;
       
$('#plans').change(function(){
    globalVar.currPlanId=$(this).val();
    $('#payment_url').val('');
    if(globalVar.currPlanId>0){
        $('.loader_image').show();
        $('#plan_price,#plan_name,#basic_jobs,#premium_jobs,#plan_features').html('');
        globalFunc.ajaxCall('api/get-plan-details-by-id', {plan_id:globalVar.currPlanId}, 'POST', globalFunc.before, globalFunc.resSuccess, globalFunc.error, globalFunc.complete);
    } else {
        $('.plan_details').hide();
   }
});

globalFunc.resSuccess=function(data){
    $('.plan_details').show();
    if(data.planDetails){
        $('#plan_price').html('&#8377; '+data.planDetails.price);
        $('#plan_name').html(data.planDetails.name);
        $('#basic_jobs').html(data.planDetails.basic_jobs);
        $('#premium_jobs').html(data.planDetails.premium_jobs);

    }
    if(data.planFeatures){
        $.each(data.planFeatures,function(index,val){
            $('#plan_features').append('<tr><td><i class="fa fa-caret-right"></i>&nbsp;&nbsp;'+val.title+'</td></tr>');
        });
    }
    $('.loader_image').hide();
}

  $(document).on('click','#gen_url',function(){
      $('.loader_image').show();
      var postData = {plan_id:globalVar.currPlanId, user_id: {{ $user_id }} };
      globalFunc.ajaxCall('api/generate-payment-url', postData, 'POST', globalFunc.before, globalFunc.generatedUrl, globalFunc.error, globalFunc.complete);
  });
  globalFunc.generatedUrl=function(data){
    $('.loader_image').hide();
    $('#send_invoice').show();
    $('#payment_url').val(data.url);
    globalVar.currPaymentId=data.payemt_id;
  }


  $(document).on('click','#send_invoice',function(){
      $('.loader_image').show();
      var postData = { payment_id:globalVar.currPaymentId };
      globalFunc.ajaxCall('api/send-unpaid-invoice', postData, 'POST', globalFunc.before, globalFunc.successSendInvoice, globalFunc.error, globalFunc.complete);
  });
  globalFunc.successSendInvoice=function(data){
    alert('Invoice Send Successfully.');
    $('.loader_image').hide();
  }
  
</script>
@stop
