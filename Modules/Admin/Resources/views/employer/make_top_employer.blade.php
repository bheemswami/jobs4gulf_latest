@extends('admin::layouts.master')
@section('content')
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Employer Job Plans</h4>
          </div>
          <div class="form-parentBlock-inner">
               @include('admin::includes.purchase_plans')
          </div>
     </div>

     <div class="row form-parentBlock" style="position: relative;"> 
        <img src="{{url('public/images/image_1153550.gif')}}" class="hide-elem loader_image">
        {{ Form::open(array('url'=>route('make-top-member'),'class'=>'make-top-form'))}}  
        {{ Form::hidden('emp_id',Request::segment(3))}}              
          <div class="block-title">
            <h4>Assign Job Plan</h4>
          </div>
         <div class="form-parentBlock-inner">              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Select Plan</label>
                {{Form::select('plan_id',$job_plans,null,['class'=>"form-control",'id'=>'plans','placeholder'=>'--Select Plan--'])}}
              </div>
              
              <div class="form-parentBlock-inner plan_details hide-elem">
                <table class="table table-bordered features hide-elema" cellspacing="0" width="100%">
                   <tr>
                    <th>Plan Name</th>
                    <td id="plan_name">N/A</td>
                    <th>Plan Price</th>
                    <td id="plan_price">0</td>
                  </tr>
                  <tr>
                    <th>No. of Basic Jobs</th>
                    <td id="basic_jobs">0</td>
                    <th>No. of Premium Jobs</th>
                    <td id="premium_jobs">0</td>
                  </tr>
                </table>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                  <label for="">No. of Walkins</label>
                  {{Form::number('no_of_walkins',null,['class'=>"form-control",'min'=>'0','placeholder'=>'No. of Walkins'])}}
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                  <label for="">Days of Validity (Walkins/Top Employer)</label>
                  {{Form::text('acc_validity',null,['class'=>"form-control",'id'=>'acc_validity','placeholder'=>'Account Validity','readonly'=>true])}}
                </div>
                <div class="col-lg-12">
                    <button name="status" type="submit" value="1" class="btn btn-success">Submit</button>
                </div>
              </div>
          </div>
          {{ Form::close() }}
     </div>
<script>
 globalVar.currPlanId=0;
       
$('#plans').change(function(){
    globalVar.currPlanId=$(this).val();
    $('#payment_url').val('');
    if(globalVar.currPlanId>0){
        $('.loader_image').show();
        $('#plan_price,#plan_name,#basic_jobs,#premium_jobs,#plan_features').html('');
        globalFunc.ajaxCall('api/get-plan-details-by-id', {plan_id:globalVar.currPlanId}, 'POST', globalFunc.before, globalFunc.resSuccess, globalFunc.error, globalFunc.complete);
    } else {
        $('.plan_details').hide();
   }
});

globalFunc.resSuccess=function(data){
    $('.plan_details').show();
    if(data.planDetails){
        $('#plan_price').html('&#8377; '+data.planDetails.price);
        $('#plan_name').html(data.planDetails.name);
        $('#basic_jobs').html(data.planDetails.basic_jobs);
        $('#premium_jobs').html(data.planDetails.premium_jobs);

    }
    if(data.planFeatures){
        $.each(data.planFeatures,function(index,val){
          if(val.feature_id==3){
            $('#acc_validity').val(val.units);
            return false;
          }
        });
    }
    $('.loader_image').hide();
}

 
  
</script>
@stop
