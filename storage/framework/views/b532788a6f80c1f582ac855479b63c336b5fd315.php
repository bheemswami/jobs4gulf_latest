<div class="col-sm-3 user-summery">
  <div class="panel widget light-widget panel-bd-top">
  <div class="panel-body profile-body">
    <div class="text-center vd_info-parent"> <img alt="example image" src="<?php echo e(checkFile($user->avatar,'uploads/user_img/','user_img.png')); ?>"> </div>
    <div class="row">
      <div class="col-xs-12"> 
        <span class="btn vd_btn vd_bg-blue btn-xs btn-block no-br"><i class="fa fa-suitcase append-icon"></i><?php echo e(($user->additional_info!=null) ? $user->additional_info->current_position : ''); ?></span>
      </div>     
    </div>
    
    <h2 class="font-semibold mgbt-xs-5 text-center"><?php echo e($user->name); ?></h2>
    <h5><strong>Company : </strong> <?php echo e(($user->additional_info!=null && $user->additional_info->company!=null) ? $user->additional_info->company->name : 'None'); ?></h5>
    <h5><strong>Email : </strong><span class="t-none"><?php echo e($user->email); ?></span> <?php echo ($user->email_verified==0) ? '<span class="unverified">Not verified</span>' : '<span class="verifylink">Verified</span>'; ?> </h5>
    <h5><strong>Tel : </strong> <?php echo e(($user->country_code!='') ? '+'.$user->country_code.'-' : ''); ?><?php echo e($user->mobile); ?> <?php echo ($user->mobile_verified==0) ? '<span class="unverified">Not verified</span>' : '<span class="verifylink">Verified</span>'; ?> </h5>
  </div>
  </div>
</div>
