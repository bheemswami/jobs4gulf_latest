
<?php $__env->startSection('content'); ?>
<div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 jobDetailBox">
            <div class="row paddingJobDetailBox">
                <div class="col-lg-3 text-center">
                    <?php if($job_detail->employer_info!=null): ?>
                        <img src="<?php echo e(checkFile($job_detail->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="jobDetailImg">
                    <?php else: ?> 
                        <img src="<?php echo e(url('public/images/default/company_logo.png')); ?>" class="jobDetailImg">
                    <?php endif; ?>
                </div>
                <div class="col-lg-9">
                    <div class="jobLabel1"><?php echo e(($job_detail->employer_info!=null) ? $job_detail->employer_info->comp_name : ''); ?></div>
                    <div class="jobLabel2"><?php echo e($job_detail->job_title); ?></div>
                    <div class="jobLabel3">Lorem ipsum dolor sit amet, eu cibo modus eruditi nam. Cu cetero appetere explicari eam. Usu ne electram disputationi. Erat blandit cotidieque vix ei, cu stet clita democritumnobis dicunt virtute ne sed</div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img class="jobDetailVacancy" src="<?php echo e(url('public/images/vacancy.png')); ?>">No. of Vacancy
                               </div>
                               <div class="vacancyNumber">
                                    <?php echo e($job_detail->total_vacancy); ?>

                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img class="jobDetailMoney" src="<?php echo e(url('public/images/doller.png')); ?>">Salary Range
                               </div>
                               <div class="vacancyNumber">
                                   <?php echo e($job_detail->currency . $job_detail->salary_min); ?> - <?php echo e($job_detail->currency . $job_detail->salary_max); ?>

                                   
                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img src="<?php echo e(url('public/images/location-icon.png')); ?>" class="jobDetailLocation">Location
                               </div>
                               <div class="vacancyNumber">
                                    <?php echo e(($job_detail->country!=null) ? $job_detail->country->name : ''); ?>

                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img class="jobDetailExp" src="<?php echo e(url('public/images/breifcase.png')); ?>">Experience
                               </div>
                               <div class="vacancyNumber">
                                    <?php echo e($job_detail->min_experience); ?>-<?php echo e($job_detail->max_experience); ?> Years
                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img class="jobDetailApplicants" src="<?php echo e(url('public/images/document.png')); ?>">Jobs Applicants
                               </div>
                               <div class="vacancyNumber">
                                    <?php echo e(count($job_detail->job_applicants)); ?>

                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 marginVacany">
                            <div class="jobDetailHeader">
                               <div>
                                   <img src="<?php echo e(url('public/images/postedOn.png')); ?>" class="jobDetailPOstedOn">Posted on
                               </div>
                               <div class="vacancyNumber">
                                    <?php echo e(dateConvert($job_detail->created_at,'jS M Y')); ?>

                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 jobDescritionCard1">
            <div class="paddingJobDetailBox">
                <div class="jobDescriptionLabel">
                    Job Description
                </div>
                <div class="jobDescription">
                    <p><?php echo $job_detail->job_description; ?></p>
                </div>
                <div class="jobDescriptionLabel mrgnTp30">
                    Job Summary
                </div>
                <div class="jobDescription">
                    <span><strong>Industry : </strong></span>
                    <span><?php echo e(($job_detail->industry!=null) ? $job_detail->industry->name : ''); ?></span><br/>
                    <span><strong>Functional Area : </span></strong>
                    <span><?php echo e(($job_detail->functional_area!=null) ? $job_detail->functional_area->name : ''); ?></span><br/>
                    <span><strong>Gender </span>:</strong>
                    <span><?php echo e($job_detail->gender); ?></span><br/>
                </div>
                <div class="jobDescriptionLabel mrgnTp30">
                    You can Contact
                </div>
                <div class="jobDescription">
                    <?php if($job_detail->response_email!=''): ?>
                        <?php
                            $emails=[];
                            $expEmails = explode(',', $job_detail->response_email);
                        ?>
                        <?php if(!empty($expEmails)): ?>
                            <strong><span>Email</span>:</strong>
                            <?php $__currentLoopData = $expEmails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $emails[]='<a href="mailto:'.$val.'">'.$val.'</a>';?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>     
                        <span><?php echo join(', ',$emails); ?></span><br/>  
                    <?php endif; ?>
    
                    <?php if($job_detail->cp_name!=''): ?>
                        <strong><span>Person </span>:</strong>
                        <span><?php echo e($job_detail->cp_title); ?> <?php echo e($job_detail->cp_name); ?></span><br/>
                    <?php endif; ?>

                    <?php if($job_detail->cp_mobile!=''): ?>
                        <strong><span>Contact Number </span>:</strong>
                        <span>+<?php echo e($job_detail->dial_number); ?>-<?php echo e($job_detail->cp_mobile); ?></span><br/>
                    <?php endif; ?>
                    <?php if($job_detail->walkin_address!=''): ?>
                        <strong><span>Address</span>:</strong>
                        <span><?php echo e($job_detail->walkin_address); ?></span><br/>
                    <?php endif; ?>
                </div>
                <div class="jobDescriptionLabel mrgnTp30">
                    Skills
                </div>
                <div class="row skillsRow">
                    <div class="col-xs-12 skills">
                        <?php if($job_detail->skills!=null): ?>
                            <?php $__currentLoopData = $job_detail->skills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($val->skill_keywords!=null): ?>
                                    <span><?php echo e($val->skill_keywords->name); ?></span>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="jobSkillsDesc">
                    Repeatedly dreamed alas opossum but dramatically despite expeditiously that jeepers loosely yikes that as or eel underneath kept and slept compactly far purred sure abidingly up above fitting to strident wiped set way
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 jobDescritionCard2">
            <div class="paddingJobDetailBox text-center">
                <div class="applyNowBox">
                    <?php if(Auth::check()): ?>
                        <?php if(Auth::user()->role=='2'): ?> 
                          <?php if(jobAppliedOrNot($job_detail->id)==0): ?>  
                            <a class="btn btn-success success-btn searchButton cardOpening" onclick="return confirm('Are you sure you want to apply?')" href="<?php echo e(url('apply-job/'.$job_detail->id)); ?>"><img src="<?php echo e(url('public/images/ap_now@2x.png')); ?>">Apply Now</a>
                          <?php else: ?>
                              <a class="btn btn-success success-btn searchButton cardOpening" href="#"><img src="<?php echo e(url('public/images/ap_now@2x.png')); ?>">Already Applied
                          <?php endif; ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <a class="btn btn-success success-btn searchButton cardOpening" data-toggle="modal" data-target="#loginModal">Apply Now</a>    
                    <?php endif; ?>
                </div>
                <div class="applicationEndStatus">
                    Application end in 5m 13d 22h 10min
                </div>
            </div>
            <div class="mrgnTp20">
                <div id="map"></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 similarJobsHeader">
            <div class="jobDescriptionLabel">
                Similar Jobs You May Like
            </div>
        </div>
        <?php $rJobs = getRelatedJobs($job_detail->id);?>
        <!-- related jobs -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 jobDescritionCard3">
            <?php $__currentLoopData = $rJobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <a href="<?php echo e(route('job-details',['jobid'=>$val->id])); ?>">
                    <div class="jobDetailCardBox2">
                        <div class="row">
                            <div class="col-xs-3 paddingRyt0">
                                <?php if($val->employer_info!=null): ?>
                                    <img src="<?php echo e(checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="cardImg2">
                                <?php else: ?> 
                                    <img src="<?php echo e(url('public/images/default/company_logo.png')); ?>" class="cardImg2">
                                <?php endif; ?>
                                
                            </div>
                            <div class="col-xs-9">
                                <div class="jobName"><?php echo e(($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt. Ltd.'); ?></div>
                                <div class="jobDesignation"><?php echo e($val->job_title); ?></div>
                                <div class="jobLocation"><img src="<?php echo e(url('public/images/location-icon.png')); ?>"><?php echo e(getCityNames($val->jobs_in_city)); ?> - <?php echo e(($val->country!=null) ? $val->country->name : ''); ?></div>
                            </div>
                        </div>
                        <div class="row skillsRow">
                            <div class="col-xs-12 skills">
                                <?php if($val->skills!=null): ?>
                                    <?php $counter=1; ?>
                                    <?php $__currentLoopData = $val->skills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($records->skill_keywords!=null && $counter<4): ?>
                                            <span><?php echo e($records->skill_keywords->name); ?></span>
                                        <?php endif; ?>
                                        <?php $counter++; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 mrgnTp12">
                                <span class="jobAmount"><img src="<?php echo e(url('public/images/doller.png')); ?>">
                                    <?php echo e($val->currency . $val->salary_min); ?> - <?php echo e($val->currency . $val->salary_max); ?>

                                </span>
                                <span class="jobDate"><?php echo countDays($val->created_at); ?></span>
                            </div>
                        </div>
        
                    </div>
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 jobDescritionCard2 margintp10">
            <div class="paddingJobDetailBox2">
                <div class="latestNewsBox">
                    <?php 
                        $latestNews = getLatestNews();
                    ?>  
                    <div class="jobDescriptionLabel2">
                        Latest News
                    </div>
                    <?php $__empty_1 = true; $__currentLoopData = $latestNews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="jobDescriptionLabel3">
                            <p><?php echo $val->title; ?></p>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <div class="jobDescriptionLabel3">
                            <p>No Latest News</p>
                        </div>
                        <li><i class="fa fa-flash"></i> <p>No Latest News</p></li>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>


<script>
    function myMap() {
        var myCenter = new google.maps.LatLng(12.893265, 77.622374);
        var mapCanvas = document.getElementById("map");
        var mapOptions = {center: myCenter, zoom: 14};
        var map = new google.maps.Map(mapCanvas, mapOptions);
        var marker = new google.maps.Marker({position:myCenter});
        marker.setMap(map);

        // Zoom to 9 when clicking on marker
        google.maps.event.addListener(marker,'click',function() {
            map.setZoom(9);
            map.setCenter(marker.getPosition());
        });
    }

    function placeMarker(map, location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        var infowindow = new google.maps.InfoWindow({
            content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
        });
        infowindow.open(map,marker);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
        
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>