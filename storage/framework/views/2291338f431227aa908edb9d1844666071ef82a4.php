<?php
if(!isset($form_class)){
  $form_class='';
}
$country = getAllCountries();
 foreach($country as $val){
     $all_countries[$val->id]=$val->name;
    if($val->is_gulf==1){
      $gulf_countries[$val->id]=$val->name;
    }
  }
?>
<?php if(Request::segment(1)==null): ?>

    <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 banner-form">
        <div class="search-header"><h2>SEARCH JOBS</h2></div>
        <?php echo e(Form::open(array('url'=>route('search-jobs'),'class'=>$form_class,'method' =>'GET','id'=>'search-job'))); ?>

              <div class="col-xs-12 col-sm-4 form-block nopadding">
                <?php echo e(Form::text('key_skills',null,['id'=>'skill','class'=>'job-title','placeholder'=>'Job Title, Keywords'])); ?>

              </div>
              <div class="col-xs-12 col-sm-3 form-block nopadding">
                <?php echo e(Form::select('exp_year',getDigitRange(0,5),null,['id'=>'exp','class'=>'job-exp','placeholder'=>'Exp (Years)'])); ?>

              </div>
              <div class="col-xs-12 col-sm-3 form-block nopadding">
                <?php echo e(Form::select('country_ids',['Gulf Country'=>$gulf_countries,'All Country'=>$all_countries],null,['id'=>'location','class'=>'job-location','placeholder'=>'Location'])); ?>

              </div>
              <div class="col-xs-12 col-sm-2 form-block nopadding">
                <button type="search" class="">Find Jobs</button>
              </div>
        <?php echo e(Form::close()); ?>

    </div>  
<?php else: ?>

        <div id="modifySearch" class="modal fade" role="dialog">
            <div class="modal-dialog modalModifySearch">
                <!-- Login model content-->
                <div class="modal-content modalModifyContent paddingBodySearch">
                    <div class="modal-body ">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-lg-offset-1"> 
                        <?php echo e(Form::open(array('url'=>route('search-jobs'),'class'=>$form_class,'method' =>'GET','id'=>'search-job'))); ?>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                <div class="searchTitle">
                                    Enter Keywords
                                </div>
                                <div>
                                    <div class="form-group">
                                    <?php echo e(Form::text('key_skills',null,['id'=>'skill','class'=>'form-control searchBtn job-title','placeholder'=>'Enter keywords (e.g. Architect )'])); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
                                <div class="searchTitle">
                                    Location
                                </div>
                                <div>
                                    <div class="form-group">
                                    <?php echo e(Form::select('country_ids',['Gulf Country'=>$gulf_countries,'All Country'=>$all_countries],null,['id'=>'location','class'=>'form-control searchBtn job-location','placeholder'=>'All Countries'])); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
                                <div class="searchTitle">
                                    Experience
                                </div>
                                <div>
                                    <div class="form-group">
                                    <?php echo e(Form::select('exp_year',getDigitRange(0,5),null,['id'=>'exp','class'=>'form-control searchBtn job-exp','placeholder'=>'Exp (Years)'])); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
                                <div>
                                    <div class="form-group">
                                        <button class="btn btn-success success-btn searchButton2" type="submit">Modify Search</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo e(Form::close()); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
<?php endif; ?>