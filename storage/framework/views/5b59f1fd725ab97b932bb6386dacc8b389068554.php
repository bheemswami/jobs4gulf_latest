
<?php $__env->startSection('content'); ?>
<div class="row topEmployerBanner">
    <span> Select Membership Type</span>
</div>
<div class="container">

    <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 premiumPlansHeader">
        <div class="jobDescriptionLabel">
            Select Membership type
        </div>
    </div> -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 premiumPlansBox text-center">
        <div class="row paddingJobDetailBox4 text-center">
            <span class="plansHeader">Thank You For Your Registeration</span>
        </div>
        <div class="jobDescriptionDesc text-center">
            Please Check Your email for the Verification Link <br>
            <span class="red">Click Here</span> to send again
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 premiumPlansHeader2">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 mt20 noPadding responsiveCard">
            <div class="cardBox3">
                <div class="ribbon-none mrgn_top_t col-sm-11 col-sm-offset-1">
                    
                </div>
                <div class="row planHeading1 text-center">
                    Free
                </div>
                <div class="row planHeading2 text-center">
                    0$ Lifetime
                </div>
                <div class="row planHeading3 text-center more">
                    Create Account, Search and apply for jobs.Get job alert and receive newsletters
                </div>
                <div class="row planHeading4 text-center">
                    <button type="button" class="btn btn-default color_litegrn">Continue with free</button>
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 mt20 noPadding responsiveCard">
            <div class="cardBox3">
                <div class="ribbon_green col-sm-11 col-sm-offset-1">
                    <p class="ribbon_text">
                        Most Popular
                    </p>
                </div>
                <div class="row planHeading1 text-center">
                    Pro
                </div>
                <div class="row planHeading2 text-center">
                    35$ for 3 Months
                </div>
                <div class="row planHeading3 text-center more">
                    Stand out of the croud ! We highlight your profile to get noticed by maximum employers/Recruiters
                </div>
                <div class="row planHeading4 text-center">
                    <button type="button" class="btn btn-default color_litegrn">Become a Pro</button>
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 mt20 noPadding responsiveCard">
            <div class="cardBox3">
                <div class="ribbon_orange col-sm-11 col-sm-offset-1">
                    <p class="ribbon_text">
                        Best Value
                    </p>
                </div>
                <div class="row planHeading1 text-center">
                    Super Change
                </div>
                <div class="row planHeading2 text-center">
                    55$ for 6 Months
                </div>
                <div class="row planHeading3 text-center more">
                    Stand out of the croud ! We highlight your profile to get noticed by maximum employers/Recruiters
                </div>
                <div class="row planHeading4 text-center">
                    <button type="button" class="btn btn-success colorGreen">Supercharge Now</button>
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 mt20 noPadding responsiveCard">
            <div class="cardBox3 marginNegative5">
                <div class="ribbon-none mrgn_top_t col-sm-11 col-sm-offset-1">
                    
                </div>
                <div class="row planHeading1 text-center">
                    Super Change
                </div>
                <div class="row planHeading2 text-center">
                    55$ for 6 Months
                </div>
                <div class="row planHeading3 text-center more">
                    Stand out of the croud ! We highlight your profile to get noticed by maximum employers/Recruiters
                </div>
                <div class="row planHeading4 text-center">
                    <button type="button" class="btn btn-success colorGreen">Supercharge Now</button>
                </div>

            </div>
        </div>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 premiumPlansBox text-center">
        <div class="row paddingJobDetailBox4 text-center">
            <span class="plansHeader">Benifits of Being Pro</span>
        </div>
        <div class="row marginBtm30">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center marginPlanBox paddingltr">
                <div class="seprate_boxes">
                <img src="<?php echo e(url('public/images/plan1.png')); ?>">
                <div class="planHeader">
                    Highlight Your<br>Profile
                </div>
                 <div class="planHeader2 more">
                    Your profile will be marked as <strong>"premium"</strong> to make you stand out in the jobs4gulf database as an active jobseeker.And it increase cv view by upto 70%.Premium candidates profile will be displayed on homepage and in oue exclusive panel for premium candidates.
                </div> 
            </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center marginPlanBox paddingltr">
                <div class="seprate_boxes">
                <img src="<?php echo e(url('public/images/plan2.png')); ?>">
                <div class="planHeader">
                    Increase Your Chances Of <br>Employment
                </div>
                <div class="planHeader2 more">
                   Premium candidates profiles are available to registered companies / consultants for Free.It increases your chances of getting more calls from Companies/consultants.
                </div>
            </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center marginPlanBox paddingltr">
                <div class="seprate_boxes">
                <img src="<?php echo e(url('public/images/plan3.png')); ?>">
                <div class="planHeader">
                    Get Priority In CV <br> Search
                </div>
                <div class="planHeader2 more">
                     Stand out of the croud ! your profile will be highlighted and will get Priority in cv search result by Companies/consultants.
                </div>
            </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center marginPlanBox paddingltr">
                <div class="seprate_boxes">
                <img src="<?php echo e(url('public/images/plan4.png')); ?>">
                <div class="planHeader">
                    Add Weight to your<br>Candidature
                </div>
                <div class="planHeader2 more">
                    Whenever Recruiters will search profile similar to your Category,your profile will get priority in search results and will be highlighted with bold eye catching boder with a tag of"Premium Candidate" his will add weight to your Candidature and chances of being viewed by Companies/Consultants

                </div>
            </div>
            </div>
        </div>

    </div>

</div>

<script>
    $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 100;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less ^";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>