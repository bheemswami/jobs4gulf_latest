<?php
  $banner = getPageBanner(Request::path(),Request::segment(1));
?>
<section id="inner-banner" class="custom-banner" style="background: url(<?php echo e(checkFile($banner->image,'/uploads/banner_img/','default_banner.png')); ?>) no-repeat center top;">
     
        <div class="container">
        <div class="banner-contents">
                <h1><?php echo e($banner->title); ?></h1>
                <span class="banner-text-fs_medium"><?php echo e($banner->description); ?></span>
                <span class="banner-text-fs_small"><?php echo e($banner->description_2); ?></span>
                <div class="banner-callus-button">Questions? Call us: </div>
        </div>
    </div>
  
</section>