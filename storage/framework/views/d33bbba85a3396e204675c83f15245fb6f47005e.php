<style>
.content{
    padding: 8px 31px !important;
    font-size: 13px  !important;
}
</style>
<?php echo $__env->make('email_templates.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <tr>
        <td class="content">
            <h5>Hi <?php echo e(ucfirst($params['user_name'])); ?></h5>
            <p>Welcome to Jobs4Gulf, now it's time to accelerate your job hunt. </p> 
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <strong>Here's what you can do with your Jobs4Gulf account </strong>
        </td>
    </tr>
     <tr>
        <td class="content">
           <img src="">
            <strong>Apply for jobs</strong><br/>
            <span>Your CV will be instantly delivered to the employer and they can contact you directly for an interview. </span>
        </td>
    </tr>
    <tr>
        <td class="content">
           <img src="">
            <strong>Get Contacted by top employers</strong><br/>
            <span>You can be contacted directly by the employers and recruitment agencies who search our CV database (you can limit who can see your profile). </span>
        </td>
    </tr>
    <tr>
        <td class="content">
           <img src="">
            <strong>Receive regular job updates</strong><br/>
            <span>You will receive an automated email from us everytime there is an opportunity matching your profile and preferences. 
    </tr>
    <tr>
        <td>                       
           <br/>
            <p>Wish you all the best with your job search !<br/>
            <em>Team Jobs4Gulf</em></p>                        
        </td>
    </tr>

<?php echo $__env->make('email_templates.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>