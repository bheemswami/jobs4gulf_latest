
<?php $__env->startSection('content'); ?>

  <?php 
      $flag=0;
      $heading='Add';
      if(isset($industry_data) && !empty($industry_data)){
          $flag=1;
          $heading='Update';
      }
  ?>
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4><?php echo e($heading); ?> Industry</h4>
          </div>
          <?php if($flag==1): ?>
                  <?php echo e(Form::model($industry_data,array('url'=>route('save-industry'),'class'=>'industry-form','files' => true))); ?>

                      <?php echo e(Form::hidden('id',null)); ?>

              <?php else: ?>
                  <?php echo e(Form::open(array('url'=>route('save-industry'),'class'=>'industry-form','files' => true))); ?>

              <?php endif; ?>
          <div class="form-parentBlock-inner"> 
              
              <?php echo e(csrf_field()); ?>

              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 form-group">
                <label for="">Name</label>
                <?php echo e(Form::text('name',null,['class'=>"form-control",'placeholder'=>'Enter Name'])); ?>

              </div>
              
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 form-group">
                <label for="">Status</label>
                <?php echo e(Form::select('status',config('constants.status'),1,['class'=>"form-control",'placeholder'=>'--Select--'])); ?>

              </div>

              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 form-group">
                <label for="">Icon</label>
                <div class="box">
                  <?php echo e(Form::file('img',['class'=>"form-control",'id'=>'file-1'])); ?>

                </div>                
                  <?php if($flag==1): ?>
                    <img class="icon-medium" src="<?php echo e(checkFile($industry_data->icon,'uploads/industry_img/','no-img.png')); ?>"/>
                  <?php endif; ?>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 form-group">
                <label for="">Icon</label>
                <div class="box">
                  <?php echo e(Form::file('img_hover',['class'=>"form-control",'id'=>'file-1'])); ?>

                </div>                
                  <?php if($flag==1): ?>
                    <img class="icon-medium" src="<?php echo e(checkFile($industry_data->icon_hover,'uploads/industry_img/','no-img.png')); ?>"/>
                  <?php endif; ?>
              </div>
       </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      <?php echo e(Form::close()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>