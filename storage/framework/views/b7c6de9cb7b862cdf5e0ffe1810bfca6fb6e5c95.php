

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('front_panel.includes.employer_page_banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">

      <?php if($user->email_verified==0): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
           <a href="<?php echo e(route('send-verify-mail')); ?>">Verify</a> your email to increase your visibility and search-ability.
        </div>
      <?php endif; ?>
     
      
      <div class="row">       
        <?php echo $__env->make('front_panel.includes.employer_dashboard_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>   
              
        <div class="col-sm-9">
          <div class="tabs widget profile-area">
            <div class="tab-content">
              <div id="profile-tab" class="tab-pane active">
                <div class="pd-20">       
                    <div class="vd_info tr"> <a class="btn vd_btn btn-xs vd_bg-yellow" href="<?php echo e(route('emp-profile-update')); ?>"> <i class="fa fa-pencil append-icon"></i> Edit </a> </div>
                    <div class="row">
                     
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
                        <div class="form-title">
                            <h4 class="contactinfo">Contact Information</h4>
                        </div>
                         <div class="col-sm-12">
                          <div class="row mgbt-xs-0">
                            <label class="col-xs-6 control-label">Username:</label>
                            <div class="col-xs-6 controls">
                              <?php echo e($user->email); ?> <?php echo ($user->email_verified==0) ? '<span class="unverified">Not Verified</span>' : '<span class="verifylink">Verified</span>'; ?>

                            </div>
                          </div>
                        </div>
                        <?php if($user->employer_info !=null): ?>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Contact Person:</label>
                              <div class="col-xs-6 controls">
                                <?php echo e(ucfirst($user->employer_info->cp_name)); ?>

                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Designation:</label>
                              <div class="col-xs-6 controls">
                                <?php echo e(($user->employer_info->designations !=null) ? $user->employer_info->designations->name : 'None'); ?>

                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Phone:</label>
                              <div class="col-xs-6 controls">
                                <?php echo e(($user->employer_info->dial_code>0) ? '+'.$user->employer_info->dial_code.'-' : ''); ?><?php echo e(($user->employer_info !=null) ? $user->employer_info->cp_mobile : '**********'); ?>

                              </div>
                            </div>
                          </div>
                        <?php endif; ?>
                      </div>
                   
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
                        <div class="form-title">
                          <h4 class="factory">Company Information</h4>
                        </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Name:</label>
                              <div class="col-xs-6 controls"><?php echo e(($user->employer_info !=null) ? $user->employer_info->comp_name : 'None'); ?></div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Address:</label>
                              <div class="col-xs-6 controls">
                                <?php echo e(($user->employer_info !=null) ? $user->employer_info->comp_address : ''); ?>,
                                <?php echo e(($user->employer_info->city !=null) ? $user->employer_info->city->name : ''); ?>,
                                <?php echo e(($user->employer_info->country !=null) ? $user->employer_info->country->name : ''); ?>

                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Website:</label>
                              <div class="col-xs-6 controls"><?php echo e(($user->employer_info !=null) ? $user->employer_info->comp_website : 'None'); ?></div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Industry:</label>
                              <div class="col-xs-6 controls"><?php echo e(($user->employer_info->Industry !=null) ? $user->employer_info->industry->name : 'None'); ?></div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Profile:</label>
                              <div class="col-xs-6 controls"><?php echo ($user->employer_info !=null) ? $user->employer_info->comp_about : 'None'; ?></div>
                            </div>
                          </div>
                          
                      </div>
                      <?php if($user->employer_info!=null): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
                          <div class="form-title">
                            <h4 class="contactinfo">Social Links</h4>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Facebook Url:</label>
                              <div class="col-xs-6 controls"><?php echo e(($user->employer_info->fb_link !='') ? $user->employer_info->fb_link : 'NA'); ?></div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">Twitter Url:</label>
                              <div class="col-xs-6 controls"><?php echo e(($user->employer_info->twitter_link !='') ? $user->employer_info->twitter_link : 'NA'); ?></div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-6 control-label">G-Plus Url:</label>
                              <div class="col-xs-6 controls"><?php echo e(($user->employer_info->gplus_link !='') ? $user->employer_info->gplus_link : 'NA'); ?></div>
                            </div>
                          </div>
                        </div>
                      <?php endif; ?>
                    </div>
                </div>
              </div>
            </div>
            <!-- tab-content --> 
          </div>
          <!-- tabs-widget -->              
        </div>
      </div>
    </div>
  </section>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>