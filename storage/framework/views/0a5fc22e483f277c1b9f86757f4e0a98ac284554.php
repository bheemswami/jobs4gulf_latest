
<?php $__env->startSection('content'); ?>

<section id="inner-banner" class="dashboard-banner" style="background: url(<?php echo e(url('public/images/inner-banner.jpg')); ?>) no-repeat center top;">
<div class="overlay"></div>
 <?php echo $__env->make('front_panel/includes/search_job_section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</section>

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">

      <?php if($user->email_verified==0): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
           <a href="<?php echo e(route('send-verify-mail')); ?>">Verify</a> your email to increase your visibility and search-ability.
        </div>
      <?php endif; ?>
     
      
      <div class="row">      	
        <?php echo $__env->make('front_panel.includes.candidate_dashboard_sidebar',['user'=>$user], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
              
        <div class="col-sm-9">
          <div class="tabs widget profile-area">
				  <ul class="nav nav-tabs widget">
				    <li class="active"> <a data-toggle="tab" href="#profile-tab"> Profile <span class="menu-active"><i class="fa fa-caret-up"></i></span>  </a></li>
				    <li class=""> <a data-toggle="tab" href="#applied-tab"> Applied Jobs  <span class="menu-active"><i class="fa fa-caret-up"></i></span></a></li>    
				    <li> <a data-toggle="tab" href="#activity-tab"> Last Activity  <span class="menu-active"><i class="fa fa-caret-up"></i></span></a></li>
				    <li> <a data-toggle="tab" href="#rec-tab"> Recommended Jobs  <span class="menu-active"><i class="fa fa-caret-up"></i></span></a></li>
				  </ul>

<div class="tab-content">
  <div id="profile-tab" class="tab-pane active">
    <div class="pd-20">       
            <div class="vd_info tr"> <a class="btn vd_btn btn-xs vd_bg-yellow" href="<?php echo e(route('candidate-profile')); ?>"> <i class="fa fa-pencil append-icon"></i> Edit </a> </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
              <h4 class="personal_detail">Your Personal Details</h4>
            </div>
            
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Full Name:</label>
                <div class="col-xs-6 controls"><?php echo e($user->title); ?> <?php echo e(ucfirst($user->name)); ?></div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">User Name:</label>
                <div class="col-xs-6 controls"><?php echo e($user->email); ?></div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">City:</label>
                <div class="col-xs-6 controls"><?php echo e(($user->city!=null) ? $user->city->name : '-'); ?></div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Country:</label>
                <div class="col-xs-6 controls"><?php echo e(($user->country!=null) ? $user->country->name : '-'); ?></div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Date of Birth:</label>
                <div class="col-xs-6 controls"><?php echo e(dateConvert($user->dob,'d-m-Y')); ?></div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Phone:</label>
                <div class="col-xs-6 controls"><?php echo e(($user->country_code!='') ? '+'.$user->country_code.'-' : ''); ?><?php echo e($user->mobile); ?></div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
                <h4 class="edu_detail">Your Education Details</h4>
            </div>
           
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">                
                <div class="col-xs-12 controls">
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Education</th>
                      <th>Course</th>
                      <th>Specialization</th>
                      <th>Year</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>Basic</th>
                      <td><?php echo e(($user->education !=null) ? @$user->education->basic_course->name : ''); ?></td>
                      <td><?php echo e(($user->education !=null) ? @$user->education->basic_specialization->name : ''); ?></td>
                      <td><?php echo e(($user->education !=null) ? @$user->education->basic_comp_year : ''); ?></td>
                    </tr>
                     <?php if($user->education !=null && $user->education->master_course!=null): ?>
                    <tr>
                      <th>Master</th>
                      <td><?php echo e(($user->education !=null) ? @$user->education->master_course->name : ''); ?></td>
                      <td><?php echo e(($user->education !=null) ? @$user->education->master_specialization->name : ''); ?></td>
                      <td><?php echo e(($user->education !=null) ? @$user->education->master_comp_year : ''); ?></td>
                    </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                </div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Institute / University:</label>
                <div class="col-xs-6 controls"><?php echo e(($user->education !=null) ? $user->education->institute : ''); ?></div>
                <!-- col-sm-10 --> 
              </div>
            </div>
          </div>
       
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
              <h4 class="work_exp">Your Current Employment Details</h4>
            </div>
            <?php if($user->additional_info!=null && $user->additional_info->exp_level): ?>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Work Experiance:</label>
                  <div class="col-xs-6 controls"><?php echo e($user->additional_info->exp_year); ?> Year <?php echo e($user->additional_info->exp_month); ?> Month</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Employer Name:</label>
                  <div class="col-xs-6 controls"><?php echo e(($user->additional_info!=null) ? $user->additional_info->current_company : 'None'); ?></div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Employer's Industry:</label>
                  <div class="col-xs-6 controls"><?php echo e($user->additional_info->industry->name); ?></div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Position:</label>
                  <div class="col-xs-6 controls"><?php echo e($user->additional_info->current_position); ?></div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Monthly Salary:</label>
                  <div class="col-xs-6 controls"><?php echo e($user->additional_info->salary); ?> (<?php echo e(config('constants.currency')[$user->additional_info->salary_in]); ?>)</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Functional Area / Department:</label>
                  <div class="col-xs-6 controls"><?php echo e($user->additional_info->function_area->name); ?></div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
            <?php endif; ?>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Key Skills:</label>
                  <div class="col-xs-6 controls"><?php echo e(($user->additional_info!=null) ? $user->additional_info->key_skills : ''); ?></div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
          </div>

          
      </div>
    </div>
      <!-- pd-20 --> 
  </div>
    <!-- home-tab -->
    <?php 
    $total_ojobs=$total_cjobs=0; 
    ?>
    <div id="applied-tab" class="tab-pane">
      <div class="pd-20">       
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
                <h4 class="open_job">Open Jobs (<jobcount id="ojob">0</jobcount>)</h4>
            </div>
            <div class="col-sm-12">
              <table class="table table-striped table-bordered bdata-table" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Job Title</th>
                    <th>Total Vacancy</th>
                    <th>Experience</th>
                    <th>Salary</th>
                    <th>Apply Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__empty_1 = true; $__currentLoopData = $applied_jobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <?php if($val->jobs!=null && strtotime($val->jobs->expiry_date)>=strtotime(date('Y-m-d'))): ?>
                      <?php $total_ojobs=$total_ojobs+1; ?>
                      <tr>
                        <td><?php echo e(++$k); ?>.</td>
                        <td><?php echo e($val->jobs->job_title); ?></td>
                        <td><?php echo e($val->jobs->total_vacancy); ?></td>
                        <td><?php echo e($val->jobs->min_experience); ?>-<?php echo e($val->jobs->max_experience); ?> Year</td>
                        <td><?php echo e($val->jobs->salary_min); ?>-<?php echo e($val->jobs->salary_max); ?> <?php echo e($val->jobs->currency); ?></td>
                        <td><?php echo e(dateConvert($val->created_at,'d M Y')); ?></td>
                        <td><a href="<?php echo e(route('job-details',['jobid'=>$val->jobs->id])); ?>" class="btn btn-info btn-xs">Detail</a></td>
                      </tr>
                    <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <?php endif; ?>               
                </tbody>
              </table>
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
                <h4 class="close_job">Close Jobs (<jobcount id="cjob">0</jobcount>)</h4>
            </div>
            <div class="col-sm-12">
              <table class="table table-striped table-bordered bdata-table" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Job Title</th>
                    <th>Total Vacancy</th>
                    <th>Experience</th>
                    <th>Salary</th>
                    <th>Expire Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__empty_1 = true; $__currentLoopData = $applied_jobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <?php if($val->jobs!=null && strtotime($val->jobs->expiry_date)<strtotime(date('Y-m-d'))): ?>
                      <?php $total_cjobs=$total_cjobs+1; ?>
                      <tr>
                        <td><?php echo e(++$k); ?>.</td>
                        <td><?php echo e($val->jobs->job_title); ?></td>
                        <td><?php echo e($val->jobs->total_vacancy); ?></td>
                        <td><?php echo e($val->jobs->min_experience); ?>-<?php echo e($val->jobs->max_experience); ?> Year</td>
                        <td><?php echo e($val->jobs->salary_min); ?>-<?php echo e($val->jobs->salary_max); ?> <?php echo e($val->jobs->currency); ?></td>
                        <td><?php echo e(dateConvert($val->jobs->expire_date,'d M Y')); ?></td>
                      </tr>
                    <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <?php endif; ?>               
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
    
    <div id="activity-tab" class="tab-pane">
    	<div class="row">
          <div class="col-sm-12">
            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-globe mgr-10 profile-icon"></i> ACTIVITY</h3>
            <div class="">
              <div class="content-list">
                <div data-rel="scroll" class="mCustomScrollbar _mCS_6" style="overflow: hidden;"><div class="mCustomScrollBox mCS-light" id="mCSB_6" style="position: relative; height: 100%; overflow: hidden; max-width: 100%; max-height: 400px;"><div class="mCSB_container" style="position: relative; top: 0px;">
                  <ul class="list-wrapper">
                    <li> <span class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></span> <span class="menu-text"> Someone has give you a surprise <span class="menu-info"><span class="menu-date"> ~ 12 Minutes Ago</span></span> </span>  </li>
                    <li> <span class="menu-icon vd_blue"><i class=" fa fa-user"></i></span> <span class="menu-text"> Change your user profile details <span class="menu-info"><span class="menu-date"> ~ 1 Hour 20 Minutes Ago</span></span> </span> </li>
                    <li> <span class="menu-icon vd_red"><i class=" fa fa-cogs"></i></span> <span class="menu-text"> Your setting is updated <span class="menu-info"><span class="menu-date"> ~ 12 Days Ago</span></span> </span></li>
                    <li>  <span class="menu-icon vd_green"><i class=" fa fa-book"></i></span> <span class="menu-text"> Added new article <span class="menu-info"><span class="menu-date"> ~ 19 Days Ago</span></span> </span>  </li>
                    <li>  <span class="menu-icon vd_red"><i class=" fa fa-cogs"></i></span> <span class="menu-text"> Your setting is updated <span class="menu-info"><span class="menu-date"> ~ 12 Days Ago</span></span> </span>  </li>
                    <li>  <span class="menu-icon vd_green"><i class=" fa fa-book"></i></span> <span class="menu-text"> Added new article <span class="menu-info"><span class="menu-date"> ~ 19 Days Ago</span></span> </span> </li>
                  </ul>
                </div><div class="mCSB_scrollTools" style="position: absolute; display: block; opacity: 0;"><div class="mCSB_draggerContainer"><div class="mCSB_dragger" style="position: absolute; top: 0px; height: 352px;" oncontextmenu="return false;"><div class="mCSB_dragger_bar" style="position: relative; line-height: 352px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>
                <div class="closing text-center" style=""> <a href="#">See All Activities <i class="fa fa-angle-double-right"></i></a> </div>
              </div>
            </div>
          </div>
                   
        </div>	 
    </div> <!-- photos tab -->
    <div id="rec-tab" class="tab-pane">
	    	<h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-suitcase mgr-10 profile-icon"></i> Recommended Jobs</h3>
	    
    	<ul>
        <?php $__empty_1 = true; $__currentLoopData = $recommended_jobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    		<li>
    	   	 <p><?php echo e(++$k); ?>. <a href="<?php echo e(route('job-details',['jobid'=>$val->id])); ?>" target="_blank"><?php echo e($val->job_title); ?> ( <?php echo e($val->min_experience); ?> - <?php echo e($val->max_experience); ?> yrs. ) </a></p>
			    <p><span class="text-secondary"><?php echo e($val->comp_name); ?> </span></p>
			    <p><span class="text-success"><?php echo e($val->country_name); ?>  - United Arab Emirates</span></p>
			 </li>
       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
       <li><p>No jobs available for now, Click here to <a href="<?php echo e(route('search-jobs')); ?>" target="_blank">explore more</a></p></li>
       <?php endif; ?>
			 
		</ul>	 
    </div>  <!-- photos tab -->  
    
    
  </div>
  <!-- tab-content --> 
</div>
<!-- tabs-widget -->              
</div>
      </div>
    </div>
  </section>
</section>
<script>
  $(document).ready(function() {
    $('#ojob').text(<?php echo e($total_ojobs); ?>);
    $('#cjob').text(<?php echo e($total_cjobs); ?>);
    $('.bdata-table').DataTable({
        "paging": true,
        "bLengthChange": false,
        "ordering": false, 
        "searching":false
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>