<?php 
$user=loggedEmployerProfile();
$freePlan=planPurchasedOrNot(1);
?>

<div class="col-sm-3 user-summery">
  <div class="panel widget light-widget panel-bd-top">
  <div class="panel-body profile-body">
    <div class="text-center vd_info-parent"> <img alt="example image" src="<?php echo e(checkFile($user->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>"> </div>
   <div class="row">
      <div class="col-xs-12"> 
        <span class="btn vd_btn vd_bg-blue btn-xs btn-block no-br">
            
            <?php echo e(($user->employer_info!=null) ? $user->employer_info->comp_name : 'None'); ?>

        </span>
      </div>     
    </div>


<div class="row">
    <!-- uncomment code for absolute positioning tweek see top comment in css -->
    
    <!-- Menu -->
    <div class="side-menu">
    
    <nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
  
    <!-- Main Menu -->

    <div class="side-menu-container">
        <ul class="nav navbar-nav">

            <li class="active"><a href="<?php echo e(route('employer-dashboard')); ?>"><i class="fa fa-list-ul append-icon"></i>Dashboard</a></li>
            
            <li><a href="<?php echo e(route('slider-images')); ?>"><i class="fa fa-globe append-icon"></i> Slider Image</a></li>
            <li><a href="<?php echo e(route('purchased-plans')); ?>"><i class="fa fa-globe append-icon"></i> Purchased Plans</a></li>
           
            <li><a href="<?php echo e(route('job-plans')); ?>"><i class="fa fa-globe append-icon"></i> Upgrade Plans
                <?php if($freePlan && strtotime($freePlan->plan_end_date)>strtotime(date('Y-m-d'))): ?>
                <img src="<?php echo e(url('public/images/new.gif')); ?>">
                <?php endif; ?>
            </a></li>
            <li class="panel panel-default" id="dropdown">
                <a data-toggle="collapse" href="#dropdown-lvl1">
                    <span class="glyphicon glyphicon-user"></span> Response Manager <span class="caret"></span>
                </a>
                <!-- Dropdown level 1 -->
                <div id="dropdown-lvl1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo e(route('job-post')); ?>"><i class="fa fa-globe append-icon"></i> Post A Job</a></li>
                            <li><a href="<?php echo e(route('posted-jobs')); ?>"><i class="fa fa-globe append-icon"></i> Posted Jobs</a></li>
                            </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
    
    </div>

   
</div>

  </div>
  </div>
</div>
