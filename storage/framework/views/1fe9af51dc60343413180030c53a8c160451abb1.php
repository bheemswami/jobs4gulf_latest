
<?php if($errors->all()): ?>
<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 banner-form">
	<ul>
		  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  	<li><?php echo e($val); ?></li>
		  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
  </div>
<?php endif; ?>
