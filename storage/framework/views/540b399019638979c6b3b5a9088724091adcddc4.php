<?php echo $__env->make('email_templates.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 
                <tr>
                    <td class="content">

                        <h5>Dear <?php echo e(ucfirst($params['user_name'])); ?></h5>
                        <p>Thank you for your interest in creation of Jobs4Gulf account.</p>
                        <p>Please confirm your email address by clicking on the button below. We'll communicate with you from time to time via email so it's important that we have an up-to-date email address on file. </p>

                        <table>
                            <tr>
                                <td align="center">
                                    <p>
                                        <a href="<?php echo e(route('email-verify',['uid'=>base64_encode($params['user_id']),'email'=>base64_encode($params['email'])])); ?>" class="button">Verify Your E-mail</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <h6>Why e-mail verification is required?</h6>
                        <p class="light-color">1. We want to be sure that it is you who operate/use this e-mail ID.<br/>
                        2. It is only after you confirm your e-mail; our customer service team will review your details for creation of a your account.</p>
                        <br/>
                        <p>Regards,<br/>
                        <em>Team Jobs4Gulf</em></p>                        
                    </td>
                </tr>
            </table>

        </td>
    </tr>
   
<?php echo $__env->make('email_templates.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>