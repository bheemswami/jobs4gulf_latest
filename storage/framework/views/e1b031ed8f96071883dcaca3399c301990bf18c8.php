
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('front_panel/includes/page_banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<section class="main-inner-page lite-greyBg">
    <div class="container contact">
      <div class="row">       
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 category-parent-box card">
            <h2>Site Map</h2>
            
            <div class="col-lg-4">
                      <strong>Global</strong>
                      <ul class="sm_UL">
                          <li>
                              <a href="<?php echo e(url('/')); ?>">Home</a>
                              <a href="<?php echo e(route('about-us')); ?>" target="_blank">About Us</a>
                              <a href="<?php echo e(route('contact-us')); ?>">Contact Us</a>
                          </li>
                      </ul>
                       <br>
                     <strong>Job Seeker</strong>
                      <ul class="sm_UL">
                          <li>
                              <a class="registerRR" href="<?php echo e(route('candidate-register')); ?>">Post Your CV</a>
                              <a href="<?php echo e(route('search-jobs')); ?>">Search Jobs</a>
                          </li>
                      </ul>
                       <br>
                       <strong>Jobs by Country</strong>
                      <ul class="sm_UL">
                          <li>
                              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <a href="<?php echo e(url('search-jobs?country_ids='.$val->id)); ?>" >Jobs for <?php echo e($val->name); ?></a>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                              <a href="<?php echo e(route('jobs-by',['country'])); ?>">View All Countries</a>
                          </li>
                      </ul>
                      <br>
                      <strong>Jobs by Industry</strong>
                      <ul class="sm_UL">
                        <li>
                         <?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <a href="<?php echo e(url('search-jobs?industry_ids='.$val->id)); ?>" > <?php echo e($val->name); ?></a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       <a href="<?php echo e(route('jobs-by',['industry'])); ?>">View All Industries</a>
                     </li>
                      </ul>
                      
                      
                  </div>

                 
          </div>
      </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>