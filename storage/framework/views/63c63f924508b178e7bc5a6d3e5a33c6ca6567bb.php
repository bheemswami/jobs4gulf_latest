
<?php $__env->startSection('content'); ?>
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All Industry</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th>Image </th>
                        <th>Name </th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($k+1); ?>. </td>
                            <td><img width="60px" src="<?php echo e(checkFile($val->icon,'uploads/industry_img/','no-img.png')); ?>"></td>
                            <td> <?php echo e($val->name); ?></td>
                            <td> 
                                <?php if($val->status==1): ?>
                                    <span class="label label-success"> Active</span> 
                                <?php else: ?>
                                    <span class="label label-default"> Inactive</span>
                                <?php endif; ?>
                            </td>
                            <td>
                               <a class="btn btn-default btn-sm btn-icon icon-left" href="<?php echo e(route('update-industry',[$val->id])); ?>" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                               <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="<?php echo e(route('delete-industry',[$val->id])); ?>" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>