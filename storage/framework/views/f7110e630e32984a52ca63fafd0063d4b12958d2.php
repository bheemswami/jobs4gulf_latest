
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row mrginTpBtm20">
        <div class="col-xs-6 jobs_country_head">
            <h4>Jobs By <?php echo e(isset($title) ? $title : ''); ?></h4>
        </div>
        <div class="col-xs-6">
            <?php echo e(Form::open(array('url'=>route('jobs-by',$type)))); ?>

            <div class="input-group add-on">
                <input class="form-control searchInputTE" placeholder="Search"  type="text" name="search">
                <div class="input-group-btn">
                    <button class="btn btn-success success-btn searchButton" type="submit">Search</button>
                </div>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <script>
        function toggleLeftNav(id){
            $("#"+id).toggle();
        }
    </script>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding ">
            <?php if($title=='Country'): ?>
                <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-sm-3 col-md-3 col-lg-3 mt20 responsiveCard">
                        <div class="cardBox">
                            <div class="row">
                                <div class="col-xs-12 text-center margin_btm10">
                                    <?php if($val->icon!=''): ?>
                                        <img class="flagimg" alt="image" src="<?php echo e($val->icon); ?>">
                                    <?php else: ?>
                                        <img class="flagimg" alt="image" src="<?php echo e(checkFile($val->icon,'uploads/country_img/','small-logo.png')); ?>">
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyName"><a href="<?php echo e(url('search-jobs?country_ids='.$val->id)); ?>" ><?php echo e($val->name); ?></a></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyLoc">(<?php echo e((isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0); ?>)</div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <?php if($title=='City'): ?>
                <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-sm-3 col-md-3 col-lg-3 mt20 responsiveCard">
                        <div class="cardBox">
                            <div class="row">
                                <div class="col-xs-12 text-center margin_btm10">
                                    <?php if($val->icon!=''): ?>
                                        <img class="flagimg" alt="image" src="<?php echo e(checkFile($val->icon,'uploads/city_img/','small-logo.png')); ?>">
                                    <?php else: ?>
                                        <img class="flagimg" alt="image" src="<?php echo e(checkFile($val->icon,'uploads/city_img/','small-logo.png')); ?>">
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyName"><a href="<?php echo e(url('search-jobs?city_ids='.$val->id)); ?>" ><?php echo e($val->name); ?></a></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyLoc">(<?php echo e((isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0); ?>)</div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <?php if($title=='Industry'): ?>
                <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-sm-3 col-md-3 col-lg-3 mt20 responsiveCard">
                        <div class="cardBox">
                            <div class="row">
                                <div class="col-xs-12 text-center margin_btm10">
                                    <?php if($val->icon!=''): ?>
                                        <img class="flagimg" alt="image" src="<?php echo e(checkFile($val->icon,'uploads/industry_img/','small-logo.png')); ?>">
                                    <?php else: ?>
                                        <img class="flagimg" alt="image" src="<?php echo e(checkFile($val->icon,'uploads/industry_img/','small-logo.png')); ?>">
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyName"><a href="<?php echo e(url('search-jobs?industry_ids='.$val->id)); ?>" ><?php echo e($val->name); ?></a></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center cardCompanyLoc">(<?php echo e((isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0); ?>)</div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
            <?php endif; ?>
        </div>
    </div>
    <div class="row margnBtm30">
        <div class="col-lg-12 mt20">
            <div class="paginationContainer">
                <?php echo e($list->links()); ?>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>