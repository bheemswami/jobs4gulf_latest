

<?php $__env->startSection('content'); ?>
<?php
$defaultImages=['job-interview-tips.jpg','job-interview.jpg','Business-Photos-1.jpg'];
?>
  <div id="first-slider">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
        <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php if($employer_details->slider_images!=null): ?>
                <?php $__empty_1 = true; $__currentLoopData = $employer_details->slider_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="item <?php echo e(($k==0) ? 'active' : ''); ?> slide<?php echo e($k+1); ?>" style="background-image: url(<?php echo e(checkFile($val->image,'uploads/slider_image/','no_images.png')); ?>">
                      <div class="row">
                        <div class="container"></div>
                      </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <?php $__currentLoopData = $defaultImages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item <?php echo e(($k==0) ? 'active' : ''); ?> slide<?php echo e($k+1); ?>" style="background-image: url(<?php echo e(checkFile($val,'img/','no_images.png')); ?>">
                      <div class="row">
                        <div class="container"></div>
                      </div>
                    </div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            <?php endif; ?>
            
        </div>
        <!-- End Wrapper for slides-->
         <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
        </a>
    </div>
</div>


<section style="margin-top: 55px;margin-bottom: 20px;">
  <div class="taber ">
  <div class="container">
    <div class="row">
        <div class="col-lg-12">    
          <div class="ear">
            <div class="clearfix job-detail-inner">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 text-left">
                  <h3><?php echo e($employer_details->comp_name); ?></h3>
                  <h5>Saicon Consultant is Deals in all Sectors of Recruitment as Well as Placement.</h5>
                  <p><b class="icon-user small vam blue-text mr5px"></b><span class="pl5px" itemprop="member"><?php echo e(ucfirst($employer_details->contact_person)); ?></span></p>
                  <div class="post-details">
                  
                  </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <img src="<?php echo e(checkFile($employer_details->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="com">
                </div>
            </div>
             <div class="haf post-details">
              <p> <span><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo e(($employer_details->city!=null) ? $employer_details->city->name : ''); ?> - <?php echo e(($employer_details->country!=null) ? $employer_details->country->name : ''); ?> </span>
                      <?php if($employer_details->comp_website!=''): ?>
                        <span><i class="fa fa-globe" aria-hidden="true"></i> <a target="blank" href="<?php echo e($employer_details->comp_website); ?>">Visit</a> </span>
                      <?php endif; ?>
                    <span><i class="fa fa-suitcase" aria-hidden="true"></i> <?php echo e(($employer_details->job_post!=null) ? count($employer_details->job_post) : 0); ?> Active Jobs</span>
                      </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="container">
<div class="row">
  <div class="col-md-6">
<h2 class="title-widget-sidebar note2 color-is" style="color:black; ">Company profile 
</h2>
<?php echo $employer_details->comp_profile; ?>

</div>

<div class="col-md-6">
 <h2 class="title-widget-sidebar note2 color-is" style="color:black;">Contact Us </h2>
<div id="show_more" class="dn" style="display: block;">
    
    <ul class="fo ac-fl ac-w45 mt20px">
      <li class="mr15px" itemscope="" itemtype="http://schema.org/Organization">
      <p class="blue-text"><?php echo e(($employer_details->city!=null) ? ucfirst($employer_details->city->name) : ''); ?> :</p>
      <p><b class="icon-user small vam blue-text mr5px"></b><span class="pl5px" itemprop="member"><?php echo e(ucfirst($employer_details->contact_person)); ?> <?php echo e(($employer_details->designations!=null) ? ucfirst($employer_details->designations->name) : ''); ?></span></p>
      <p itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress"> 
        <b class="icon-map-marker small vam blue-text mr5px"></b>
        <span class="pl5px" itemprop="streetAddress"><?php echo e($employer_details->comp_address); ?> , <?php echo e(($employer_details->city!=null) ? $employer_details->city->name : ''); ?> , <?php echo e(($employer_details->country!=null) ? $employer_details->country->name : ''); ?></span> </p>
        <a class="bdr p5px10px bgf7f7f7 dib mt2px blue-text" href="javascript:void(0)"> <i class="icon icon-phone hig vam"></i> <?php echo e($employer_details->phone); ?> </a>
      
      </li>
   </ul>
</div>
                    
</div>

</div>
</div>
</section>

<?php if($employer_details->job_post!=null && count($employer_details->job_post)>0): ?>
  <section style="margin-bottom: 211px;">
    <div class="container">
      <h2 class="title-widget-sidebar note2 color-is" style="color:black;">Careers</h2>
      <div class="row">
        <?php $__currentLoopData = $employer_details->job_post; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-xs-12 col-md-6 related-block">
          <div class="clearfix nopadding lcop">
            <div class="job-box">
              <div class="row flexbox">
                <div class="col-xs-3 col-sm-2 left">
                  <div class="div-table">
                    <div class="table-cell-div">
                      <img src="<?php echo e(checkFile($employer_details->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="center-block">
                     </div>
                  </div>
                </div>
                <div class="col-xs-9 col-sm-10 right">
                  <h2><a href="<?php echo e(route('job-details',['jobid'=>$val->id])); ?>"><?php echo e($val->job_title); ?></a></h2>
                  <h5><?php echo e(($val->employer_info!=null) ? $val->employer_info->comp_name : ''); ?></h5>
                  <div class="post-details">
                    <p><?php echo e($val->key_skill); ?></p>
                    <p> 
                      <span><i class="fa fa-suitcase" aria-hidden="true"></i><?php echo e($val->min_experience); ?>-<?php echo e($val->max_experience); ?> years</span> 
                      <span><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo e($val->job_in_city); ?> - <?php echo e(($val->country!=null) ? $val->country->name : ''); ?></span>
                    </p>
                  </div>
                </div>
              </div>
              <div class="job_optwrap">
                <p>Posted : <?php echo e(dateConvert($val->created_at,'jS M Y')); ?></p>
              </div>
            </div>
          </div>
        </div>  
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  </section>
<?php endif; ?>
<script>
(function( $ ) {

    //Function to animate slider captions 
  function doAnimations( elems ) {
    //Cache the animationend event in a variable
    var animEndEv = 'webkitAnimationEnd animationend';
    
    elems.each(function () {
      var $this = $(this),
        $animationType = $this.data('animation');
      $this.addClass($animationType).one(animEndEv, function () {
        $this.removeClass($animationType);
      });
    });
  }
  
  //Variables on page load 
  var $myCarousel = $('#carousel-example-generic'),
    $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
    
  //Initialize carousel 
  $myCarousel.carousel();
  
  //Animate captions in first slide on page load 
  doAnimations($firstAnimatingElems);
  
  //Pause carousel  
  $myCarousel.carousel('pause');
  
  
  //Other slides to be animated on carousel slide event 
  $myCarousel.on('slide.bs.carousel', function (e) {
    var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
    doAnimations($animatingElems);
  });  
    $('#carousel-example-generic').carousel({
        interval:3000,
        pause: "false"
    });
  
})(jQuery); 

</script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>