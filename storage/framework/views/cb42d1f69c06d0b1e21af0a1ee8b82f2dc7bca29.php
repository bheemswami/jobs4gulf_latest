
<?php $__env->startSection('content'); ?>
<div class="row topEmployerBanner2">
    <div class="centered_1">Recruitment Solutions<br>
          <span>Browse our latest plans designed for all your need job need.<br> Our packages
             designed for all type of your job requirement.</span>
        </div>
       
    </div>    
    <div class="container marginbtm_15">
        <?php $counter=1;?>
        <?php $__currentLoopData = $service_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($counter%2!=0): ?>
                <div class="row marginTp15">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class=" planHeader">
                                <?php echo e($val->title); ?>

                            </div>
                            <div class=" planDesc">
                                <p><?php echo e($val->description); ?>></p>
                            </div>
                        <div class=" planDesc margin-tp-40">
                            <?php echo e($val->title); ?>

                        </div>
                        <div class=" planPoints" >
                            <?php if($val->features!=null): ?>
                                <ul class="list">
                                    <?php $__currentLoopData = $val->features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($value->title); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            <?php endif; ?>     
                        </div>
                        <div>
                            <?php if($val->show_btn==2): ?>
                                <a href="job-plans"  class="hvr-sweep-to-top btn  success-btn searchButton2 btn_design btnSubmit"> Find Out More</a>
                            <?php elseif($val->show_btn==1): ?>
                                <a data-toggle="modal" data-target="#enquiryModel" class="hvr-sweep-to-top btn  success-btn searchButton2 btn_design btnSubmit"> Find Out More</a>
                            <?php endif; ?>

                            
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <img src="<?php echo e(url('public/images/plan11.png')); ?>" width="80%">
                    </div>
                </div>
            <?php else: ?> 
                <div class="row marginTp15">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <img src="<?php echo e(url('public/images/plan22.png')); ?>" width="75%">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class=" planHeader">
                            <?php echo e($val->title); ?>

                        </div>
                        <div class=" planDesc">
                            <p><?php echo e($val->description); ?>></p>
                        </div>
                        <div class="planDesc margin-tp-40">
                            <?php echo e($val->title); ?>

                        </div>
                        <div class="planPoints" >
                            <?php if($val->features!=null): ?>
                                <ul class="list">
                                    <?php $__currentLoopData = $val->features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($value->title); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            <?php endif; ?> 
                        </div>
                        <div>
                            <?php if($val->show_btn==2): ?>
                                <a href="job-plans"  class="hvr-sweep-to-top btn  success-btn searchButton2 btn_design btnSubmit"> Find Out More</a>
                            <?php elseif($val->show_btn==1): ?>
                                <a data-toggle="modal" data-target="#enquiryModel" class="hvr-sweep-to-top btn  success-btn searchButton2 btn_design btnSubmit"> Find Out More</a>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if($val->title=="Top Employer Zone"): ?>
                
                    <div class="row theme_clr"> 
                    <div class="row pricingHeading3 text-center">
                    <span>Why Choose Jobs4Gulf ?</span>
                </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mrgnTpBtm5">
                        <div class="row text-center">
                            <img class="pricingImg img-rounded" src="<?php echo e(url('public/images/pricing1.png')); ?>">
                        </div>
                        <div class="row text-center pricingHeading4">
                            More Candidates
                        </div>
                        <div class="row text-center pricingHeading5">
                            149,000+ new registration <br> every month
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mrgnTpBtm5">
                        <div class="row text-center">
                            <img class="pricingImg img-rounded" src="<?php echo e(url('public/images/pricing2.png')); ?>">
                        </div>
                        <div class="row text-center pricingHeading4">
                            More Response
                        </div>
                        <div class="row text-center pricingHeading5">
                            3.5 million applications <br> a Month
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mrgnTpBtm5">
                        <div class="row text-center">
                            <img class="pricingImg img-rounded" src="<?php echo e(url('public/images/pricing3.png')); ?>">
                        </div>
                        <div class="row text-center pricingHeading4">
                            More Relevance
                        </div>
                        <div class="row text-center pricingHeading5">
                            Target the best candidates <br> quickly
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 mrgnTpBtm5">
                        <div class="row text-center">
                            <img class="pricingImg img-rounded" style="width: 93px;" src="<?php echo e(url('public/images/pricing4.png')); ?>">
                        </div>
                        <div class="row text-center pricingHeading4">
                            Our Dedicated Support
                        </div>
                        <div class="row text-center pricingHeading5">
                            We finally found a host that truly <br> understood the unique
                        </div>
                    </div>
                    </div>
                 
            <?php endif; ?>
            <?php $counter++;?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                
        

       
       
    </div>
<!-- Start Enquiry Modal -->
<?php
$pro_interest_in=config('constants.pro_interest_in');
sort($pro_interest_in);
?>
  <div class="modal fade" id="enquiryModel" role="dialog" aria-labelledby="enquiryModel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title text-center">Your message will be sent to  </h4> 
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">       
                <?php echo e(Form::open(array('url'=>route('save-service-enquiry'),'class'=>'form-horizontal','id'=>'save-service-enquiry-form'))); ?>

                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Name</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::text('name',null,array('class'=>'form-control','placeholder'=>'Enter Name'))); ?>  
                    </div>                                   
                </div> 
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Designation</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::text('designation',null,array('class'=>'form-control','placeholder'=>'Enter Designation'))); ?>  
                    </div>                                   
                </div> 
                <div class="form-group row">
                    <label class="col-sm-12 form-label">Email Id</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::text('email',null,array('class'=>'form-control','placeholder'=>'Enter Email Id'))); ?>  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Mobile</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::text('mobile',null,array('class'=>'form-control','placeholder'=>'Enter Mobile No.'))); ?>  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Landline</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::text('landline',null,array('class'=>'form-control','placeholder'=>'Enter Landline No.'))); ?>  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Company</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::text('company',null,array('class'=>'form-control','placeholder'=>'Enter Company Name'))); ?>  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Location</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::text('location',null,array('class'=>'form-control','placeholder'=>'Enter Location'))); ?>  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Product Interested In</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::select('pro_interest_in',$pro_interest_in,null,array('class'=>'form-control','placeholder'=>'--Select--'))); ?>  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Comments</label>
                    <div class="col-sm-12">
                        <?php echo e(Form::textarea('comments',null,array('cols'=>'50','rows'=>3,'class'=>'form-control','placeholder'=>'Enter Comment'))); ?>  
                    </div>                                   
                </div>

                <div class="form-group row"> 
                    <div class="col-xs-12">            
                        <button type="submit" class="btn-danger">Submit</button>  
                    </div>
                </div>
                <?php echo e(Form::close()); ?>

        </div>
      </div>
    </div>
  </div>
<!-- End Enquiry Modal -->
<?php $__env->stopSection(); ?>



<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>