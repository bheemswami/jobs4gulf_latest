
<?php $__env->startSection('content'); ?>

<div class="row topEmployerBanner">
   <div class="centered">
    Top Employer Zone<br>
   <span>Best Companies In Gulf Hiring Now! Grab Your Opportunity.<br> An Exclusiv Zone Of Top Companies From gulf
        </span>
  </div>
        
    </div>
    <div class=" marginSearchHeader">
            <div class=" row padding14px" style="height:auto;">
                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 searchHeading">You search for</div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 searchedFeilds">
                          <div id='search_key'>  </div>
                    </div>
                </div>
            </div>
        
        </div>
    <div class="container">
        <script>
            function toggleLeftNav(id){
                $("#"+id).toggle();
            }
        </script>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20 subHeaderResp">
                <span class="subHeaderLabel">Home / Top Employers </span>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20">
                <div class="leftSearchBox">
                    <div class="paddingSearchBox">
                        <label class="LeftSearchMainHeading">Refine Search</label>
                        <form method="get" id="filter_form">
                        <!--For Country-->
                        <div class="borderBtm mrgntp10">
                            <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                            <label class="LeftSearchSubHeading">By Location <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('country')" aria-hidden="true"></i></label>
                            
                            <div id="country" class="row nomrgn ">
                                <div class="col-xs-12">
                                    <div class="checkbox">
                                        <label class="searchField">
                                            <input type="checkbox" name="" class="searchCheckBox country-all change-checkbox" value="" checked>
                                            <span class="optionDiv">All</span>
                                            <span class="optionDiv2"></span>
                                        </label>
                                        <?php $counter=1; ?>
                                        <?php $__currentLoopData = $country_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $counter++; ?>
                                            <?php if($counter<2): ?>
                                                <label class="searchField">
                                                    <input type="checkbox" name="country_ids" class="searchCheckBox change-checkbox" value="<?php echo e($val->id); ?>">
                                                    <span class="optionDiv"><?php echo e($val->name); ?></span>
                                                    <span class="optionDiv2"><?php echo e(count($val->employer)); ?></span>
                                                </label>
                                            <?php else: ?>
                                                <div class="hide-elem country-list">
                                                    <label class="searchField">
                                                        <input type="checkbox" name="country_ids" class="searchCheckBox change-checkbox" value="<?php echo e($val->id); ?>">
                                                        <span class="optionDiv"><?php echo e($val->name); ?></span>
                                                        <span class="optionDiv2"><?php echo e(count($val->employer)); ?></span>
                                                    </label>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <div class="show-country-list showMore">Show more</div>
                                </div>
                            </div>
                        </div>
                        <!--For City-->
                        
                        <!--For City-->
                        <div class="borderBtm mrgntp10">
                            <!--Note : In toggleLeftNav pass the Id of the filtertype like country -->
                            <label class="LeftSearchSubHeading">By Industry <i class="fa fa-angle-down openCloseNav countryNav" onclick="toggleLeftNav('category')" aria-hidden="true"></i></label>
                            <div id="category" class="row nomrgn" style="display: none">
                                <div class="col-xs-12">
                                        <div class="checkbox">
                                            <label class="searchField">
                                                <input type="checkbox" name="" class="searchCheckBox industry-all change-checkbox" value="" checked>
                                                <span class="optionDiv">All</span>
                                                <span class="optionDiv2"></span>
                                            </label>
                                            <?php $counter=1; ?>
                                            <?php $__currentLoopData = $industry_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($counter<2): ?>
                                                <label class="searchField">
                                                    <input type="checkbox" class="searchCheckBox change-checkbox" name="industry_ids" value="<?php echo e($val->id); ?>">
                                                    <span class="optionDiv"><?php echo e($val->name); ?></span>
                                                    <span class="optionDiv2"><?php echo e(count($val->employer)); ?></span>
                                                </label>
                                                <?php else: ?>
                                                    <div class="industry-list hide-elem">
                                                        <label class="searchField"> 
                                                            <input type="checkbox" class="searchCheckBox change-checkbox" name="industry_ids" value="<?php echo e($val->id); ?>">
                                                            <span class="optionDiv"><?php echo e($val->name); ?></span>
                                                            <span class="optionDiv2"><?php echo e(count($val->employer)); ?></span>
                                                        </label>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $counter++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    <div class="show-industry-list showMore">Show more</div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding " id="load">
                <?php echo $__env->make('front_panel/employer/top_employer_list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </div>

<script>
  var url = page_url='<?php echo e(route('top-employers')); ?>';
  var industry_ids = [];
    var city_ids = [];
    var country_ids = [];
  $(document).on('click','.custom-pagination a',renderData);
  

$(document).on('click','.country-all',function(){
    country_ids=[];
    $("input[name='country_ids']").prop("checked",false);
    $(".country-all").prop( "checked",true);
});
$(document).on('click','.industry-all',function(){
    industry_ids=[];
    $("input[name='industry_ids']").prop("checked",false);
    $(".industry-all").prop( "checked",true);
});

  $(document).on('click','.change-checkbox',function(){
    country_ids=[];industry_ids=[];
    var form = $('#filter_form').serializeArray();
    $.each(form,function(index,value){
      if(value.name=='country_ids'){
        $(".country-all").prop( "checked",false);   
        country_ids.push(value.value);
      }
      else if(value.name=='city_ids'){
        city_ids.push(value.value);
      }else if(value.name=='industry_ids'){
        $(".industry-all").prop("checked",false);
        industry_ids.push(value.value);
      }
    });
    var data = {'country_ids':country_ids.join(),'city_ids':city_ids.join(),'industry_ids':industry_ids.join(),'page':1};
    page_url="<?php echo e(route('top-employers')); ?>"+"?page=1";
    ajaxCall(data);

  });

  function renderData(e){
      e.preventDefault();
      url = $(this).attr('href');
      ajaxCall();
      window.history.pushState("", "", url);
  }
  function ajaxCall(data=NaN){
    $.ajax({url: url,data:data, success: function(result){
        $('#load').html(result); 
    }});
    if(data!==NaN)
    {
        window.history.pushState("", "", page_url);
    }
  }
  /* function ajaxCall(data=NaN){
    $.ajax({
          url : url,
          data : data,
      }).done(function (data) {
          $('#load').html(data); 
      }).fail(function () {
        
      }).complete(function(){
    });
  } */
  $(document).on('click','.show-country-list',function(){
      $(".country-list").show();
      $(this).removeClass('show-country-list');
      $(this).addClass('hide-country-list');
      $(this).html('Show Less');

  })
  $(document).on('click','.hide-country-list',function(){
      $(".country-list").hide();
      $(this).addClass('show-country-list');
      $(this).removeClass('hide-country-list');
      $(this).html('Show more');
  })
  $(document).on('click','.show-industry-list',function(){
      $(".industry-list").show();
      $(this).removeClass('show-industry-list');
      $(this).addClass('hide-industry-list');
      $(this).html('Show Less');

  })
  $(document).on('click','.hide-industry-list',function(){
      $(".industry-list").hide();
      $(this).addClass('show-industry-list');
      $(this).removeClass('hide-industry-list');
      $(this).html('Show more');
  })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>