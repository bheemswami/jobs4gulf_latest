<style>
.content{
    padding: 8px 31px !important;
    font-size: 13px  !important;
}
</style>
<?php echo $__env->make('email_templates.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <tr>
        <td class="content">
            <span><strong>By : </strong> <?php echo e(@$params['name']); ?> (<?php echo e(@$params['email']); ?>) </span><br/>
            <span><strong>Subject : </strong> <?php echo e(@$params['title']); ?> </span><br/>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <strong>Message </strong>
        </td>
    </tr>
     <tr>
        <td class="content">
          <?php echo @$params['message']; ?>

        </td>
    </tr>
<?php echo $__env->make('email_templates.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>