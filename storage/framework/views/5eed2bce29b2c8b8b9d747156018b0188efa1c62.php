
<?php $__env->startSection('og_url'); ?><?php echo e(url()->current()); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('og_image'); ?><?php echo e(checkFile($walkins->comp_logo,'uploads/walkin_comp_logo/','job_role_default.png')); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('og_title'); ?><?php echo e($walkins->title); ?> <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<?php echo $__env->make('front_panel/includes/page_banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="clearfix"></div>
<section class="main-inner-page">
    <div class="container"> <!-- container-->
        <div class="row">
            <!-- main content-->
            <div class="col-xs-12 col-sm-9 mt-15">
                <div class="row">
                    <div class="col-sm-12">
                      <div class="job_detail_block">
                          <!-- job info section-->
                          <div class="job-detail white-block mt-120">
                            <h1> <?php echo e($walkins->title); ?></h1> 
                              <div class="">
                                  <div class="job-detail-company-logo">
                                    <img src="<?php echo e(checkFile($walkins->comp_logo,'uploads/walkin_comp_logo/','job_role_default.png')); ?>" class="card-img-top busimg">
                                  </div>
                                  <div class="job-info-block">
                                    <h2 class="job-title"><?php echo e($walkins->comp_name); ?></h2>
                                    <h3 class="job-company-name">
                                      <span><?php echo e(($walkins->industry !=null) ? $walkins->industry->name : ''); ?></span>
                                      <span><i class="fa fa-circle" aria-hidden="true"></i></span> 
                                      <span> <?php echo e(($walkins->country !=null) ? $walkins->country->name : ''); ?></span>
                                    </h3>
                                    <div class="job-info-action">
                                      <?php if(Auth::check()): ?>
                                        <a data-toggle="modal" data-target="#queryModel" class="btn jobs-info-apply-button ">Apply Now</a>
                                        <!-- <a onclick="return confirm('Are you sure ?')" href="<?php echo e(url('apply-walkin/'.$walkins->id)); ?>" class="btn jobs-info-apply-button ">Apply Now</a> -->
                                      <?php else: ?>                                      
                                        <button type="button" class="btn jobs-info-apply-button" data-toggle="modal" data-target="#userLoginModal">Apply Now</button>
                                      <?php endif; ?>
                                        <a href="#" class="btn jobs-info-share-button">Share with Friends</a>
                                        <div class="social_share">
                                          <a id="button" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(urldecode(url()->current())); ?>&text=<?php echo e($walkins->title); ?>&summary=ssss&description=ssss">
	                                         <img src="<?php echo e(url('public/images/facebook.png')); ?>">
	                                      	</a>
	                                      	
                                      	</div>
                                      </div>
                                  </div>
                                </div>
                            </div>
                          <!--About job -->
                          <div class="job-about white-block">
                            <h3>About </h3>
                            <p>
                             <?php echo $walkins->about_us; ?>

                             </p>  
                          </div>
                          <!--Job description-->
                          <div class="job-description white-block">
                            <h3> Job Details </h3>
                            <?php echo $walkins->job_description; ?>

                          </div>
                      </div>

                    </div>
                </div>
            </div><!-- main content-->

           <!-- sidebar-->
            <div class="col-xs-12 col-sm-3 sidebar-parent">
                <div class="right-sidebar walkin-side-details">
                <h3>Interview Details</h3>
                <?php if($walkins->walkin_interview!=null): ?>
                 <ul>
                    <?php $__currentLoopData = $walkins->walkin_interview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                          $locations[]=[$v->city->name,$v->address,''];
                        ?>
                        <li><lable>Location: </lable><span><?php echo e(($v->city !=null) ? $v->city->name : ''); ?></span><li>
                        <li><lable>Walk-in Date: </lable><span><?php echo e(dateConvert($v->date_from,'dS')); ?>-<?php echo e(dateConvert($v->date_to,'dS M Y')); ?></span><li>
                        <li><lable>Walk-in Time: </lable><span><?php echo e(dateConvert($v->date_from,'h:i A')); ?> to <?php echo e(dateConvert($v->date_to,'h:i A')); ?></span><li>
                        <li><lable>Walk-in Location: </lable><span><?php echo e($v->address); ?></span><li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 </ul>
                <?php endif; ?>
                
                </div>
                <div id="map">

                 </div>

            </div><!-- sidebar-->

        </div><!-- container-->

</section>
<!-- Start Enquiry Modal --> 
  <div class="modal fade" id="queryModel" role="dialog" aria-labelledby="queryModel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title text-center">Apply Walkins
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">       
                <?php echo e(Form::open(array('url'=>url('apply-walkin'),'class'=>'form-horizontal'))); ?>

                <?php echo e(Form::hidden('walkin_id',$walkins->id)); ?>

                <div class="row">
                    <label class="col-sm-4">Company</label>
                    <div class="col-sm-8">
                        <?php echo e(Form::text('company',null,array('class'=>'form-control','placeholder'=>'Enter Company Name'))); ?>  
                    </div>                                   
                </div> 
                <div class="row">
                    <label class="col-sm-4">Company Email</label>
                    <div class="col-sm-8">
                        <?php echo e(Form::text('company_email',null,array('class'=>'form-control','placeholder'=>'Enter Company E-mail'))); ?>  
                    </div>                                   
                </div> 
                <div class="row">
                    <label class="col-sm-4">Vacancy</label>
                    <div class="col-sm-8">
                        <?php echo e(Form::text('vacancy',null,array('class'=>'form-control','placeholder'=>'Enter Vacancy Name'))); ?>  
                    </div>                                   
                </div>
                <div class="row">
                    <label class="col-sm-4">Vacancy Ref No.</label>
                    <div class="col-sm-8">
                        <?php echo e(Form::text('vacancy_ref_no',null,array('class'=>'form-control','placeholder'=>'Enter Vacancy Number'))); ?>  
                    </div>                                   
                </div>
                <div class="row">
                    <label class="col-sm-4">Your Name</label>
                    <div class="col-sm-8">
                        <?php echo e(Form::text('name',null,array('class'=>'form-control','placeholder'=>'Enter Full Name'))); ?>  
                    </div>                                   
                </div>
                <div class="row">
                    <label class="col-sm-4">Your Email</label>
                    <div class="col-sm-8">
                        <?php echo e(Form::text('email',null,array('class'=>'form-control','placeholder'=>'Enter E-maill Address'))); ?>  
                    </div>                                   
                </div>
                <div class="row">
                    <label class="col-sm-4">Email Subject</label>
                    <div class="col-sm-8">
                        <?php echo e(Form::text('email_subject',null,array('class'=>'form-control','placeholder'=>'Enter Subject'))); ?>  
                    </div>                                   
                </div>
                <div class="row">
                    <label class="col-sm-4">Messages</label>
                    <div class="col-sm-8">
                        <?php echo e(Form::textarea('messages',null,array('cols'=>'50','rows'=>3,'class'=>'form-control','placeholder'=>'Enter Message'))); ?>  
                    </div>                                   
                </div>
                <br/>
                <div class="row"> 
                    <div class="col-xs-12">            
                        <button type="submit" class="btn-danger">Submit</button>  
                    </div>
                </div>
                <?php echo e(Form::close()); ?>

        </div>
      </div>
    </div>
  </div>
<!-- End Enquiry Modal -->
<?php $__env->stopSection(); ?>

<style>
  #map {
    height: 400px;
  }
</style>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXkuWv5uEZFVDx6XG3PQU5XwrFqUQj_CM&callback=initMap"></script>
<script>
var locations =<?php echo json_encode($locations,true); ?>;
var geocoder;
var map;
var bounds;
function initMap() {
  geocoder = new google.maps.Geocoder();
  bounds = new google.maps.LatLngBounds();
  map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(-33.92, 151.25),
      mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  if (geocoder) {
     for (i = 0; i < locations.length; i++) {
        geocodeAddress(locations, i);
      }
  }
}

function geocodeAddress(locations, i) {
  var title = locations[i][0];
  var address = locations[i][1];
  var url = locations[i][2];
  geocoder.geocode({
      'address': locations[i][1]
  },

    function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var marker = new google.maps.Marker({
          //icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
          map: map,
          position: results[0].geometry.location,
          title: title,
          animation: google.maps.Animation.DROP,
          address: address,
          url: url
        })
        infoWindow(marker, map, title, address, url);
        bounds.extend(marker.getPosition());
        map.fitBounds(bounds);
      } else {
        //alert("geocode of " + address + " failed:" + status);
      }
    });
}
function infoWindow(marker, map, title, address, url) {
  google.maps.event.addListener(marker, 'click', function() {
    var html = "<div><h5>" + title + "</h5><p>" + address + "<br></div></p></div>";
    iw = new google.maps.InfoWindow({
      content: html,
      maxWidth: 350
    });
    iw.open(map, marker);
  });
}

function createMarker(results) {
  var marker = new google.maps.Marker({
   // icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
    map: map,
    position: results[0].geometry.location,
    title: title,
    animation: google.maps.Animation.DROP,
    address: address,
    url: url
  })
  bounds.extend(marker.getPosition());
  map.fitBounds(bounds);
  infoWindow(marker, map, title, address, url);
  return marker;
}
</script>


<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>