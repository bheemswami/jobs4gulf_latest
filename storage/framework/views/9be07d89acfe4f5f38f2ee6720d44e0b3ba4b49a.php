
<?php $__env->startSection('page_title'); ?> | Register <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="jobseeker-banner" >  
    <div class="centered">Register As Job Seeker<br>
          <span>Let The Top Employers From Gulf Reach You,Rgister On Job4gulf Now!</span>
        </div>
      </div>
   
   <div class="container">
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registerHeaderBox2">
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mrginTB15">
               <div class="width100 ">
                   <img src="<?php echo e(url('public/images/pic1.png')); ?>" class="width50">
               </div>
               <div class="width100 mrginTpBtm20">
                   <p>Build an effective<br>professional profile</p>
               </div>
           </div>
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mrginTB15">
               <div class="width100 ">
                   <img src="<?php echo e(url('public/images/pic2.png')); ?>" class="width50">
               </div>
               <div class="width100 mrginTpBtm20">
                   <p>Apply to over<br>50,000 live jobs</p>
               </div>
           </div>
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mrginTB15">
               <div class="width100 ">
                   <img src="<?php echo e(url('public/images/pic3.png')); ?>" class="width50">
               </div>
               <div class="width100 mrginTpBtm20">
                   <p>Have full control over<br>your privacy settings</p>
               </div>
           </div>
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mrginTB15">
               <div class="width100 ">
                   <img src="<?php echo e(url('public/images/pic4.png')); ?>" class="width50">
               </div>
               <div class="width100 mrginTpBtm20">
                   <p>Get headhunted by<br>top employers in Gulf</p>
               </div>
           </div>
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mrginTB15">
               <div class="width100 ">
                   <img src="<?php echo e(url('public/images/pic5.png')); ?>" class="width50">
               </div>
               <div class="width100 mrginTpBtm20">
                   <p>Receive relevant<br>Job Alerts in your email</p>
               </div>
           </div>
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 text-center mrginTB15">
               <div class="width100 ">
                   <img src="<?php echo e(url('public/images/pic5.png')); ?>" class="width50">
               </div>
               <div class="width100 mrginTpBtm20">
                   <p>Simple Membership plan<br>and no hidden cost</p>
               </div>
           </div>
   
       </div>
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registerHereBox">
           <div class="col-md-3 col-lg-3"></div>
           <div class="col-md-6 col-lg-6">
                <?php echo e(Form::open(array('url'=>route('save-candidate'),'class'=>'form','id'=>'canditate-registration-form','files'=>true))); ?> 
               <div class="text-center registerHereText">REGISTER HERE</div>
               
               <div class="registerHereSubText">Account Information</div>
               <div class="form-group marginT20">
                    <?php echo e(Form::email('email',null,array('class'=>"form-control registerHereSubTextInput ic_email",'id'=>"candidate_email",'placeholder'=>"Enter your email address*"))); ?>

               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::password('password',array('class'=>"form-control registerHereSubTextInput pwd ic_psswrd",'id'=>"candidate_password",'placeholder'=>"Enter Password*"))); ?>

                   
                   
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::password('confirm_password',array('class'=>"form-control registerHereSubTextInput pwd ic_psswrd",'id'=>"confirm_password",'placeholder'=>"Confirm Password*"))); ?>

                   
               </div>
   
               <div class="registerHereSubText2">Your Personnel Details</div>
   
               <div class="row marginT20">
                   <div class="col-xs-2 pdlt0">
                        <?php echo e(Form::select('name_title',config('constants.name_titles'),'Mr.',array('class'=>"form-control resp",'id'=>"name_title","style"=>"padding: 0px 12px;"))); ?> 
                       
                   </div>
                   <div class="col-xs-10 pdrt0">
                       
                       <?php echo e(Form::text('name',null,array('class'=>"form-control registerHereSubTextInput ic_user",'hint-class'=>"fname_s",'id'=>"name",'data-toggle'=>"tooltip",'placeholder'=>"Enter Full Name*"))); ?>

                    </div>
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::text('dob',null,array('class'=>"form-control dob_pick registerHereSubTextInput ic_dob",'id'=>"datepicker","readonly"=>true,'autocomplete'=>'off',"placeholder"=>"Date of Birth*"))); ?>

                   
                   <span id="errorDOB"></span>
               </div>
               
               <div class="form-group marginT20">
                    <input type="hidden" name="check" id="check" value="0" >
                    <input type="hidden" name="country_code" id="country_code" value="0" >
                    <input type="text" name="phone" id="contact_number" class="form-control registerHereSubTextInput contact_number" placeholder="Phone No.*" >
               </div>
               <div class="row marginT20">
                   <div class="col-xs-6">
                        <?php echo e(Form::select('country',['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,array('class'=>"form-control ic_cuntry",'id'=>"country",'placeholder'=>"Select Country*"))); ?> 
                   </div>
                   <div class="col-xs-6">
                        <?php echo e(Form::text('city',null,array('class'=>"form-control ic_city",'id'=>"city",'placeholder'=>"Enter City*"))); ?>

                        <span id="errorCity"></span>
                   </div>
               </div>
               <div class="form-group marginT20">
                  <!--  <input type="password" placeholder="Select Nationality" class="form-control registerHereSubTextInput"> -->
                  <?php echo e(Form::select('nationality',config('constants.nationality'),null,array('class'=>"form-control ic_nation",'id'=>"nationality",'placeholder'=>"Select Nationality*"))); ?> 
               </div>
               
   
                <div class="registerHereSubText2">Your Education Details</div>
                <div class="form-group marginT20">
                    <?php echo e(Form::select('basic_course_id',$basic_courses,null,array('class'=>"form-control registerHereSubTextInput ic_bsic_edu","id"=>"basic_course",'placeholder'=>"Basic Education*"))); ?> 
                </div>
                <div class="basic-elem hide-elem">
                    <div class="form-group marginT20">
                        <?php echo e(Form::select('basic_specialization_id',[],null,array('class'=>"form-control ic_special","id"=>"basic_specialization",'placeholder'=>"Specialization*"))); ?> 
                    </div>
                    <div class="form-group marginT20">
                        <?php echo e(Form::select('basic_comp_year',getDigitRange(1960,date('Y')),null,array('class'=>"form-control ic_dob",'id'=>"year_of_graduation",'placeholder'=>"Completion Year*"))); ?> 
                    </div>
                    <div class="form-group marginT20">
                        <?php echo e(Form::select('basic_institute',$institutes,null,array('class'=>"form-control institutes registerHereSubTextInput",'placeholder'=>"Insitiute University*"))); ?> 
                        
                        <span id="errorbasicInstitute"></span>
                    </div>
                </div>
                <div class="form-group marginT20">
                    <?php echo e(Form::select('master_course_id',$master_courses,null,array('class'=>"form-control registerHereSubTextInput ic_master_edu","id"=>"master_course",'placeholder'=>"Master Education"))); ?> 
                   
                </div>
                <div class="master-elem hide-elem">
                    <div class="form-group marginT20">
                        <?php echo e(Form::select('master_specialization_id',[],null,array('class'=>"form-control ic_special","id"=>"master_specialization",'placeholder'=>"Specialization*"))); ?> 
                      </div>
      
                    <div class="form-group marginT20">
                        <?php echo e(Form::select('master_comp_year',getDigitRange(1960,date('Y')),null,array('class'=>"form-control ic_dob",'id'=>"year_of_postgraduation",'placeholder'=>"Completion Year*"))); ?> 
                    </div>
                    <div class="form-group marginT20">
                        <?php echo e(Form::select('master_institute',$institutes,null,array('class'=>"form-control institutes registerHereSubTextInput",'placeholder'=>"Insitiute University*"))); ?> 
                        
                        <span id="errorInstitute"></span>
                    </div>

                </div>
               
   
               <div class="registerHereSubText2">Your Current Employment Details</div>
               <div class="registerHereSubText3">Experience Level*</div>
               <div class="radio row marginT20 radioDiv">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> 
                       <label class="radioText">
                            <input onclick="openMoreMenu('exp')" type="radio" name="experience_level" value='1'>I have work Experience
                        </label>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <label class="radioText">
                            <input onclick="openMoreMenu('freshr')" type="radio" name="experience_level" value='0'>I am a Fresher
                        </label>
                    </div>
                   <span id="errorExperienceLevel"></span>
               </div>
               <!--//For Exp-->
   
               <div id="experienced" class="hide-elem">
                   <div class="registerHereSubText3">Total Work Experience</div>
                   <div class="row marginT20">
                       <div class="col-xs-6">
                            <?php echo e(Form::select('year_experiance',getDigitRange(0,30),null,array('class'=>"form-control ic_dob",'id'=>"year_experiance",'placeholder'=>"Select Years*"))); ?>

                       </div>
                       <div class="col-xs-6">
                            <?php echo e(Form::select('month_experiance',getDigitRange(0,12),null,array('class'=>"form-control ic_dob",'id'=>"month_experiance",'placeholder'=>"Select Months*"))); ?>

                       </div>
                   </div>
                   <div class="form-group marginT20">
                        <?php echo e(Form::text('current_company',null,array('class'=>"form-control ic_company",'hint-class'=>"",'id'=>"current_company",'placeholder'=>"Enter Company Name*"))); ?>

                   </div>
                   <div class="row marginT20">
                       <div class="col-xs-12">
                            <?php echo e(Form::text('current_position',null,array('class'=>"form-control",'id'=>"current_position",'placeholder'=>"Select Current Position*"))); ?>

                            <span id="errorCurrentPosition"></span>
                       </div>
                   </div>
                    <div class="row marginT20">
                       <div class="col-xs-12">
                           <?php echo e(Form::select('current_industry',$industry_list,null,array('class'=>"form-control ic_industry",'id'=>"current_industry",'placeholder'=>"Select Current Employer Industry*"))); ?>

                       </div>
                    </div>
                    <?php
                        $currencyList=config('constants.currency');
                        sort($currencyList);
                    ?>
                    <div class="row marginT20">
                        <div class="col-xs-6">
                            <?php echo e(Form::text('salary',null,array('class'=>"form-control ic_salary",'id'=>"salary",'placeholder'=>"Enter Salary*"))); ?>

                        </div>
                        <div class="col-xs-6">
                            <?php echo e(Form::select('currency',$currencyList,null,array('class'=>"form-control ic_salary",'id'=>"currency",'placeholder'=>"Select Currency*"))); ?>

                        </div>
                    </div>

                    <div class="row marginT20">
                        <div class="col-xs-6">
                            <?php echo e(Form::selectYear('duration_from',2000,date('Y'),null,array('class'=>"form-control ic_dob",'placeholder'=>"Working Duration from*"))); ?>

                        </div>
                        <div class="col-xs-6">
                            <?php echo e(Form::select('duration_to',['Currently Working'=>'Currently Working','2018'=>'2018','2017'=>'2017'],null,array('class'=>"form-control ic_dob",'placeholder'=>"Working Duration to*"))); ?>

                        </div>
                    </div>
                   <div class="row marginT20">
                       <div class="col-xs-12">
                            <?php echo e(Form::select('functional_area',$functional_area,null,array('class'=>"form-control ic_function",'id'=>"functional_area",'placeholder'=>"Select Functional Area*"))); ?>

                       </div>
                   </div>
                   
   
               </div>
               <div id="fresher" class="fresher_level_fields hide-elem">
                   <div class="form-group marginT20">
                       
                       <?php echo e(Form::text('key_skills',null,array('class'=>"form-control",'hint-class'=>"keyskills_s",'id'=>"key_skills",'data-toggle'=>"tooltip",'placeholder'=>"Enter Key Skills*"))); ?>

                        <span id="errorKeySkills"></span>
                   </div>
   
               </div>
               <!--/////-->
   
               <div class="form-group marginT20">
                    <?php echo e(Form::text('resume_headline',null,array('class'=>"form-control ic_headline",'id'=>"resume_headline",'placeholder'=>"Enter Resume Headline"/* ,'data-toggle'=>"popover","data-content"=>"CV Headline is a one line description of your CV. A good headline can kelp you get shortlisted." */ ))); ?>

                   
               </div>
               <div class="form-group row marginT20">
                   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><img class="profileImg" src="<?php echo e(url('public/images/demoUser.png')); ?>"></div>
                   <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                       <div class="profileDesc">
                           <span class="profileSpan">A Profile with Photo has a 40% higher chance of getting viewed by Employers</span>
                           <div class="profilePic2-button">
                              <input type="file" id="upload_profile_photo" class="file-input" name="user_img">
                              <label for="upload_profile_photo">
                              Upload Your Photo
                              </label> 
                           </div>
                           
                           <spna id="errorProfile"></spna>
                       </div>
                   </div>
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::select('perferred_location',getGulfCountries(),null,array('class'=>"form-control registerHereSubTextInput ic_location",'placeholder'=>"Select Preferred Location"))); ?> 
                 </div>
               
                <div class="registerHereSubText3 uploadDiv">
                    <span class="labelUploadPic">Upload Resume*</span>
                    <span class="uploadResume-button">  
                        <label for="upload_resume">
                        <img src="<?php echo e(url('public/images/uploadResume.png')); ?>">
                        </label> 
                        <input type="file" id="upload_resume" class="file-input" name="resume_file" required>
                        <span id="curr_selected_file"></span>
                    </span>
                    <span id="errorResumeFile"></span>
                </div>
                <div class="registerHereSubText3 marginT60 text-center">
                    <span class="submitResume-button text-center">
                        <button type="submit">Submit Resume</button>
                    </span>
               </div>
               <?php echo e(Form::close()); ?>

           </div>
           <div class="col-md-3 col-lg-3"></div>
   
       </div>
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registerHereBox2">
           <div class="row">
               <div class="outer"><span class="inner">Want to Know More?</span></div>
           </div>
           <div class="row text-center getIntouchText">
               <span>Give us a missed call and we''ll be in touch with you!</span>
           </div>
           <div class="row text-center getIntouchTextPh">
               <span>Call us at +91-9876543210</span>
           </div>
       </div>
   
   </div>
<script>
  function openMoreMenu(str){
        $('#experienced').hide();
        $('#fresher').hide();
        if(str=='exp')
            $('#experienced').show();
        if(str=='freshr')
            $('#fresher').show();
        $('.fresher_level_fields').show();
    }

    var defaultImg = '<?php echo e(url('public/images/deuser.png')); ?>';
    $(document).ready(function() {
        $("#contact_number").intlTelInput();
        var initialCountry = $("#contact_number").intlTelInput("getSelectedCountryData").dialCode;
        $('#country_code').val(initialCountry);
        $("#contact_number").on("countrychange", function(e, countryData) {
            $('#country_code').val(countryData.dialCode);
        });
    });
$("input[name=resume_file]").change(function () {
    printSelectedFileName($(this).val(),'#curr_selected_file');
});
$("#upload_profile_photo").change(function(){
    previewImage(this,'.profileImg','<?php echo e(url('public/images/deuser.png')); ?>');
});

  $(function(){
    $('#current_position').selectize({
        maxItems: 1,
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        create: true,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });

   
  });
  $('#key_skills').selectize({
      plugins: ['remove_button'],
      maxItems: 15,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
      options: <?php echo json_encode($skill_keywords); ?>,
    
  });

  $('.institutes').selectize({
      maxItems: 1,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
  });


  $('.exp_level').click(function(e){
      if($(this).val()==0){
        $('.exp_level_fields').hide();
      } else {
        $('.exp_level_fields').show();
      }
      $('.fresher_level_fields').show();
  });


$('#basic_course').change(function(e){
    if($(this).val()>0){
      $('.basic-elem').show();
    } else {
      $('.basic-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationBasic, globalFunc.error, globalFunc.complete);
});
$('#master_course').change(function(e){
    if($(this).val()>0){
      $('.master-elem').show();
    } else {
      $('.master-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationMaster, globalFunc.error, globalFunc.complete);
});

$('#country_code').change(function(){
      globalVar.countryId = $(this).val().split('~')[1];
      $('#country').val(globalVar.countryId);
      $('#country').change();
});
 $('#country').change(function(){
      globalVar.countryId = $(this).val();
      var selectize = $("#city")[0].selectize; 
      selectize.clear();
      selectize.clearOptions();
      selectize.renderCache['option'] = {}; 
  });

  $('#city').selectize({
    plugins: ['remove_button'],
    maxItems: 1,
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    create: false,
    closeAfterSelect: true,
    options: [],
    render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            if(query.length>=3){
             $.ajax({
                  url: base_url+'/api/city-suggestion',
                  type: 'GET',
                  dataType: 'json',
                  data: {
                      country_id: globalVar.countryId,
                      q: query
                  },           
                  success: function(res) {
                      callback(res);
                  },
                  error: function(error) {
                      callback();
                  }
              });
            } else {
              var selectize = $("#city")[0].selectize; 
             selectize.renderCache['option'] = {}; 
             return callback();
            }
        }
  });
  
 $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-58:+0",
      showOn: "both",
      buttonImage: "<?php echo e(url('public/images/icons/calendar.png')); ?>",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "dd-mm-yy",
      maxDate:'0'
      
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>