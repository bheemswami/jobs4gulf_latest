
<?php $__env->startSection('page_title'); ?> | Register <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<?php echo $__env->make('front_panel/includes/validation_msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    <div class="employers-banner" >  
        <div class="centered">Register As Employer<br>
          <span>Find Your Next Hire With The Fastest Growing Database Of Gulf Candidates.Find The Best People For Your Business.</span>
        </div>
    </div>
   
   <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registerHeaderBox2">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center registerHeaderMiddle">
                <span>Why Us?</span>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 text-center mrginTB15">
                <div class="width100 ">
                    <img src="<?php echo e(url('public/images/pic1EmployyeBanner.png')); ?>" class="width70">
                </div>
                <div class="width100 mrginTpBtm20">
                    <p>Easy &amp; Hassle<br>Free Registeration</p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 text-center mrginTB15">
                <div class="width100 ">
                    <img src="<?php echo e(url('public/images/pic2EmployerBanner.png')); ?>" class="width70">
                </div>
                <div class="width100 mrginTpBtm20">
                    <p>Large Database<br>of Employees</p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 text-center mrginTB15">
                <div class="width100 ">
                    <img src="<?php echo e(url('public/images/pic3EmployerBanner.png')); ?>" class="width70">
                </div>
                <div class="width100 mrginTpBtm20">
                    <p>Manage Interviews,<br>Schedule and Timelines</p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 text-center mrginTB15">
                <div class="width100 ">
                    <img src="<?php echo e(url('public/images/pic4EmployerBanner.png')); ?>" class="width70">
                </div>
                <div class="width100 mrginTpBtm20">
                    <p>Shortlist your favourite<br>candidates</p>
                </div>
            </div>
        
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registerHereBox">
           <div class="col-md-3 col-lg-3"></div>
           <div class="col-md-6 col-lg-6">
                <?php echo e(Form::open(array('url'=>route('save-employer'),'class'=>'form','id'=>'employer-registration-form','files'=>true))); ?>

                <div class="text-center registerHereText">REGISTER HERE</div>
                <div class="registerHereSubText">Account Information</div>
                <div class="form-group marginT20">
                    <?php echo e(Form::email('email',null,array('class'=>"form-control registerHereSubTextInput ic_email",'id'=>"emp_email",'placeholder'=>"Company E-mail ID*"))); ?>   
                   
                </div>
                <div class="form-group marginT20">
                    <?php echo e(Form::password('password',array('class'=>"form-control registerHereSubTextInput ic_psswrd",'id'=>"emp_password",'placeholder'=>"Create Password*"))); ?>

                </div>
                <div class="form-group marginT20">
                    <?php echo e(Form::password('confirm_password',array('class'=>"form-control registerHereSubTextInput ic_psswrd",'id'=>"con_password",'placeholder'=>"Confirm Password*"))); ?>

                   
               </div>
   
               <div class="registerHereSubText">Contact Person Information</div>
   
               <div class="row marginT20">
                   <div class="col-xs-2 pdlt0">
                        <?php echo e(Form::select('cp_title',config('constants.name_titles'),null,array('class'=>"form-control resp",'id'=>"cp_title",'style'=>"padding: 0px 12px;"))); ?>

                       
                   </div>
                   <div class="col-xs-10 pdrt0">
                        <?php echo e(Form::text('cp_name',null,array('class'=>"form-control registerHereSubTextInput ic_user",'id'=>"cp_name",'placeholder'=>"Enter Full Name*"))); ?>

                       
                    </div>
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::text('cp_designation',null,array('class'=>"form-control registerHereSubTextInput hintable",'hint-class'=>"",'id'=>"designation",'placeholder'=>"Enter Designation*"))); ?>

                    <span id="errorDesignation"></span>
                   
               </div>
               
               <div class="form-group marginT20">
                        <input type="hidden" name="check" id="check" value="0" >
                        <input type="hidden" name="dial_code" id="country_code" value="0" >
                        <input type="text" name="cp_mobile" id="contact_number" class="form-control registerHereSubTextInput contact_number" placeholder="Phone No.*" >
                </div>
                <span class="error1" id="contact_number-error" ></span>
            
               <div class="registerHereSubText">Company Information</div>
               <div class="registerHereSubText3">Member Type*</div>
               <div class="radio row marginT20 radioDiv">
                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> 
                        <label class="radioText"><?php echo e(Form::radio('comp_type','1',false,array('class'=>""))); ?>Consultant</label>
                       
                    </div>
                   <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> 
                        <label class="radioText"><?php echo e(Form::radio('comp_type','2',false,array('class'=>""))); ?>Employer</label>
                       
                    </div>
                    <span id="errorCompType">
                        
                    </span>
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::text('comp_name',null,array('class'=>"form-control registerHereSubTextInput ic_company",'id'=>"company_name",'placeholder'=>"Enter Company Name*"))); ?>

                   
               </div>
   
               <div class="form-group marginT20">
                    <?php echo e(Form::textarea('comp_about',null,array('class'=>"form-control",'id'=>"company_about",'placeholder'=>"About Company*", "rows"=>"5"))); ?>

                   
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::text('comp_website',null,array('class'=>"form-control registerHereSubTextInput ic_company",'id'=>"company_website",'placeholder'=>"Company Website"))); ?>

                   
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::select('comp_industry[]',$industry_list,null,array('class'=>"form-control ic_company",'id'=>"industry_type","multiple"))); ?> 
                   

                   <span id="errorCompIndustry"></span>
               </div>
   
               <div class="registerHereSubText3">Upload Company Logo</div>
               <div class="form-group row marginT20">
                   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><img class="profileCompanyImg" src="<?php echo e(url('public/images/demoUser.png')); ?>"></div>
                   <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                       <div class="profileDesc2">
                           <span class="profileSpan">Upload only Image (.png/.jpg/.jpeg) formats. </span>
                           
                           <div class="profilePic2-button">
                              <input type="file" id="upload_profile_photo" class="file-input" name="myFile">
                              <label for="upload_profile_photo">
                             Upload Your Logo
                              </label>
                           </div>
                       </div>
                   </div>
               </div>
   
               <div class="registerHereSubText">Mailing Address</div>
   
               <div class="row marginT20">
                   <div class="col-xs-6">
                        <?php echo e(Form::select('comp_country',
                        ['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,
                        array('class'=>"form-control ic_cuntry",'id'=>"country",'placeholder'=>"Select Country*"))); ?>

                       
                   </div>
                   <div class="col-xs-6">
                        <?php echo e(Form::text('comp_city',null,array('class'=>"form-control",'id'=>"city",'autocompletion'=>'off','placeholder'=>"Enter City*"))); ?>

                        <span id="errorCompCity"></span>
                       
                   </div>
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::textarea('comp_address',null,array('class'=>"form-control",'id'=>"company_address",'placeholder'=>"Address*",'rows'=>5))); ?>

                   
               </div>
               <div class="form-group marginT20">
                    <?php echo e(Form::text('zip_code',null,array('class'=>"form-control registerHereSubTextInput ic_po_box",'id'=>"zip_code",'placeholder'=>"Enter PO Box/ Zip Code"))); ?>

                   
               </div>
   
   
   
               <div class="registerHereSubText3 marginT60 text-center">
                    <span class="submitResume2-button text-center">
                        <button type="submit">Register</button>
                       
                   </span>
               </div>
               <?php echo e(Form::close()); ?>

           </div>
           <div class="col-md-3 col-lg-3"></div>
       </div>
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registerHereBox2">
           <div class="row">
               <div class="outer"><span class="inner">Want to Know More?</span></div>
           </div>
           <div class="row text-center getIntouchText">
               <span>Give us a missed call and we''ll be in touch with you!</span>
           </div>
           <div class="row text-center getIntouchTextPh">
               <span>Call us at +91-9876543210</span>
           </div>
       </div>   
   </div>

<script>
    $(document).ready(function(){
        $("#industry_type").multiselect({
            minHeight: 50,  
            maxHeight: null,
            placeholder : "Select Industry Type*",
            width:"100%"
        });
    });
  globalVar.countryId=0;
  $("#upload_profile_photo").change(function(){
    previewImage(this,'.profileCompanyImg','<?php echo e(url('public/images/default/no_logo.png')); ?>');
  });
  $('.rmv_img').click(function () {
    $("#upload_profile_photo").val(null);
    $(".profileCompanyImg").attr('src','<?php echo e(url('public/images/default/no_logo.png')); ?>');
  });

    $('#designation').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'/api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });

  $('#country').change(function(){
      globalVar.countryId = $(this).val();
      var selectize = $("#city")[0].selectize; 
      selectize.clear();
      selectize.clearOptions();
      selectize.renderCache['option'] = {}; 
  });

  $('#city').selectize({
    plugins: ['remove_button'],
    maxItems: 1,
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    create: false,
    closeAfterSelect: true,
    options: [],
    render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            if(query.length>=3){
             $.ajax({
                  url: base_url+'/api/city-suggestion',
                  type: 'GET',
                  dataType: 'json',
                  data: {
                      country_id: globalVar.countryId,
                      q: query
                  },           
                  success: function(res) {
                      callback(res);
                  },
                  error: function(error) {
                      callback();
                  }
              });
            } else {
              var selectize = $("#city")[0].selectize; 
             selectize.renderCache['option'] = {}; 
             return callback();
            }
        }
  });
</script>
    <script>
        $("#contact_number").intlTelInput();
        var initialCountry = $("#contact_number").intlTelInput("getSelectedCountryData").dialCode;
        $('#country_code').val(initialCountry);
        $("#contact_number").on("countrychange", function(e, countryData) {
            $('#country_code').val(countryData.dialCode);
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>