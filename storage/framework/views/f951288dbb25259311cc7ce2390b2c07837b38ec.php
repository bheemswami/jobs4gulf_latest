
<?php $__env->startSection('content'); ?>

<div class="jobseeker-banner" >  
    <div class="centered">Register As Job Seeker<br>
          <span>Let The Top Employers From Gulf Reach You,Rgister On Job4gulf Now!</span>
        </div>
      </div>

<div class="row noPadding nomrgn">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pricingHeader text-center">
    <div class="row pricingHeading1 text-center">
        <span class="plansHeader">Supercharge your Company Productivity</span>
    </div>

    <div class="row pricingHeading2 text-center">
        Job4Gulf is perfect for Individuals, Small, Medium and Large Companies
    </div>
    

</div>
</div>
<div class="container">
    <div class="row marginTpBtm20">
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <!--Header-->
           <div class="row brdrBtm2">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingHeader8 ">
                   Features
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingHeader6 text-center">
                    <div>BASICS</div>
                    <div class="pricingHeader7">Free 2 Jobs</div>
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingHeader6  text-center">
                   <div>PREMIUM</div>
                   <div>Starts at $1999</div>
                   <img src="<?php echo e(url('public/images/popular.png')); ?>" class="popularImg">
               </div>
           </div>
           <?php $counter=1; ?>
           <?php $__currentLoopData = $features; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    
            <div class="row <?php echo e(($counter%2==0) ? 'colorBlue' : ''); ?>">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                    <?php echo $val->feature_title; ?>

                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                    <?php if($val->val_type==0): ?>
                        <?php echo e($val->basic_benefits); ?>

                    <?php else: ?>
                        <?php echo ($val->basic_benefits==1) ? '<img src="'.url("public/images/checked@2x.png").'"  width="18px">' : '<img src="'.url("public/images/cross.png").'"  width="12px">'; ?>

                    <?php endif; ?>
               </div>
               <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                    <?php if($val->val_type==0): ?>
                        <?php echo e($val->premium_benefits); ?>

                    <?php else: ?>
                        <?php echo ($val->premium_benefits==1) ? '<img src="'.url("public/images/checked@2x.png").'"  width="18px">' : '<img src="'.url("public/images/cross.png").'"  width="12px">'; ?>

                    <?php endif; ?>
               </div>
           </div>
           <?php $counter++; ?>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           
           
       </div>
    </div>
    <div class="row marginTpBtm20_2">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricingTableLabel">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2 pricingTableLabel text-center">
                    <button type="button" class="btn btn-success colorGreen2">Post a job</button>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 pricingTableLabel text-center">
                    <button type="button" class="btn btn-success colorNavy">Get a Quote</button>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row noPadding nomrgn">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pricingHeader2 text-center">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 ">
            <div class="pricingHeader3">Thousands of Companies <br> Trust Job4Gulf as a best <br> source for Human Resource.</div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 marginTpBtm120">
            <span><img src="<?php echo e(url('public/images/layer0.png')); ?>"></span>
            <span><img src="<?php echo e(url('public/images/lifehacker@2x.png')); ?>"></span>
            <span><img src="<?php echo e(url('public/images/logoTechCrunch2x@2x.png')); ?>"></span>
            <span><img src="<?php echo e(url('public/images/tnw-logo2x@2x.png')); ?>"></span>
        </div>
    </div>
</div>
<div class="row noPadding nomrgn">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pricingHeader text-center">
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 ">
            <div class="pricingHeader4">
                Frequently asked <br> questions
                <div class="pricingHeader5">Need more help? Contact us at <br>1800-204-1477</div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="pricingHeader9">
                    <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                    <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                    <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                    <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                    <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                    <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>

                    <div class="faqQuestions">What are my payment options - credit card and/or invoicing?</div>
                    <div class="faqAnswers">We support Visa, Mastercard and American Express. For yearly plans for 6 users or more we offer invoiced billing. Click here to speak to the team.</div>
            </div>
        </div>
    </div>
</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>