
<?php $__env->startSection('content'); ?>
<style>
    body{
        background-color: #FFF !important;
    }
</style>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-content">
            <div class="height-padding"></div>
            <div class="head-text text-center">
                <h2>Find Your Dream job in Gulf</h2>
                <h2>Join Top Companies Currently Hiring</h2>
                <h3>Search through 1,15,000 Jobs</h3>
            </div>
            <?php
                if(!isset($form_class)){
                    $form_class='';
                }
                $country = getAllCountries();
                foreach($country as $val){
                    $all_countries[$val->id]=$val->name;
                    if($val->is_gulf==1){
                        $gulf_countries[$val->id]=$val->name;
                    }
                }
            ?>
            <div class="form-box">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?php echo e(Form::open(array('url'=>route('search-jobs'),'class'=>$form_class." form-inline",'method' =>'GET','id'=>'search-job'))); ?>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 nopadding">
                            <div class="form-group">
                                <label for="skill">Enter Keywords</label>
                                <?php echo e(Form::text('key_skills',null,['id'=>'skill','class'=>'form-control type-it rounded searchInput job-title','placeholder'=>'Enter skills, designations or company name'])); ?>

                                
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 nopadding">
                            <div class="form-group">
                                <label for="city">Location</label>
                                <?php echo e(Form::select('country_ids',['Gulf Country'=>$gulf_countries,'All Country'=>$all_countries],null,['id'=>'country','class'=>'form-control type-it rounded searchInput roundedNA','placeholder'=>'City or country'])); ?>

                                
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 nopadding">
                            <div class="form-group">
                                <label for="year">Experience</label>
                                <?php echo e(Form::select('exp_year',getDigitRange(0,5),null,['id'=>'exp year','class'=>'form-control type-it rounded searchInput roundedNA','placeholder'=>'Exp (Years)'])); ?>

                                
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 nopadding">
                            <label class="searchBlank" for="search">&nbsp;</label><br class="searchBlank">
                            <button type="submit" id="search" class="btn btn-success success-btn">Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="form-box">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPad1024">
                        <div class="second-navbar">
                                <ul class="optionUl">
                                    <li class="active"><a href="#">Browse job</a></li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Functions<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                            <li class="second_dropdown"><a href="#">Search Job</a></li>
                                            <li class="second_dropdown"><a href="#">Job by Country</a></li>
                                            <li class="second_dropdown"><a href="#">Job by City</a></li>
                                            <li class="second_dropdown"><a href="#">Job by Industry</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Location<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                            <li class="second_dropdown"><a href="#">Search Job</a></li>
                                            <li class="second_dropdown"><a href="#">Job by Country</a></li>
                                            <li class="second_dropdown"><a href="#">Job by City</a></li>
                                            <li class="second_dropdown"><a href="#">Job by Industry</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Industry<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                            <li class="second_dropdown"><a href="#">Search Job</a></li>
                                            <li class="second_dropdown"><a href="#">Job by Country</a></li>
                                            <li class="second_dropdown"><a href="#">Job by City</a></li>
                                            <li class="second_dropdown"><a href="#">Job by Industry</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Best Consultant Zone</a>                                
                                    </li>
                                    <!--<li><a href="#">Placement</a></li>-->
                                    <li> <a href="#">Walk-In Zone</a>                                   
                                    </li>
                                    <!--<li> <a href="#">more</a></li>-->
                                </ul>
                            </div>
                   
                </div>
            </div>
            <div class="form-box">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="header-button text-center">
                        <a href="<?php echo e(route('candidate-register')); ?>">Upload CV! It's Free</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="paddingJobs">
                    <div class="page-heading discover">
                        <span onclick="javascript:;">
                            Discover Our Hiring Partner
                        </span>
                        <a href="<?php echo e(route('top-employers')); ?>" class="pull-right">View All</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center mrgnBtm50px">
                        <div class="width80">
                            <div class="owl-carousel" id="premium">
                            <?php $__currentLoopData = $top_employers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item top-candidate">
                                    <span>
                                        <img src="<?php echo e(checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="sliderImg">
                                    </span>
                                    <h4 class="sliderHeader"><?php echo e(substr($val->comp_name,0,50)); ?> </h4>
                                <h5 class="sliderDesc"><?php echo e((isset($val->country)) ? "(".$val->country->name .")": ''); ?></h5>
                                <?php if($val->job_post!=null && count($val->job_post)>0): ?>
                                    <a href="<?php echo e(url('companies/'.$val->id)); ?>"><?php echo e(count($val->job_post)); ?> OPENING</a>
                                <?php else: ?>
                                    <a href="#" class="disableAnchor" >NO OPENING</a>
                                <?php endif; ?>
                                </div>  
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row noPadding noMrgn bckclrWhoru">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mrgnTpwhoru">
                <div class="col-md-6">
                    <div class="card text-center">
                        <img class="imgWhoru" class="card-img-top" src="<?php echo e(url('public/images/Mask Group 2@2x.png')); ?>" class="img-responsive" alt="Card image cap">
                         <div class="card-body">
                            <h3 class="card-title">Are you a recruiter?</h3>
                            <div class="col-md-9 col-md-offset-3">
                            <ul>
                           <li>  Advertise your Jobs to get qualified candidates</li>
                             <li>  CVs direct to your inbox</li>
                              <li>  Get customized recruiters packages as per your needs</li>
                            
                            </ul>
                            </div>
                            <div class="upload_cv">
                                <a href="<?php echo e(route('free-job-post')); ?>">Post a Job Free ! &nbsp;&nbsp;</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card text-center">
                        <img class="imgWhoru" class="card-img-top" src="<?php echo e(url('public/images/Mask Group 3@2x.png')); ?>" class="img-responsive" alt="Card image cap">
                         <div class="card-body">
                            <h3 class="card-title">Are you job seeker?</h3>
                             <div class="col-md-9 col-md-offset-3">
                            <ul class="clr-grn">
                            <li>Upload your CV to get your next dream job in Gulf</li>
                           <li> Get relevant jobs in your inbox</li>
                            <li> Apply to over 5000 Jobs in Gulf.</li>
                         </ul>
                     </div>
                            <div class="post_new_job">
                            <a href="<?php echo e(route('candidate-register')); ?>">Upload CV ! It's Free</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="paddingJobs">
                    <div class="page-heading discover">
                        <span>Premium Jobs</span>
                    </div>
                </div>
            </div>
        </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <?php 
                            $pJobs = getPremiumJobs();
                            $pJobIds=[];
                        ?>
                        
                        <?php $__currentLoopData = $pJobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php 
                            $pJobIds[]=$val->id;
                            ?>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left premium-job-pannel">
                                <div class="col-xs-12 col-md-3 text-center">
                                    <?php if($val->employer_info!=null): ?>
                                        <img src="<?php echo e(checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="premium-job-logo">
                                    <?php else: ?> 
                                        <img src="<?php echo e(url('public/images/default/company_logo.png')); ?>" class="premium-job-logo">
                                    <?php endif; ?>
                                    
                                </div>
                                <div class="col-xs-12 col-md-6 premium-job-details">
                                    <p class="premium-job-heading"><?php echo e(($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt.Ltd'); ?>

                                        </p>
                                    <a href="<?php echo e(route('job-details',['jobid'=>$val->id])); ?>"><h4><?php echo e($val->job_title); ?></h4></a>
                                    
                                    <p class="location"><img src="<?php echo e(url('public/images/location-icon.png')); ?>"> <?php echo e(getCityNames($val->jobs_in_city)); ?> - <?php echo e(($val->country!=null) ? $val->country->name : ''); ?> </p>
                                </div>
                                <div class="col-xs-12 col-md-6 premium-job-details">
                                    <?php $counter=1; ?>
                                    <?php $__currentLoopData = $val->skills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>      
                                        <?php if(isset($data->skill_keywords) && $counter<4): ?>          
                                            <button type="button" class="btn skill-btn"> <?php echo e($data->skill_keywords->name); ?></button>
                                            <?php $counter++; ?>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    
                                </div>
                                <div class="col-xs-12 col-md-3 pricing">
                                    <?php if($val->salary_min!=null): ?>
                                    <p><img src="<?php echo e(url('public/images/doller.png')); ?>"> 
                                        <?php echo e($val->currency); ?><?php echo e($val->salary_min); ?>- <?php echo e($val->currency); ?><?php echo e($val->salary_max); ?>

                                    </p>
                                    <?php else: ?>
                                        <h5 class="no-salary-text">As per market standard</h5>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                        <div class="col-xs-12 col-lg-12 col-md-12 view-all text-center">
                            <a href="javascript:;" class="">View all</a>
                        </div>
                    </div>
                </div>
            </div> 

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 knowMoreAbtus">
                <div class="paddingJobs">
                    <!-- <div class="col-md-8 heightDiv">
                        <div class="knowAbtMoreHeader">
                            Know more about us
                        </div>
                        <div class="knowAbtMoredesc">
                            Check out our about us sectiona and see how are helping enterprises and job seekers
                            to get the best resource possible.
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo e(url('public/images/knowMoreAbtUs.png')); ?>" class="knowMoreBtusImg">
                    </div> -->
                </div>

            </div>
        </div>

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="paddingJobs">
                    <div class="page-heading discover">
                        <span>Latest Jobs</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="paddingJobs">
                        <?php 
                        $rJobs = getRecentJobs($pJobIds);
                    ?>
                    <?php $__currentLoopData = $rJobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 mrgn-btm">
                            <div class="carrer_pannel">
                                <div class="row">
                                    <div class="col-xs-4 align_centered full-width">
                                        <?php if($val->employer_info!=null): ?>
                                            <img src="<?php echo e(checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="logoImgJobs">
                                        <?php else: ?> 
                                            <img src="<?php echo e(url('public/images/default/company_logo.png')); ?>" class="logoImgJobs">
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-xs-8 align_centered full-width skills_btn pdtop text-right">
                                        <?php $counter=1; ?>
                                        <?php $__currentLoopData = $val->skills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>      
                                            <?php if(isset($data->skill_keywords) && $counter<4): ?>          
                                                <span> <?php echo e($data->skill_keywords->name); ?></span>
                                                <?php $counter++; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                                    </div>
                                </div>
                                <div class="row mt-top">
                                    <div class="col-xs-8 full-width">
                                        <div class="job_name"><?php echo e(($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt.Ltd'); ?></div>
                                        <div class="job_designation"><?php echo e($val->job_title); ?></div>
                                        <div class="job_location">
                                            <img src="public/images/location-icon.png">
                                            <?php echo e(getCityNames($val->jobs_in_city)); ?> - <?php echo e(($val->country!=null) ? $val->country->name : ''); ?>

                                        </div>
                                    </div>
                                    <div class="col-xs-4 full-width mt-top-5">
                                        <span class="job_amount nomargin">
                                            <img src="public/images/doller.png">
                                            <?php if($val->salary_min!=null): ?>
                                                <?php echo e($val->currency); ?><?php echo e($val->salary_min); ?>- <?php echo e($val->currency); ?><?php echo e($val->salary_max); ?>

                                            <?php else: ?>
                                                <h5 class="no-salary-text">As per market standard</h5>
                                            <?php endif; ?>
                                        </span>
                                        <span class="job_date">
                                            <?php echo countDays($val->created_at); ?>

                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    

    
                    <!-- <?php 
                        $rJobs = getRecentJobs($pJobIds);
                    ?>
                    <?php $__currentLoopData = $rJobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 mrgnBtm15 mrgnRyt25">
                            <div class="text-left latest-job-pannel">
                                <div class="col-xs-12 col-md-12 col-lg-4 nopadding logoContainer">
                                    <?php if($val->employer_info!=null): ?>
                                        <img src="<?php echo e(checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="logoImgJobs">
                                    <?php else: ?> 
                                        <img src="<?php echo e(url('public/images/default/company_logo.png')); ?>" class="logoImgJobs">
                                    <?php endif; ?>
                                    
                                </div>
                                <div class="col-xs-12 col-md-12 col-lg-8 details">
                                    <p><?php echo e(($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt.Ltd'); ?></p>
                                    <a href="<?php echo e(route('job-details',['jobid'=>$val->id])); ?>"><h4><?php echo e($val->job_title); ?></h4></a>
                                    <p class="location">
                                        <span class="nameLoc"><img class="locationIcon" src="<?php echo e(url('public/images/location-icon.png')); ?>">
                                        <?php echo e(getCityNames($val->jobs_in_city)); ?> - <?php echo e(($val->country!=null) ? $val->country->name : ''); ?> </span>
                                    </p>
                                </div>
                                <div class="col-xs-12 col-md-12 col-lg-8">
                                    <?php if($val->salary_min!=null): ?>
                                    <p class="price">
                                        <img class="doller" src="<?php echo e(url('public/images/moneyBag.png')); ?>"> <?php echo e($val->currency); ?><?php echo e($val->salary_min); ?>- <?php echo e($val->currency); ?><?php echo e($val->salary_max); ?>

                                    </p>
                                    <?php else: ?>
                                        <h5 class="no-salary-text">As per market standard</h5>
                                    <?php endif; ?>
                                    <?php $counter=1; ?>
                                    <?php $__currentLoopData = $val->skills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>      
                                        <?php if(isset($data->skill_keywords) && $counter<4): ?>          
                                            <button type="button" class="btn skill-btn-Design mrgnlft0 noMargnSkills"> <?php echo e($data->skill_keywords->name); ?></button>
                                            <?php $counter++; ?>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo countDays($val->created_at); ?>

                                    
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                </div>
            </div>
             -->
            <div class="col-xs-12 col-lg-12 col-md-12 view-all text-center">
                <a href="javascript:;" class="">View all</a>
            </div>

        </div>
    </div>
</div>

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 questionBkclr">
                <!-- <div class="paddingJobs">
                    <div class="col-md-4 text-center">
                        <img src="<?php echo e(url('public/images/questnImg.png')); ?>" class="questionsImg">
                    </div>
                    <div class="col-md-8">
                        <div class="questionsMoreHeader">
                            Have a question? Contact us now
                        </div>
                        <div class="knowAbtMoredesc">
                            We're here to help. Check out our FAQs, send us an email or call us at <br>1(800)555-5555
                        </div>
                    </div>
                </div> -->
            </div>
        </div>

        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-12">
                <div class="page-heading discover">
                    <span>Popular Categories</span>
                </div>
            </div>
            <div class="padding10">
                <?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 cursorPointer">
                        <div class="popular-block text-center ">
                            
                    <img class="industry_icon" src="<?php echo e(checkFile($val->icon,'uploads/industry_img/','gas-pump-1.svg')); ?>" data-hovericon="<?php echo e(checkFile($val->icon_hover,'uploads/industry_img/','gas-pump.svg')); ?>" data-hoverouticon="<?php echo e(checkFile($val->icon,'uploads/industry_img/','gas-pump-1.svg')); ?>">                       
                    <a href="<?php echo e(url('search-jobs?industry_ids='.$val->id)); ?>"><h5 class="popular-block-heading"><?php echo e($val->name); ?></h5></a>
                            <p class="popular-block-description">(<?php echo e((isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0); ?>) Jobs</p>                         

                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                

                <div class="col-lg-12 view-all text-center">
                    <a href="javascript:;" class="">View all</a>
                </div>
            </div>
        </div>
        <div class="row noPadding noMrgn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="page-headingTc discover">
                    <span>Premium Candidates</span>
                </div>
                <div class="page-heading2">
                    <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Choose from the list of most popular sectors.</span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 candidateBox">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="width90">
                            <div class="owl-carousel" id="candidates">

                                <div class="item cursorPointer candidate">
                                        <img src="<?php echo e(url('public/images/profile1.png')); ?>" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                        <img src="<?php echo e(url('public/images/profile2.png')); ?>" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                        <img src="<?php echo e(url('public/images/profile3.png')); ?>" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                        <img src="<?php echo e(url('public/images/profile1.png')); ?>" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                        <img src="<?php echo e(url('public/images/profile1.png')); ?>" class="candiddateProfilePic">
                                        <div>Ayush Morya</div>
                                        <p>Delhi</p>
                                        <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                    <img src="<?php echo e(url('public/images/profile3.png')); ?>" class="candiddateProfilePic">
                                    <div>Ayush Morya</div>
                                    <p>Delhi</p>
                                    <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                <div class="item cursorPointer candidate">
                                    <img src="<?php echo e(url('public/images/profile3.png')); ?>" class="candiddateProfilePic">
                                    <div>Ayush Morya</div>
                                    <p>Delhi</p>
                                    <label class="mrgnBtm25">UI UX Designer</label>
                                </div>
                                 <div class="item cursorPointer candidate">
                                    <img src="<?php echo e(url('public/images/profile3.png')); ?>" class="candiddateProfilePic">
                                    <div>Ayush Morya</div>
                                    <p>Delhi</p>
                                    <label class="mrgnBtm25">UI UX Designer</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script>
    /* $('.closemodal').click(function() {
        $('#alertModal').modal('hide');
    });
    $(function() {
        // for key skill //
        $( "#skills" ).autocomplete({
        source: '<?php echo e(url("listing/get_key")); ?>'
        });
    });
    $(function() {
        // for location //
        $( "#location" ).autocomplete({
        source: '<?php echo e(url("listing/country_state_city")); ?>'
        });
    }); */
</script>
<?php $__env->stopSection(); ?>


     
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>