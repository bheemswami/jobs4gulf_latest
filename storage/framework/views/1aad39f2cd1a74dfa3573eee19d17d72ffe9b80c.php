
<?php $__env->startSection('content'); ?>

<?php echo $__env->make('front_panel/includes/page_banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="taber ">
  <div class="container">
    <div class="row">
        <div class="col-lg-9">    
          <div class="ear">
            <div class="clearfix job-detail-inner">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 text-left">
                  <h3><?php echo e($job_detail->job_title); ?></h3>
                  <h5><?php echo e(($job_detail->employer_info!=null) ? $job_detail->employer_info->comp_name : ''); ?></h5>
                  <div class="post-details">
                  <p> <span><i class="fa fa-suitcase" aria-hidden="true"></i><?php echo e($job_detail->min_experience); ?>-<?php echo e($job_detail->max_experience); ?> years</span> 
                      <span><i class="fa fa-map-marker" aria-hidden="true"></i> <!-- <?php echo e($job_detail->job_in_city); ?> - --> <?php echo e(($job_detail->country!=null) ? $job_detail->country->name : ''); ?> </span></p>
                  </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                  <?php if($job_detail->employer_info!=null): ?>
                      <img src="<?php echo e(checkFile($job_detail->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="com">
                    <?php else: ?> 
                      <img src="<?php echo e(url('public/images/default/company_logo.png')); ?>" class="com">
                    <?php endif; ?>
                </div>
            </div>
             <div class="haf post-details">
              <p> <span><i class="fa fa-clock-o" aria-hidden="true"></i>Posted : <?php echo e(dateConvert($job_detail->created_at,'jS M Y')); ?></span> 
                    <span> Job Applicants: <strong><?php echo e(count($job_detail->job_applicants)); ?></strong>  </span>
                    <span> No. of Vacancies: <strong><?php echo e($job_detail->total_vacancy); ?></strong>  </span></p>
            </div>
          </div>

          <div class="ear">
            <div class="clearfix job-detail-inner">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left job-desc">
                  <h3 class="heading"><span class="grey">Job Description</span></h3>
                  <div class="post-details">
                    <?php echo $job_detail->job_description; ?>

                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left job-desc">
                  <h3 class="heading"><span class="grey">Key Skills</span></h3>
                  <div class="post-details">
                    <?php
                    $skills=[];
                    if($job_detail->skills!=null){
                      foreach($job_detail->skills as $val){
                          if($val->skill_keywords!=null){
                            $skills[]=$val->skill_keywords->name;
                          }
                      }
                    }
                    ?>
                  <p> <?php echo e(join(', ',$skills)); ?></p>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left job-desc">
                  <h3 class="heading"><span class="grey">Job Summary</span></h3>
                  <div class="post-details">
                    
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Functional Area:</label>
                          <div class="col-xs-7 controls"><?php echo e(($job_detail->functional_area!=null) ? $job_detail->functional_area->name : ''); ?></div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Industry:</label>
                          <div class="col-xs-7 controls"><?php echo e(($job_detail->industry!=null) ? $job_detail->industry->name : ''); ?></div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
              
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Gender:</label>
                          <div class="col-xs-7 controls"> <?php echo e($job_detail->gender); ?></div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Number of Vacancy:</label>
                          <div class="col-xs-7 controls"> <?php echo e($job_detail->total_vacancy); ?></div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Job Country:</label>
                          <div class="col-xs-7 controls"><?php echo e(($job_detail->country!=null) ? $job_detail->country->name : ''); ?></div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
                      
                     
                    </div>

                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left job-desc">
                  <h3 class="heading"><span class="grey">You Can Contact</span></h3>
                  <?php if($job_detail->response_email!=''): ?>
                    <?php
                      $emails=[];
                      $expEmails = explode(',', $job_detail->response_email);
                      if(!empty($expEmails)){
                        foreach($expEmails as $val){
                          $emails[]='<a href="mailto:'.$val.'">'.$val.'</a>';
                        }
                      }
                    ?>                  
                    <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Email:</label>
                          <div class="col-xs-7 controls"><?php echo join(', ',$emails); ?></div>
                        </div>
                    </div>
                  <?php endif; ?>

                  <?php if($job_detail->cp_name!=''): ?>
                    <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Contact Person:</label>
                          <div class="col-xs-7 controls"><?php echo e($job_detail->cp_title); ?> <?php echo e($job_detail->cp_name); ?></div>
                        </div>
                    </div>
                  <?php endif; ?>

                  <?php if($job_detail->cp_mobile!=''): ?>
                    <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Contact Number:</label>
                          <div class="col-xs-7 controls">+<?php echo e($job_detail->dial_number); ?>-<?php echo e($job_detail->cp_mobile); ?></div>
                        </div>
                    </div>
                  <?php endif; ?>

                  <?php if($job_detail->walkin_address!=''): ?>
                    <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Address:</label>
                          <div class="col-xs-7 controls"><?php echo e($job_detail->walkin_address); ?></div>
                        </div>
                    </div>
                  <?php endif; ?>

                </div>
            </div>
             
          </div>
    
            <div class="refer">
              <div class="col-md-12 nopadding">
                  <ul> 
                    <?php if(Auth::check()): ?>
                      <?php if(Auth::user()->role=='2'): ?> 
                        <?php if(jobAppliedOrNot($job_detail->id)==0): ?>  
                            <li><a onclick="return confirm('Are you sure you want to apply?')" href="<?php echo e(url('apply-job/'.$job_detail->id)); ?>">Apply Now</a>
                        <?php else: ?>
                            <li><a href="#">Already Applied</a>
                        <?php endif; ?>
                      <?php endif; ?>
                    <?php else: ?>
                      <li><a data-toggle="modal" data-target="#userLoginModal">Apply Now</a>    
                   <?php endif; ?>
                    
                  </ul>
              </div>
        </div>
        </div>
    
<?php 
  $latestNews = getLatestNews();
?>    
    <div class="col-lg-3">
    
    <div class="latest">
      <h3 class="heading"><span class="grey">Latest News</span></h3>
        <ul>
          <?php $__empty_1 = true; $__currentLoopData = $latestNews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <li><i class="fa fa-flash"></i> <p><?php echo $val->title; ?></p></li>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <li><i class="fa fa-flash"></i> <p>No Latest News</p></li>
          <?php endif; ?>
        </ul>   
    </div>
    
    </div>
    
    </div>
</div>
</div>
  
<?php 
  $rJobs = getRelatedJobs($job_detail->id);
?>

<div class="carr waz">
    <div class="container">
      <h3 class="heading"><span class="grey">Related Jobs</span></h3>
      <div class="row">
        <?php $__currentLoopData = $rJobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-xs-12 col-md-6">
          <div class=" job-box">
            <div class="row flexbox">
              <div class="col-xs-3 col-sm-2 left">
                <div class="div-table">
                  <div class="table-cell-div">
                    <?php if($val->employer_info!=null): ?>
                      <img src="<?php echo e(checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="center-block">
                    <?php else: ?> 
                      <img src="<?php echo e(url('public/images/default/company_logo.png')); ?>" class="center-block">
                    <?php endif; ?>

                  </div>
                </div>
              </div>
              <div class="col-xs-9 col-sm-10 right">
                <h2><a href="<?php echo e(route('job-details',['jobid'=>$val->id])); ?>"><?php echo e($val->job_title); ?></a></h2>
                <h5><?php echo e(($val->employer_info!=null) ? $val->employer_info->comp_name : ''); ?></h5>
                <div class="post-details">
                  <p><?php echo e(limit_text($val->key_skill,80)); ?></p>
                  <p> 
                    <span><i class="fa fa-suitcase" aria-hidden="true"></i><?php echo e($val->min_experience); ?>-<?php echo e($val->max_experience); ?> years</span> 
                    <span><i class="fa fa-map-marker" aria-hidden="true"></i>                     
                     <?php echo e(getCityNames($val->jobs_in_city)); ?> - <?php echo e(($val->country!=null) ? $val->country->name : ''); ?>

                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="job_optwrap">
              <p>Posted : <?php echo e(dateConvert($val->created_at,'jS M Y')); ?></p>
            </div>
          </div>
        </div>  
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </div>
  </div>

    </div>

    
      

<div class="clearfix"></div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>