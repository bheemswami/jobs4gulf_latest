<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding">
          <?php $__empty_1 = true; $__currentLoopData = $jobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt20">
                    <div class="cardBox2">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-xs-3 paddingRyt0">
                                        <?php if($val->employer_info!=null): ?>
                                        <img src="<?php echo e(checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>" class="cardImg2">
                                        <?php else: ?> 
                                        <img src="<?php echo e(url('public/images/default/company_logo.png')); ?>" class="cardImg2">
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="jobName"><a href="<?php echo e(route('job-details',['jobid'=>$val->id])); ?>"><?php echo e($val->job_title); ?></a></div>
                                        <div class="jobDesignation"><?php echo e(($val->employer_info!=null) ? $val->employer_info->comp_name : 'Netmed Pvt. Ltd.'); ?></div>
                                        <div class="jobLocation"><img src="<?php echo e(url('public/images/location-icon.png')); ?>"><?php echo e((isset($val->country)) ? $val->country->name : ''); ?></div>
                                    </div>
                                </div>
                                <div class="row skillsRow">
                                    <div class="col-xs-3 paddingRyt0">
                                    </div>
                                    <div class="col-xs-9 skills noPadding">
                                        <?php $counter=1; ?>
                                        <?php $__currentLoopData = $val->skills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>      
                                            <?php if(isset($data->skill_keywords) && $counter<4): ?>          
                                                <span> <?php echo e($data->skill_keywords->name); ?></span>
                                                <?php $counter++; ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-xs-12 mrgnTp8">
                                        <span class="jobAmountGrid">
                                            <?php if($val->salary_min!=null): ?>
                                                <img src="<?php echo e(url('public/images/doller.png')); ?>"><?php echo e($val->currency); ?><?php echo e($val->salary_min); ?> - <?php echo e($val->currency); ?><?php echo e($val->salary_max); ?>

                                            <?php else: ?>
                                                <h5 class="no-salary-text">As per market standard</h5>
                                            <?php endif; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="jobDateGrid"><?php echo countDays($val->created_at); ?></span>
                    </div>
                </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?> 
              <div class="col-xs-12 col-md-12 nopadding">
              <div class=" job-box">
                <div class="row flexbox">
                  
                  <div class="col-xs-12 col-sm-12">
                    
                    <img src="<?php echo e(url('public/images/default/nojobsfound.png')); ?>" class="center-block" width="240px" style="margin-top: 5%;">
                    <div class="post-details">
                      
                    </div>
                  </div>
                </div>
                
              </div>
            </div> 
          <?php endif; ?>
          </div>
        <div class="row margnBtm30">
            <div class="col-lg-12 mt20">
                <div class="paginationContainer">
                    <div class="custom-pagination">
                        <?php echo e($jobs->appends(['view' =>'list'])->links()); ?>

                    </div>
                </div>
            </div>
        </div>

    
<script>
var search='<?php echo e($search_tag); ?>';
var array = search.split(',');
if(search)
{
  $('#search_key').html('');
  $.each( array, function( key, value ) {
    if(value )
    $('#search_key').append('<span>'+value+'</span>|');
  });
}

</script>