<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/css/bootstrap.min.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/css/font-awesome.min.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/css/admin_style.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('public/css/media.css')); ?>">
	<title>Job4Gulf</title>
</head>
<body style="background: #fff;">

<div id="login-panel">
	<div id="login-panel-inner">
		<div class="login-panel-header text-center">
			
				<img class="img-responsive center-block" src="<?php echo e(url('public/images/logo.png')); ?>">
				
		</div>
		<!-- <div class="container"> -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php if(session('success')): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo e(session('success')); ?>

                    </div>
                <?php elseif(session('error')): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo e(session('error')); ?>

                    </div>
                <?php elseif(session('info')): ?>
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo e(session('info')); ?>

                    </div>
                <?php endif; ?>
				<?php echo e(Form::open(array('url'=>route('login-process'),'class'=>'text-center','id' => 'loginForm'))); ?>

                <?php echo e(csrf_field()); ?>

					<input type="email" name="email" placeholder="Email" required value="bheemswami808@gmail.com" id="">
					<input type="password" name="password" placeholder="Password" required value="12345678" id="">
					<button type="submit">Login</button>
				<?php echo e(Form::close()); ?>

			</div>
		<!-- </div> -->
	</div>
</div>

</body>
</html>