<?php $__currentLoopData = $top_employers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-sm-6 col-md-6 col-lg-4 mt20 responsiveCard">
        <div class="cardBox" style="min-height:330px">
            <div class="row">
                <div class="col-xs-12 text-center"><img class="cardImg" src="<?php echo e(checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center cardCompanyName"><?php echo e(substr($val->comp_name,0,50)); ?> </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center cardCompanyLoc"><?php echo e((isset($val->country)) ? "(".$val->country->name .")": ''); ?></div>
            </div>
            <div class="row  text-center">
                <div class="col-xs-12 text-center cardCompanyOpenings">
                    <?php if($val->job_post!=null && count($val->job_post)>0): ?>
                        <a class="btn btn-success success-btn searchButton cardOpening" href="<?php echo e(url('companies/'.$val->id)); ?>"><?php echo e(count($val->job_post)); ?>>OPENINGS</a>
                    
                <?php else: ?>
                    <a href="<?php echo e(url('companies/'.$val->id)); ?>" class="btn btn-green" >NO OPENING</a>
                <?php endif; ?>
                    
                </div>
            </div>


        </div>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<div class="row margnBtm30">
    <div class="col-lg-12 mt20">
        <div class="paginationContainer">      
            <div class="custom-pagination">
                <?php echo e($top_employers->links()); ?>

            </div>
        </div>
    </div>
</div>
<script>
    var search='<?php echo e($search_tag); ?>';
    var array = search.split(',');
    if(search)
    {
        $('#search_key').html('');
        $.each( array, function( key, value ) {
        if(value )
        $('#search_key').append('<span>'+value+'</span>|');
        });
    }
    
</script>
                             