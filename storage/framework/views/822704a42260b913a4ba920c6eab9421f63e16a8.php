<?php $__empty_1 = true; $__currentLoopData = $consultants; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <div class="col-sm-6 col-md-6 col-lg-4 mt20">
        <div class="cardBox" style="min-height:282px;">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <img class="cardImg" src="<?php echo e(checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')); ?>"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center cardCompanyName"><a href="consultant-detail/<?php echo e($val->id); ?>"<?php echo e(substr($val->comp_name,0,50)); ?></a></div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center cardCompanyLoc2"><?php echo e($val->country->name); ?></div>
            </div>
            <div class="row  text-center">
                <div class="col-xs-12 text-center cardCompanyOpenings2">
                <?php if($val->job_post!=null && count($val->job_post)>0): ?>
                        <a  class="btn btn-green" href="consultant-detail/<?php echo e($val->id); ?>"><?php echo e(count($val->job_post)); ?>>OPENINGS</a>
                    
                <?php else: ?>
                    <a href="consultant-detail/<?php echo e($val->id); ?>" class="btn-green" >NO OPENING</a>
                <?php endif; ?>
                    
                </div>
            </div>


        </div>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <div class="col-xs-12 col-md-12 nopadding">
              <div class=" job-box">
                <div class="row flexbox">
                  
                  <div class="col-xs-12 col-sm-12">
                    
                    <img src="<?php echo e(url('public/images/default/nojobsfound.png')); ?>" class="center-block" width="240px" style="margin-top: 5%;">
                    <div class="post-details">
                      
                    </div>
                  </div>
                </div>
                
              </div>
            </div>  
<?php endif; ?>         
<div class="row margnBtm30">
    <div class="col-lg-12 mt20">
        <div class="paginationContainer">
            <?php echo e($consultants->links()); ?>

        </div>
    </div>
</div>
<script>

var search='<?php echo e($search_tag); ?>';
var array = search.split(',');
if(search)
{
    $('#search_key').html('');
    $.each( array, function( key, value ) {
        if(value)
            $('#search_key').append('<span>'+value+'</span>|');
    });
}
</script>
                             