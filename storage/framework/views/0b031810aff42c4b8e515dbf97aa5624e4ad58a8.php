
<?php $__env->startSection('content'); ?>
<style>
  body{
    background-color: white !important;
  }
.padding14px {
    height: auto;
}
.cardBox3:hover {
    border-top: none;
    background-color: white; 
}
.label {
     padding: none;
     font-size: 16px;
    font-weight: 700;
     color: #000;
     text-align: left;
}
.ms-options-wrap > .ms-options > ul label{
    font-weight: 400 !important;
    font-size: 14px !important;
}
</style>
<?php 
    $gulfCountry=getGulfCountries();
    $otherCountry=getOtherCountries();
?>
<?php echo e(Form::model(session()->get('tmpPostJobData'),array('url'=>route('save-free-job'),'class'=>'dasboard-form','id'=>'post-job-free-form','files'=>true))); ?>

<!-- <div class="marginSearchHeader">
    <div class="row padding14px">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-7 postAJobHeader">
                <a href="javascript:;">Dashboard</a>
                <a  href="javascript:;">Slider Image</a>
                <a  href="javascript:;">Purchased plans</a>
                <a  href="javascript:;">Jobs</a>
                <a  href="javascript:;">Profile</a>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-3">
            <i class="fa fa-search imgSearch" aria-hidden="true"></i>
            <input type="text" placeholder="Search Candidate" class="postAJobBtn">
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <div class="input-group add-on">
                <div class="input-group-btn">
                    <button class="btn btn-success success-btn searchButton postAJobBtn newfclassub" type="submit">Post a job</button>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="jobseeker-banner" >  
    <div class="centered">Post A Job<br>
          <span>Let The Top Employers From Gulf Reach You,Rgister On Job4gulf Now!</span>
        </div>
      </div>
<div class="container mt25">
    <!-- <div class="col-lg-12">
        <div class="md-stepper-horizontal orange">
            <div class="md-step active">
                <div class="md-step-circle"><span></span></div>
                <div class="md-step-title">Job Detail</div>
                <div class="md-step-bar-left"></div>
                <div class="md-step-bar-right"></div>
            </div>
            <div class="md-step active">
                <div class="md-step-circle"><span></span></div>
                <div class="md-step-title">Job Classification</div>
                <div class="md-step-bar-left"></div>
                <div class="md-step-bar-right"></div>
            </div>
            <div class="md-step ">
                <div class="md-step-circle"><span></span></div>
                <div class="md-step-title">Eligiblity Criteria</div>
                <div class="md-step-bar-left"></div>
                <div class="md-step-bar-right"></div>
            </div>
            <div class="md-step">
                <div class="md-step-circle"><span></span></div>
                <div class="md-step-title">Manage Response</div>
                <div class="md-step-bar-left"></div>
                <div class="md-step-bar-right"></div>
            </div>
        </div>

    </div> -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Job Detail
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                <div class="form-group marginT20">
                    <label class="inputHeading">Job Type</label>
                    <select class="form-control resp jobTypeSelect"  required>
                        <option value="" class="placeholder" hidden>Select Job Type</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading">Job Title</label>
                    
                    <?php echo e(Form::text('job_title',null,array('class'=>"form-control registerHereSubTextInput",'id'=>"job_title ",'placeholder'=>"Job Title"))); ?> 
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading">Job Description</label>
                    <?php echo e(Form::textarea('job_description',null,array('class'=>"form-control",'id'=>"job_description",'placeholder'=>"Enter Job Description","rows"=>"5"))); ?>

                    
                </div>

                <div class="form-group marginT20">
                    <div class="row noPadding nomrgn">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddingryt5">
                            <label class="inputHeading">Country</label>
                            <?php echo e(Form::select('job_in_country',
                            ['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,
                        array('class'=>"form-control resp jobTypeSelect",'id'=>"job_country",'placeholder'=>"Select Country"))); ?>

                            
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">City</label>
                            <?php echo e(Form::TEXT('job_in_city',null,array('class'=>"form-control resp jobTypeSelect",'id'=>"job_city",'autocompletion'=>'off','placeholder'=>"Enter City"))); ?> 
                         <span id="errorJobInCity"></span>  
                            
                        </div>
                    </div>
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading">Monthly Salary</label>
                    <?php echo e(Form::select('salary_min',config('constants.min_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_min",'placeholder'=>"in US$"))); ?>

                    
                </div>

                <div class="form-group marginT20 mrgnBtm50">
                    <label class="inputHeading">No. of Vacancies</label>
                    <?php echo e(Form::number('total_vacancy',null,array('class'=>"form-control registerHereSubTextInput",'id'=>"total_vacancy",'placeholder'=>"Enter Vacancy number"))); ?> 
                    
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Job Classification
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                <div class="form-group marginT20">
                    <label class="inputHeading">Industry Type</label>
                    <?php echo e(Form::select('industry_id',$industry_list,null,array('class'=>"form-control",'id'=>"job_industry",'placeholder'=>"Select Industry"))); ?>

                    
                </div>
                <div class="form-group marginT20">
                    <label class="inputHeading">Functional Area</label>
                    <?php echo e(Form::select('functional_area_id',$functional_area,null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select Functional Area"))); ?>

                    
                </div>

                <div class="form-group marginT20 mrgnBtm50">
                    <label class="inputHeading">Keywords</label>
                    <?php echo e(Form::textarea('keywords',null,array('class'=>"form-control registerHereSubTextInput",'id'=>"key_skills",'placeholder'=>"Enter important keywords that can describe this job"))); ?>

                    <span id="errorKeywords"></span>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Eligiblity Criteria
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                <div class="form-group marginT20">
                    <label class="inputHeading">Select Qualification</label>
                    <?php echo e(Form::select('quilification[]',$educations,null,array('class'=>"form-control multiselect-ui",'id'=>"qualifications",'multiple'=>"multiple"))); ?>

                    
                </div>
                <div class="form-group marginT20">
                    <label class="inputHeading">Specialization</label>
                    <?php echo e(Form::select('specialization[]',[],null,array('class'=>"form-control",'id'=>"specialization",'multiple'=>"multiple"))); ?>

                    
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading">Keywords</label>
                    <input type="text" placeholder="Enter important keywords that can describe this job" class="form-control registerHereSubTextInput">
                </div>

                <div class="form-group marginT20">
                    <label class="inputHeading">Gender</label>
                    <div class="genderCheckBoxes">
                        <span for="male"><?php echo e(Form::radio('gender','Male',false,array('class'=>"",'id'=>"male"))); ?>Male</span>
                        <span for="female"><?php echo e(Form::radio('gender','Female',false,array('class'=>"",'id'=>"female"))); ?>Female</span>
                          <span for="anyone"><?php echo e(Form::radio('gender','No Preference',false,array('class'=>"",'id'=>"anyone"))); ?>No Preference</span>
                          <span id="errorGender"></span>
                    </div>
                </div>
                <div class="form-group marginT20">
                    <label class="inputHeading">Work Experience</label>
                    <?php echo e(Form::select('min_experience',getDigitRange(0,30,'year'),null,array('class'=>"form-control registerHereSubTextInput",'id'=>"exp_year_from",'placeholder'=>"in Year"))); ?> 
                    
                </div>
                <div class="form-group marginT20">
                    <label class="inputHeading">Nationality</label>
                    <?php echo e(Form::select('nationality_id[]',config('constants.nationality'),null,array('class'=>"form-control multiselect-ui",'id'=>"nationality",'multiple'=>"multiple"))); ?>

                    
                </div>
                <div class="form-group marginT20 mrgnBtm50">
                    <div class="row noPadding nomrgn current_location_section">
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5 paddingryt5">
                            <label class="inputHeading">Current Location</label>
                            <?php echo e(Form::select('candidate_current_country[0]',['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,array('class'=>"form-control resp curr_loc jobTypeSelect",'placeholder'=>"Select Country"))); ?> 
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5 paddinglft5">
                            <label class="inputHeading ">&nbsp;</label>
                            <?php echo e(Form::select('candidate_current_city[0][]',[],null,array('class'=>"form-control resp curr_city jobTypeSelect",'id'=>'c_city0','multiple'=>"multiple"))); ?> 
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 add_more"><i class="fa fa-plus fa-icon-green" aria-hidden="true"></i></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 mt30 responsiveCard">
        <div class="row cardBox3">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPaddingResp2 headerAppliedJobs">
                Manage Response
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddingPostAJob">
                <div class="form-group marginT20">
                    <label class="inputHeading">Seeker will Respond you at</label>
                    <div class="genderCheckBoxes">
                        <span><input onclick="viewRespond(this)" id="email" value="Respond" type="checkbox">Email</span>
                        <span><input onclick="viewRespond(this)" id="contactDetails" type="checkbox" value="Respond" >Contact Details</span>
                        <span><input onclick="viewRespond(this)" id="walkin" type="checkbox" value="Respond">WalkIn</span>
                        <span><input onclick="viewRespond(this)" id="all" type="checkbox" value="Respond">All</span>
                    </div>
                </div>
                <div id="mailEmail" class="form-group marginT20 displayNone">
                    <label class="inputHeading">No. of Vacancies</label>
                    <input type="nunber" placeholder="Enter Vacancy number" class="form-control registerHereSubTextInput">
                </div>
                <div id="contactDetailsDiv" class="form-group marginT20 displayNone">
                    <div class="row marginT20 noPadding nomrgn">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddingryt5">
                            <label class="inputHeading">Contact Person</label>
                            <select class="form-control" id="job_cp_title1" name="job_cp_title"><option value="Mr.">Mr.</option><option value="Mrs.">Mrs.</option><option value="Miss.">Miss.</option></select>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">&nbsp;</label>
                            <input type="text" placeholder="Enter Contact Person Name" class="form-control registerHereSubTextInput">
                        </div>
                    </div>
                    <div class="row  noPadding nomrgn marginT20">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddingryt5">
                            <label class="inputHeading">Contact Number</label>
                            <select class="form-control" id="contact_code" name="job_dial_number"><option value="971">AE (+971)</option><option value="93">AF (+93)</option><option value="1268">AG (+1268)</option><option value="1264">AI (+1264)</option><option value="355">AL (+355)</option><option value="374">AM (+374)</option><option value="244">AO (+244)</option><option value="54">AR (+54)</option><option value="1684">AS (+1684)</option><option value="43">AT (+43)</option><option value="61">CX (+61)</option><option value="297">AW (+297)</option><option value="994">AZ (+994)</option><option value="387">BAM (+387)</option><option value="1246">BB (+1246)</option><option value="880">BD (+880)</option><option value="32">BE (+32)</option><option value="226">BF (+226)</option><option value="359">BG (+359)</option><option value="973">BH (+973)</option><option value="257">BI (+257)</option><option value="229">BJ (+229)</option><option value="1441">BM (+1441)</option><option value="673">BND (+673)</option><option value="55">BR (+55)</option><option value="1242">BS (+1242)</option><option value="975">BT (+975)</option><option value="267">BW (+267)</option><option value="375">BY (+375)</option><option value="501">BZ (+501)</option><option value="1">USD (+1)</option><option value="236">CF (+236)</option><option value="242">CG (+242)</option><option value="41">CH (+41)</option><option value="682">CK (+682)</option><option value="56">CL (+56)</option><option value="237">CM (+237)</option><option value="86">CN (+86)</option><option value="57">CO (+57)</option><option value="506">CR (+506)</option><option value="53">CU (+53)</option><option value="357">CY (+357)</option><option value="49">DE (+49)</option><option value="253">DJ (+253)</option><option value="45">DK (+45)</option><option value="1767">DM (+1767)</option><option value="1809">DO (+1809)</option><option value="213">DZ (+213)</option><option value="593">EC (+593)</option><option value="372">EE (+372)</option><option value="20">EG (+20)</option><option value="212">MA (+212)</option><option value="291">ER (+291)</option><option value="34">ES (+34)</option><option value="251">ET (+251)</option><option value="358">FI (+358)</option><option value="679">FJ (+679)</option><option value="298">FO (+298)</option><option value="33">FR (+33)</option><option value="241">GA (+241)</option><option value="44">JE (+44)</option><option value="1473">GD (+1473)</option><option value="995">GE (+995)</option><option value="594">GF (+594)</option><option value="233">GH (+233)</option><option value="350">GI (+350)</option><option value="299">GL (+299)</option><option value="220">GM (+220)</option><option value="224">GN (+224)</option><option value="590">GP (+590)</option><option value="240">GQ (+240)</option><option value="30">GR (+30)</option><option value="502">GT (+502)</option><option value="1671">GU (+1671)</option><option value="245">GW (+245)</option><option value="592">GY (+592)</option><option value="852">HK (+852)</option><option value="504">HN (+504)</option><option value="385">HR (+385)</option><option value="509">HT (+509)</option><option value="36">HU (+36)</option><option value="62">ID (+62)</option><option value="972">IL (+972)</option><option value="91">IN (+91)</option><option value="964">IQ (+964)</option><option value="98">IRR (+98)</option><option value="354">IS (+354)</option><option value="39">IT (+39)</option><option value="1876">JM (+1876)</option><option value="962">JO (+962)</option><option value="81">JP (+81)</option><option value="254">KE (+254)</option><option value="996">KG (+996)</option><option value="855">KH (+855)</option><option value="686">KI (+686)</option><option value="269">KM (+269)</option><option value="1869">KN (+1869)</option><option value="965">KW (+965)</option><option value="1345">KY (+1345)</option><option value="76">KZ (+76)</option><option value="856">LAK (+856)</option><option value="961">LB (+961)</option><option value="1758">LC (+1758)</option><option value="423">LI (+423)</option><option value="94">LK (+94)</option><option value="231">LR (+231)</option><option value="266">LS (+266)</option><option value="370">LT (+370)</option><option value="352">LU (+352)</option><option value="371">LV (+371)</option><option value="218">LY (+218)</option><option value="377">MC (+377)</option><option value="373">MDL (+373)</option><option value="261">MG (+261)</option><option value="692">MH (+692)</option><option value="389">MKD (+389)</option><option value="223">ML (+223)</option><option value="976">MN (+976)</option><option value="853">MOP (+853)</option><option value="1670">MP (+1670)</option><option value="596">MQ (+596)</option><option value="222">MR (+222)</option><option value="1664">MS (+1664)</option><option value="356">MT (+356)</option><option value="230">MU (+230)</option><option value="960">MV (+960)</option><option value="265">MW (+265)</option><option value="52">MX (+52)</option><option value="60">MY (+60)</option><option value="258">MZ (+258)</option><option value="264">NA (+264)</option><option value="687">NC (+687)</option><option value="227">NE (+227)</option><option value="672">NF (+672)</option><option value="234">NG (+234)</option><option value="505">NI (+505)</option><option value="31">NL (+31)</option><option value="47">NO (+47)</option><option value="977">NP (+977)</option><option value="674">NR (+674)</option><option value="683">NU (+683)</option><option value="64">NZ (+64)</option><option value="968">OM (+968)</option><option value="507">PA (+507)</option><option value="51">PE (+51)</option><option value="689">PF (+689)</option><option value="675">PG (+675)</option><option value="63">PH (+63)</option><option value="92">PK (+92)</option><option value="48">PL (+48)</option><option value="508">PM (+508)</option><option value="1787">PR (+1787)</option><option value="351">PT (+351)</option><option value="680">PW (+680)</option><option value="595">PY (+595)</option><option value="974">QA (+974)</option><option value="262">YT (+262)</option><option value="7">RUB (+7)</option><option value="250">RW (+250)</option><option value="966">SA (+966)</option><option value="677">SB (+677)</option><option value="248">SC (+248)</option><option value="249">SD (+249)</option><option value="46">SE (+46)</option><option value="65">SG (+65)</option><option value="386">SI (+386)</option><option value="421">SK (+421)</option><option value="232">SL (+232)</option><option value="378">SM (+378)</option><option value="221">SN (+221)</option><option value="252">SO (+252)</option><option value="597">SR (+597)</option><option value="239">STD (+239)</option><option value="503">SV (+503)</option><option value="268">SZ (+268)</option><option value="235">TD (+235)</option><option value="66">TH (+66)</option><option value="992">TJ (+992)</option><option value="690">TK (+690)</option><option value="993">TM (+993)</option><option value="216">TN (+216)</option><option value="676">TO (+676)</option><option value="90">TR (+90)</option><option value="688">TV (+688)</option><option value="886">TW (+886)</option><option value="255">TZS (+255)</option><option value="380">UA (+380)</option><option value="256">UG (+256)</option><option value="599">USD (+599)</option><option value="691">USD (+691)</option><option value="598">UY (+598)</option><option value="998">UZ (+998)</option><option value="1784">VC (+1784)</option><option value="58">VEF (+58)</option><option value="678">VU (+678)</option><option value="681">XPF (+681)</option><option value="685">WS (+685)</option><option value="967">YE (+967)</option><option value="27">ZA (+27)</option><option value="260">ZM (+260)</option><option value="263">ZW (+263)</option></select></div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">&nbsp;</label>
                            <input type="text" placeholder="Enter Contact Person Number" class="form-control registerHereSubTextInput">
                        </div>
                    </div>
                </div>

                <div id="walkinDiv" class="displayNone">
                    <div class="form-group marginT20 ">
                        <label class="inputHeading">Address</label>
                        <textarea class="form-control" placeholder="Enter Job Description" rows="5"></textarea>
                    </div>
                    <div class="form-group marginT20 ">
                        <label class="inputHeading">Walkin Date</label>
                        <input type="text" placeholder="Enter Date" class="form-control registerHereSubTextInput">
                    </div>
                    <div class="row noPadding nomrgn marginT20">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddingryt5">
                            <label class="inputHeading">Walkin Time</label>
                            <select class="form-control" id="wst" name="walkin_start_time"><option selected="selected" value="">Select Time</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="01:00">01:00</option><option value="01:30">01:30</option><option value="02:00">02:00</option><option value="02:30">02:30</option><option value="03:00">03:00</option><option value="03:30">03:30</option><option value="04:00">04:00</option><option value="04:30">04:30</option><option value="05:00">05:00</option><option value="05:30">05:30</option><option value="06:00">06:00</option><option value="06:30">06:30</option><option value="07:00">07:00</option></select>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 paddinglft5">
                            <label class="inputHeading">&nbsp;</label>
                            <select class="form-control" name="time_meridiem"><option value="AM">AM</option><option value="PM">PM</option></select>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
<?php echo e(Form::close()); ?>

<script type="text/javascript">
$(function() {
    $('.multiselect-ui').multiselect({
        minHeight: 50,  
        maxHeight: 400,
        //includeSelectAllOption: true
    });
});
function viewRespond(name) {
    if(name.id == 'email'){
        if($('input[id="email"]:checked').length > 0){
            $('#mailEmail').show();
        }
        else{
            $('#mailEmail').hide();
        }
    }
    if(name.id == 'contactDetails'){
        if($('input[id="contactDetails"]:checked').length > 0){
            $('#contactDetailsDiv').show();
        }
        else{
            $('#contactDetailsDiv').hide();
        }
    }
    if(name.id == 'walkin'){
        if($('input[id="walkin"]:checked').length > 0){
            $('#walkinDiv').show();
        }
        else{
            $('#walkinDiv').hide();
        }
    }
    if(name.id == 'all'){
        if($('input[id="all"]:checked').length > 0){
            $('#mailEmail').show();
            $('#walkinDiv').show();
            $('#contactDetailsDiv').show();
        }
        else{
            $('#mailEmail').hide();
            $('#walkinDiv').hide();
            $('#contactDetailsDiv').hide();
        }
    }
}
</script>
<script>
  globalVar.qIds={};
  globalVar.countryIdComp=0;
  globalVar.countryId=0;
  globalVar.jobCountryId=0;
  globalVar.currCityElem='';
 
</script>
<?php echo $__env->make('front_panel/includes/add_job_js_code', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>