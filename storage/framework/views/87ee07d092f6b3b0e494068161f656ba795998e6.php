<?php 
$settings = getSettings();
//session()->put('CALL_US',$settings->mobile);
?>
<html>

<head>
  <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta property="og:url" content="<?php echo $__env->yieldContent('og_url'); ?>" />
      <meta property="og:image" content="<?php echo $__env->yieldContent('og_image'); ?>" />
      <meta property="og:title" content="<?php echo $__env->yieldContent('og_title'); ?>" />
      <title><?php echo e($settings->meta_title); ?> <?php echo $__env->yieldContent('page_title'); ?></title>
      <script>
        var base_url="<?php echo e(url('/').'/'); ?>";
        var csrf_token="<?php echo e(csrf_token()); ?>";
        var globalFunc = {};
        var globalVar = {};
        var currentThis = '';
        var minChars = 2;
      </script>
  
  <link rel="shortcut icon" type="image/png" href="<?php echo e(url('public/images/favicon.png')); ?>">
  <script src="<?php echo e(URL::asset('public/js/jquery-3.3.1.min.js')); ?>"></script> 
  <script type="text/javascript" src="<?php echo e(URL::asset('public/js/bootstrap.js')); ?>"></script>
  <!-- owl-javascript -->
  
  <script src="<?php echo e(URL::asset('public/build/js/intlTelInput.js')); ?>"></script>
  <script src="<?php echo e(URL::asset('public/js/owl.carousel.js')); ?>"></script>
  <!-- Owl Stylesheets -->
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/owl.carousel.min.css')); ?>">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <!-- Bootstrap CSS -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/bootstrap.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/plugins/multiSelect/jquery.multiselect.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/bootstrap-multiselect.css')); ?>">
  
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/style.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/home/homeResponsive.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/media.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/styleNav.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/home/homeNav.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/plugins/selectize/selectize.css')); ?>">

  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

  
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/build/css/intlTelInput.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(URL::asset('public/css/page/consultant_details.css')); ?>">


  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script src="<?php echo e(URL::asset('public/js/jquery.validate.min.js')); ?>" type="text/javascript"></script>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
  <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
  <script src="<?php echo e(URL::asset('public/plugins/selectize/selectize.js')); ?>"></script>
  <script src="<?php echo e(URL::asset('public/plugins/multiSelect/jquery.multiselect.js')); ?>"></script>
</head>

<body>
  <!-- first section -->




  <div id="loginModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

          <!-- Login model content-->
          <div class="modal-content ">
              <div class="modal-body paddingBody">
                  <div class="form-group text-center">
                      <button type="button" class="btn btn-default active marginNegative cornerRight">Candidate Login</button>
                      <button type="button"  data-toggle="modal" data-target="#loginModal_employers" class="btn btn-default marginNegative cornerLeft">Employer Login</button>
                  </div>
                  <div class="form-group marginTp30">
                      <label class="fontLogin">Login</label>
                  </div>
                  <div id="success_error_msg" class="jobseeker-msg text-center"></div>
                      <form class="login-form form-1" id="login-form" autocomplete="on">
                          <div class="form-group">
                              <label for="usr">Enter Email</label>
                              <input type="email" class="form-control loginBtn" name="candidate_username" id="cand_login_username">
                          </div>
                          <div class="form-group">
                              <label for="pwd">Enter Password</label>
                              <input type="password" class="form-control loginBtn error" id="cand_login_password" name="candidate_password">
                          </div>
                          <div class="form-group marginTp30">
                              <a class="forgetPass" href="javascript:;">Forgot Password?</a>
                              <button type="submit" class="btn btn-success right">Login</button>
                          </div>
                      </form>
                  <div class="form-group text-center marginTp30 dontHaveAccount">
                      Don't have a account yet?<a href="javascript:void(0);"> Create New</a>
                  </div>
              </div>
          </div>

      </div>
  </div>

  <div id="loginModal_employers" class="modal fade" role="dialog">
      <div class="modal-dialog">

          <!-- Login model content-->
          <div class="modal-content ">
              <div class="modal-body paddingBody">
                  <div class="form-group text-center">
                      <button type="button" data-toggle="modal" data-target="#loginModal_employers" class="btn btn-default  marginNegative cornerRight">Candidate Login</button>
                      <button type="button" class="btn btn-default active marginNegative_1 cornerLeft">Employer Login</button>
                  </div>
                  <div class="form-group marginTp30">
                      <label class="fontLogin">Login</label>
                  </div>
                  <div id="success_error_msg" class="employer-msg text-center"></div>
                  <form class="login-form form-2" id="" autocomplete="on">
                      <div class="form-group">
                          <label for="usr">Enter Email</label>
                          <input type="email" name="employer_username" value="" id="emp_login_username" class="form-control loginBtn" placeholder="Enter Username" title="Please Enter Username" required/>
                          
                      </div>
                      <div class="form-group">
                          <label for="pwd">Enter Password</label>
                          <input type="password" name="employer_password" value="" id="emp_login_password" class="form-control loginBtn" placeholder="Enter Password" required/>
                          
                      </div>
                      <div class="form-group marginTp30">
                          <a class="forgetPass" href="javascript:;">Forgot Password?</a>
                          <button type="submit" class="btn btn-success right">Login</button>
                      </div>
                  </form>
                  <div class="form-group text-center marginTp30 dontHaveAccount">
                      Don't have a account yet?<a href="javascript:void(0);"> Create New</a>
                  </div>
              </div>
          </div>

      </div>
  </div>
      
          <nav class="navbar navbar-default navbar-fixed-top noShadow">
              <div class="container" style="padding: 10px;">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                      
                  <a class="navbar-brand" href="<?php echo e(url('/')); ?>"><img class="brandLogo" src="<?php echo e(url('public/images/job 4 gulf@2x.png')); ?>"></a>
                  </div>
                  <div id="navbar" class="navbar-collapse collapse navbar-right">
                      <ul class="nav navbar-nav">
                          <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Browse Jobs</a>
                              <ul class="dropdown-menu">
                                  <li><a href="<?php echo e(route('search-jobs')); ?>">Search Jobs</a></li>
                                  <li><a href="<?php echo e(route('jobs-by','country')); ?>">Jobs by Country</a></li>
                                  <li><a href="<?php echo e(route('jobs-by','city')); ?>">Jobs by City</a></li>
                                  <li><a href="<?php echo e(route('jobs-by','industry')); ?>">Jobs by Industry</a></li>
                              </ul>
                          </li>
                          <li><a href="<?php echo e(route('consultant')); ?>">Best Consultant Zone</a></li>
                          <li><a href="<?php echo e(route('walkins')); ?>">Walkings</a></li>
                          <?php if(Auth::check() && Auth::user()->role==2): ?>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> <?php echo e(ucfirst(Auth::user()->name)); ?> </a>
                                  <ul class="dropdown-menu">
                                      <li><a href="<?php echo e(route('candidate-dashboard')); ?>">Dashboard</a></li>
                                      <li><a href="<?php echo e(route('logout')); ?>">Logout</a></li>
                                      <li><a href="<?php echo e(route('thank-you')); ?>">Be a Premium Candidate</a></li>
                                  </ul>
                              </li>
                          <?php else: ?> 
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Job Seeker </a>
                                  <ul class="dropdown-menu">
                                      <li><a data-toggle="modal" data-target="#loginModal" href="javascript:;">Login</a></li>
                                      <li><a href="<?php echo e(route('candidate-register')); ?>">Register</a></li>
                                      <li><a href="<?php echo e(route('thank-you')); ?>">Be a Premium Candidate</a></li>
                                  </ul>
                              </li>
                          <?php endif; ?>
                          <?php if(Auth::check() && Auth::user()->role==3): ?>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> <?php echo e(ucfirst(Auth::user()->name)); ?> </a>
                                  <ul class="dropdown-menu">
                                      <li><a href="<?php echo e(route('employer-dashboard')); ?>">Dashboard</a></li>
                                      <li class="no-vertical-line"><a href="<?php echo e(route('emp-profile-view')); ?>">Profile</a></li>
                                      <li><a href="<?php echo e(route('logout')); ?>">Logout</a></li>
                                  </ul>
                              </li>
                          <?php else: ?>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Employers </a>
                                  <ul class="dropdown-menu">
                                      <li><a data-toggle="modal" data-target="#loginModal_employers" href="javascript:;">Login</a></li>
                                      <li><a href="<?php echo e(route('employer-register')); ?>">Register</a></li>
                                        <li><a href="<?php echo e(route('our-plans')); ?>">Recruitment Solutions</a></li>
                                  </ul>
                              </li>
                          <?php endif; ?>
                            <li><a href="<?php echo e(route('contact-us')); ?>">Contact us</a></li>
                          
                      </ul>
                  </div><!--/.nav-collapse -->
              </div>
          </nav>
          <?php echo $__env->make('layouts.flash_msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php echo $__env->yieldContent('content'); ?>
<?php
  $countryList=getFooterData('country');
  $industryList=getFooterData('industry');
?>
<!--seven section-->
<section id="seven-section" class="banner-4">
  <div class="container">
      <div class="row footer-menu-text">
          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
              <h4 class="move-left">Jobs By</h4>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                  <h5 class="hr-line">Location</h5>
                  <ul>
                      <?php $__currentLoopData = $countryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="<?php echo e(url('search-jobs?country_ids='.$val->id)); ?>" >Jobs in <?php echo e($val->name); ?></a></li>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <li><a href="<?php echo e(route('jobs-by',['country'])); ?>">more</a></li>
                  </ul>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                  <h5 class="hr-line">Category</h5>
                  <ul>
                      <?php $__currentLoopData = $industryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <li><a href="<?php echo e(url('search-jobs?industry_ids='.$val->id)); ?>" > <?php echo e($val->name); ?></a></li>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <li><a href="<?php echo e(route('jobs-by',['industry'])); ?>">more</a></li>

                  </ul>
              </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
              <h4 class="move-left">Users</h4>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                  <h5 class="hr-line">Job Seekers</h5>
                  <ul>
                      <?php if(Auth::check() && Auth::user()->role==2): ?>
                          <li><a href="#"><?php echo e(ucfirst(Auth::user()->name)); ?></a>
                          <li><a href="<?php echo e(route('logout')); ?>">Logout</a></li>
                      <?php else: ?> 
                          <li><a data-toggle="modal" data-target="#loginModal" href="javascript:;">Login</a></li>
                          <li><a href="<?php echo e(route('candidate-register')); ?>">Register</a></li>
                          <li><a href="<?php echo e(route('thank-you')); ?>">Be a Premium Candidate</a></li>
                      <?php endif; ?> 
                  </ul>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                  <h5 class="hr-line">Employers</h5>
                  <ul>
                      <?php if(Auth::check() && Auth::user()->role==3): ?>
                          <li><a href="#"><?php echo e(ucfirst(Auth::user()->name)); ?></a>
                          <li><a href="<?php echo e(route('logout')); ?>">Logout</a></li>
                      <?php else: ?> 
                          <li><a data-toggle="modal" data-target="#loginModal_employers" href="javascript:;">Login</a></li>
                          <li><a href="<?php echo e(route('employer-register')); ?>">Register</a></li>
                      <?php endif; ?>
                      <li><a href="#">Our Plans</a></li>
                  </ul>
              </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
              <h4 class="move-left">Jobs4Gulf</h4>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
                  <h5 class="hr-line">Quick Links</h5>
                  <ul>
                      <li><a href="<?php echo e(route('about-us')); ?>">About Us</a></li>
                      <li><a href="<?php echo e(route('contact-us')); ?>">Contact Us</a></li>
                      <li><a href="<?php echo e(route('site-map')); ?>">Site Map</a></li>
                  </ul>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding footer-social">
                      <h5 class="hr-line">Follow Us</h5>
                      <ul>
                            <?php if($settings->fb_link!=''): ?> 
                                <li><a target="_blank" href="<?php echo e($settings->fb_link); ?>"><i class="fab fa-facebook-f"></i></a></li> 
                            <?php endif; ?>
                            <?php if($settings->twitter_link!=''): ?>
                                <li><a target="_blank" href="<?php echo e($settings->twitter_link); ?>"><i class="fab fa-twitter"></i></a></li> 
                            <?php endif; ?>
                            <?php if($settings->gplus_link!=''): ?>
                                <li><a target="_blank" href="<?php echo e($settings->gplus_link); ?>"><i class="fab fa-google-plus-g"></i></a></li>
                            <?php endif; ?>
                          
                      </ul>
                  </div>
              </div>
          </div>
      </div>


      <div class="copyright">
          <p class="text-center">All rights reserved &#9400; <?php echo e(date('Y')); ?> Jobs4Gulf </p>
      </div>
  </div>
</section>
<!-- seven-secton-end -->
<!-- vendors -->
<script src="<?php echo e(URL::asset('public/js/frontpanel.js')); ?>"></script>
<script src="<?php echo e(URL::asset('public/js/global_custom.js')); ?>"></script>
  <script>
      $('.popular-block').mouseover(function(){
          var imgAttr=$(this).children('img');
          var hoverImgSrc = imgAttr.data('hovericon');
          imgAttr.attr('src',hoverImgSrc);

      });
      $('.popular-block').mouseout(function(){
          var imgAttr=$(this).children('img');
          var outImgSrc = imgAttr.data('hoverouticon');
          imgAttr.attr('src',outImgSrc);
      });
  </script>
<script>
  $("#top-candidates").owlCarousel({
      loop:true,
      // lazyLoad:true,
      items: 4,
      margin: 30,
           autoplay:true,
       autoplayTimeout:5000,
//            autoplaySpeed: 100,
      // dots:true,
      // nav:true,
      // dots: true,
      //  navText:['next','pre'],
      // navContainer: '#slideNav',
      // dotsContainer: '#slidedots',
      responsiveClass:true
    });
    $(document).ready(function(){

    $("#premium").owlCarousel({
      loop:true,
      items: 4,
        autoplay: true,
        autoplayHoverPause: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:4,
            nav:true,
            loop:true
        }
    },
      margin: 30,
       autoplayTimeout:1500,
      dots:true,
      nav:true,
      dots: true,
        navText:['<img src="<?php echo e(url('public/images/Group 636.png')); ?>">','<img src="<?php echo e(url('public/images/Group 637.png')); ?>">'],
      // navContainer: '#slideNav',
      // dotsContainer: '#slidedots',
      responsiveClass:true
    });

        $("#candidates").owlCarousel({
      loop:true,
      items: 4,
      autoplay: true,
      autoplayHoverPause: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:4,
            nav:true,
            loop:true
        }
    },
      margin: 30,
       autoplayTimeout:1500,
      dots:true,
      nav:true,
      dots: true,
        navText:['<img src="<?php echo e(url('public/images/Group 636.png')); ?>">','<img src="<?php echo e(url('public/images/Group 637.png')); ?>">'],
      // navContainer: '#slideNav',
      // dotsContainer: '#slidedots',
      responsiveClass:true
    });



    $("#jobPannel").owlCarousel({
      loop:true,
      items: 4,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:2,
            nav:true,
            loop:false
        }
    },
      margin: 30,
       autoplayTimeout:500,
      dots:true,
      nav:true,
      dots: true,
        navText:['<img src="<?php echo e(url('public/images/Group 636.png')); ?>">','<img src="<?php echo e(url('public/images/Group 637.png')); ?>">'],
      // navContainer: '#slideNav',
      // dotsContainer: '#slidedots',
      responsiveClass:true
    });
    });
</script>
<style>

</style>
</body>

</html>