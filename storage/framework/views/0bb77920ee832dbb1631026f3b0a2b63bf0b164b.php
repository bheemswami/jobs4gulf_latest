
<?php $__env->startSection('content'); ?>
<style>
    body{
        background-color: white !important;
    }
    .leftSearchBox {
        border-radius: 5px;
        box-shadow: rgba(0,0,0,0.1) 0 0 20px;
    }
    .cardBox3:hover {
        border-top: none !important;
        background-color:white !important;
    }
    .responsiveCard {
        padding-left: 5px !important;
        padding-right: 5px !important;
        padding-top: 0px; 
        padding-bottom: 0px; 
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 mt20">
            <div class="leftSearchBox">
                <div class="paddingSearchBox">
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu">Job</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="<?php echo e(route('applied-jobs')); ?>"><img class="imgBreifcae" src="<?php echo e(url('public/images/job4.png')); ?>">Applied Jobs</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a href="<?php echo e(route('last-activity')); ?>"><img class="imgBreifcae" src="<?php echo e(url('public/images/job1.png')); ?>">Last Activity</a>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a class="activeTab" href="<?php echo e(route('recommended-jobs')); ?>"><img class="imgBreifcae" src="<?php echo e(url('public/images/job2.png')); ?>">Recommended Jobs</a>
                    </div>
                    <div>
                        <label class="LeftSearchMainHeading labelLeftMenu2">Account</label>
                    </div>
                    <div class="leftSearchMainSubHeading">
                        <a  href="<?php echo e(route('candidate-dashboard')); ?>"><img class="imgBreifcae" src="<?php echo e(url('public/images/job3.png')); ?>">Profile</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 nopadding mt20" >
                <div class="col-lg-12 nopadding nomrgn recommendedJobsHeader">
                    Recommended Jobs
                </div>
                <div>
                    <?php $__empty_1 = true; $__currentLoopData = $recommended_jobs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard">
                            <a href="<?php echo e(route('job-details',['jobid'=>$val->id])); ?>">
                            <div class="cardBox2">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-xs-3 paddingRyt0">
                                                <img class="cardImg2" src="<?php echo e(url('public/images/atlas-1.png')); ?>">
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="jobName"><?php echo e($val->comp_name); ?></div>
                                                <div class="jobDesignation"><?php echo e($val->job_title); ?></div>
                                                <div>
                                                    <span class="jobDesignation">Exp Level: ( <?php echo e($val->min_experience); ?> - <?php echo e($val->max_experience); ?> yrs. )</span>
                                                    <span class="jobLocation"><img src="<?php echo e(url('public/images/location-icon.png')); ?>"><?php echo e($val->country_name); ?></span>
                                                </div>
                                                </div>
                                        </div>
                                        <div class="row skillsRow">
                                            <div class="col-xs-3 paddingRyt0">
                                            </div>
                                            <div class="col-xs-9 skills noPadding">
                                                <span>Design</span>
                                                <span>UI/UX</span>
                                                <span>Web Development</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-xs-12 mrgnTp8">
                                                <span class="jobAmountGrid jobAmountGrid2"><img src="<?php echo e(url('public/images/doller.png')); ?>">$<?php echo e($val->salary_min); ?> - $<?php echo e($val->salary_max); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="jobDateGrid"><?php echo countDays($val->created_at); ?></span>
                            </div>
                            </a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt21 responsiveCard">
                        <div class="cardBox2">
                            <p>No jobs available for now, Click here to <a href="<?php echo e(route('search-jobs')); ?>" target="_blank">explore more</a></p>
                        </div>
                    </div>
                   <?php endif; ?>
                </div>
                <div class="row margnBtm30">
                        <div class="col-lg-12 mt20">
                            <div class="paginationContainer">
                                <?php echo e($recommended_jobs->links()); ?>

                            </div>
                        </div>
            
                    </div>
            </div>
    </div>
</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('/layouts/front_panel_master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>