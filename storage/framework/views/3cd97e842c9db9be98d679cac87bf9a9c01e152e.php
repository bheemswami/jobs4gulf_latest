<div class="col-sm-12"> 
    <?php $__empty_1 = true; $__currentLoopData = $walkins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$vals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt20">
            <div class="cardBox2">
                <div class="row walkinHeader">
                    <div class="col-xs-12">
                        <h3>
                        <?php echo e($vals->title); ?></h3>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="row text-center">
                            <div class="img_back_div">
                            <a href="<?php echo e(url('walkins-detail/'.$vals->id)); ?>">
                                <img class="cardImg4 img-thumbnail" src="<?php echo e(checkFile($vals->comp_logo,'uploads/walkin_comp_logo/','job_role_default.png')); ?>">
                            </a>
                        </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                        <div class="row walkinHeader2">
                            <h4>
                            <?php echo e($vals->comp_name); ?></h4>
                        </div>
                        <?php  $interview_city=[]; $date_from=$date_to=''; ?>
                        <?php if($vals->walkin_interview!=null): ?>
                            <?php $__currentLoopData = $vals->walkin_interview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    if($v->city!=null){
                                        $interview_city[]= $v->city->name;         
                                    }
                                ?>
                                <?php if($k==0): ?>
                                    <?php $date_to=$v->date_to;
                                        $date_from=$v->date_from;
                                    ?>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        <div class="row marginTPBtm10">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 noPadding1">
                                <div class="jobLocation"><img src="<?php echo e(url('public/images/calendarBlck.png')); ?>">Walk-In Date:</div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-8 noPadding1">
                                <div class="detailsWalkin">
                                    <?php if($date_from!='' && $date_to!=''): ?>
                                        <?php echo e(dateConvert($date_from,'dS')); ?>-<?php echo e(dateConvert($date_to,'dS M Y')); ?>

                                    <?php else: ?>
                                        N/A
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row marginTPBtm10">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 noPadding1">
                                <div class="jobLocation"><img src="<?php echo e(url('public/images/clockBlack.png')); ?>">Walk-In Time:</div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-8 noPadding1">
                                <div class="detailsWalkin">
                                    <?php if($date_from!='' && $date_to!=''): ?>
                                        <?php echo e(dateConvert($date_from,'h:i A')); ?> to <?php echo e(dateConvert($date_to,'h:i A')); ?>

                                    <?php else: ?>
                                        N/A
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row marginTPBtm10">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4  noPadding1">
                                <div class="jobLocation"><img src="<?php echo e(url('public/images/locationBlack.png')); ?>">Walk-In Location:</div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-8 noPadding1">
                                <div class="detailsWalkin"><?php echo e(join(',',$interview_city)); ?></div>
                            </div>
                        </div>
                        <div class="row walkinHeader3">Position:</div>

                        <div class="row walkinHeader4">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 noPadding1">
                                <?php if($vals->walkin_position!=null): ?>
                                    <?php $designations=[];  ?>
                                    <?php $__currentLoopData = $vals->walkin_position; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php 
                                        if($d->designation!=null)
                                            $designations[]= $d->designation->name; 
                                        ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo e(limit_text(join(',',$designations),140)); ?>

                                <?php else: ?>
                                    N/A
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <div class="col-xs-12 col-md-12 nopadding">
            <div class=" job-box">
            <div class="row flexbox">
                
                <div class="col-xs-12 col-sm-12">
                
                <img src="<?php echo e(url('public/images/default/nojobsfound.png')); ?>" class="center-block" width="240px" style="margin-top: 5%;">
                <div class="post-details">
                    
                </div>
                </div>
            </div>
            
            </div>
        </div> 
    <?php endif; ?>                       
    <div class="row margnBtm30">
            <div class="col-lg-12 mt20">
                <div class="paginationContainer">
                    <div class="custom-pagination">
                        <?php echo e($walkins->links()); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<script>

var search='<?php echo e($search_tag); ?>';
var array = search.split(',');

if(search)
{
  $('#search_key').html('');
  $.each( array, function( key, value ) {
    if(value )
    $('#search_key').append('<span>'+value+'</span>|');
  });
}
</script>